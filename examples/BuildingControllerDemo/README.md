# SmartGridToolbox BuildingControllerDemo

In this demo, we simulate a building that is equipped with PV, a battery and an HVAC system. We model the heat loss from the building using a lumped thermal model. External temperature, internal waste heat production, electricity consumption and a time-dependent electricity price are taken from time series data.

## Building and Running 
In addition to SmartGridToolbox, this demo requires the Gurobi optimiser to be installed.

To build:
```bash
meson setup build
ninja -C build
```

To run:
```bash
build/building_controller_demo building_controller_demo.yaml out.tsv
```

The output file (`out.tsv`) is a tab-separated-value file that contains various output quantities such as elapsed time, electricity price, consumption load, PV and so on.

To plot results (using python 3): 
```bash
python3 plot_results.py
```

## Explanation

The thermal properties of the building, along with the HVAC system, are modelled using a custom class, `Building` (see `Building.h` and `Building.cc`). The battery, PV and consumption are modelled using SmartGridToolbox's regular `Battery`, `SolarPv` and `TimeSeriesZip` classes. External temperature is modelled using SmartGridToolbox's `Weather` class.

The HVAC system and battery are controlled by a special `BuildingController` that receives predictions of electricity price, electricity consumption and external temperature, and optimises the battery charging and HVAC use over a rolling horizon. The optimisation is performed using the Gurobi library, which must be installed to use this demo.
