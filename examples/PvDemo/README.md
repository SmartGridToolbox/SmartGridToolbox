# SmartGridToolbox PvDemo

In this demo, we explore how residential PV inverters can be used to do optimized volt-VAR control on a network.

## Building and Running 

To build:
```bash
meson setup build
ninja -C build
```

To run:
```bash
build/pvdemo {input_yaml_filename} {output_filename} {V_min} {V_max} {use_special_inverters}
```
e.g.
```bash
build/pvdemo pvdemo_ieee57.yaml out.tsv 0.95 1.05 T
```

The output file is a tab-separated-value file that contains the columns time/conventional generated complex power/PV
complex power/maximum bus voltage/minimum bus voltage.

To generate and plot results (using python 3) vs a control case:
```bash
# Generate optimised results
build/pvdemo pvdemo_ieee57.yaml out.tsv 0.95 1.05 T
# Now generate non-optimised, control results.
# First convert special PV inverters to regular inverters in the input:
sed -e 's/- pv_inverter/- inverter/g' pvdemo_ieee57.yaml > pvdemo_ieee57_ctrl.yaml
# Now run:
build/pvdemo pvdemo_ieee57_ctrl.yaml out_ctrl.tsv 0.95 1.05 T
# Plot the results:
python3 plot_results.py
```

## Explanation

The code uses two custom classes: PvInverter and PvDemoSolver.

A PvInverter is a special inverter that acts as a generator, in that it can supply variable/optimized P and Q to the
network, depending on the results of the network solver. In particular, we are interested in the fact that it can supply
variable reactive power Q, which is generated for free, and can help to stablilize the voltage on the network. Of
course, P is bounded by the photovoltaic power generated, and the apparent power is bounded by the power capacity of the
inverter. Thus, if we tried to generate too much reactive power, we would need to reduce the amount of active power,
which would then need to be imported from elsewhere on the network, ie. from a conventional generator, and would come at
a cost. A ParserPlugin, using the keyword `pv_inverter`, allows PvInverters to be parsed in as part of the YAML file.

PvDemoSolver is like a standard OPF solver (class `OpfSPolSolver`), except that it also incorporates some extra
constraints and variables at the PvInverters. There is a constraint corresponding to an upper bound on the the apparent
power of the inverter. Another constraint/variable pair models the deviation from the nominal voltage, which we
penalized by a small cost (we probably don't want to forgo free solar power just to drive the voltage closer to
nominal). Finally, another constraint pair penalizes voltage deviations from the specified upper and lower bounds; this
time the penalty is much larger.
