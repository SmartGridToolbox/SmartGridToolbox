// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "PvDemoSolver.h"

#include "PvInverter.h"

using namespace MadOpt;

namespace Sgt
{
    void PvDemoSolver::addPvInverter(const SimComponentPtr<PvInverter>& inv)
    {
        invs_.push_back(inv);
    }

    void PvDemoSolver::customiseModel()
    {
        V2SlackA_ = nlp_->madMod_.addVar(0, MadOpt::INF, 0.0, "V2SlackA");
        V2SlackB_ = nlp_->madMod_.addVar(0, MadOpt::INF, 0.0, "V2SlackB");
        nlp_->obj_ += 5 * V2SlackA_;
        nlp_->obj_ += 3e3 * V2SlackB_;
        nlp_->madMod_.setObj(nlp_->obj_); // TODO: is this necessary? Probably.

        // We need to handle voltage constraints as true constraints, not variable bounds.
        // Set the variable bounds to [0, infinity] and add additional constraints.
        for (const auto& bus : opfMod_->buses())
        {
            for (size_t iBusPh = 0; iBusPh < bus.nPhase(); ++iBusPh)
            {
                auto& busDatI = nlp_->busData_[bus.idx][iBusPh];
                busDatI.vVar.lb(0.0);
                busDatI.vVar.ub(MadOpt::INF);
                busDatI.vLb = nlp_->madMod_.addConstr(bus.vBnds[0], busDatI.vVar + V2SlackB_);
                busDatI.vUb = nlp_->madMod_.addConstr(busDatI.vVar - V2SlackB_, bus.vBnds[1]);
                busDatI.nomVPlus = nlp_->madMod_.addConstr(pow(busDatI.vVar, 2) - V2SlackA_, 1.01);
                busDatI.nomVMinus = nlp_->madMod_.addConstr(0.99, pow(busDatI.vVar, 2) + V2SlackA_);
            }
        }

        for (auto& inv : invs_)
        {
            // Add an extra constraint for max apparent power.
            double maxSMag = netw_->S2Pu(inv->maxSMag());
            auto genId = inv->id();
            const auto& modGen = opfMod_->gen(genId);
            auto& genDat = nlp_->genData_[modGen.idx];
            for (size_t iGenPh = 0; iGenPh < modGen.nPhase(); ++iGenPh)
            {
                auto& genDatI = genDat[iGenPh];
                genDatI.sMagMax = nlp_->madMod_.addConstr(pow(genDatI.pVar, 2) + pow(genDatI.qVar, 2), pow(maxSMag, 2));
            }
        }
    }
}
