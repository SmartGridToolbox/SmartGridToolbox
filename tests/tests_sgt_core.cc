// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#define BOOST_TEST_MODULE sgt_tests

#include <SgtCore/config.h>

#include <boost/test/included/unit_test.hpp>

#include "../SgtCore/SgtCore.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <future>
#include <set>
#include <thread>
#include <vector>

using namespace arma;
using namespace Sgt;
using namespace std;
using namespace boost::unit_test;

struct Init
{
    Init()
    {
        unit_test_log.set_threshold_level(boost::unit_test::log_messages);
        BOOST_TEST_MESSAGE("\nTesting " << framework::current_test_case().p_name);
    }
    ~Init()
    {
        BOOST_TEST_MESSAGE("Finished " << framework::current_test_case().p_name << "\n");
    }
};

BOOST_FIXTURE_TEST_SUITE(core_tests, Init)

BOOST_AUTO_TEST_CASE (test_loops)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_loops.yaml", netw);
    auto buses = netw.buses();
    BOOST_CHECK(buses.size() == 6);
    BOOST_CHECK(buses[0]->id() == "b");
    BOOST_CHECK(buses[1]->id() == "ba");
    BOOST_CHECK(buses[2]->id() == "bb");
    BOOST_CHECK(buses[3]->id() == "c");
    BOOST_CHECK(buses[4]->id() == "ca");
    BOOST_CHECK(buses[5]->id() == "cb");
}

BOOST_AUTO_TEST_CASE (test_overhead_compare_carson_1)
{
    // For this test, we compare values for an overhead line examined in one of Kersting's papers:
    // docs/background/Kersting_Carson.pdf.
    Network netw;
    Parser<Network> p;
    p.parse("test_overhead_compare_carson_1.yaml", netw);

    auto oh = netw.branches()["line_1_2"].as<OverheadLine>();

    Mat<Complex> ZPrim = oh->ZPrim();
    Mat<Complex> ZPhase = oh->ZPhase();

    Complex cmp;
    cmp = {1.3369, 1.3331};
    double err00 = abs(ZPhase(0,0) - cmp) / abs(cmp);
    cmp = {0.2102, 0.5778};
    double err01 = abs(ZPhase(0, 1) - cmp) / abs(cmp);
    cmp = {0.2132, 0.5014};
    double err02 = abs(ZPhase(0, 2) - cmp) / abs(cmp);
    cmp = {1.3239, 1.3557};
    double err11 = abs(ZPhase(1, 1) - cmp) / abs(cmp);
    cmp = {0.2067, 0.4591};
    double err12 = abs(ZPhase(1, 2) - cmp) / abs(cmp);
    cmp = {1.3295, 1.3459};
    double err22 = abs(ZPhase(2, 2) - cmp) / abs(cmp);

    BOOST_CHECK(err00 < 0.001);
    BOOST_CHECK(err01 < 0.001);
    BOOST_CHECK(err02 < 0.001);
    BOOST_CHECK(err11 < 0.001);
    BOOST_CHECK(err12 < 0.001);
    BOOST_CHECK(err22 < 0.001);

    BOOST_CHECK(abs(ZPhase(0,1) - ZPhase(1,0)) < 1e-6);
    BOOST_CHECK(abs(ZPhase(0,2) - ZPhase(2,0)) < 1e-6);
    BOOST_CHECK(abs(ZPhase(1,2) - ZPhase(2,1)) < 1e-6);
}

BOOST_AUTO_TEST_CASE (test_overhead_compare_carson_2)
{
    // For this test, we compare values for an overhead line examined in one of Kersting's papers:
    // docs/background/Kersting_Carson.pdf.
    Network netw;
    Parser<Network> p;
    p.parse("test_overhead_compare_carson_2.yaml", netw);

    netw.solvePowerFlow();

    auto bus2 = netw.buses()["bus_2"];

    Complex cmp;
    cmp = polar(14.60660, -0.62 * pi / 180.0);
    double err0 = abs(bus2->V()(0) - cmp) / abs(cmp);
    cmp = polar(14.72669, -121.0 * pi / 180.0);
    double err1 = abs(bus2->V()(1) - cmp) / abs(cmp);
    cmp = polar(14.80137, 119.2 * pi / 180.0);
    double err2 = abs(bus2->V()(2) - cmp) / abs(cmp);

    BOOST_CHECK(err0 < 0.001);
    BOOST_CHECK(err1 < 0.001);
    BOOST_CHECK(err2 < 0.001);
}

BOOST_AUTO_TEST_CASE (test_underground_conc_compare_carson)
{
    // For this test, we compare values for an underground line examined in one of Kersting's papers:
    // docs/background/Kersting_Carson.pdf.
    Network netw;
    Parser<Network> p;
    p.parse("test_underground_conc_compare_carson.yaml", netw);

    netw.solvePowerFlow();

    auto ug = netw.branches()["line_1_2"].as<UndergroundLine>();
    Mat<Complex> ZPhase = ug->ZPhase() * 1609.344; // Convert to ohms per mile.
    Mat<Complex> ZPhaseKersting{{
        {Complex(0.7981, 0.4463), Complex(0.3191, 0.0328), Complex(0.2849, -0.0143)},
        {Complex(0.3191, 0.0328), Complex(0.7891, 0.4041), Complex(0.3191, 0.0328)},
        {Complex(0.2849, -0.0143), Complex(0.3191, 0.0328), Complex(0.7981, 0.4463)}
    }};
    BOOST_TEST_MESSAGE(ZPhase);
    BOOST_TEST_MESSAGE(ZPhaseKersting);
    double err = norm(ZPhase - ZPhaseKersting, "inf");
    BOOST_TEST_MESSAGE("Err = " << err);
    BOOST_CHECK(err < 0.0005);
}

BOOST_AUTO_TEST_CASE (test_underground_tape_compare_carson)
{
    // For this test, we compare values for an underground line examined in one of Kersting's papers:
    // docs/background/Kersting_Carson.pdf.
    Network netw;
    Parser<Network> p;
    p.parse("test_underground_tape_compare_carson.yaml", netw);

    netw.solvePowerFlow();

    auto ug = netw.branches()["line_1_2"].as<UndergroundLine>();
    Mat<Complex> ZPrim = ug->ZPrim() * 1609.344; // Convert to ohms per mile.
    Mat<Complex> ZPhase = ug->ZPhase() * 1609.344; // Convert to ohms per mile.
    Complex ZPhaseKersting = Complex(1.3219, 0.6743);
    BOOST_TEST_MESSAGE(ZPhase(0));
    BOOST_TEST_MESSAGE(ZPhaseKersting);
    double err = norm(ZPhase - ZPhaseKersting, "inf");
    BOOST_TEST_MESSAGE("Err = " << err);
    BOOST_CHECK(err < 0.0006);
}

#ifndef ENABLE_OPF
BOOST_AUTO_TEST_CASE (test_matpower, *boost::unit_test::expected_failures(0))
#else 
BOOST_AUTO_TEST_CASE (test_matpower, *boost::unit_test::expected_failures(0))
#endif 
{
    BOOST_TEST_MESSAGE("Starting matpower tests");
    using namespace Sgt;

    vector<string> cases =
    {
        "caseSLPQ",
        "caseSLPV",
        "caseSLPQPV",
        "transformer",
        "nesta_case3_lmbd",
        "case4gs",
        "nesta_case4_gs",
        "case5",
        "nesta_case5_pjm",
        "case6ww",
        "nesta_case6_c",
        "nesta_case6_ww",
        "case9",
        "case9Q",
        "case9target",
        "nesta_case9_wscc",
        "case14",
        "case14_shift",
        "nesta_case14_ieee",
        "case24_ieee_rts",
        "nesta_case29_edin",
        "case30",
        "case30Q",
        "case30_all",
        "case30pwl",
        "case_ieee30",
        "nesta_case30_as",
        "nesta_case30_fsr",
        "nesta_case30_ieee",
        "case39",
        "nesta_case39_epri",
        "case57",
        "nesta_case57_ieee",
        "nesta_case73_ieee_rts",
        "case89pegase",
        "nesta_case89_pegase",
        "case118",
        "nesta_case118_ieee",
        "nesta_case162_ieee_dtc",
        "nesta_case189_edin",
        "case300",
        "nesta_case300_ieee",
        "case1354pegase",
        "nesta_case1354_pegase",
        "nesta_case1394sop_eir",
        "nesta_case1397sp_eir",
        "nesta_case1460wp_eir",
        "nesta_case2224_edin",
        "case2383wp",
        "nesta_case2383wp_mp",
        "case2736sp",
        "nesta_case2736sp_mp",
        "case2737sop",
        "nesta_case2737sop_mp",
        "case2746wop",
        "case2746wp",
        "nesta_case2746wop_mp",
        "nesta_case2746wp_mp",
        "case2869pegase",
        "nesta_case2869_pegase",
        "case3012wp", // TODO This fails, probably because SGT doesn't enforce reactive limits. See e.g. bus 70.
        "nesta_case3012wp_mp",
        "case3120sp",
        "nesta_case3120sp_mp",
        "case3375wp", // Can't solve correctly, but then neither can Matpower...
        "nesta_case3375wp_mp", // See above, doesn't converge.
        "case9241pegase",
        "nesta_case9241_pegase"
    };

    // Exclude maps: cases that are expected to fail. Value is true if we should try these anyway.

	std::map<std::string, bool> rectExclude{
		{"nesta_case300_ieee", false}, // Doesn't converge.
		{"case1354pegase", false}, // Doesn't converge.
		{"nesta_case1354_pegase", false}, // Doesn't converge.
		{"case2869pegase", false}, // Doesn't converge.
		{"nesta_case2869_pegase", false}, // Doesn't converge.
		{"case3375wp", false}, // Doesn't converge, neither does matpower.
		{"nesta_case3375wp_mp", false}, // Doesn't converge, neither does matpower.
		{"case9241pegase", false} // Doesn't converge.
	};

    map<string, bool> opfExclude({
        {"case1354pegase", false},
        {"nesta_case1354_pegase", false},
		{"nesta_case1394sop_eir", false},
		{"nesta_case1397sp_eir", false},
		{"nesta_case1460wp_eir", false},
		{"nesta_case2224_edin", false},
		{"case2383wp", false},
		{"nesta_case2383wp_mp", false},
		{"case2736sp", false},
		{"nesta_case2736sp_mp", false},
		{"case2737sop", false},
		{"nesta_case2737sop_mp", false},
		{"case2746wop", false},
		{"case2746wp", false},
		{"nesta_case2746wop_mp", false},
		{"nesta_case2746wp_mp", false},
		{"case2869pegase", false},
		{"nesta_case2869_pegase", false},
		{"case3012wp", false},
		{"nesta_case3012wp_mp", false},
		{"case3120sp", false},
		{"nesta_case3120sp_mp", false},
		{"case3375wp", false},
		{"nesta_case3375wp_mp", false},
		{"case9241pegase", false},
		{"nesta_case9241_pegase", false}
	});
    
    enum class SolveResult
    {
        RIGHT_SOLN,
        WRONG_SOLN,
        NO_SOLN,
        EXCLUDED
    };

    auto doTest = [&](Network& nw, const string& c, const map<string, bool>& excluded)->pair<SolveResult, double>
    {
        double t{-1.0};

		{
			auto it = excluded.find(c);
			if (it != excluded.end())
			{
				BOOST_TEST_MESSAGE("Excluded.");
				if (!(it->second))
				{
					BOOST_TEST_MESSAGE("Not trying.");
					return {SolveResult::EXCLUDED, t}; 
				}
				else
				{
					BOOST_TEST_MESSAGE("Test is expected to fail. Trying anyway.");
				}
			}
		}

        Stopwatch sw;
        sw.start();
        bool ok = nw.solvePowerFlow();
        sw.stop();

        if (ok)
        {
            t = sw.cpuSeconds();
        }
        else
        {
            BOOST_ERROR("Case " << c << " could not be solved.\n");
            return {SolveResult::NO_SOLN, t};
        }
        BOOST_TEST_MESSAGE("Solve time = " << t);

        ifstream compareName(string("mp_compare/") + c + ".compare");

        int nBadV = 0;
        int nBadS = 0;
        for (auto bus : nw.buses())
        {
            double Vr, Vi, P, Q;
            compareName >> Vr >> Vi >> P >> Q;
            assert(!compareName.eof());

            if (!bus->isInService()) continue;

            Complex V = {Vr, Vi};
            Complex S = {P, Q};
            if (abs(V - bus->V()(0) / bus->VBase()) >= 1e-3)
            {
                BOOST_TEST_MESSAGE("Bus V mismatch at bus " << bus->id()
                        << ": expected = " << V << ", actual = " << bus->V()(0) / bus->VBase());
                ++nBadV;
            }
            if (abs(S - bus->SGen()(0)) / nw.PBase() >= 1e-3)
            {
                BOOST_TEST_MESSAGE("Bus S mismatch at bus " << bus->id()
                        << ": expected = " << S << ", actual = " << bus->SGen()(0));
                ++nBadS;
            }
        }
		BOOST_CHECK_MESSAGE(nBadV + nBadS == 0, c << ": (V, S) doesn't agree with Matpower at (" 
			<< nBadV << ", " << nBadS << ") buses.\n");

        SolveResult sr = (nBadV == 0 && nBadS == 0) ? SolveResult::RIGHT_SOLN : SolveResult::WRONG_SOLN;
        return {sr, t};
    };
    
    auto loadNetw = [](Network& nw, const string& c){
        string yamlStr = string("--- [{matpower : {input_file : matpower_test_cases/") 
            + c + ".m, default_kV_base : 11}}]";
        nw.setPBase(100.0);
        YAML::Node n = YAML::Load(yamlStr);
        Parser<Network> p;
        p.parse(n, nw);
    };
   
    map<string, vector<pair<SolveResult, double>>> results;

    for (auto c : cases)
    {
        BOOST_TEST_MESSAGE("\nCase " << c);

        {
            BOOST_TEST_MESSAGE("\nnr_rect");
            Network nw; loadNetw(nw, c);
            auto solver = make_unique<PowerFlowNrRectSolver>();
            nw.setSolver(std::move(solver));
            results[c].push_back(doTest(nw, c, rectExclude));
        }

#ifdef ENABLE_OPF
        {
            BOOST_TEST_MESSAGE("\nopf_s_pol");
            Network nw; loadNetw(nw, c);
            auto solver = std::make_unique<OpfSPolPfSolver<>>();
            solver->setOptions({{"pf_mode", true}});
            nw.setSolver(std::move(solver));
            results[c].push_back(doTest(nw, c, opfExclude));
        }
#endif // ENABLE_OPF
    }

    BOOST_TEST_MESSAGE(setw(32) << left << "Case"
            << setw(16) << left << "nr_rect" 
            << setw(16) << left << "opf_s_pol");
    BOOST_TEST_MESSAGE("----------------------------------------------------------------");
    for (const auto& c : cases)
    {
        stringstream ss;
        ss << setw(32) << left << c;
        auto r1 = results[c];    
        for (const auto& r2 : r1)
        {
            string s;
            switch (r2.first)
            {
                case SolveResult::NO_SOLN:
                    s = "N ";
                    break;
                case SolveResult::RIGHT_SOLN:
                    s = "G ";
                    s += std::to_string(r2.second);
                    break;
                case SolveResult::WRONG_SOLN:
                    s = "B ";
                    s += std::to_string(r2.second);
                    break;
                case SolveResult::EXCLUDED:
                    s = "E ";
            }
            ss << setw(16) << left << s; 
        }

        BOOST_TEST_MESSAGE(ss.str());
    }
}

BOOST_AUTO_TEST_CASE (test_const_I)
{
    using namespace Sgt;

    string yamlStr(
            "--- [{matpower : {input_file : matpower_test_cases/nesta_case189_edin.m, " "default_kV_base : 11}}]");
    Network nw(100.0);
    YAML::Node n = YAML::Load(yamlStr);
    Parser<Network> p;
    p.parse(n, nw);

    string bus1Id = "bus_174";
    auto bus1 = nw.buses()[bus1Id]; // No existing zip generation. 
    assert(bus1->zips().size() == 0);

    string branchId = "branch_194_bus_173_bus_174";
    auto branch = nw.branches()[branchId].as<CommonBranch>();

    auto bus0 = branch->bus0();

    auto zip = make_shared<Zip>("zip_I_const", 1, Zip::wyeConnection(1));
    Complex Ic = polar(1.0, 15.0 * pi / 180.0) * 0.789;
    zip->setIConst({Ic});
    nw.addZip(zip, bus1Id, Phase::BAL);

    // for (string solverType : {"nr_pol", "nr_rect"})
    for (string solverType : {"nr_rect"})
    {
        BOOST_TEST_MESSAGE("Solver type = " << solverType);
        /*
        auto solver = solverType == "nr_pol" 
            ? unique_ptr<PowerFlowSolver>(new PowerFlowNrPolSolver)
            : unique_ptr<PowerFlowSolver>(new PowerFlowNrRectSolver);
        */
        auto solver = unique_ptr<PowerFlowSolver>(new PowerFlowNrRectSolver);
        nw.setSolver(std::move(solver));
        bool ok = nw.solvePowerFlow();
        BOOST_CHECK(ok == true);

        Complex V0 = bus0->V()(0);
        Complex V1 = bus1->V()(0);
        Complex S1 = bus1->SZip()(0);

        BOOST_CHECK(abs(S1 - conj(Ic) * abs(V1)) < 1e-4);

        Complex I1 = -(branch->Y() * Col<Complex>({V0, V1})).eval()(1); // Current entering bus 1 from bus 0.
        BOOST_CHECK(abs(Ic * V1 / abs(V1)  - I1) < 1e-4);
    }
}

BOOST_AUTO_TEST_CASE (test_properties)
{
    class Foo : public HasProperties
    {
        public:
            SGT_PROPS_INIT(Foo);                                                               

            int iByVal() const {return i_;}
            void setI(const int& i) {i_ = i;}
            SGT_PROP_GET(iByValGet, int, iByVal);
            SGT_PROP_SET(iByValSet, int, setI);
            SGT_PROP_GET_SET(iByValGetSet, int, iByVal, setI);
            
            const int& iByCRef() const {return i_;}
            SGT_PROP_GET(iByCRefGet, const int&, iByCRef);
            SGT_PROP_GET_SET(iByCRefGetSet, const int&, iByCRef, setI);

            int i_{100};
    };

    class Bar : public Foo
    {
        public:
            SGT_PROPS_INIT(Bar);
            SGT_PROPS_INHERIT(Foo);          

            const int& x() const {return x_;}
            void setX(const int& x) {x_ = x;}
            SGT_PROP_GET_SET(x, const int&, x, setX);

        public:
            int x_{200};
    };

    Foo foo;
    Bar bar;
    bool caught = false;
   
    BOOST_TEST(bool(typeid(foo.property("iByValGet").get()) == typeid(json)));
    BOOST_CHECK_EQUAL(foo.property("iByValGet").get(), json(100));
    BOOST_CHECK_EQUAL(foo.property("iByValGetSet").get(), json(100));

    BOOST_TEST(bool(typeid(foo.property("iByValGet").getUnderlying<int>()) == typeid(int)));
    BOOST_CHECK_EQUAL(foo.property("iByValGet").getUnderlying<int>(), 100);
    BOOST_CHECK_EQUAL(foo.property("iByValGetSet").getUnderlying<int>(), 100);

    caught = false;
    try {foo.property("iByValGet").set(101);} catch (const NotSettableException& e) {caught = true;}
    BOOST_TEST(caught);
    
    caught = false;
    try {foo.property("iByValSet").get();} catch (const NotGettableException& e) {caught = true;}
    BOOST_TEST(caught);

    foo.property("iByValSet").set(101);
    BOOST_CHECK_EQUAL(foo.iByVal(), 101);

    foo.property("iByValSet").setUnderlying<int>(102);
    BOOST_CHECK_EQUAL(foo.iByVal(), 102);
    
    foo.property("iByValGetSet").set(103);
    BOOST_CHECK_EQUAL(foo.iByVal(), 103);

    const int& iRef = foo.property("iByCRefGetSet").getUnderlying<const int&>(); 
    BOOST_CHECK_EQUAL(iRef, 103);
    foo.setI(104);
    BOOST_CHECK_EQUAL(iRef, 104);

    int nInBar = 0;
    for ([[maybe_unused]] const auto& p: bar.properties()) ++nInBar;
    BOOST_CHECK_EQUAL(nInBar, 6);

    BOOST_CHECK_EQUAL(bar.property("iByValGetSet").getUnderlying<int>(), 100);
    BOOST_CHECK_EQUAL(bar.property("x").getUnderlying<const int&>(), 200);
};

BOOST_AUTO_TEST_CASE (test_phases_A)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_phases_A.yaml", netw);
    netw.solvePowerFlow();
    auto V0 = netw.buses()[0]->V();
    auto V1 = netw.buses()[1]->V();
    BOOST_CHECK_SMALL(abs(V0(0) - V1(1)), 1e-9);
    BOOST_CHECK_SMALL(abs(V0(1) - V1(2)), 1e-9);
    BOOST_CHECK_SMALL(abs(V0(2) - V1(0)), 1e-9);
}

BOOST_AUTO_TEST_CASE (test_phases_B)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_phases_B.yaml", netw);
    netw.solvePowerFlow();
    auto V0 = netw.buses()[0]->V();
    auto V1 = netw.buses()[1]->V();
    BOOST_CHECK_SMALL(abs(V0(0) - V1(2)), 1e-9);
    BOOST_CHECK_SMALL(abs(V0(1) - V1(0)), 1e-9);
    BOOST_CHECK_SMALL(abs(V0(2) - V1(1)), 1e-9);
}

BOOST_AUTO_TEST_CASE (test_phases_C)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_phases_C.yaml", netw);
    netw.solvePowerFlow();
    auto V0 = netw.buses()[0]->V();
    auto V1 = netw.buses()[1]->V();
    auto S0Gen = netw.buses()[0]->SGen();
    auto S1Zip = netw.buses()[1]->SZip();
    auto y = netw.branches()[0]->Y()(0, 0);
    BOOST_CHECK_SMALL(abs(V0(0) - V1(0)), 1e-9);
    BOOST_CHECK_SMALL(abs(S0Gen(0)), 1e-9);
    BOOST_CHECK_SMALL(abs(S0Gen(1)), 1e-9);
    auto delta = (V0(2) - V1(1));
    auto diff = S0Gen(2) - Complex(S1Zip(1, 1)) - delta * conj(y) * conj(delta);
    BOOST_CHECK_SMALL(abs(diff), 1e-9);
}

BOOST_AUTO_TEST_CASE (test_line_flows)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_line_flows.yaml", netw);
    netw.solvePowerFlow();

    const auto& bus1 = *netw.buses()["bus_1"];
    const auto& bus2 = *netw.buses()["bus_2"];
    const auto& branch = *netw.branches()["branch_1_2"];

    // Documentation says STerm is power out of bus, mapped to branch terms.
    // This means SGen = -STerm[0] and SLoad = STerm[1]

    const Col<Complex> SGen = bus1.SGen();
    const Col<Complex> SLoad(bus2.SZip().diag());
    const Col<Complex> STerm0 = branch.STerm()[0];
    const Col<Complex> STerm1 = branch.STerm()[1];
    const Col<Complex> VTerm0 = bus1.V();
    const Col<Complex> VTerm1 = bus2.V();
    const Col<Complex> ITerm0 = branch.ITerm()[0];
    const Col<Complex> ITerm1 = branch.ITerm()[1];
    const Col<Complex> IV0 = conj(ITerm0) % VTerm0;
    const Col<Complex> IV1 = conj(ITerm1) % VTerm1;

    BOOST_CHECK_SMALL(sum(abs(SGen - STerm0)), 1e-9);
    BOOST_CHECK_SMALL(sum(abs(SLoad + STerm1)), 1e-9);

    BOOST_CHECK_SMALL(sum(abs(IV0 - STerm0)), 1e-9);
    BOOST_CHECK_SMALL(sum(abs(IV1 - STerm1)), 1e-9);
}

BOOST_AUTO_TEST_CASE (test_three_phase_transformers)
{
    YAML::Node templ = YAML::LoadFile("test_three_phase_transformers.yaml");
    struct vgProps
    {
        string vg;
        bool isGroundedP;
        bool isGroundedS;
        double nomTurns;
        Col<double> vMag;
        Col<double> vAngDeg;
    };
    vector<vgProps> vgs {
        {"yy0", true, true, 26.462, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {0.0, -120.0, 120.0}},
        {"yy2", true, true, 26.462, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {60.0, -60.0, 180.0}},
        {"yy4", true, true, 26.462, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {120.0, 0.0, -120.0}},
        {"yy6", true, true, 26.462, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {180.0, 60.0, -60.0}},
        {"yy8", true, true, 26.462, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {-120.0, 120.0, 0.0}},
        {"yy10", true, true, 26.462, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {-60.0, 180.0, 60.0}},

        {"dy1", false, true, 45.833, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {90.0, -30.0, -150.0}},
        {"dy3", false, true, 45.833, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {150.0, 30.0, -90.0}},
        {"dy5", false, true, 45.833, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {-150.0, 90.0, -30.0}},
        {"dy7", false, true, 45.833, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {-90.0, 150.0, 30.0}},
        {"dy9", false, true, 45.833, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {-30.0, -150.0, 90.0}},
        {"dy11", false, true, 45.833, {0.240/0.95, 0.240/0.95, 0.240/0.95}, {30.0, -90.0, 150.0}}
    };

    for (const auto& vg : vgs)
    {
        cout << "Testing " << vg.vg << endl;
        templ[5]["transformer"]["vector_group"] = vg.vg;
        templ[5]["transformer"]["is_grounded_p"] = vg.isGroundedP;
        templ[5]["transformer"]["is_grounded_s"] = vg.isGroundedS;
        templ[5]["transformer"]["nom_turns_ratio"] = vg.nomTurns;

        auto argnorm = [](double d){while (d <= 180) d += 360; while (d > 180) d -= 360; return d;};

        Network netw;
        Parser<Network> p;
        p.parse(templ, netw);
        netw.solvePowerFlow();

        auto V0 = netw.buses()[0]->V();
        auto V1 = netw.buses()[1]->V();
        cout << abs(V0).eval() << (arg(V0) * 180 / pi).eval() << endl;
        cout << abs(V1).eval() << (arg(V1) * 180 / pi).eval() << endl;
        cout << endl;
        for (auto i = 0; i < 3; ++i)
        {
            BOOST_CHECK_SMALL(abs(V1[i]) - vg.vMag[i], 1e-4);
            BOOST_CHECK_SMALL(argnorm(arg(V1[i]) * 180 / pi - vg.vAngDeg[i]), 1e-3);
        }
    }
}

BOOST_AUTO_TEST_CASE (test_single_phase_transformers)
{
    struct VgProps
    {
        string vecGp;
        bool isGroundedP;
        bool isGroundedS;
        unsigned int np;
        unsigned int ns;
        string vNomS;
        double nomTurns;
        Col<double> vAngS;
    };
    
    YAML::Node templ = YAML::LoadFile("test_single_phase_transformers.yaml");
    YAML::Node params = templ[0]["parameters"];

    constexpr double VMagS = 0.240/0.95;
    vector<VgProps> vgs {
        {"yy0", true, true, 1, 1, "[<vl>]", 26.4602, {0.0}},
        {"yy6", true, true, 1, 1, "[<vl>D180]", 26.4602, {180.0}},
        {"dy11", false, true, 2, 1, "[<vl>D30]", 45.833, {30.0}},
        {"dy5", false, true, 2, 1, "[<vl>D-150]", 45.833, {-150.0}}
    };

    for (const auto& vg : vgs)
    {
        cout << "Testing " << vg.vecGp << endl;

        auto ph = YAML::Load("[A]");
        auto ph2 = YAML::Load("[A, B]");
        auto yld = YAML::Load("[<y1>]");
        auto yld2 = YAML::Load("[<y1>,<y1>]");
        
        params["phases_p"] = vg.np == 1 ? ph : ph2;
        params["phases_s"] = vg.ns == 1 ? ph : ph2;
        params["vector_group"] = vg.vecGp;
        params["is_grounded_p"] = vg.isGroundedP;
        params["is_grounded_s"] = vg.isGroundedS;
        params["v_nom_s"] = YAML::Load(vg.vNomS);
        params["nom_turns_ratio"] = vg.nomTurns;
        params["Y_const"] = vg.isGroundedS ? yld : yld2;
        
        auto argnorm = [](double d){while (d <= 180) d += 360; while (d > 180) d -= 360; return d;};

        Network netw;
        Parser<Network> p;
        p.parse(templ, netw);
        netw.solvePowerFlow();

        auto V0 = netw.buses()[0]->V();
        auto V1 = netw.buses()[1]->V();
        cout << abs(V0).eval() << (arg(V0) * 180 / pi).eval() << endl;
        cout << abs(V1).eval() << (arg(V1) * 180 / pi).eval() << endl;
        cout << endl;
        for (uword i = 0; i < V1.size(); ++i)
        {
            BOOST_CHECK_SMALL(abs(V1[i]) - VMagS, 1e-4);
            BOOST_CHECK_SMALL(argnorm(arg(V1[i]) * 180 / pi - vg.vAngS[i]), 1e-3);
        }
    }
}

BOOST_AUTO_TEST_CASE (test_vv_transformer)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_vv_transformer.yaml", netw);
    netw.solvePowerFlow();

    auto V0 = netw.buses()[0]->V();
    auto V1 = netw.buses()[1]->V();
    auto V0ab = V0(0) - V0(1);
    auto V0bc = V0(1) - V0(2);
    auto V0ca = V0(2) - V0(0);
    auto V1ab = V1(0) - V1(1);
    auto V1bc = V1(1) - V1(2);
    auto V1ca = V1(2) - V1(0);
    cout << abs(V0(0)) << " " << abs(V0(1)) << " " << abs(V0(2)) << endl;
    cout << abs(V1(0)) << " " << abs(V1(1)) << " " << abs(V1(2)) << endl;
    cout << abs(V0ab) << " " << abs(V0bc) << " " << abs(V0ca) << endl;
    cout << abs(V1ab) << " " << abs(V1bc) << " " << abs(V1ca) << endl;
    cout << abs(V0ab/V1ab) << " " << abs(V0bc/V1bc) << " " << abs(V0ca/V1ca) << endl;
    BOOST_CHECK_CLOSE(abs(V0ab/V1ab), 0.95 * 0.95, 1e-6);
    BOOST_CHECK_CLOSE(abs(V0bc/V1bc), 0.95 * 1.05, 1e-6);
}

BOOST_AUTO_TEST_CASE (test_delta_autotransformer)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_delta_autotransformer.yaml", netw);
    netw.solvePowerFlow();

    auto V0 = netw.buses()[0]->V();
    auto V1 = netw.buses()[1]->V();
    auto Vab = V0(0) - V0(1);
    auto Vbc = V0(1) - V0(2);
    auto Vca = V0(2) - V0(0);
    auto VAb = V0(0) - V1(1);
    auto VBc = V0(1) - V1(2);
    auto VCa = V0(2) - V1(0);
    auto VAB = V1(0) - V1(1);
    auto VBC = V1(1) - V1(2);
    auto VCA = V1(2) - V1(0);
    cout << abs(V0(0)) << " " << abs(V0(1)) << " " << abs(V0(2)) << endl;
    cout << abs(V1(0)) << " " << abs(V1(1)) << " " << abs(V1(2)) << endl;
    cout << endl;
    cout << abs(Vab) << " " << abs(Vbc) << " " << abs(Vca) << endl;
    cout << abs(VAb) << " " << abs(VBc) << " " << abs(VCa) << endl;
    cout << abs(VAB) << " " << abs(VBC) << " " << abs(VCA) << endl;
    cout << endl;
    cout << abs(Vab/VAb) << " " << abs(Vbc/VBc) << " " << abs(Vca/VCa) << endl;
    BOOST_CHECK_CLOSE(abs(Vab/VAb), 0.95 * 0.95, 1e-6);
    BOOST_CHECK_CLOSE(abs(Vbc/VBc), 0.95 * 1.00, 1e-6);
    BOOST_CHECK_CLOSE(abs(Vca/VCa), 0.95 * 1.05, 1e-6);
}

BOOST_AUTO_TEST_CASE (test_load_model_a)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_load_model_a.yaml", netw);
    netw.solvePowerFlow();

    Complex y{0.1, -0.1};
    Complex s{0.1, 0.05};

    const auto& bus1 = *netw.buses()["bus_1"];
    const auto& bus2 = *netw.buses()["bus_2"];

    const Col<Complex>& V1 = bus1.V();
    const Col<Complex>& V2 = bus2.V();

    BOOST_CHECK_SMALL(norm(V1 - bus1.VNom()), 1e-7);

    {
        Complex IA = y * (V1(0) - V2(0));
        Complex IAB = conj(s/(V2(0) - V2(1)));
        Complex ICA = conj(s/(V2(2) - V2(0)));
        BOOST_CHECK_SMALL(abs(IA - IAB + ICA), 1e-7);
    }
   
    {
        Complex IB = y * (V1(1) - V2(1));
        Complex IBC = conj(s/(V2(1) - V2(2)));
        Complex IAB = conj(s/(V2(0) - V2(1)));
        BOOST_CHECK_SMALL(abs(IB - IBC + IAB), 1e-7);
    }
   
    {
        Complex IC = y * (V1(2) - V2(2));
        Complex ICA = conj(s/(V2(2) - V2(0)));
        Complex IBC = conj(s/(V2(1) - V2(2)));
        BOOST_CHECK_SMALL(abs(IC - ICA + IBC), 1e-7);
    }
}

BOOST_AUTO_TEST_CASE (test_load_model_b)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_load_model_b.yaml", netw);
    netw.solvePowerFlow();

    Complex y{0.1, -0.1};
    Complex s{0.1, 0.05};

    const auto& bus1 = *netw.buses()["bus_1"];
    const auto& bus2 = *netw.buses()["bus_2"];

    const Col<Complex>& V1 = bus1.V();
    const Col<Complex>& V2 = bus2.V();

    BOOST_CHECK_SMALL(norm(V1 - bus1.VNom()), 1e-7);

    {
        Complex IA = y * (V1(0) - V2(0));
        Complex IAB = conj(s/(V2(0) - V2(1)));
        BOOST_CHECK_SMALL(abs(IA - IAB), 1e-7);
    }
   
    {
        Complex IB = y * (V1(1) - V2(1));
        Complex IAB = conj(s/(V2(0) - V2(1)));
        BOOST_CHECK_SMALL(abs(IB + IAB), 1e-7);
    }
   
    {
        Complex IC = y * (V1(2) - V2(2));
        BOOST_CHECK_SMALL(abs(IC), 1e-7);
    }
}

BOOST_AUTO_TEST_CASE (test_load_model_c)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_load_model_c.yaml", netw);
    netw.solvePowerFlow();

    Complex y{0.1, -0.1};
    Complex i{0.1, 0.05};

    const auto& bus1 = *netw.buses()["bus_1"];
    const auto& bus2 = *netw.buses()["bus_2"];

    const Col<Complex>& V1 = bus1.V();
    const Col<Complex>& V2 = bus2.V();

    BOOST_CHECK_SMALL(norm(V1 - bus1.VNom()), 1e-7);

    {
        Complex IA = y * (V1(0) - V2(0));
        Complex IAB = i * (V2(0) - V2(1)) / abs(V2(0) - V2(1));
        BOOST_CHECK_SMALL(abs(IA - IAB), 1e-7);
    }
   
    {
        Complex IB = y * (V1(1) - V2(1));
        Complex IAB = i * (V2(0) - V2(1)) / abs(V2(0) - V2(1));
        BOOST_CHECK_SMALL(abs(IB + IAB), 1e-7);
    }
   
    {
        Complex IC = y * (V1(2) - V2(2));
        BOOST_CHECK_SMALL(abs(IC), 1e-7);
    }
}

BOOST_AUTO_TEST_CASE (test_load_model_d)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_load_model_d.yaml", netw);
    netw.solvePowerFlow();

    const auto& bus2 = *netw.buses()["bus_2"];
    const auto& branch12 = *netw.branches()["branch_1_2"];
    const auto& zip2 = *netw.zips()["zip_2"];

    Col<Complex> iFromBranch = -branch12.ITerm()[1]; // Current FROM branch INTO bus, = current FROM bus INTO load.
    Col<Complex> sExpected = conj(iFromBranch) % bus2.V();
    Col<Complex> sZip = zip2.S();
    BOOST_CHECK_SMALL(abs(sum(iFromBranch)), 1e-7); // Delta, current A = -current B.
    BOOST_CHECK_SMALL(abs(sZip(0) - sum(sExpected)), 1e-7);
}

BOOST_AUTO_TEST_CASE (test_include)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_include_A.yaml", netw);
    cout << netw << endl;
    BOOST_CHECK(netw.buses().size() == 2);
    BOOST_CHECK(netw.branches().size() == 1);
    BOOST_CHECK(netw.gens().size() == 1);
}

BOOST_AUTO_TEST_CASE (test_neutral)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_neutral.yaml", netw);
    netw.solvePowerFlow();
    auto bus2 = netw.buses()["bus_2"];
    Col<double> VAbs = abs(bus2->V());
    Col<double> VArg = arg(bus2->V()) * 180 / pi;
    // TODO: no real basis behind these checks other than they look sensible.
    // Will only catch regression to different values.
    BOOST_CHECK_SMALL(VAbs[0] - 6.32559, 1e-5);
    BOOST_CHECK_SMALL(VAbs[1] - 6.32965, 1e-5);
    BOOST_CHECK_SMALL(VAbs[2] - 6.16351, 1e-5);
    BOOST_CHECK_SMALL(VAbs[3] - 0.198181, 1e-5);
    BOOST_CHECK_SMALL(VArg[0] - -0.0299123, 1e-5);
    BOOST_CHECK_SMALL(VArg[1] - -120.082, 1e-2);
    BOOST_CHECK_SMALL(VArg[2] - 118.887, 1e-2);
    BOOST_CHECK_SMALL(VArg[3] - 156.109, 1e-2);
}

BOOST_AUTO_TEST_CASE (test_terms)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_terms.yaml", netw);
    netw.solvePowerFlow();
    auto bus1 = netw.buses()["bus_1"];
    auto bus2 = netw.buses()["bus_2"];
    auto gen = netw.gens()["gen"];
    auto zip1 = netw.zips()["zip_1"];
    auto zip2 = netw.zips()["zip_2"];
    auto branch = netw.branches()["branch"];
    cout << bus1->V() << " " << bus2->V() << endl;
    cout << gen->VTerm() << " " << gen->ITerm() << " " << gen->STerm() << endl;
    cout << zip1->VTerm() << " " << zip1->ITerm() << " " << zip1->STerm() << endl;
    cout << zip2->VTerm() << " " << zip2->ITerm() << " " << zip2->STerm() << endl;
    cout << branch->VTerm()[0] << " " << branch->ITerm()[0] << " " << branch->STerm()[0] << endl;
    cout << branch->VTerm()[1] << " " << branch->ITerm()[1] << " " << branch->STerm()[1] << endl;
    BOOST_CHECK_SMALL(sum(abs(gen->VTerm() - bus1->V())), 1e-6);
    BOOST_CHECK_SMALL(abs(zip2->VTerm()(0) - bus2->V()(1)), 1e-6);
    BOOST_CHECK_SMALL(abs(sum(gen->ITerm() + branch->ITerm()[0])), 1e-6);
    BOOST_CHECK_SMALL(abs(sum(gen->STerm() + branch->STerm()[0])), 1e-6);
    BOOST_CHECK_SMALL(abs(sum(
                    mapPhases(zip1->ITerm(), zip1->phases(), bus2->phases()) + 
                    mapPhases(zip2->ITerm(), zip2->phases(), bus2->phases()) + 
                    branch->ITerm()[1])), 1e-6);
    BOOST_CHECK_SMALL(abs(sum(
                    mapPhases(zip1->STerm(), zip1->phases(), bus2->phases()) + 
                    mapPhases(zip2->STerm(), zip2->phases(), bus2->phases()) + 
                    branch->STerm()[1])), 1e-6);
    BOOST_CHECK(gen->STerm()(0).real() < 0); // -ve draw
    BOOST_CHECK(zip2->STerm()(0).real() > 0);
    BOOST_CHECK(zip1->STerm()(0).real() + zip1->STerm()(1).real() > 0);
}

BOOST_AUTO_TEST_CASE (test_v_lims)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_limits.yaml", netw);
    netw.solvePowerFlow();
    auto b2 = netw.buses()["bus_2"];
    BOOST_CHECK(!b2->VMagMinViolated());
    BOOST_CHECK(!b2->VMagMaxViolated());
    b2->setVMagMin(6.2303);
    BOOST_CHECK(!b2->VMagMinViolated());
    b2->setVMagMin(6.2305);
    BOOST_CHECK(b2->VMagMinViolated());
    b2->setVMagMax(6.2305);
    BOOST_CHECK(!b2->VMagMaxViolated());
    b2->setVMagMax(6.2303);
    BOOST_CHECK(b2->VMagMaxViolated());
}

BOOST_AUTO_TEST_CASE (test_branch_lims)
{
    Network netw;
    Parser<Network> p;
    p.parse("test_limits.yaml", netw);
    netw.solvePowerFlow();
    auto b12 = netw.branches()["branch_1_2"];
    BOOST_CHECK(!b12->SMagMaxViolated());
    BOOST_CHECK(!b12->IMagMaxViolated());
    b12->setIMagMax(0.019);
    BOOST_CHECK(!b12->IMagMaxViolated());
    b12->setIMagMax(0.015);
    BOOST_CHECK(b12->IMagMaxViolated());
    b12->setSMagMax(0.12);
    BOOST_CHECK(!b12->SMagMaxViolated());
    b12->setSMagMax(0.11);
    BOOST_CHECK(b12->SMagMaxViolated());
}

BOOST_AUTO_TEST_SUITE_END()
