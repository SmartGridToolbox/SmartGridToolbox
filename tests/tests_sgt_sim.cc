// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#define BOOST_TEST_MODULE sgt_tests

#include <SgtCore/config.h>

#include <boost/test/included/unit_test.hpp>

#include "../SgtCore/SgtCore.h"
#include "../SgtSim/SgtSim.h"
#include "../SgtSim/WeakOrder.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <future>
#include <set>
#include <thread>
#include <vector>

using namespace arma;
using namespace Sgt;
using namespace std;
using namespace boost::unit_test;

struct Init
{
    Init()
    {
        unit_test_log.set_threshold_level(boost::unit_test::log_messages);
        BOOST_TEST_MESSAGE("\nTesting " << framework::current_test_case().p_name);
    }
    ~Init()
    {
        BOOST_TEST_MESSAGE("Finished " << framework::current_test_case().p_name << "\n");
    }
};

BOOST_FIXTURE_TEST_SUITE(sim_tests, Init)

BOOST_AUTO_TEST_CASE (test_data_timeseries)
{
    auto addPoint = [](auto& series, const TimePoint& t, const auto& v){series.addPoint(t, v);};

    vector<TimePoint> times = {epoch + hours(1), epoch + hours(2), epoch + hours(3), epoch + hours(4)};
    TimePoint t0 = times[0] - minutes(30);

    {
        StepwiseTimeSeries<TimePoint, double> series(0.0);
        vector<double> vals{1.0, 2.0, 3.0, 4.0};
        for (auto i = 0u; i < times.size(); ++i) addPoint(series, times[i], vals[i]);

        vector<double> expected{0.0, 1.0, 1.0, 2.0, 2.0, 3.0, 3.0, 4.0, 0.0};
        TimePoint t = t0;
        for (auto it = expected.begin(); it != expected.end(); ++it, t += minutes(30))
        {
            std::cout << series.value(t) << " " << *it << std::endl;
            BOOST_CHECK_CLOSE(series.value(t), *it, 1e-6);
        }
    }

    std::cout << std::endl;

    {
        LerpTimeSeries<TimePoint, double> series(0.0);
        vector<double> vals{1.0, 2.0, 3.0, 4.0};
        for (auto i = 0u; i < times.size(); ++i) addPoint(series, times[i], vals[i]);

        vector<double> expected{0.0, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 0.0};
        TimePoint t = t0;
        for (auto it = expected.begin(); it != expected.end(); ++it, t += minutes(30))
        {
            std::cout << series.value(t) << " " << *it << std::endl;
            BOOST_CHECK_CLOSE(series.value(t), *it, 1e-6);
        }
    }
    
    std::cout << std::endl;

    {
        StepwiseTimeSeries<TimePoint, Col<Complex>> series({{0.0, 0.0}, {0.0, 0.0}});
        vector<Col<Complex>> vals{
            {{1.0, 10.0}, {100.0, 1000.0}},
            {{2.0, 20.0}, {200.0, 2000.0}},
            {{3.0, 30.0}, {300.0, 3000.0}},
            {{4.0, 40.0}, {400.0, 4000.0}}
        };
        for (auto i = 0u; i < times.size(); ++i) addPoint(series, times[i], vals[i]);

        vector<Col<Complex>> expected{
            {{0.0, 0.0}, {0.0, 0.0}},
            {{1.0, 10.0}, {100.0, 1000.0}},
            {{1.0, 10.0}, {100.0, 1000.0}},
            {{2.0, 20.0}, {200.0, 2000.0}},
            {{2.0, 20.0}, {200.0, 2000.0}},
            {{3.0, 30.0}, {300.0, 3000.0}},
            {{3.0, 30.0}, {300.0, 3000.0}},
            {{4.0, 40.0}, {400.0, 4000.0}},
            {{0.0, 0.0}, {0.0, 0.0}}
        };
        TimePoint t = t0;
        for (auto it = expected.begin(); it != expected.end(); ++it, t += minutes(30))
        {
            std::cout << series.value(t) << " " << *it << std::endl;
            std::cout << (norm(series.value(t) - *it, 2)) << " " << *it << std::endl;
            BOOST_CHECK_SMALL(norm(series.value(t) - *it, 2), 1e-6);
        }
    }

    std::cout << std::endl;

    {
        LerpTimeSeries<TimePoint, Col<Complex>> series({{0.0, 0.0}, {0.0, 0.0}});
        vector<Col<Complex>> vals{
            {{1.0, 10.0}, {100.0, 1000.0}},
            {{2.0, 20.0}, {200.0, 2000.0}},
            {{3.0, 30.0}, {300.0, 3000.0}},
            {{4.0, 40.0}, {400.0, 4000.0}}
        };
        for (auto i = 0u; i < times.size(); ++i) addPoint(series, times[i], vals[i]);

        vector<Col<Complex>> expected{
            {{0.0, 0.0}, {0.0, 0.0}},
            {{1.0, 10.0}, {100.0, 1000.0}},
            {{1.5, 15.0}, {150.0, 1500.0}},
            {{2.0, 20.0}, {200.0, 2000.0}},
            {{2.5, 25.0}, {250.0, 2500.0}},
            {{3.0, 30.0}, {300.0, 3000.0}},
            {{3.5, 35.0}, {350.0, 3500.0}},
            {{4.0, 40.0}, {400.0, 4000.0}},
            {{0.0, 0.0}, {0.0, 0.0}}
        };
        TimePoint t = t0;
        for (auto it = expected.begin(); it != expected.end(); ++it, t += minutes(30))
        {
            std::cout << series.value(t) << " " << *it << std::endl;
            std::cout << (norm(series.value(t) - *it, 2)) << " " << *it << std::endl;
            BOOST_CHECK_SMALL(norm(series.value(t) - *it, 2), 1e-6);
        }
    }
}

BOOST_AUTO_TEST_CASE (test_function_timeseries)
{
    FunctionTimeSeries <TimePoint, double> fts([] (TimePoint td) {return 2 * dSeconds(td - epoch);});
    BOOST_CHECK(fts.value(epoch + seconds(-1)) == -2.0);
    BOOST_CHECK(fts.value(epoch + seconds(3)) == 6.0);
}

BOOST_AUTO_TEST_CASE (test_weak_order)
{
    WoGraph g(6);
    g.link(3, 1);
    g.link(4, 1);
    g.link(0, 4);
    g.link(1, 0);
    g.link(1, 2);
    g.link(0, 5);
    g.link(5, 2);
    // Order should be 3, (0, 1, 4), 5, 2.

    g.weakOrder();

    BOOST_CHECK(g.nodes()[0]->index() == 3);
    BOOST_CHECK(g.nodes()[1]->index() == 0);
    BOOST_CHECK(g.nodes()[2]->index() == 1);
    BOOST_CHECK(g.nodes()[3]->index() == 4);
    BOOST_CHECK(g.nodes()[4]->index() == 5);
    BOOST_CHECK(g.nodes()[5]->index() == 2);
}

BOOST_AUTO_TEST_CASE (test_battery_and_inverter)
{
    Simulation sim;
    Parser<Simulation> p;
    p.parse("test_battery_and_inverter.yaml", sim);

    Battery& batt = *sim.simComponent<Battery>("battery");
    Inverter& inv = *sim.simComponent<Inverter>("inverter");

    batt.setInitSoc(0.0098);
    batt.setRequestedPower(-0.002);

    auto dtHrs = dSeconds(batt.dt()) / 3600.0;

    for (auto i = 0u; i < 2; ++i)
    {
        sim.initialize();

        BOOST_CHECK_CLOSE(batt.requestedPower(), -0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.requestedPDc(), -0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.actualPDc(), -0.002, 1e-4);
        BOOST_CHECK_CLOSE(sum(inv.zip()->STot()).real(), -batt.actualPDc() / inv.efficiencyAcToDc(), 1e-4);

        sim.doTimestep();

        BOOST_CHECK_CLOSE(batt.requestedPower(), -0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.requestedPDc(), -0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.actualPDc(), -0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.soc(), 0.0098 - batt.actualPDc() * dtHrs * batt.chargeEfficiency(), 1e-4);

        sim.doTimestep();

        BOOST_CHECK_CLOSE(batt.requestedPower(), -0.002, 1e-4);
        BOOST_CHECK_SMALL(batt.requestedPDc(), 1e-8);
        BOOST_CHECK_SMALL(batt.actualPDc(), 1e-8);
        BOOST_CHECK_CLOSE(batt.soc(), 0.01, 1e-4); // Max SOC.
    }

    for (auto i = 0u; i < 2; ++i)
    {
        batt.setInitSoc(0.0002);
        batt.setRequestedPower(0.002);
        sim.initialize();

        BOOST_CHECK_CLOSE(batt.requestedPower(), 0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.requestedPDc(), 0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.actualPDc(), 0.002, 1e-4);
        BOOST_CHECK_CLOSE(sum(inv.zip()->STot()).real(), -batt.actualPDc() * inv.efficiencyDcToAc(), 1e-4);

        sim.doTimestep();

        BOOST_CHECK_CLOSE(batt.requestedPower(), 0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.requestedPDc(), 0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.actualPDc(), 0.002, 1e-4);
        BOOST_CHECK_CLOSE(batt.soc(), 0.0002 - batt.actualPDc() * dtHrs / batt.dischargeEfficiency(), 1e-4);

        sim.doTimestep();

        BOOST_CHECK_CLOSE(batt.requestedPower(), 0.002, 1e-4);
        BOOST_CHECK_SMALL(batt.requestedPDc(), 1e-8);
        BOOST_CHECK_SMALL(batt.actualPDc(), 1e-8);
        BOOST_CHECK_SMALL(batt.soc(), 1e-8);
    }

    batt.setInitSoc(0.0);
    batt.setRequestedPower(-0.005);
    sim.initialize();
    BOOST_CHECK_CLOSE(batt.requestedPower(), -0.005, 1e-4);
    BOOST_CHECK_CLOSE(batt.requestedPDc(), -0.002, 1e-4);
    BOOST_CHECK_CLOSE(batt.actualPDc(), -0.002, 1e-4);
    
    batt.setInitSoc(0.01);
    batt.setRequestedPower(0.005);
    sim.initialize();
    BOOST_CHECK_CLOSE(batt.requestedPower(), 0.005, 1e-4);
    BOOST_CHECK_CLOSE(batt.requestedPDc(), 0.002, 1e-4);
    BOOST_CHECK_CLOSE(batt.actualPDc(), 0.002, 1e-4);
    
    batt.setInitSoc(0.0);
    batt.setRequestedPower(-0.002);
    inv.setMaxSMag(0.001);
    sim.initialize();
    BOOST_CHECK_CLOSE(batt.requestedPower(), -0.002, 1e-4);
    BOOST_CHECK_CLOSE(batt.requestedPDc(), -0.002, 1e-4);
    BOOST_CHECK_CLOSE(batt.actualPDc() / inv.efficiencyAcToDc(), -0.001, 1e-4);
    BOOST_CHECK_CLOSE(sum(inv.zip()->STot()).real(), 0.001, 1e-4);
}

BOOST_AUTO_TEST_CASE (test_tap_changer)
{
    Simulation sim;
    Parser<Simulation> simParser;
    simParser.parse("test_tap_changer.yaml", sim);
    auto& netw = sim.simComponents()["network"].as<SimNetwork>()->network();
    auto zip4 = netw.zips()["zip_4"];
    auto bus3 = netw.buses()["bus_3"];
    auto tx = netw.branches()["trans_2_3"].as<Transformer>();
    Complex s0{0.5, 0.025};
    Col<Complex> s{s0, 0.0, 0.0};
    sim.initialize();
    for (int i = -10; i <= 10; ++i)
    {
        Col<Complex> si = i * s;
        zip4->setSConst(si);
        sim.doTimestep(); 
        cout << zip4->S() << " " << tx->taps() << " " << Col<double>(abs(bus3->V())) << " " << Col<double>(arg(bus3->V())) << endl;
        BOOST_CHECK_SMALL(abs(abs(bus3->V()[0]) - 7.0), 0.1); 
    }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(rt_tests, Init)

BOOST_AUTO_TEST_CASE (test_rt_clock)
{
    Simulation sim;
    Parser<Simulation> simParser;
    simParser.parse("test_rt_clock.yaml", sim);
    auto rtClock = std::make_shared<RealTimeClock>("clock", seconds(2));
    sim.addSimComponent(rtClock);
    Stopwatch sw;
    
    for (auto i = 0u; i < 2; ++i)
    {
        // Three steps, with a 2 second dt. 500 ms delay to make sure this doesn't add to the total time.
        // Real : Sim (@ end of update)
        // 0    : 0
        // 2    : 2
        // 4    : 4
        // 6    : 6
        sim.initialize();
        sw.reset();
        sw.start();
        for (int i = 1; i <= 3; ++i)
        {
            this_thread::sleep_for(chrono::milliseconds(500));
            sim.doTimestep();
            sgtLogMessage() << i << ": " << sw.wallSeconds() << std::endl;
            BOOST_CHECK_CLOSE(sw.wallSeconds(), 2.0 * i, 10.0);
        }
        sgtLogMessage() << sw.wallSeconds() << std::endl;
    }

    sgtLogMessage() << "----------------" << std::endl;

    for (auto i = 0u; i < 2; ++i)
    {
        // 0    : Initialization update.
        // 0    : Update -> 2 called, waiting for 2 seconds.
        // 1    : FF -> 3 interrupts from different thread.
        // 1    : Update 0 -> 2 finishes early.
        // 1    : Update 2 -> 3 runs  
        // 1    : Real time starts again
        // 1    : Update 3 -> 4 starts
        // 2    : Update 3 -> 4 ends
        sim.initialize();
        sw.reset();
        sw.start();
        auto f2 = std::async(std::launch::async, [&]{
                std::this_thread::sleep_for(std::chrono::seconds(1));
                rtClock->fastForward(sim.startTime() + seconds(3), true);
                });
        BOOST_CHECK_SMALL(sw.wallSeconds(), 1e-1);
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 1.0, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(2));
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 1.0, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(3));
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 2.0, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(4));
    }

    sgtLogMessage() << "----------------" << std::endl;

    for (auto i = 0u; i < 2; ++i)
    {
        // 0    : Initialization update.
        // 0    : Update -> 2 called, waiting for 2 seconds.
        // 1    : FF -> 2 interrupts from different thread.
        // 1    : Update 0 -> 2 finishes early.
        // 1    : Update 2 -> 4 runs  
        // 3    : Update 2 -> 4 ends
        sim.initialize();
        sw.reset();
        sw.start();
        auto f2 = std::async(std::launch::async, [&]{
                std::this_thread::sleep_for(std::chrono::seconds(1));
                rtClock->fastForward(sim.startTime() + seconds(2), true);
                });
        BOOST_CHECK_SMALL(sw.wallSeconds(), 1e-1);
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 1.0, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(2));
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 3.0, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(4));
    }

    sgtLogMessage() << "----------------" << std::endl;

    for (auto i = 0u; i < 2; ++i)
    {
        // 0    : Initialization update.
        // 0    : Update -> 2 called, waiting for 2 seconds.
        // 1    : FF -> 4 interrupts from different thread.
        // 1    : Update 0 -> 2 finishes early.
        // 1    : Update 2 -> 4 finishes early.
        // 1    : Update 4 -> 6 runs  
        // 3    : Update 4 -> 6 ends
        sim.initialize();
        sw.reset();
        sw.start();
        auto f2 = std::async(std::launch::async, [&]{
                std::this_thread::sleep_for(std::chrono::seconds(1));
                rtClock->fastForward(sim.startTime() + seconds(4), true);
                });
        BOOST_CHECK_SMALL(sw.wallSeconds(), 1e-1);
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 1.0, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(2));
        this_thread::sleep_for(chrono::milliseconds(500)); // Just an extra complication!
        sim.doTimestep();
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 1.5, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(4));
        this_thread::sleep_for(chrono::milliseconds(500)); // Just an extra complication!
        sim.doTimestep();
        sgtLogMessage() << sw.wallSeconds() << std::endl;
        BOOST_CHECK_CLOSE(sw.wallSeconds(), 3.5, 10.0);
        BOOST_CHECK(sim.currentTime() == sim.startTime() + seconds(6));
    }

    sgtLogMessage() << "RT clock test done." << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
