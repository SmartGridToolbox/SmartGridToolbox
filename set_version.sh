#!/bin/bash

VERSION=$1

echo $VERSION > SGT_VERSION
meson rewrite kwargs set project / version $VERSION
(
    cd python_bindings
    sed -e "s/^ *VERSION.*=.*/VERSION = \'$VERSION\'/g" setup.py > temp_setup.py && mv temp_setup.py setup.py
)
