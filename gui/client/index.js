require('file-loader?name=[name].[ext]!./index.html');
require('!style-loader!css-loader!bootstrap/dist/css/bootstrap.min.css');
require('!style-loader!css-loader!@fortawesome/fontawesome-free/css/fontawesome.min.css');
require('!style-loader!css-loader!@fortawesome/fontawesome-free/css/fontawesome.min.css');
require('!style-loader!css-loader!@fortawesome/fontawesome-free/css/solid.min.css');
require('!style-loader!css-loader!leaflet/dist/leaflet.css');
require('!style-loader!css-loader!FlexiJsonEditor/jsoneditor.css');
require('!style-loader!css-loader!./index.css');

const url = 'http://localhost:34568';

let eNetworkViewer = null;

window.onload = function() {
    const viewer = require('e-network-viewer');
    const networkDiv = document.getElementById('network'); 
    const propertiesDiv = document.getElementById('properties'); 
    eNetworkViewer = viewer.ENetworkViewer(networkDiv, propertiesDiv);
    eNetworkViewer.setPropertiesUpdate(propertiesUpdate);

    syncUseSprings();
    syncPinNodes();
    syncVLow();
    syncVHigh();
    syncPScale();
    syncSpringCoeff();
    syncRepulsion();
    syncDrag();
};

async function fileSelected(files) {
    let file = files[0];
    try {
        let resp = await fetch(url + '?action=load_network', {method: 'POST', body: file, mode: 'no-cors'});
    } catch(err) {
        alert('Failed to load new network');
    }

    let netwProm = fetch(url + '?view=network').then(resp => resp.json());

    try {
        netwJsn = await netwProm;
    } catch(err) {
        alert('Failed to obtain network. ' + err);
    }

    eNetworkViewer.loadNetwork(netwJsn);
}

async function solve() {
    try {
        let resp = await fetch(url + '?action=solve_network', {method: 'POST'}).then(resp => resp.json());
    } catch(err) {
        alert('Failed to solve network. ' + err);
    }

    let stateProm = fetch(url + '?view=state').then(resp => resp.json());

    try {
        stateJsn = await stateProm;
    } catch(err) {
        alert('Failed to obtain network state. ' + err);
    }

    eNetworkViewer.refreshNetworkState(stateJsn);
}

async function propertiesUpdate(compId, compType) {
    let resp = null;
    try {
        resp = await fetch(url + '?view=' + compType + '&id=' + compId, {method: 'GET'}).then(resp => resp.json());
    } catch(err) {
        alert('Failed to solve network. ' + err);
    }
    return resp;
}

function syncUseSprings() {
    let checkbox = document.getElementById('use-spring-layout');
    eNetworkViewer.setUseSprings(checkbox.checked);
}

function syncPinNodes() {
    let checkbox = document.getElementById('pin-nodes');
    eNetworkViewer.setPinNodes(checkbox.checked);
}

function syncShowHeatmap() {
    let checkbox = document.getElementById('show-heatmap');
    eNetworkViewer.setShowHeatmap(checkbox.checked);
}

function syncShowMap() {
    let checkbox = document.getElementById('show-map');
    eNetworkViewer.setShowMap(checkbox.checked);
}

function syncVLow() {
    let sliderLow = document.getElementById('range-v-low')
    let sliderHigh = document.getElementById('range-v-high')
    let labelLow = document.getElementById('label-v-low')
    vLow = Number(sliderLow.value);
    vHigh = Number(sliderHigh.value);
    if (vLow > vHigh) {
        vLow = vHigh;
        sliderLow.value = vLow;
    }
    labelLow.innerHTML = vLow.toFixed(3);
    eNetworkViewer.setVLow(vLow);
}

function syncVHigh() {
    let sliderLow = document.getElementById('range-v-low')
    let sliderHigh = document.getElementById('range-v-high')
    let labelHigh = document.getElementById('label-v-high')
    vLow = Number(sliderLow.value);
    vHigh = Number(sliderHigh.value);
    if (vLow > vHigh) {
        vHigh = vLow;
        sliderHigh.value = vHigh;
    }
    labelHigh.innerHTML = vHigh.toFixed(3);
    eNetworkViewer.setVHigh(vHigh);
}

function syncPScale() {
    let slider = document.getElementById('range-p-scale')
    let label = document.getElementById('label-p-scale')
    let val = Number(slider.value);
    label.innerHTML = val.toFixed(5);
    eNetworkViewer.setPScale(val);
}

function syncSpringCoeff() {
    let slider = document.getElementById('range-spring-coeff')
    let label = document.getElementById('label-spring-coeff')
    let val = 0.0025 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    eNetworkViewer.setSpringCoeff(val);
}

function syncRepulsion() {
    let slider = document.getElementById('range-repulsion')
    let label = document.getElementById('label-repulsion')
    let val = -2.0 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    eNetworkViewer.setRepulsion(val);
}

function syncDrag() {
    let slider = document.getElementById('range-drag')
    let label = document.getElementById('label-drag')
    let val = 0.1 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    eNetworkViewer.setDrag(val);
}

function syncScale() {
    let slider = document.getElementById('range-scale')
    let label = document.getElementById('label-scale')
    let val = 10.0 * Number(slider.value);
    label.innerHTML = val.toFixed(5);
    eNetworkViewer.setScale(val);
}

function search() {
    let searchBar = document.getElementById('search-field')
    eNetworkViewer.search(searchBar.value);
}

function centerSel() {
    eNetworkViewer.centerSel();
}

window.api = {
    'fileSelected': fileSelected,
    'solve': solve,
    'syncUseSprings': syncUseSprings,
    'syncPinNodes': syncPinNodes,
    'syncShowHeatmap': syncShowHeatmap,
    'syncShowMap': syncShowMap,
    'syncVLow': syncVLow,
    'syncVHigh': syncVHigh,
    'syncPScale': syncPScale,
    'syncPScale': syncPScale,
    'syncSpringCoeff': syncSpringCoeff,
    'syncRepulsion': syncRepulsion,
    'syncDrag': syncDrag,
    'syncScale': syncScale,
    'search': search,
    'centerSel': centerSel
};

// vim: ts=4 sw=4:
