# SgtClient: HTML viewer client for use with SgtServer

## Prerequisites:
### npm
ubuntu: `sudo apt-get install nodejs npm`
macOS: `brew install node npm`

## Build:
```bash
npm install
npm run build
```

## Run:
First, in a convenient window, run SgtServer (see ../server/README.md for details):
```bash
sgt_gui_server
```
Now, open the file dist/index.html in your browser. SgtClient has been tested on MacOS with Safari and Google Chrome.
