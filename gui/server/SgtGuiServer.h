#ifndef SGT_SERVER_DOT_H
#define SGT_SERVER_DOT_H

#include "util.h"

#include <SgtCore/Common.h>
#include <SgtCore/json.h>
#include <SgtCore/Network.h>

#include <cpprest/http_listener.h>
#include <cpprest/uri.h>
#include <cpprest/asyncrt_utils.h>

#include <boost/filesystem.hpp>

namespace Sgt
{
    class SgtGuiServer
    {
        public:
        SgtGuiServer();
        template<typename T> void setLoader(T&& loadNetwork) {loadNetwork_ = std::move(loadNetwork);}
        template<typename T> void setLoader(const T& loadNetwork) {loadNetwork_ = loadNetwork;}
        void run();
        
        void loadNetwork(const std::string& networkData);
        Sgt::json solveNetwork();

        private:
        pplx::task<void> open() {return listener_.open();}
        pplx::task<void> close() {return listener_.close();}

        void handleGet(web::http::http_request message);
        void handlePost(web::http::http_request message);

        private:
        web::http::experimental::listener::http_listener listener_;   
        std::unique_ptr<Network> network_;
        Sgt::json mapJsn_;
        std::function<void(const std::string&, Network&)> loadNetwork_;
    };
}

#endif // SGT_SERVER_DOT_H
