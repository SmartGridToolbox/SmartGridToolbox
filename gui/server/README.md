# `sgt_gui_server`
`sgt_gui_server` is a http server that can be used with the viewer client code found in ../client.

## Prerequisites
1. SmartGridToolbox
2. cpprestsdk. To install on debian/ubuntu: `sudo apt-get install libcpprest-dev`. To install on MacOS: `brew install cpprestsdk`.

## Build and install
```bash
meson setup build
cd build
ninja -j4
sudo ninja install # You may be able to omit sudo, try without if in doubt. 
```

## Run
```bash
sgt_gui_server
```
