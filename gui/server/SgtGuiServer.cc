#include "SgtGuiServer.h"

#include "util.h"

#include "SgtCore/NetworkParser.h"
#include "SgtCore/Stopwatch.h"
#include "SgtCore/Transformer.h"

#include <cstring>
#include <regex>

using namespace arma;
using namespace Sgt;
using namespace std;

using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;

using Json = Sgt::json;

SgtGuiServer::SgtGuiServer() : listener_(uri_builder("http://localhost:34568").to_uri().to_string())
{
    listener_.support(methods::GET, bind(&SgtGuiServer::handleGet, this, placeholders::_1));
    listener_.support(methods::POST, bind(&SgtGuiServer::handlePost, this, placeholders::_1));

    loadNetwork_ = [](const string& netwData, Network& netw){
        bool isMatpower = netwData.substr(0, 14) == "function mpc =";
        if (isMatpower)
        {
            string newNetwData = string("        ") + regex_replace(netwData, regex("\n"), "\n        ");
            std::string yamlStr = std::string("- matpower : \n    default_kV_base: 11\n    embedded_input: |\n")
                + newNetwData;
            NetworkParser().parse(YAML::Load(yamlStr), netw);
        }
        else
        {
            NetworkParser().parse(YAML::Load(netwData), netw);
        }
    };
    addProperties();
}

void SgtGuiServer::run()
{
    open().wait();
    sgtLogMessage() << "Listening for requests. " << endl;
    sgtLogMessage() << "Press ENTER to exit." << endl;

    string line;
    getline(cin, line);

    close().wait();
}

void SgtGuiServer::loadNetwork(const std::string& networkData)
{
    network_.reset(new Network);
    loadNetwork_(networkData, *network_);
}

Json SgtGuiServer::solveNetwork()
{
    Stopwatch sw;
    sw.start();
    bool ok = network_->solvePowerFlow();
    sw.stop();
    int solveTimeMs = int(1000 * sw.cpuSeconds());
    return json::object({{"ok", ok}, {"solve_time_ms", solveTimeMs}});
}

namespace Sgt
{
    namespace
    {
        json busJson(const Bus& b)
        {
            auto llIter = b.userData().find("lat_long");
            std::vector<double> ll = llIter != b.userData().end()
                ? llIter->get<std::vector<double>>() : std::vector<double>{0.0, 0.0};
            return 
            {
                {"id", b.id()},
                {"phs", b.phases().toJson()},
                {"isInService", true},
                {"latlong", {{"lat", ll[0]}, {"long", ll[1]}}},
                {"vBase", b.VBase()},
                {"vRmsPu", rms(b.V()) / b.VBase()},
                {"properties", componentJson(b)}
            };
        }

        json branchJson(const BranchAbc& b)
        {
            const auto& bus0Id = b.bus0()->id();
            const auto& bus1Id = b.bus1()->id();
            const auto& phases0 = b.phases0().toJson();
            const auto& phases1 = b.phases0().toJson();
            auto cons = json::array({
                    json::object({{"bus", bus0Id}, {"phs", phases0}}),
                    json::object({{"bus", bus1Id}, {"phs", phases1}})
                    });
            string branchType = dynamic_cast<const Transformer*>(&b) != nullptr ? "transformer" : "line";
            auto it = b.userData().find("branch_type");
            if (it != b.userData().end()) {
                branchType = *it;
            }
            return
            {
                {"id", b.id()},
                {"isInService", b.isInService()},
                {"cons", cons},
                {"branchType", branchType},
                {"properties", componentJson(b)}
            };
        }

        json genJson(const Gen& g)
        {
            const auto& busId = g.bus()->id();
            const auto& phases = g.phases().toJson();
            auto con = json::object({{"bus", busId}, {"phs", phases}});
            return
            {
                {"id", g.id()},
                {"isInService", g.isInService()},
                {"con", con},
                {"pTot", g.property("PTot").get()},
                {"properties", componentJson(g)}
            };
        }

        json loadJson(const Zip& z)
        {
            const auto& busId = z.bus()->id();
            const auto& phases = z.phases().toJson();
            auto con = json::object({{"bus", busId}, {"phs", phases}});
            return
            {
                {"id", z.id()},
                {"isInService", z.isInService()},
                {"con", con},
                {"pTot", z.property("PTot").get()},
                {"properties", componentJson(z)}
            };
        }
    }
}

void SgtGuiServer::handleGet(http_request request)
{
    sgtLogMessage() << "GET received." << endl;
    sgtLogMessage() << request.body() << endl;
    LogIndent _;
    auto pathsVec = http::uri::split_path(http::uri::decode(request.relative_uri().path()));
    auto query = web::uri::split_query(request.relative_uri().query());
    for (const auto& item: query) sgtLogMessage() << item.first << " = " << item.second << endl;

    status_code status = status_codes::OK;
    Json reply;
    try
    {
        std::string view = query.at("view"); 
        if (view == "network")
        {
            auto buses = json::array();
            auto branches = json::array();
            auto gens = json::array();
            auto loads = json::array();

            for (const auto& b : network_->buses())
            {
                buses.push_back(busJson(*b));
            }
            for (const auto& b : network_->branches())
            {
                branches.push_back(branchJson(*b));
            }
            for (const auto& g : network_->gens())
            {
                gens.push_back(genJson(*g));
            }
            for (const auto& z : network_->zips())
            {
                loads.push_back(loadJson(*z));
            }
            
            reply = {
                {"branches", branches},
                {"buses", buses},
                {"gens", gens},
                {"loads", loads},
            };
        }
        else if (view == "state")
        {
            auto busVoltages = json::array();
            for (auto b : network_->buses())
            {
                busVoltages.push_back(rms(b->V()) / b->VBase());
            }
            
            auto genPowers = json::array();
            auto loadPowers = json::array();
            for (auto g : network_->gens())
            {
                genPowers.push_back(g->property("PTot").get());
            }
            for (auto z : network_->zips())
            {
                loadPowers.push_back(z->property("PTot").get());
            }

            reply = {{"busVoltages", busVoltages}, {"genPowers", genPowers}, {"loadPowers", loadPowers}};
        }
        else if (view == "bus")
        {
            std::string id = query.at("id");
            reply = busJson(*network_->buses()[id]);
        }
        else if (view == "branch")
        {
            std::string id = query.at("id"); 
            reply = branchJson(*network_->branches()[id]);
        }
        else if (view == "gen")
        {
            std::string id = query.at("id"); 
            reply = genJson(*network_->gens()[id]);
        }
        else if (view == "load")
        {
            std::string id = query.at("id"); 
            reply = loadJson(*network_->zips()[id]);
        }
    }
    catch (const out_of_range& e)
    {
        status = status_codes::BadRequest;
    }

    http_response resp(status);
    resp.set_body(reply.dump(2));
    resp.headers().add(U("Access-Control-Allow-Origin"), "*");
    resp.headers().add(U("Content-Type"), "application/json");
    request.reply(resp);
}

void SgtGuiServer::handlePost(http_request request)
{
    sgtLogMessage() << "POST received." << endl;
    LogIndent _;
    auto query = web::uri::split_query(request.relative_uri().query());
    for (const auto& item: query) sgtLogMessage() << item.first << " = " << item.second << endl;

    status_code status = status_codes::OK;
    Json reply;
    try
    {
        std::string action = query.at("action"); 

        if (action == "load_network")
        {
            loadNetwork(request.extract_string(true).get()); // get() rather than then(), so reply indicates completion.
        }
        if (action == "solve_network")
        {
            reply = solveNetwork(); // get() rather than then(), so reply indicates completion.
        }
        else if (action == "toggle")
        {
            auto toggle = [](const auto& query, auto& comps)->bool
            {
                std::string id = query.at("id");
                auto comp = comps[id];
                bool exists = (comp != nullptr);
                if (exists)
                {
                    comp->setIsInService(!comp->isInService());
                }
                return exists;
            };

            toggle(query, network_->branches()) ||
                toggle(query, network_->gens()) ||
                toggle(query, network_->zips());
        }
    }
    catch (const out_of_range& e)
    {
        status = status_codes::BadRequest;
    }

    http_response resp(status);
    resp.set_body(reply.dump(2));
    resp.headers().add(U("Access-Control-Allow-Origin"), "*");
    resp.headers().add(U("Content-Type"), "application/json");
    request.reply(resp);
}
