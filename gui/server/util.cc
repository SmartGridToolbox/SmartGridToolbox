#include "util.h"

#include <SgtCore/Network.h>
#include <SgtCore/Transformer.h>

using namespace arma;
using namespace Sgt;
using namespace std;

namespace
{
    Col<double> argDeg(const Col<Complex>& x)
    {
        Col<double> result(x.size(), fill::none);
        transform(x.begin(), x.end(), result.begin(), [](const Complex& y)->double{return arg(y);});
        return result * 180 / pi;
    }
}
        
void addProperties()
{
    Bus::classProperties().addGet<Col<double>>("VMag_kV",
            [](const Bus& bus)->Col<double>{return abs(bus.V());});

    Bus::classProperties().addGet<Col<double>>("VllMag_kV",
            [](const Bus& bus)->Col<double>{return abs(toDelta(bus.V()));});

    Bus::classProperties().addGet<Col<double>>("VMagPu",
            [](const Bus& bus)->Col<double>{return abs(bus.V())/(bus.VBase());});

    Bus::classProperties().addGet<Col<double>>("VllMagPu",
            [](const Bus& bus)->Col<double>{return abs(toDelta(bus.V())/(sqrt(3) * bus.VBase()));});

    Bus::classProperties().addGet<Col<double>>("VAngDeg",
            [](const Bus& bus)->Col<double>{return argDeg(bus.V());});

    Bus::classProperties().addGet<Col<double>>("VllAngDeg",
            [](const Bus& bus)->Col<double>{return argDeg(toDelta(bus.V()));});
    
    Bus::classProperties().addGet<double>("VRms_kV",
            [&](const Bus& bus)->double{return rms(bus.V());});
    
    Bus::classProperties().addGet<double>("VRmsPu",
            [&](const Bus& bus)->double{return rms(bus.V())/bus.VBase();});
    
    Bus::classProperties().addGet<double>("PTot",
            [&](const Bus& bus)->double{return real(bus.SZipTot() - bus.SGenTot());});

    Zip::classProperties().addGet<double>("PTot",
            [&](const Zip& zip)->double{return accu(real(zip.SConst()));});

    Sgt::Gen::classProperties().addGet<double>("PTot",
            [&](const Sgt::Gen& gen)->double{return sum(real(gen.S()));});
}

template<> Sgt::json componentJson<Sgt::BranchAbc>(const Sgt::BranchAbc& comp)
{
    Sgt::json result{json::object()};

    // TODO: KLUDGE for transformers should be fixed up.
    const auto& trans = dynamic_cast<const Sgt::Transformer*>(&comp);
    result = trans == nullptr ? componentPropsJson(comp) : componentPropsJson(*trans);
    result["bus0"] = comp.bus0() != nullptr ? Sgt::json(comp.bus0()->id()) : Sgt::json();
    result["bus1"] = comp.bus1() != nullptr ? Sgt::json(comp.bus1()->id()) : Sgt::json();
    return result;
}

template<> Sgt::json componentJson<Sgt::Bus>(const Sgt::Bus& comp)
{
    Sgt::json result = componentPropsJson(comp);

    auto branches0 = comp.branches0();
    auto branches1 = comp.branches1();
    auto gens = comp.gens();
    auto zips = comp.zips();

    auto getId = [](const Sgt::Component* x){return x->id();};

    std::vector<string> branch0Ids;
    transform(branches0.begin(), branches0.end(), back_inserter(branch0Ids), getId);
    result["branches0"] = branch0Ids;

    std::vector<string> branch1Ids;
    transform(branches1.begin(), branches1.end(), back_inserter(branch1Ids), getId);
    result["branches1"] = branch1Ids;

    std::vector<string> genIds;
    transform(gens.begin(), gens.end(), back_inserter(genIds), getId); 
    result["gens"] = genIds;

    std::vector<string> zipIds;
    transform(zips.begin(), zips.end(), back_inserter(zipIds), getId); 
    result["zips"] = zipIds;

    return result;
}

template<> Sgt::json componentJson<Sgt::Gen>(const Sgt::Gen& comp)
{
    Sgt::json result = componentPropsJson(comp);
    result["bus"] = comp.bus() != nullptr ? Sgt::json(comp.bus()->id()) : Sgt::json();
    return result;
}

template<> Sgt::json componentJson<Sgt::Zip>(const Sgt::Zip& comp)
{
    Sgt::json result = componentPropsJson(comp);
    result["bus"] = comp.bus() != nullptr ? Sgt::json(comp.bus()->id()) : Sgt::json();
    return result;
}
