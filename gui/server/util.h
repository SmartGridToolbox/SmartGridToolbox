#ifndef UTIL_DOT_H
#define UTIL_DOT_H

#include <SgtCore/json.h>
#include <SgtCore/Network.h>

void addProperties();

template<typename T> Sgt::json componentPropsJson(const T& comp)
{
    Sgt::json result;
    for (const auto& propPair : comp.properties())
    {
        if (propPair.second.isGettable())
        {
            result[propPair.first] = propPair.second.get();
        }
    }
    return result;
}

template<typename T> Sgt::json componentJson(const T& comp)
{
    return componentPropsJson(comp);
}

template<> Sgt::json componentJson<Sgt::BranchAbc>(const Sgt::BranchAbc& comp);
template<> Sgt::json componentJson<Sgt::Bus>(const Sgt::Bus& comp);
template<> Sgt::json componentJson<Sgt::Gen>(const Sgt::Gen& comp);
template<> Sgt::json componentJson<Sgt::Zip>(const Sgt::Zip& comp);

template<typename T> Sgt::json componentsJson(const T& compVec)
{
    Sgt::json result;
    for (auto comp : compVec)
    {
        result.push_back(componentJson(*comp));
    }
    return result;
}

#endif // UTIL_DOT_H
