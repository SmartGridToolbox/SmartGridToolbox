# Introduction
SmartGridToolbox is a C++14 library for developing smart grid simulations, which can be used on Mac OS X or Linux
systems. It was developed by the Energy Systems subgroup of National ICT Australia (NICTA)'s Optimisation Research
Group, with the help of the [Australian National University](http://www.anu.edu.au) and
[Actew-AGL](http://www.actewagl.com.au). It is currently maintained at the [Planning and Optimisation
Group](https://cs.anu.edu.au/research/intelligence/planning-and-optimisation) of the ANU's [Research School of Computer
Science](https://cs.anu.edu.au).

SmartGridToolbox's simulation engine uses agent-based and discrete event simulation concepts. Research into future
electricity grids requires the ability to handle a wide range of control strategies and simulation components.
SmartGridToolbox aims to provide this ability through an API that emphasises flexibility and extensibility.

# Getting Started
The main GitLab repository for SmartGridToolbox is available
[here](https://gitlab.com/SmartGridToolbox/SmartGridToolbox). It contains detailed instructions about how to install
SmartGridToolbox for Linux or MacOS.

The [API reference](doxygen_html/index.html) provides a detailed reference for the C++14 API. In particular, the
[modules](doxygen_html/modules.html) page gives an organised overview of the code. The [YAML
specifications](doxygen_html/group___yaml_spec.html) for SmartGridToolbox configuration files are also documented.

Finally, the [tutorials](#tutorials) and [examples](#examples) may also be of use.

# <a name="tutorials"></a>Tutorials

The tutorials can be found in `SmartGridToolbox/tutorials`.

[Basics Tutorial](basics_tutorial.html) - Basic and utility classes and functions.

[Network Tutorial](network_tutorial.html) - Construct and solve networks.

[ComponentCollection Tutorial](component_collection_tutorial.html) - Learn about the `ComponentCollection` class,
frequently used to store collections of objects.

[Parsing Tutorial](parsing_tutorial.html) - See how SmartGridToolbox parses [YAML](http://yaml.org) configuration files.

[Simulation Tutorial](simulation_tutorial.html) - Construct and solve a basic simulation.

[Time Varying Loads Tutorial](tvloads_tutorial.html) - See how time series data can be incorporated into simulations.

[Properties Tutorial](properties_tutorial.html) - See how SmartGridToolbox Properties work.

# <a name="examples"></a>Examples
The examples can be found in `SmartGridToolbox/examples`. They are intended to be an introduction to more sophisticated applications of SmartGridToolbox, involving optimisation and custom components. They contain README.md files that will give details that will help you to build and understand the code.

# <a name="python_bindings"></a>Python Bindings
Python 3 bindings for the SgtCore part of SmartGridToolbox (which includes network modelling and solving but excludes
the discrete event simulation library) are available in `extras/python_bindings`. See [here](python_bindings.html)
for details.
