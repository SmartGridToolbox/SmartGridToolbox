#!/bin/bash

html_name () {
    MD=${1}
    SUF=${2}
    B=${MD%.md}
    B=${B##*/}
    echo ${B}${SUF}.html
}

make_html () {
    MD=${1}
    HT=${2}
    CMD="m2h.sh ${MD} ${HT}" 
    echo ${CMD}
    eval ${CMD}
}

for MD in $(ls md/*.md); do
    HT=../public/$(html_name ${MD})
    make_html ${MD} ${HT}
done

cd ../../../tutorials > /dev/null
build_tutorials.sh
cd - > /dev/null

for MD in $(ls ../../../tutorials/*/*.md); do
    HT=../public/$(html_name ${MD} _tutorial)
    make_html ${MD} ${HT}
done

MD=../../../extras/python_bindings/README.md
HT=../public/python_bindings.html
make_html ${MD} ${HT}

for D in ../../../tutorials/*/images; do
    cp -r $D ../public
done

cp -r ../../../extras/SgtClient/dist ../public/viewer
