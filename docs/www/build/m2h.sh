#!/bin/bash

INFILE=${1}
OUTFILE=${2}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
HEAD_HTML="${SCRIPT_DIR}/head.html"
TAIL_HTML="${SCRIPT_DIR}/tail.html"

if [ -n "$(which gsed)" ]; then SED=gsed; else SED=sed; fi

BASE=${OUTFILE%.*}
BASE=${BASE##*/}
TITLE=$(echo ${BASE} | tr '_' ' ' | ${SED} -e 's/\b\(.\)/\u\1/g')

TMP=$(mktemp)
cat ${HEAD_HTML} ${INFILE} ${TAIL_HTML} > ${TMP}

pandoc \
    -f gfm \
    -t html \
    --standalone \
    --css=stylesheets/stylesheet.css \
    --metadata pagetitle="${TITLE}" \
    -o ${OUTFILE} \
    ${TMP}

rm ${TMP}
