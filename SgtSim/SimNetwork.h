// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SIM_NETWORK_DOT_H
#define SIM_NETWORK_DOT_H

#include <SgtSim/SimComponent.h>

#include <SgtCore/Network.h>

namespace Sgt
{
    class SimNetwork;

    /// @brief Utility base class for branches in SimNetwork.
    ///
    /// Provides convenience accessors.
    /// Allows dependency ensuring I will update before network.
    class SimBranch : virtual public SimComponent
    {
        public:

        SimBranch(const std::string& simNetworkId, const std::string& branchId) :
            simNetworkId_(simNetworkId), branchId_(branchId) {};

        ConstComponentPtr<BranchAbc> branch() const {return branch_;}
        ComponentPtr<BranchAbc> branch() {return branch_;}

        virtual void initializeState() override
        {
            branch_ = nullptr;
        }

        virtual void setDependencies(SimDependencies& simDeps) override;

        private:

        std::string simNetworkId_;
        std::string branchId_;

        ComponentPtr<BranchAbc> branch_;
    };

    /// @brief Utility base class for buses in SimNetwork.
    ///
    /// Provides convenience accessors.
    /// Allows dependency ensuring I will update before network.
    class SimBus : virtual public SimComponent
    {
        public:

        SimBus(const std::string& simNetworkId, const std::string& busId) :
            simNetworkId_(simNetworkId), busId_(busId) {};

        ConstComponentPtr<Bus> bus() const {return bus_;}
        ComponentPtr<Bus> bus() {return bus_;}

        virtual void initializeState() override
        {
            bus_ = nullptr;
        }

        virtual void setDependencies(SimDependencies& simDeps) override;

        private:

        std::string simNetworkId_;
        std::string busId_;

        ComponentPtr<Bus> bus_;
    };

    /// @brief Utility base class for gens in SimNetwork.
    ///
    /// Provides convenience accessors.
    /// Allows dependency ensuring I will update before network.
    class SimGen : virtual public SimComponent
    {
        public:

        SimGen(const std::string& simNetworkId, const std::string& genId) :
            simNetworkId_(simNetworkId), genId_(genId) {};

        ConstComponentPtr<Gen> gen() const {return gen_;}
        ComponentPtr<Gen> gen() {return gen_;}

        virtual void initializeState() override
        {
            gen_ = nullptr;
        }

        virtual void setDependencies([[maybe_unused]] SimDependencies& simDeps) override;

        private:

        std::string simNetworkId_;
        std::string genId_;

        ComponentPtr<Gen> gen_;
    };

    /// @brief Utility base class for zips in SimNetwork.
    ///
    /// Provides convenience accessors.
    /// Allows dependency ensuring I will update before network.
    class SimZip : virtual public SimComponent
    {
        public:

        SimZip(const std::string& simNetworkId, const std::string& zipId) :
            simNetworkId_(simNetworkId), zipId_(zipId) {};

        ConstComponentPtr<Zip> zip() const {return zip_;}
        ComponentPtr<Zip> zip() {return zip_;}

        virtual void initializeState() override
        {
            zip_ = nullptr;
        }

        virtual void setDependencies([[maybe_unused]] SimDependencies& simDeps) override;

        private:

        std::string simNetworkId_;
        std::string zipId_;

        ComponentPtr<Zip> zip_;
    };

    /// @brief SimNetwork : A SimComponent for an electrical network.
    class SimNetwork : virtual public SimComponent
    {
        public:

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("sim_network");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        /// @brief Constructor taking a shared_ptr.
        SimNetwork(const std::string& id, std::shared_ptr<Network> network);

        /// @brief Non-owning constructor taking a reference.
        ///
        /// Useful if we already have a network that is not a shared_ptr.
        SimNetwork(const std::string& id, Network& network);

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        // virtual json toJson() const override; // TODO

        /// @}

        /// @name Network access:
        /// @{

        const Network& network() const
        {
            return *network_;
        }

        Network& network()
        {
            return *network_;
        }

        /// @}

        protected:

        /// @name Overridden member functions from SimComponent:
        /// @{

        virtual void initializeState() override;
        virtual void updateState(const TimePoint& t) override;

        /// @}

        private:

        std::shared_ptr<Network> network_;
        Event networkChanged_;
    };
}

#endif // SIM_NETWORK_DOT_H
