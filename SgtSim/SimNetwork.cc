// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SimNetwork.h"


namespace Sgt
{
    void SimBranch::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        auto simNetw = simDeps.addDependency(id(), simNetworkId_, false).second<SimNetwork>();
        branch_ = simNetw->network().branches()[branchId_];
    }

    void SimBus::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        auto simNetw = simDeps.addDependency(id(), simNetworkId_, false).second<SimNetwork>();
        bus_ = simNetw->network().buses()[busId_];
    }

    void SimGen::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        auto simNetw = simDeps.addDependency(id(), simNetworkId_, false).second<SimNetwork>();
        gen_ = simNetw->network().gens()[genId_];
    }

    void SimZip::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        auto simNetw = simDeps.addDependency(id(), simNetworkId_, false).second<SimNetwork>();
        zip_ = simNetw->network().zips()[zipId_];
    }

    namespace
    {
        template <typename T> struct EmptyDeleter
        {
            EmptyDeleter() {}

            template <typename U> EmptyDeleter(const EmptyDeleter<U>&,
                    typename std::enable_if<std::is_convertible<U*, T*>::value>::type* = nullptr) {}

            void operator()(T* const) const {}
        };
    }

    SimNetwork::SimNetwork(const std::string& id, std::shared_ptr<Network> network) : 
        Component(id),
        network_(network),
        networkChanged_("SimNetwork network changed")
    {
        needsUpdate().addTrigger(networkChanged_);
    }

    SimNetwork::SimNetwork(const std::string& id, Network& network) :
        SimNetwork(id, std::shared_ptr<Network>(&network, EmptyDeleter<Network>())) {};

    void SimNetwork::initializeState()
    {
        networkChanged_.clearTriggers();
        for (auto branch: network_->branches())
        {
            networkChanged_.addTrigger(branch->admittanceChanged());
            networkChanged_.addTrigger(branch->isInServiceChanged());
        }
        for (auto bus: network_->buses())
        {
            networkChanged_.addTrigger(bus->setpointChanged());
            networkChanged_.addTrigger(bus->isInServiceChanged());
        }
        for (auto gen: network_->gens())
        {
            networkChanged_.addTrigger(gen->setpointChanged());
            networkChanged_.addTrigger(gen->isInServiceChanged());
        }
        for (auto zip: network_->zips())
        {
            networkChanged_.addTrigger(zip->injectionChanged());
            networkChanged_.addTrigger(zip->setpointChanged());
            networkChanged_.addTrigger(zip->isInServiceChanged());
        }
    }
    
    void SimNetwork::updateState([[maybe_unused]] const TimePoint& t)
    {
        sgtLogDebug() << "SimNetwork : update state." << std::endl;
        network_->solvePowerFlow(); // TODO: inefficient to rebuild even if not needed.
    }
}
