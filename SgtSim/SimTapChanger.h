// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SIM_TAP_CHANGER_DOT_H
#define SIM_TAP_CHANGER_DOT_H

#include <SgtSim/Heartbeat.h>
#include <SgtSim/SimComponent.h>
#include <SgtSim/TimeSeries.h>

#include <SgtCore/Bus.h>
#include <SgtCore/Transformer.h>

#include <functional>

namespace Sgt
{
    class SimTapChangerAbc : virtual public SimComponent
    {
        public:

        /// @name Lifecycle:
        /// @{

        SimTapChangerAbc(const std::string& simNetworkId, const std::string& transId, const TcTap& tcTap):
            simNetworkId_(simNetworkId), transId_(transId), tcTap_(tcTap)
        {}

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        // virtual json toJson() const override; TODO

        /// @}
        
        /// @name My functions:
        /// @{
        
        ConstComponentPtr<BranchAbc, Transformer> transformer() const {return trans_;}
        ComponentPtr<BranchAbc, Transformer> transformer() {return trans_;}
        
        const TapChanger& tapChanger() const {return trans_->tapChanger(tcTap_);}
        TapChanger& tapChanger() {return trans_->tapChanger(tcTap_);}

        /// @}

        protected:

        virtual void setDependencies(SimDependencies& simDeps) override;

        protected:

        std::string simNetworkId_; // Target transformer ID.
        std::string transId_; // Target transformer ID.
        TcTap tcTap_; // Which winding does this tap changer use?
        
        ComponentPtr<BranchAbc, Transformer> trans_{nullptr}; // Target transformer.
    };

    //  Init :   
    //      setTap(tap).
    //      Device fires event
    //      network updates etc.
    //  Bus updates :
    //      contingent update of this.
    //  Update:
    //      getCtrlV() to assess setpoint; bus is already updated. 
    //      setTap(tap) if necessary :
    //          bus setpoint changed which triggers another cycle.

    class AutoTapChanger : public SimTapChangerAbc
    {
        public:

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("auto_tap_changer");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        AutoTapChanger(const std::string& id, const std::string& simNetworkId, const std::string& transId,
                const TcTap& tcTap);

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        // virtual json toJson() const override; TODO

        /// @}

        /// @name SimComponent virtual overridden member functions:
        /// @{

        virtual TimePoint validUntil() const override
        {
            return TimeSpecialValues::pos_infin; // Never undergo a scheduled update.
        }

        virtual void setDependencies([[maybe_unused]] SimDependencies& simDeps) override;

        virtual void initializeState() override;

        virtual void updateState(const TimePoint& t) override;

        /// @}

        /// @name My functions:
        /// @{

        const Duration& delay() const
        {
            return delay_;
        }
        
        void setDelay(const Duration& delay)
        {
            delay_ = delay;
        }

        /// @}

        private:

        Duration delay_{seconds(0)};
        TimePoint prevTimestep_{TimeSpecialValues::not_a_date_time};
        size_t iter_{0};
        double ctrlV_{0};
        TimePoint lastNotAbove_{TimeSpecialValues::not_a_date_time};
        TimePoint lastNotBelow_{TimeSpecialValues::not_a_date_time};
    };
    
    class TimeSeriesTapChanger : public Heartbeat, public SimTapChangerAbc
    {
        public:

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("time_series_tap_changer");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        TimeSeriesTapChanger(
                const std::string& id,
                const std::string& simNetworkId,
                const std::string& transId,
                const std::string& seriesId,
                const TcTap& tcTap,
                const Duration& dt
                ) :
            Component(id),
            Heartbeat(dt),
            SimTapChangerAbc(simNetworkId, transId, tcTap),
            timeSeriesId_(seriesId)
        {
            // Empty.
        }

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        // virtual json toJson() const override; TODO

        /// @}

        /// @name SimComponent virtual overridden member functions:
        /// @{

        virtual void setDependencies(SimDependencies& simDeps) override;

        virtual void updateState(const TimePoint& t) override;

        /// @}

        private:

        std::string transId_;
        std::string timeSeriesId_;

        ConstTimeSeriesPtr<StepwiseTimeSeries<TimePoint, double>> series_{nullptr};
    };
}

#endif // SIM_TAP_CHANGER_DOT_H
