// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TimeSeriesTapChangerParserPlugin.h"

#include "Inverter.h"
#include "SimNetwork.h"
#include "Simulation.h"
#include "SimTapChanger.h"

namespace Sgt
{
    void TimeSeriesTapChangerParserPlugin::parse(const YAML::Node& nd, Simulation& sim, const ParserBase& parser) const
    {
        assertFieldPresent(nd, "id");
        assertFieldPresent(nd, "sim_network_id");
        assertFieldPresent(nd, "transformer_id");
        assertFieldPresent(nd, "tap");
        assertFieldPresent(nd, "time_series_id");
        assertFieldPresent(nd, "dt");

        std::string id = parser.expand<std::string>(nd["id"]);
        std::string simNetworkId = parser.expand<std::string>(nd["sim_network_id"]);
        std::string transId = parser.expand<std::string>(nd["transformer_id"]);
        std::string tapStr = parser.expand<std::string>(nd["tap"]);
        TcTap taps = tapStr == "all" ? tcAllTaps() : tcSingleTap(parser.expand<int>(nd["tap"]));

        std::string timeSeriesId = parser.expand<std::string>(nd["time_series_id"]);
        Duration dt = parser.expand<Duration>(nd["dt"]);

        sim.newSimComponent<TimeSeriesTapChanger>(id, simNetworkId, transId, timeSeriesId, taps, dt);
    }
}
