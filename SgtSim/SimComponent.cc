// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SimComponent.h"
#include "WeakOrder.h"

namespace Sgt
{
    SimDependencies::SimDependencies(
            ComponentCollection<SimComponent>& simComps, ComponentCollection<TimeSeriesBase>& timeSeries) :
        simComps_{simComps}, timeSeries_{timeSeries} {}

    SimDependency SimDependencies::addDependency(const std::string& updateFirst, const std::string& updateSecond,
            bool forceUpdate)
    {
        auto key = std::make_pair(updateFirst, updateSecond);
        auto first = simComps_[updateFirst];
        sgtAssert(first != nullptr, "Can't find first member " << updateFirst << " of dependency ("
                << updateFirst << ", " << updateSecond << ")");
        auto second = simComps_[updateSecond];
        sgtAssert(second != nullptr, "Can't find second member " << updateSecond << " of dependency ("
                << updateFirst << ", " << updateSecond << ")");
        SimDependency dep{first, second};
        deps_.insert({key, dep});
        if (forceUpdate)
        {
            dep.second<>()->needsUpdate().addTrigger(dep.first<>()->didUpdate());
        }
        return dep;
    }

    void SimDependencies::weakOrder()
    {
        for (auto i = 0u; i < simComps_.size(); ++i) simComps_[i]->setRank(i);

        WoGraph g(simComps_.size());
        for (auto& depKv : deps_)
        {
            auto& dep = depKv.second;
            g.link(dep.first<>()->rank(), dep.second<>()->rank());
        }
        g.weakOrder();

        for (auto i = 0u; i < simComps_.size(); ++i)
        {
            std::size_t j = g.nodes()[i]->index();
            simComps_[j]->setRank(i);
        }

        std::sort(simComps_.begin(), simComps_.end(), 
                [](const ComponentPtr<SimComponent>& lhs, const ComponentPtr<SimComponent>& rhs)
                {return lhs->rank() < rhs->rank();});
    }

        
    SimComponent::SimComponent():
        willUpdate_{id() + ": Will update"},
        didUpdate_{id() + ": Did update"},
        needsUpdate_{id() + ": Needs update"},
        willStartNewTimestep_{id() + ": Will start new timestep"},
        didCompleteTimestep_{id() + ": Did complete timestep"}
    {
    }

    void SimComponent::initialize()
    {
        sgtLogDebug(LogLevel::VERBOSE) << "SimComponent " << id() << " initialize." << std::endl;
        lastUpdated_ = TimeSpecialValues::not_a_date_time;
        initializeState();
    }

    void SimComponent::update(const TimePoint& t)
    {
        sgtLogDebug(LogLevel::VERBOSE) << "SimComponent " << id() << " update from " << lastUpdated_ << " to " 
            << t << std::endl;
        if (lastUpdated_.is_not_a_date_time() || lastUpdated_ < t)
        {
            willStartNewTimestep_.trigger();
        }
        willUpdate_.trigger();
        updateState(t);
        lastUpdated_ = t;
        didUpdate_.trigger();
    }
}
