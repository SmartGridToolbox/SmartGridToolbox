// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TimeSeriesZip.h"

#include <SgtCore/Zip.h>

using namespace arma;

namespace Sgt
{
    TimeSeriesZip::TimeSeriesZip(const std::string& id, const std::string& simNetworkId, const std::string& zipId,
            const std::string& seriesId, const Duration& dt,
            const Col<uword>& dataIdxsY,
            const Col<uword>& dataIdxsI,
            const Col<uword>& dataIdxsS) :
        Component(id),
        Heartbeat(dt),
        SimZip(simNetworkId, zipId),
        seriesId_(seriesId),
        dataIdxsY_(dataIdxsY),
        dataIdxsI_(dataIdxsI),
        dataIdxsS_(dataIdxsS)
    {
        // Empty.
    }
            
    void TimeSeriesZip::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        SimZip::setDependencies(simDeps);
        series_ = simDeps.timeSeries<TimeSeries<TimePoint, Col<Complex>>>(seriesId_);
    }

    void TimeSeriesZip::updateState(const TimePoint& t)
    {
        Heartbeat::updateState(t);
        zip()->setYConst(YConst(t));
        zip()->setIConst(IConst(t));
        zip()->setSConst(SConst(t));
    }

    Col<Complex> TimeSeriesZip::YConst(const TimePoint& t) const
    {
        return dataIdxsY_.size() == 0 
            ? Col<Complex>(zip()->nComps(), fill::zeros)
            : scaleFactorY_ * series_->value(t)(dataIdxsY_);
    }

    Col<Complex> TimeSeriesZip::IConst(const TimePoint& t) const
    {
        return dataIdxsI_.size() == 0 
            ? Col<Complex>(zip()->nComps(), fill::zeros)
            : scaleFactorI_ * series_->value(t)(dataIdxsI_);
    }

    Col<Complex> TimeSeriesZip::SConst(const TimePoint& t) const
    {
        return dataIdxsS_.size() == 0 
            ? Col<Complex>(zip()->nComps(), fill::zeros)
            : scaleFactorS_ * series_->value(t)(dataIdxsS_);
    }

    void TimeSeriesZip::initializeState()
    {
        Heartbeat::initializeState();
        SimZip::initializeState();
    }
}
