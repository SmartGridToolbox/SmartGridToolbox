// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SIM_COMPONENT_DOT_H
#define SIM_COMPONENT_DOT_H

#include <SgtCore/Common.h>
#include <SgtCore/Component.h>
#include <SgtCore/ComponentCollection.h>
#include <SgtCore/Event.h>

#include <SgtSim/TimeSeries.h>

namespace Sgt
{
    class SimComponent;
    class Simulation;
    class TimeSeriesBase;

    template<typename T = SimComponent> using SimComponentPtr = ComponentPtr<SimComponent, T>;
    template<typename T = SimComponent> using ConstSimComponentPtr = ConstComponentPtr<SimComponent, T>;
    template<typename T = TimeSeriesBase> using ConstTimeSeriesPtr = ConstComponentPtr<TimeSeriesBase, T>;

    using SimDepKey = std::pair<std::string, std::string>;

    class SimDependency
    {
        friend class SimDependencies;

        public:

        template<typename T = SimComponent> SimComponentPtr<T> first() {return first_.as<T>();}
        template<typename T = SimComponent> SimComponentPtr<T> second() {return second_.as<T>();}

        private:

        SimDependency(const SimComponentPtr<>& first, const SimComponentPtr<>& second) :
            first_{first}, second_{second} {}

        SimComponentPtr<> first_;
        SimComponentPtr<> second_;
    };
    
    class SimDependencies
    {
        friend class Simulation;

        public:

        SimDependency addDependency(const std::string& updateFirst, const std::string& updateSecond,
                bool forceUpdate=false);

        template<typename T = TimeSeriesBase> ConstTimeSeriesPtr<T> timeSeries(const std::string& id) const
        {
            return timeSeries_[id].as<T>();
        }

        private:

        SimDependencies(ComponentCollection<SimComponent>& simComps, ComponentCollection<TimeSeriesBase>& timeSeries);

        void weakOrder();

        private:

        std::map<SimDepKey, SimDependency> deps_;
        ComponentCollection<SimComponent>& simComps_;
        ComponentCollection<TimeSeriesBase>& timeSeries_;
    };

    /// @brief A base class for components of a Simulation.
    /// @ingroup SimCore
    class SimComponent : virtual public Component
    {
        friend class Simulation;

        public:

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("sim_component");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        SimComponent();

        virtual ~SimComponent() override = default;

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        // virtual json toJson() const override; // TODO

        /// @}

        /// @name Simulation flow:
        /// @{

        /// @brief Initialize state of the object.
        ///
        /// This simply does the initial work needed to set the object up, prior to simulation. The time following
        /// initialization will be set to negative infinity, and the object will be considered to be in an
        /// invalid state. To progress to a valid state, the object will need to undergo an update().
        virtual void initialize() final;

        /// @brief Bring state up to time t.
        virtual void update(const TimePoint& t) final;

        /// @}

        /// @name Virtual methods to be overridden by derived classes:
        /// @{

        /// @brief When is the component scheduled to update?
        ///
        /// An update may occur before this time if the component undergoes a contingent update. If this
        /// occurs, it is possible that validUntil() could change to a later time.
        virtual TimePoint validUntil() const
        {
            return TimeSpecialValues::pos_infin;
        }

        protected:
        
        /// @brief Reset state of the object, time will be at negative infinity.
        ///
        /// Called by Simulation::initialize(), which should be called before every simulation is started.
        virtual void initializeState()
        {
            // Empty.
        }

        /// @brief Set up dependencies immediately after initializeState is called, time will be at negative infinity.
        ///
        /// Called by Simulation::initialize(), which should be called before every simulation is started.
        virtual void setDependencies([[maybe_unused]] SimDependencies& simDeps)
        {
            // Empty.
        }
        
        /// @brief Bring state up to time current time.
        ///
        /// Note that, besides normal updates, this function will also be called as part of the initialization
        /// step, to update the time from negative infinity to the simulation start time. This can be checked by
        /// isInitialized().
        virtual void updateState([[maybe_unused]] const TimePoint& t)
        {
            // Empty.
        }
        
        /// @brief Called after all components have finished their final updateState for a timestep.
        virtual void finalizeTimestep()
        {
            // Empty.
        }
        
        /// @brief Called after the last timestep in the simulation. 
        virtual void finalizeSimulation()
        {
            // Empty.
        }

        /// @}

        public:

        /// @name Timestepping:
        /// @{

        /// @brief Get the current step for the object.
        TimePoint lastUpdated() const
        {
            return lastUpdated_;
        }

        /// @brief Have we completed initialization?
        ///
        /// true => we've finished both initializeState call and the subsequent initialization call to updateState.
        bool isInitialized() const
        {
            return (!lastUpdated_.is_not_a_date_time());
        }

        /// @}

        /// @name Rank:
        /// @brief Rank: A < B means B depends on A, not vice-versa, so A should go first.
        /// @{

        /// @brief Get the rank of the object.
        int rank() const
        {
            return rank_;
        }

        /// @brief Set the rank of the object.
        void setRank(int rank)
        {
            rank_ = rank;
        }

        /// @}
        /// @name Events:
        /// @{

        /// @brief Triggered just before my update.
        const Event& willUpdate() const {return willUpdate_;}

        /// @brief Triggered just before my update.
        Event& willUpdate() {return willUpdate_;}

        /// @brief Triggered after my update.
        const Event& didUpdate() const {return didUpdate_;}

        /// @brief Triggered after my update.
        Event& didUpdate() {return didUpdate_;}

        /// @brief Triggered when I am flagged for future update.
        const Event& needsUpdate() const {return needsUpdate_;}

        /// @brief Triggered when I am flagged for future update.
        Event& needsUpdate() {return needsUpdate_;}

        /// @brief Triggered when I am about to update to a new timestep.
        const Event& willStartNewTimestep() const {return willStartNewTimestep_;}

        /// @brief Triggered when I am about to update to a new timestep.
        Event& willStartNewTimestep() {return willStartNewTimestep_;}

        /// @brief Triggered when I just updated, completing the current timestep.
        const Event& didCompleteTimestep() const {return didCompleteTimestep_;}

        /// @brief Triggered when I just updated, completing the current timestep.
        Event& didCompleteTimestep() {return didCompleteTimestep_;}

        /// @}

        private:

        TimePoint lastUpdated_{TimeSpecialValues::not_a_date_time};
        ///< The time to which I am up to date
        
        int rank_{-1};
        ///< Evaluation rank, based on weak ordering.

        Event willUpdate_{std::string(componentType()) + ": Will update"};
        ///< Triggered immediately prior to upddate.
        Event didUpdate_{std::string(componentType()) + ": Did update"};
        ///< Triggered immediately post update.
        Event needsUpdate_{std::string(componentType()) + ": Needs update"};
        ///< Triggered when I need to be updated (contingent update).
        Event willStartNewTimestep_{std::string(componentType()) + ": Will start new timestep"};
        ///< Triggered immediately prior to time advancing.
        Event didCompleteTimestep_{std::string(componentType()) + ": Did complete timestep"};
        ///< Triggered just after fully completing a timestep.

        Action insertContingentUpdateAction_;
        ///< Used by simulation to insert contingent updates.
    };
}

#endif // SIM_COMPONENT_DOT_H
