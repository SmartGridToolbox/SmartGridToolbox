// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "InverterParserPlugin.h"

#include "Inverter.h"
#include "SimNetwork.h"
#include "Simulation.h"

#include <SgtCore/ZipParserPlugin.h>

namespace Sgt
{
    void InverterParserPlugin::parse(const YAML::Node& nd, Simulation& sim, const ParserBase& parser) const
    {
        assertFieldPresent(nd, "id");
        assertFieldPresent(nd, "sim_network_id");

        std::string id = parser.expand<std::string>(nd["id"]);
        std::string simNetworkId = parser.expand<std::string>(nd["sim_network_id"]);

        auto& simNetwork = *sim.simComponent<SimNetwork>(simNetworkId);
        Network& network = simNetwork.network();

        auto ndZip = parser.expand(nd["zip"]);
        std::string zipId;
        if (!ndZip || ndZip.IsScalar())
        {
            zipId = ndZip ? ndZip.as<std::string>() : id;
        }
        else
        {
            auto ndZipId = parser.expand(ndZip["id"]);
            if (!ndZipId) ndZip["id"] = id;
            zipId = ndZipId.as<std::string>();
            ZipParserPlugin zpp;
            zpp.parse(ndZip, network, parser);
        }
       
        auto inverter = sim.newSimComponent<Inverter>(id, simNetworkId, zipId);

        if (nd["efficiency_dc_to_ac"])
        {
            inverter->setEfficiencyDcToAc(parser.expand<double>(nd["efficiency_dc_to_ac"]));
        }
        
        if (nd["efficiency_ac_to_dc"])
        {
            inverter->setEfficiencyAcToDc(parser.expand<double>(nd["efficiency_ac_to_dc"]));
        }

        if (nd["max_S_mag"])
        {
            inverter->setMaxSMag(parser.expand<double>(nd["max_S_mag"]));
        }

        if (nd["requested_Q"])
        {
            inverter->setRequestedQ(parser.expand<double>(nd["requested_Q"]));
        }
    }
}
