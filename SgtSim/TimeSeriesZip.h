// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TIME_SERIES_ZIP_DOT_H
#define TIME_SERIES_ZIP_DOT_H

#include <SgtSim/Heartbeat.h>
#include <SgtSim/TimeSeries.h>
#include <SgtSim/SimNetwork.h> 

namespace Sgt
{
    class Zip;

    class TimeSeriesZip : public Heartbeat, public SimZip
    {
        public:

        static const std::string& sComponentType()
        {
            static std::string result("time_series_zip");
            return result;
        }

        public:

        /// @brief Constructor.
        ///
        /// @param dataIdxsY Indices into time series data of const Y component. Empty or of size zip->nComps().
        /// @param dataIdxsI Indices into time series data of const I component. Empty or of size zip->nComps().
        /// @param dataIdxsS Indices into time series data of const S component. Empty or of size zip->nComps().
        TimeSeriesZip(const std::string& id, const std::string& simNetworkId, const std::string& zipId,
                const std::string& seriesId, const Duration& dt,
                const arma::Col<arma::uword>& dataIdxsY,
                const arma::Col<arma::uword>& dataIdxsI,
                const arma::Col<arma::uword>& dataIdxsS);

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        double scaleFactorY() const
        {
            return scaleFactorY_;
        }

        void setScaleFactorY(double scaleFactorY)
        {
            scaleFactorY_ = scaleFactorY;
        }

        double scaleFactorI() const
        {
            return scaleFactorI_;
        }

        void setScaleFactorI(double scaleFactorI)
        {
            scaleFactorI_ = scaleFactorI;
        }

        double scaleFactorS() const
        {
            return scaleFactorS_;
        }

        void setScaleFactorS(double scaleFactorS)
        {
            scaleFactorS_ = scaleFactorS;
        }

        protected:

        virtual void initializeState() override;

        virtual void setDependencies(SimDependencies& simDeps) override;

        virtual void updateState(const TimePoint& t) override;

        private:

        arma::Col<Complex> YConst(const TimePoint& t) const;
        arma::Col<Complex> YConst() const
        {
            return YConst(lastUpdated());
        }

        arma::Col<Complex> IConst(const TimePoint& t) const;
        arma::Col<Complex> IConst() const
        {
            return IConst(lastUpdated());
        }

        arma::Col<Complex> SConst(const TimePoint& t) const;
        arma::Col<Complex> SConst() const
        {
            return SConst(lastUpdated());
        }

        private:

        std::string seriesId_;
        arma::Col<arma::uword> dataIdxsY_;
        arma::Col<arma::uword> dataIdxsI_;
        arma::Col<arma::uword> dataIdxsS_;
        double scaleFactorY_{1.0};
        double scaleFactorI_{1.0};
        double scaleFactorS_{1.0};

        ConstTimeSeriesPtr<TimeSeries<TimePoint, arma::Col<Complex>>> series_;
    };
}

#endif // TIME_SERIES_ZIP_DOT_H
