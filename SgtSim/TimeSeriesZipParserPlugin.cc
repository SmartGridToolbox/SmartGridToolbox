// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TimeSeriesZipParserPlugin.h"

#include "TimeSeriesZip.h"
#include "SimNetwork.h"
#include "Simulation.h"

#include <SgtCore/ZipParserPlugin.h>

namespace Sgt
{
    void TimeSeriesZipParserPlugin::parse(const YAML::Node& nd, Simulation& sim, const ParserBase& parser) const
    {
        assertFieldPresent(nd, "id");
        assertFieldPresent(nd, "sim_network_id");
        assertFieldPresent(nd, "time_series_id");
        assertFieldPresent(nd, "dt");

        std::string id = parser.expand<std::string>(nd["id"]);
        std::string simNetworkId = parser.expand<std::string>(nd["sim_network_id"]);

        auto ndZip = parser.expand(nd["zip"]);
        std::string zipId;
        if (!ndZip || ndZip.IsScalar())
        {
            zipId = ndZip ? ndZip.as<std::string>() : id;
        }
        else
        {
            auto& simNetwork = *sim.simComponent<SimNetwork>(simNetworkId);
            Network& network = simNetwork.network();
            auto ndZipId = parser.expand(ndZip["id"]);
            if (!ndZipId) ndZip["id"] = id;
            zipId = ndZipId.as<std::string>();
            ZipParserPlugin zpp;
            zpp.parse(ndZip, network, parser);
        }
        
        auto timeSeriesId = parser.expand<std::string>(nd["time_series_id"]);

        Duration dt = parser.expand<Duration>(nd["dt"]);

        arma::Col<arma::uword> dataIdxsY;
        YAML::Node ndDataIdxsY = nd["data_indices_Y"];
        if (ndDataIdxsY)
        {
            dataIdxsY = parser.expand<arma::Col<arma::uword>>(ndDataIdxsY);
        }

        arma::Col<arma::uword> dataIdxsI;
        YAML::Node ndDataIdxsI = nd["data_indices_I"];
        if (ndDataIdxsI)
        {
            dataIdxsI = parser.expand<arma::Col<arma::uword>>(ndDataIdxsI);
        }

        arma::Col<arma::uword> dataIdxsS;
        YAML::Node ndDataIdxsS = nd["data_indices_S"];
        if (ndDataIdxsS)
        {
            dataIdxsS = parser.expand<arma::Col<arma::uword>>(ndDataIdxsS);
        }
        
        auto tsZip = sim.newSimComponent<TimeSeriesZip>(id, simNetworkId, zipId, timeSeriesId, dt,
                dataIdxsY, dataIdxsI, dataIdxsS);

        auto ndScaleFactorY = nd["scale_factor_Y"];
        if (ndScaleFactorY)
        {
            double scaleFactorY = parser.expand<double>(ndScaleFactorY);
            tsZip->setScaleFactorY(scaleFactorY);
        }

        auto ndScaleFactorI = nd["scale_factor_I"];
        if (ndScaleFactorI)
        {
            double scaleFactorI = parser.expand<double>(ndScaleFactorI);
            tsZip->setScaleFactorI(scaleFactorI);
        }

        auto ndScaleFactorS = nd["scale_factor_S"];
        if (ndScaleFactorS)
        {
            double scaleFactorS = parser.expand<double>(ndScaleFactorS);
            tsZip->setScaleFactorS(scaleFactorS);
        }
    }
}
