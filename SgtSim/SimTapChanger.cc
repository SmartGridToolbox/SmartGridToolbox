// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SimTapChanger.h"

#include "SimNetwork.h"

using namespace std;
using namespace arma;

namespace Sgt
{
    void SimTapChangerAbc::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        auto simNetwork = simDeps.addDependency(simNetworkId_, id()).first<SimNetwork>();
        trans_ = simNetwork->network().branches()[transId_].as<Transformer>();
    }

    AutoTapChanger::AutoTapChanger(const std::string& id, const std::string& simNetworkId, const std::string& transId,
            const TcTap& tcTap) 
        : Component(id), SimTapChangerAbc(simNetworkId, transId, std::move(tcTap))
    {
        // Empty.
    }

    void AutoTapChanger::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        SimTapChangerAbc::setDependencies(simDeps);
        needsUpdate().clearTriggers();
        needsUpdate().addTrigger(trans_->buses()[tapChanger().tcInputWinding().side]->voltageUpdated());
    }

    void AutoTapChanger::initializeState()
    {
        prevTimestep_ = TimeSpecialValues::not_a_date_time;
        lastNotAbove_ = lastNotBelow_ = TimeSpecialValues::not_a_date_time;
    }

    // TODO: what if the deadband is too small and we're alternating taps?
    void AutoTapChanger::updateState(const TimePoint& t)
    {
        auto& tc = tapChanger();
        bool tryAgain = false;

        sgtLogDebug() << sComponentType() << " " << id() << " : Update : " << prevTimestep_ << " -> " << t << std::endl;
        sgtLogIndent();
        sgtLogDebug() << sComponentType() << " " << id() << " : Tap = " << tc.tap() << std::endl;

        bool isFirstStep = prevTimestep_.is_not_a_date_time();
        if (isFirstStep)
        {
            sgtLogDebug() << sComponentType() << " " << id() << " : First iteration" << std::endl;
            // Must be my first update in the simulation. Do a special iteration.
            tc.setTap(0);
            tryAgain = true;
        }
        else
        {
            if (lastNotAbove_.is_not_a_date_time()) lastNotAbove_ = t;
            if (lastNotBelow_.is_not_a_date_time()) lastNotBelow_ = t;

            sgtLogDebug() << sComponentType() << " " << id() << " : Not first iteration" << std::endl;
            bool isNewTimestep = (t != prevTimestep_);
            if (isNewTimestep)
            {
                sgtLogDebug() << sComponentType() << " " << id() << " : New timestep" << std::endl;
                iter_ = 0;
            }
            else
            {
                sgtLogDebug() << sComponentType() << " " << id() << " : Repeated timestep : " << iter_ << std::endl;
            }

            sgtLogDebug() << sComponentType() << " " << id() << " : Try update" << std::endl;
            ctrlV_ = tc.ctrlV();
            sgtLogDebug() << sComponentType() << " " << id() << " : CtrlV = " << ctrlV_ << std::endl;
            auto st = tc.state();
            if (st != TcState::BELOW)
            {
                lastNotBelow_ = t;
            }
            if (st != TcState::ABOVE)
            {
                lastNotAbove_ = t;
            }

            sgtLogDebug() << sComponentType() << " " << id() << " : State = " << static_cast<int>(st) << std::endl;

            if (st != TcState::WITHIN)
            {
                sgtLogDebug() << sComponentType() << " " << id() << " : "
                    << (st == TcState::ABOVE ? "Overvoltage" : "Undervoltage") << endl;
                if (
                        ((st == TcState::ABOVE) && ((t - lastNotAbove_) < delay_)) || 
                        ((st == TcState::BELOW) && ((t - lastNotBelow_) < delay_))
                   )
                {
                    sgtLogDebug() << "Waiting to change taps" << std::endl;
                }
                else
                {
                    tryAgain = tc.runOnce();
                }
            }
            else
            {
                sgtLogDebug() << sComponentType() << " " << id() << " : Within tolerance" << std::endl;
            }
        }
        sgtLogDebug() << sComponentType() << " " << id() << " : Tap = " << tc.tap() << std::endl;
        if (tryAgain)
        {
            sgtLogDebug() << sComponentType() << " " << id() << " : Tap was updated; try again" << std::endl;
        }
        prevTimestep_ = t;
        ++iter_;
    }

    void TimeSeriesTapChanger::setDependencies([[maybe_unused]] SimDependencies& simDeps)
    {
        SimTapChangerAbc::setDependencies(simDeps);
        series_ = simDeps.timeSeries<StepwiseTimeSeries<TimePoint, double>>(timeSeriesId_);
    }

    void TimeSeriesTapChanger::updateState(const TimePoint& t)
    {
        Heartbeat::updateState(t);
        tapChanger().setTap(static_cast<int>(std::lround(series_->value(t))));
    }
}
