// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TIME_SERIES_DC_POWER_SOURCE_DOT_H
#define TIME_SERIES_DC_POWER_SOURCE_DOT_H

#include <SgtSim/DcPowerSource.h>
#include <SgtSim/Heartbeat.h>
#include <SgtSim/TimeSeries.h>
#include <SgtSim/SimNetwork.h> 

namespace Sgt
{
    class DcPowerSource;

    class TimeSeriesDcPowerSource: public DcPowerSourceAbc, public Heartbeat
    {
        public:
        static const std::string& sComponentType()
        {
            static std::string result("time_series_dc_power_source");
            return result;
        }

        public:

        TimeSeriesDcPowerSource(const std::string& id, const std::string& inverterId, const std::string& seriesId,
                const Duration& dt, double scaleFactor = 1.0) :
            Component(id),
            DcPowerSourceAbc(inverterId),
            Heartbeat(dt),
            seriesId_(seriesId),
            scaleFactor_(scaleFactor)
        {
            // Empty.
        }

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        virtual double requestedPDc() const override
        {
            return scaleFactor_ * series_->value(lastUpdated());
        }

        protected:

        virtual void setDependencies([[maybe_unused]] SimDependencies& simDeps) override
        {
            DcPowerSourceAbc::setDependencies(simDeps);
            series_ = simDeps.timeSeries<TimeSeries<TimePoint, double>>(seriesId_);
        }

        private:

        std::string seriesId_;
        double scaleFactor_{1.0};

        ConstTimeSeriesPtr<TimeSeries<TimePoint, double>> series_{nullptr};
    };
}

#endif // TIME_SERIES_DC_POWER_SOURCE_DOT_H
