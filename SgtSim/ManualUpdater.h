// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef MANUAL_UPDATER_DOT_H
#define MANUAL_UPDATER_DOT_H

#include <SgtSim/SimComponent.h>

namespace Sgt
{
    /// @brief Utility base class for a component that updates according to a user-supplied schedule.
    class ManualUpdater : virtual public SimComponent
    {
        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("manual_updater");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        public:

        /// @brief Constructor.
        ManualUpdater(const std::string& id) :
            Component(id)
        {
            // Empty.
        }
        
        /// @brief Constructor for use with derived classes. 
        ManualUpdater()
        {
            // Empty.
        }

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        // virtual json toJson() const override; TODO

        /// @}

        /// @name SimComponent virtual overridden member functions:
        /// @{

        public:

        virtual TimePoint validUntil() const override
        {
            return nextUpdate_ != updates_.cend() ? *nextUpdate_ : TimeSpecialValues::pos_infin;;
        }

        protected:

        virtual void initializeState() override
        {
            nextUpdate_ = updates_.begin();
        }

        virtual void updateState(const TimePoint& t) override
        {
            while (*nextUpdate_ != *updates_.cend() && *nextUpdate_ <= t)
            {
                ++nextUpdate_;
            }
        }

        /// @}

        /// @name ManualUpdater specific member functions:
        /// @{

        public:

        const std::set<TimePoint>& updates() const
        {
            return updates_;
        }
        
        std::set<TimePoint>& updates()
        {
            return updates_;
        }

        void addSlave(const SimComponentPtr<>& slave)
        {
            slave->needsUpdate().addTrigger(didUpdate());
        }

        /// @}

        private:

        std::set<TimePoint> updates_{};

        std::set<TimePoint>::const_iterator nextUpdate_{};
    };
}

#endif // MANUAL_UPDATER_DOT_H
