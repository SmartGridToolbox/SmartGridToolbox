# SmartGridToolbox
[![pipeline status](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/badges/master/pipeline.svg)](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/commits/master)

[SmartGridToolbox](http://smartgridtoolbox.gitlab.io/SmartGridToolbox) is a C++ smart grid simulation library. It is designed to provide an extensible and flexible starting point for developing a wide variety of smart grid simulations and other applications.

## Installing SmartGridToolbox
These instructions assume a reasonably up to date version of Linux or MacOS - for example, Ubuntu 18 or greater in the case of Linux or Sierra or greater for MacOS. Mileage may vary for older versions.

The instructions will get you started with a basic installation of SmartGridToolbox, without extras or extensions.

### 1. Install dependencies 
#### Linux (e.g. Ubuntu >= 20):
```bash
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install -y build-essential
sudo apt-get install -y git 
sudo apt-get install -y meson # For Ubuntu >= v. 20, see below for older versions. 
sudo apt-get install -y pkg-config 

sudo apt-get install -y libarmadillo-dev # For Ubuntu >= v. 20, see below for older versions.
sudo apt-get install -y libboost-all-dev
sudo apt-get install -y libsuitesparse-dev
sudo apt-get install -y libyaml-cpp-dev # For Ubuntu >= v. 20, see below for older versions.
```

#### Older versions of Linux (e.g. Ubuntu 18):
For older versions of Linux, including Ubuntu 18 bionic, the packages for meson, armadillo and yaml-cpp are old and will cause build problems. The following should therefore be used instead:
```bash
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install -y build-essential
sudo apt-get install -y git 
sudo apt-get install -y pkg-config 
sudo apt-get install -y cmake

sudo apt-get install -y libboost-all-dev
sudo apt-get install -y libsuitesparse-dev

sudo apt-get install python3-pip # Only needed if not already installed.
pip3 install --user meson # And make sure $HOME/.local/bin is in your path.
```

Now, in a suitable directory, clone, build and install the yaml-cpp and armadillo libraries:
```bash
git clone https://gitlab.com/conradsnicta/armadillo-code.git
cd armadillo-code
mkdir build
cd build
cmake ..
make -j4 && sudo make install
cd ../..

git clone https://github.com/jbeder/yaml-cpp.git
cd yaml-cpp
mkdir build
cd build
cmake ..
make -j4 && sudo make install
cd ../..
```

#### MacOS:
For MacOS, we assume you've installed XCode and/or the Command Line Tools. The [Homebrew](https://brew.sh) package manager should also be installed - if it isn't, install it using the instructions in the link.
```bash
brew update
brew upgrade

brew install meson
brew install pkg-config

brew install armadillo
brew install boost
brew install suitesparse
brew install yaml-cpp
```

### 2. Obtain the SmartGridToolbox source
```bash
git clone --recurse-submodules https://gitlab.com/SmartGridToolbox/SmartGridToolbox.git # use -b dev for dev branch.
```

### 3. Build and install SmartGridToolbox
```bash
cd SmartGridToolbox
meson setup -D build_sgt_sim=true build # Use build_sgt_sim=false to omit optional SgtSim library
cd build
ninja -j4
sudo ninja install # You may be able to omit sudo, try without if in doubt. 
cd ..
```

## Using SmartGridToolbox in your C++ code 
SmartGridToolbox is bundled as two libraries: `SgtCore`, and optional `SgtSim`. To use SmartGridToolbox, your program should include the headers from these libraries. Most headers can be included with the two following `#include` statements:
```c++
#include <SgtCore.h>
#include <SgtSim.h> // Optional - to use SgtSim additional functionality
```

If you wish to include individual headers, you can do the following, e.g.:
```c++
#include <SgtCore/Common.h>
#include <SgtSim/Simulation.h>
```

You will need to link to the correct libraries. If you wish to build using meson (like SmartGridToolbox), have a look at any of the `tutorials/*/meson.build` files. Depending on whether you are interested in simple network solving or discrete event simulation, you could copy and modify e.g. `tutorials/network` or `tutorials/simulation` to use as a template for your project; the first links to SgtCore only, while the second links to SgtCore and SgtSim.

Otherwise, a good way to compile without having to manually link to all referenced libraries is to use `pkg-config`, like this, e.g.:
```bash
c++ -std=c++14 -o simulation simulation.cc $(pkg-config --cflags --libs SgtSim SgtCore)
```
For the record, the following straight compile command also works:
```
c++ -std=c++14 -o simulation simulation.cc -lSgtSim -lSgtCore -larmadillo -lyaml-cpp -lklu -lboost_date_time
```

#### Setting your LD\_LIBRARY\_PATH in Linux
In Linux, you may need to set the `LD_LIBRARY_PATH` environment variable to include `/usr/local/lib`, to avoid dynamic linking errors when running your code:
```
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib
```
You can add this line to your `.bash_profile` or equivalent to make this change permanent.

## Python bindings
SmartGridToolbox includes python bindings for `SgtCore` (not `SgtSim`), and can be used as as a python power flow library.

### Installing the python bindings
To install the python bindings, first install SmartGridToolbox, as shown above. Then, from the top level SmartGridToolbox directory (perhaps using a fresh or appropriate venv or virtualenv, you can ignore if you don't know what this is), do the following:
```
pip3 install wheel cython
pip3 install ./python_bindings
```

### Using the python bindings
SmartGridToolbox is imported as `import sgt`. Documentation is sparse, and the primary documentation is currently through pydoc. Start an interactive session, `import sgt`, and then `help(sgt)` should get you started. You can also take a look at the [test script](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/-/blob/master/python_bindings/test/test_sgt.py) to get started.

## Learning more
The [SmartGridToolbox homepage](http://smartgridtoolbox.gitlab.io/SmartGridToolbox) contains more resources to help you get started with SmartGridToolbox. In particular, The [Doxygen pages](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/index.html) provide a detailed code reference. The [modules](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/modules.html) page gives an organised overview of the code. The [YAML specifications](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/group___yaml_spec.html) for SmartGridToolbox configuration files are also documented.

The [tutorials](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/#tutorials) will help you to learn more about various aspects of SmartGridToolbox.

The [PvDemo demo](examples/PvDemo) is a more complex application of SmartGridToolbox involving optimising network voltage using PV inverters.

The [BuildingControllerDemo demo](examples/BuildingControllerDemo) shows how complex optimising loads, in this case an intelligently controlled building with PV, battery and HVAC, can be built using SmartGridToolbox and the Gurobi optimisation library.

## License
SmartGridToolbox is licensed under the Apache 2.0 License. Please see the [LICENSE](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/LICENSE) file for details. Also see the [NOTICE](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/NOTICE) file (which must be redistributed with derivative works) for details about how to cite useage.
