#!/usr/bin/env python3

import sgt # Import the sgt (SGT Python) library
import yaml # Needed for SGT configuration files.

import numpy as np # not needed but handy for matrices & vectors 

def print_ind(x, nind):
    '''
    Print x with an indent of nind spaces at each newline
    '''
    ind = ' ' * nind
    print(ind + str(x).replace('\n', '\n' + ind))

def print_bus(x):
    '''
    Print information about bus x.
    '''
    print_ind(x.id() + ':', 4)
    print_ind(
        'type     : {}\n'
        'bus_type : {}\n'
        'phases   : {}\n'
        'v_nom    : {}\n'
        'v_base   : {}\n'
        'v        : {}'
        .format(x.component_type(), x.bus_type(), x.phases(),
                x.v_nom(), x.v_base(), x.v()), 8)
    print_ind('Branches0:', 8)
    for y in x.branches0():
        print_ind(y.id(), 12)
    print_ind('Branches1:', 8)
    for y in x.branches1():
        print_ind(y.id(), 12)
    print_ind('Gens:', 8)
    for y in x.gens():
        print_ind(y.id(), 12)
    print_ind('Zips:', 8)
    for y in x.zips():
        print_ind(y.id(), 12)

def print_branch(x):
    '''
    Print information about branch x.
    '''
    print_ind(x.id() + ':', 4)
    print_ind(
        'type : {}\n'
        'y    :'
        .format(x.component_type()), 8)
    print_ind(np.matrix(x.y()), 12)

def print_gen(x):
    '''
    Print information about gen x.
    '''
    print_ind(x.id() + ':', 4)
    print_ind(
        'type   : {}'
        .format(x.component_type()), 8)

def print_zip(x):
    '''
    Print information about zip x.
    '''
    print_ind(x.id() + ':', 4)
    print_ind(
        'type       : {}\n'
        'connection : {}\n'
        's          : {}\n'
        .format(x.component_type(), x.connection(), x.s()), 8)

# Phases
print('-'*80 + '\nPhases\n')
print(sgt.Phase.A)
print(sgt.Phase.A == sgt.Phase.B)
print(sgt.Phase.A == sgt.Phase.A)

# Parse in a YAML network file
with open('test_network.yaml') as f:
    netw_yaml = yaml.load(f, Loader=yaml.FullLoader)

print('\n' + '-'*80 + '\nYAML\n')
print(netw_yaml)

# Create an empty sgt.Network object
nw = sgt.Network()

# Populate it with the parsed YAML
nw.parse_yaml(netw_yaml)

# Print a JSON representation
print('\n' + '-'*80 + '\nNetwork\n')
print(nw)

# Solve the power flow equations
nw.solve_power_flow()

# Loop though buses
print('\n' + '-'*80 + '\nBuses\n')
for x in nw.buses():
    print_bus(x)

# Loop though branches
print('\n' + '-'*80 + '\nBranches\n')
for x in nw.branches():
    print_branch(x)

# Loop though gens
print('\n' + '-'*80 + '\nGens\n')
for x in nw.gens():
    print_gen(x)

# Loop though zips
print('\n' + '-'*80 + '\nZips\n')
for x in nw.zips():
    print_zip(x)
    sc = x.s() # The zip's power

    # We're going to set the power to something different:
    for i in range(len(sc)):
        sc[i] = complex(0, 0.2)
    x.set_s_const(sc)

    print('    ->')
    print_zip(x)

# Having changed the network, let's re-solve
nw.solve_power_flow()

# Note some voltages will have changed
print('\n' + '-'*80 + '\nBuses (B)\n')
for x in nw.buses():
    print_bus(x)

# Create a new bus in the network
b = sgt.Bus(nw, 'my_new_bus', [sgt.Phase.A, sgt.Phase.C, sgt.Phase.B], 
            [complex(1, 2), complex(3, 4), complex(5, 6)], 11)
b.set_bus_type(sgt.BusType.SL)
b.set_is_in_service(False)

# Create a new gen in the network
g = sgt.Gen(nw, 'my_new_gen', 'my_new_bus', [sgt.Phase.A])
g.set_is_in_service(False)
g.set_p_max(4)

# Create a new zip in the network
z = sgt.Zip(
    nw, 'my_new_zip', 'my_new_bus', [sgt.Phase.A, sgt.Phase.B, sgt.Phase.C],
    sgt.Zip.delta_connection(3))
z.set_s_const([complex(1,2), complex(1,2), complex(1,2)])
print_zip(z)

# Create a new generic branch in the network

def y_line(z, z0, n_ph):
    if n_ph == 3:
        z_ph = sgt.approx_phase_impedance_matrix(z, z0) # Adding this to python bindings
    elif n_ph == 2:
        # This is how sgt-e-json does things, to emulate SINCAL
        a = (2 * z + z0) / 3
        b = (z0 - z) / 3
        z_ph = [[a, b], [b, a]];
    elif n_ph == 1:
        z_ph = [[z]]
    else:
        raise RuntimeError('n_ph must be 3, 2 or 1.')

    return sgt.z_line_to_y_node(z_ph) # Adding this to python bindings

b = sgt.GenericBranch(
    nw, 'my_new_branch', 'bus_1', [sgt.Phase.A, sgt.Phase.B, sgt.Phase.C],
    'bus_2', [sgt.Phase.B, sgt.Phase.A, sgt.Phase.C])

b.set_in_service_y(y_line(0.1+0.03j, 0.12+0.02j, 3))
print_branch(b)

t = sgt.Transformer(
    nw, 'my_new_trans', 'bus_1', [sgt.Phase.A, sgt.Phase.B, sgt.Phase.C],
    'bus_2', [sgt.Phase.A, sgt.Phase.B, sgt.Phase.C], 'dyn11', 3, False, True, 99.0, 1e-6, 0.0, 0, 0, 0, 0, 0)
print_branch(t)

# Reprint network to see new components:
for x in nw.buses():
    print(x.id(), x.component_type())
for x in nw.branches():
    print(x.id(), x.component_type())
    print(x.downcast(sgt.Branch) is not None)
    print(x.downcast(sgt.GenericBranch) is not None)
    print(x.downcast(sgt.Transformer) is not None)
    tx = x.downcast(sgt.Transformer)
    if tx is not None:
        print (f'    Transformer: turns ratio = {tx.turns_ratio()}.')
for x in nw.gens():
    print(x.id(), x.component_type())
for x in nw.zips():
    print(x.id(), x.component_type())
