# SmartGridToolbox Python Bindings

## Building
In what follows, you may need sudo. Also `pip[3]` -> `pip` or `pip3`, depending on your setup.

```bash
pip[3] install .
```

## Test script
The [test script](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/-/blob/master/extras/python_bindings/test/test_sgt.py) gives an overview of how the python bindings can be used.

## UTSL
Check out the main [source code](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/-/blob/master/extras/python_bindings/sgt/sgt.pyx) to get a comprehensive view of the python classes and functions available in the python bindings. 
