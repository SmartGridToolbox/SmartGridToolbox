from setuptools import setup, Extension
from setuptools.command.install import install
import subprocess

from Cython.Build import cythonize

VERSION = '10.2.0'

# ------------------------------------------------------------------------------
# Find C++ compile options.

# Helper function to run pkg-config and return the config.
def pkgconfig(*packages, **kw):
    config = kw.setdefault('config', {})
    optional_args = kw.setdefault('optional', '')

    flag_map = {
        'include_dirs': ['--cflags-only-I', 2],
        'library_dirs': ['--libs-only-L', 2],
        'libraries': ['--libs-only-l', 2],
        'extra_compile_args': ['--cflags-only-other', 0],
        'extra_link_args': ['--libs-only-other', 0],
    }
    for package in packages:
        for key, (pkg_option, n) in flag_map.items():
            items = subprocess.check_output(['pkg-config', optional_args, pkg_option, package]).decode('utf8').split()
            config.setdefault(key, []).extend([i[n:] for i in items])

    return config

conf = pkgconfig('SgtCore')
conf['extra_compile_args'].append('-std=c++14')
conf.setdefault('language', 'c++')

# ------------------------------------------------------------------------------
# Build extension, using cython.

exts = cythonize(
    [
        Extension(
            '*',
            ['sgt/sgt.pyx'],
            **conf
        ),
    ],
    include_path=['sgt']
)

# ------------------------------------------------------------------------------
# Write version.py

with open('sgt/version.py', 'w') as f:
    f.write("__version__ = '{__version__}'".format(__version__=VERSION))

# ------------------------------------------------------------------------------
# Run setup.

setup(
    ext_modules=exts,
    packages=['sgt'],
    package_data={'sgt': ['__init__.pxd', 'sgt.pxd', 'cc.pxd', 'helper.h']},
    install_requires=['cython', 'numpy', 'pyyaml', 'wheel'], # Need cython and wheel to properly install downstream
    name='sgt',
    version=VERSION,
    description='SmartGridToolbox python bindings',
    long_description='Python bindings for the SmartGridToolbox C++ smart grid library',
    author='Dan Gordon',
    author_email='dan.gordon@anu.edu.au',
    url='https://gitlab.com/SmartGridToolbox/SmartGridToolbox'
)
