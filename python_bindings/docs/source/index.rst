.. sgt documentation master file, created by
   sphinx-quickstart on Thu Feb 27 09:32:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SmartGridToolbox Python Bindings
================================
A moderately large subset of SmartGridToolbox's SgtCore library is made available in the *sgt* python module, documented here.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: sgt.sgt
   :members:
   :undoc-members:
   :show-inheritance:
