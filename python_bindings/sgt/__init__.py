"""
sgt: Python bindings for SmartGridToolbox.

The sgt package consists of a single sgt module.

The following classes from sgt.sgt are imported to package level: 

    Network, Component, Phase, LogLevel, BusType, Bus, Branch, Zip, Gen,
    GenericBranch, CommonBranch, OverheadLine, UndergroundLine,
    UndergroundLineStrandedShield, UndergroundLineTapeShield,
    Transformer

The following functions from sgt.sgt are imported to package level:

    z_line_to_y_node, approx_phase_impedance_matrix

You can type e.g. help(sgt.Bus) or help(sgt.sgt.Bus) to access documentation on
individual members.

You can type help(sgt.sgt) to see comprehensive documentation for the sgt.sgt
module.
"""

from .sgt import Phase, LogLevel, BusType, z_line_to_y_node, approx_phase_impedance_matrix, Component, Bus, Branch, \
        GenericBranch, CommonBranch, OverheadLine, UndergroundLine, UndergroundLineStrandedShield, \
        UndergroundLineTapeShield, Transformer, Gen, Zip, Network, set_message_log_level, set_warning_log_level, \
        set_debug_log_level, run_tap_changers, Sensitivity, sensitivity
from .version import __version__ as __version__

def get_include():
    import sgt
    import os.path
    return os.path.dirname(sgt.__file__)
