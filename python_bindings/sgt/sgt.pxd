# cython: language_level=3, boundscheck=False, c_string_type=unicode, c_string_encoding=utf8, infer_types=True

cimport sgt.cc as cc

cdef object topy_jsn(const cc.json& sgt_json)

cdef cc.json tocc_jsn(jsn)

cdef object topy_cplx(const cc.Complex& x)

cdef cc.Complex tocc_cplx(x)

cdef object topy_rcol(const cc.Col[double]& x)

cdef cc.Col[double] tocc_rcol(x)

cdef object topy_ccol(const cc.Col[cc.Complex]& x)

cdef cc.Col[cc.Complex] tocc_ccol(x)

cdef object topy_rmat(const cc.Mat[double]& x)

cdef cc.Mat[double] tocc_rmat(x)

cdef object topy_umat(const cc.Mat[cc.uword]& x)

cdef cc.Mat[cc.uword] tocc_umat(x)

cdef object topy_cmat(const cc.Mat[cc.Complex]& x)

cdef cc.Mat[cc.Complex] tocc_cmat(x)

cdef class Phase:
    cdef cc.Phase _phase
    
    @staticmethod
    cdef Phase wrap(const cc.Phase& phase)

cdef cc.Phases cc_phases(phases)

cdef class LogLevel:
    cdef cc.LogLevel _log_level

    @staticmethod
    cdef LogLevel wrap(const cc.LogLevel& log_level)

cdef class BusType:
    cdef cc.BusType _bus_type

    @staticmethod
    cdef BusType wrap(const cc.BusType& bus_type)

cdef class Component:
    cdef cc.Component* comp(self)

cdef class Bus(Component):
    cdef cc.ComponentPtr[cc.Bus, cc.Bus] _comp

    @staticmethod
    cdef Bus wrap(const cc.ComponentPtr[cc.Bus, cc.Bus]& sgt_bus)

    cdef cc.Component* comp(self)
    
    cdef cc.Bus* bus(self)

cdef class Branch(Component):
    cdef cc.ComponentPtr[cc.BranchAbc, cc.BranchAbc] _comp

    @staticmethod
    cdef Branch wrap(cc.ComponentPtr[cc.BranchAbc, cc.BranchAbc]& sgt_branch)
    
    cdef cc.Component* comp(self)
    
    cdef cc.BranchAbc* branch(self)
    
    cdef unsigned nPhases0(self)
    
    cdef unsigned nPhases1(self)

cdef class Gen(Component):
    cdef cc.ComponentPtr[cc.Gen, cc.Gen] _comp

    @staticmethod
    cdef Gen wrap(cc.ComponentPtr[cc.Gen, cc.Gen]& sgt_gen)
    
    cdef cc.Component* comp(self)
    
    cdef cc.Gen* gen(self)

cdef class Zip(Component):
    cdef cc.ComponentPtr[cc.Zip, cc.Zip] _comp

    @staticmethod
    cdef Zip wrap(cc.ComponentPtr[cc.Zip, cc.Zip]& sgt_zip)
    
    cdef cc.Component* comp(self)
    
    cdef cc.Zip* zip_(self)

cdef class Network:
    cdef cc.Network* _netw

cdef class GenericBranch(Branch):
    @staticmethod
    cdef GenericBranch wrap(cc.ComponentPtr[cc.BranchAbc, cc.GenericBranch]& sgt_branch)

    cdef cc.GenericBranch* generic_branch(self)

cdef class CommonBranch(Branch):
    @staticmethod
    cdef CommonBranch wrap(cc.ComponentPtr[cc.BranchAbc, cc.CommonBranch]& sgt_branch)

    cdef cc.CommonBranch* common_branch(self)

cdef class OverheadLine(Branch):
    @staticmethod
    cdef OverheadLine wrap(cc.ComponentPtr[cc.BranchAbc, cc.OverheadLine]& sgt_branch)

    cdef cc.OverheadLine* overhead_line(self)

cdef class UndergroundLine(Branch):
    @staticmethod
    cdef UndergroundLine wrap(cc.ComponentPtr[cc.BranchAbc, cc.UndergroundLine]& sgt_branch)

    cdef cc.UndergroundLine* underground_line(self)

cdef class Transformer(Branch):
    @staticmethod
    cdef Transformer wrap(cc.ComponentPtr[cc.BranchAbc, cc.Transformer]& sgt_branch)
    
    cdef cc.Transformer* transformer(self)
