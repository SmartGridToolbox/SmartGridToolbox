# cython: language_level=3, boundscheck=False, c_string_type=unicode, c_string_encoding=utf8, infer_types=True, embedsignature=True

from dataclasses import dataclass
from enum import Enum
import json
import yaml
from libcpp cimport bool
from libcpp.list cimport list as cclist
from libcpp.map cimport map
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr, make_shared, static_pointer_cast, dynamic_pointer_cast

cimport cc

# Utilities

# Conversion functions. Note overloading is busted.

cdef object topy_jsn(const cc.json& sgt_json):
    # Ugly: goes through string, but no good alternative.
    return json.loads(sgt_json.dump(0))

cdef cc.json tocc_jsn(jsn):
    # Ugly: goes through string, but no good alternative.
    return cc.json.parse(json.dumps(jsn))

cdef object topy_cplx(const cc.Complex& x):
    return complex(x.real(), x.imag())

cdef cc.Complex tocc_cplx(x):
    return cc.Complex(x.real, x.imag)

cdef object topy_rcol(const cc.Col[double]& x):
    return [x(i) for i in range(x.size())]

cdef cc.Col[double] tocc_rcol(x):
    cdef cc.Col[double] retval = cc.Col[double](len(x))
    for i in range(len(x)):
        cc.assign[double](retval(i), x[i])
    return retval

cdef object topy_ccol(const cc.Col[cc.Complex]& x):
    return [topy_cplx(x(i)) for i in range(x.size())]

cdef cc.Col[cc.Complex] tocc_ccol(x):
    cdef cc.Col[cc.Complex] retval = cc.Col[cc.Complex](len(x))
    for i in range(len(x)):
        cc.assign[cc.Complex](retval(i), tocc_cplx(x[i]))
    return retval

cdef object topy_icol(const cc.Col[int]& x):
    return [x(i) for i in range(x.size())]

cdef cc.Col[int] tocc_icol(x):
    cdef cc.Col[int] retval = cc.Col[int](len(x))
    for i in range(len(x)):
        cc.assign[int](retval(i), x[i])
    return retval

cdef object topy_ucol(const cc.Col[cc.uword]& x):
    return [x(i) for i in range(x.size())]

ctypedef unsigned int uint

cdef cc.Col[uint] tocc_ucol(x):
    cdef cc.Col[uint] retval = cc.Col[uint](len(x))
    for i in range(len(x)):
        cc.assign[uint](retval(i), x[i])
    return retval

cdef object topy_rmat(const cc.Mat[double]& x):
    m = []
    for i in range(x.n_rows):
        m.append([])
        for j in range(x.n_cols):
            m[i].append(x(i, j))
    return m

cdef cc.Mat[double] tocc_rmat(x):
    cdef cc.Mat[double] retval = cc.Mat[double](len(x), len(x[0]))
    for i in range(len(x)):
        for j in range(len(x[i])):
            cc.assign[double](retval(i, j), x[i][j])
    return retval

cdef object topy_umat(const cc.Mat[cc.uword]& x):
    m = []
    for i in range(x.n_rows):
        m.append([])
        for j in range(x.n_cols):
            m[i].append(x(i, j))
    return m

cdef cc.Mat[cc.uword] tocc_umat(x):
    cdef cc.Mat[cc.uword] retval = cc.Mat[cc.uword](len(x), len(x[0]))
    for i in range(len(x)):
        for j in range(len(x[i])):
            cc.assign[cc.uword](retval(i, j), x[i][j])
    return retval

cdef object topy_cmat(const cc.Mat[cc.Complex]& x):
    m = []
    for i in range(x.n_rows):
        m.append([])
        for j in range(x.n_cols):
            m[i].append(topy_cplx(x(i, j)))
    return m

cdef cc.Mat[cc.Complex] tocc_cmat(x):
    cdef cc.Mat[cc.Complex] retval = cc.Mat[cc.Complex](len(x), len(x[0]))
    for i in range(len(x)):
        for j in range(len(x[i])):
            cc.assign[cc.Complex](retval(i, j), tocc_cplx(x[i][j]))
    return retval

# Wrap Phase
cdef class Phase:
    """Enum to represent electricial phase."""

    @staticmethod
    cdef Phase wrap(const cc.Phase& phase):
        cdef Phase result = Phase.__new__(Phase)
        result._phase = phase
        return result

    A = Phase.wrap(cc.PH_A)
    B = Phase.wrap(cc.PH_B)
    C = Phase.wrap(cc.PH_C)
    G = Phase.wrap(cc.PH_G)
    N = Phase.wrap(cc.PH_N)
    BAL = Phase.wrap(cc.PH_BAL)
    BAD = Phase.wrap(cc.PH_BAD)

    def __repr__(self):
        return cc.to_string(self._phase)
        
cdef cc.Phases cc_phases(phases):
    cdef vector[cc.Phase] v
    cdef Phase p

    v.reserve(len(phases))
    for p in phases:
        v.push_back(p._phase)

    return cc.Phases(v)

cdef object py_phases(cc.Phases cc_phases):
    return [Phase.wrap(x) for x in cc_phases]

# Wrap LogLevel
cdef class LogLevel:
    """Enum to represent log levels."""

    @staticmethod
    cdef LogLevel wrap(const cc.LogLevel& log_level):
        result = LogLevel()
        result._log_level = log_level
        return result

    NONE = LogLevel.wrap(cc.LOG_LEVEL_NONE)
    NORMAL = LogLevel.wrap(cc.LOG_LEVEL_NORMAL)
    VERBOSE = LogLevel.wrap(cc.LOG_LEVEL_VERBOSE)

def set_message_log_level(LogLevel level):
    cc.setMessageLogLevel(level._log_level)

def set_warning_log_level(LogLevel level):
    cc.setWarningLogLevel(level._log_level)

def set_error_log_level(LogLevel level):
    cc.setErrorLogLevel(level._log_level)

def set_debug_log_level(LogLevel level):
    cc.setDebugLogLevel(level._log_level)

# Wrap BusType
cdef class BusType:
    """Enum to represent the power flow bus type."""

    @staticmethod
    cdef BusType wrap(const cc.BusType& bus_type):
        result = BusType()
        result._bus_type = bus_type
        return result

    SL = BusType.wrap(cc.BT_SL)
    PQ = BusType.wrap(cc.BT_PQ)
    PV = BusType.wrap(cc.BT_PV)
    NA = BusType.wrap(cc.BT_NA)
    BAD = BusType.wrap(cc.BT_BAD)

    def __repr__(self):
        return cc.to_string(self._bus_type)
    
# Wrap ZLine2YNode
def z_line_to_y_node(zline):
    """Convert the phase impedance matrix of a line to a nodal (bus) admittance matrix."""
    return topy_cmat(cc.ZLine2YNode(tocc_cmat(zline)))

# Wrap approxPhaseImpedanceMatrix
def approx_phase_impedance_matrix(zplus, z0, n_phase=3):
    """Calculate the phase impedance matrix of a line, based on z+ and z0 and the number of wires."""
    cdef cc.Complex cc_zplus = tocc_cplx(zplus) 
    cdef cc.Complex cc_z0 = tocc_cplx(z0) 
    cdef cc.Mat[cc.Complex] cc_mat = cc.approxPhaseImpedanceMatrix(cc_zplus, cc_z0, n_phase)
    return topy_cmat(cc_mat)

# Wrap Component
cdef class Component:
    """Base class for SmartGridToolbox components."""

    cdef cc.Component* comp(self):
        """Return a pointer to the wrapped C++ Component."""
        return NULL
    
    def __repr__(self):
        """Component representation in the form of a JSON string."""
        return json.dumps(self.to_json(), indent=4)

    def id(self):
        """Unique component ID."""
        return self.comp().id()
    
    def component_type(self):
        """Component type string, e.g. 'bus'/'generic_branch' etc."""
        return self.comp().componentType()
    
    def to_json(self):
        """Convert to python dict via JSON."""
        return topy_jsn(self.comp().toJson())
    
    def user_data(self):
        """User supplied JSON for component."""
        return topy_jsn(self.comp().userData())
    
    def set_user_data(self, user_data):
        """Set user supplied JSON for component."""
        return self.comp().setUserData(tocc_jsn(user_data))

# Wrap Bus
cdef class Bus(Component):
    """A bus in a network."""

    @staticmethod
    cdef Bus wrap(const cc.ComponentPtr[cc.Bus, cc.Bus]& sgt_bus):
        """Wrap an existing C++ Bus in python."""
        cdef Bus result = Bus.__new__(Bus)
        result._comp = sgt_bus
        return result

    @staticmethod
    def from_bus(bus: Bus):
        return bus
    
    def __init__(self, Network netw, id_, phases, v_nom, v_base):
        """Create a new bus."""
        self._comp = netw._netw.addBus(make_shared[cc.Bus](
            <string>id_, cc_phases(phases), tocc_ccol(v_nom), <double>v_base))
    
    cdef cc.Component* comp(self):
        """Return a pointer to the wrapped C++ Component."""
        return cc.raw(self._comp)
    
    cdef cc.Bus* bus(self):
        """Return a pointer to the wrapped C++ Bus."""
        return cc.raw(self._comp)

    def downcast(self, to_type):
        try:
            return to_type.from_bus(self)
        except Exception:
            return None

    def phases(self):
        """The bus's phases."""
        cdef vector[cc.Phase] cc_phases = cc.phases(self.bus().phases())
        return [Phase.wrap(x) for x in cc_phases]
    
    def v_nom(self):
        """The bus's nominal phase to ground voltage, kV, as a list of complex phase voltages."""
        return topy_ccol(self.bus().VNom())
    
    def v_base(self):
        """The bus's phase to ground base voltage, kV, e.g. 0.240."""
        return self.bus().VBase()
    
    def bus_type(self):
        """The bus's power flow type."""
        return BusType.wrap(self.bus().type())
    
    def set_bus_type(self, BusType bus_type):
        """The bus's power flow type."""
        self.bus().setType(bus_type._bus_type)

    def v_mag_setpoint(self):
        """The bus's voltage magnitude setpoint, one real number per phase."""
        return topy_rcol(self.bus().VMagSetpoint())
    
    def set_v_mag_setpoint(self, v_mag_setpoint):
        """The bus's voltage magnitude setpoint, one real number per phase."""
        self.bus().setVMagSetpoint(tocc_rcol(v_mag_setpoint))

    def v_ang_setpoint(self):
        """The bus's voltage angle setpoint, one real number per phase."""
        return topy_rcol(self.bus().VAngSetpoint())
    
    def set_v_ang_setpoint(self, v_ang_setpoint):
        """The bus's voltage angle setpoint, one real number per phase."""
        self.bus().setVMagSetpoint(tocc_rcol(v_ang_setpoint))

    def v_setpoint(self):
        """The bus's complex voltage setpoint, one real number per phase."""
        return topy_ccol(self.bus().VSetpoint())
    
    def set_v_setpoint(self, v_setpoint):
        """The bus's complex voltage setpoint, one real number per phase."""
        self.bus().setVSetpoint(tocc_ccol(v_setpoint))

    def apply_v_setpoints(self):
        """The bus's voltage according to its current setpoints."""
        self.bus().applyVSetpoints()

    def v_mag_min(self):
        """The bus's minimum allowable voltage."""
        return self.bus().VMagMin()
    
    def set_v_mag_min(self, v_mag_min):
        """The bus's minimum allowable voltage."""
        self.bus().setVMagMin(v_mag_min)
    
    def v_mag_min_violated(self):
        """Is the min voltage limit violated?"""
        return self.bus().VMagMinViolated()
    
    def v_mag_max(self):
        """The bus's maximum allowable voltage."""
        return self.bus().VMagMax()
    
    def set_v_mag_max(self, v_mag_max):
        """The bus's maximum allowable voltage."""
        self.bus().setVMagMax(v_mag_max)
    
    def v_mag_max_violated(self):
        """Is the max voltage limit violated?"""
        return self.bus().VMagMaxViolated()

    def is_in_service(self):
        """Is the bus currently in service?"""
        return self.bus().isInService()
    
    def set_is_in_service(self, is_in_service):
        """Is the bus currently in service?"""
        self.bus().setIsInService(is_in_service)
    
    def is_supplied(self):
        """Is the bus currently supplied?"""
        return self.bus().isSupplied()
    
    def set_is_supplied(self, is_supplied):
        """Is the bus currently supplied?"""
        self.bus().setIsSupplied(is_supplied)

    def v(self):
        """The bus's voltage."""
        return topy_ccol(self.bus().V())
    
    def set_v(self, v):
        """The bus's voltage."""
        self.bus().setV(tocc_ccol(v))
    
    def s_gen_unserved(self):
        """The bus's unserved generation."""
        return topy_ccol(self.bus().SGenUnserved())
    
    def set_s_gen_unserved(self, s_gen_unserved):
        """The bus's unserved generation."""
        self.bus().setSGenUnserved(tocc_ccol(s_gen_unserved))
    
    def s_zip_unserved(self):
        """The bus's unserved load."""
        return topy_cmat(self.bus().SZipUnserved())
    
    def set_s_zip_unserved(self, s_zip_unserved):
        """The bus's unserved load."""
        self.bus().setSZipUnserved(tocc_cmat(s_zip_unserved))

    def island_idx(self):
        """The index of the electrical island to which the bus currently belongs."""
        return self.bus().islandIdx()
    
    def set_island_idx(self, island_idx):
        """The index of the electrical island to which the bus currently belongs."""
        self.bus().setIslandIdx(island_idx)

    def coords(self):
        """The map coordinates or latlong of the bus."""
        return topy_rcol(self.bus().coords())
    
    def setCoords(self, coords):
        """The map coordinates or latlong of the bus."""
        self.bus().setCoords(tocc_rcol(coords))

    def branches0(self):
        """The branches whose first terminal (0) connects to the bus."""
        return [Branch.wrap(x) for x in self.bus().branches0()]
    
    def branches1(self):
        """The branches whose second terminal (1) connects to the bus."""
        return [Branch.wrap(x) for x in self.bus().branches1()]
    
    def gens(self):
        """The generators connected to the bus."""
        return [Gen.wrap(x) for x in self.bus().gens()]

    def n_in_service_gens(self):
        """The number of in-service generators connected to the bus."""
        return self.bus().nInServiceGens()
    
    def s_gen_requested(self):
        """The requested generation at the bus."""
        return topy_ccol(self.bus().SGenRequested())
    
    def s_gen(self):
        """The actual generation at the bus, per phase."""
        return topy_ccol(self.bus().SGen())
    
    def s_gen_tot(self):
        """The actual generation at the bus, sum over phases."""
        return topy_cplx(self.bus().SGenTot())
    
    def zips(self):
        """The ZIP loads connected to the bus."""
        return [Zip.wrap(x) for x in self.bus().zips()]
    
    def n_in_service_zips(self):
        """The number of in-service ZIP loads connected to the bus."""
        return self.bus().nInServiceZips()
    
    def y_const(self):
        """
        The constant Y load at the bus in the form of a matrix.

        Diagonal terms represent phase to ground connections.
        Upper triangular terms represent phase to phase connections.
        Lower triangular terms are ignored.
        """
        return topy_cmat(self.bus().YConst())
    
    def s_y_const(self):
        """Complex load power due to constant Y loads."""
        return topy_cmat(self.bus().SYConst())
    
    def i_const(self):
        """The constant I load at the bus in the form of a matrix."""
        return topy_cmat(self.bus().IConst())
    
    def s_i_const(self):
        """Complex load power due to constant I loads."""
        return topy_cmat(self.bus().SIConst())
    
    def s_const(self):
        """The constant S load at the bus in the form of a matrix."""
        return topy_cmat(self.bus().SConst())
    
    def s_zip_requested(self):
        """The requested total load power at the bus."""
        return topy_cmat(self.bus().SZipRequested())
    
    def s_zip(self):
        """The actual total load power at the bus, per phase."""
        return topy_cmat(self.bus().SZip())
    
    def s_zip_tot(self):
        """The actual total load power at the bus, sum over phases."""
        return topy_cplx(self.bus().SZipTot())
    
# Wrap BranchAbc
cdef class Branch(Component):

    @staticmethod
    cdef Branch wrap(cc.ComponentPtr[cc.BranchAbc, cc.BranchAbc]& sgt_branch):
        """Wrap an existing C++ BranchAbc in python."""
        cdef Branch result = Branch.__new__(Branch)
        result._comp = sgt_branch
        return result
    
    @staticmethod
    def from_branch(branch: Branch):
        return branch

    cdef cc.Component* comp(self):
        """Return a pointer to the wrapped C++ Component."""
        return cc.raw(self._comp)
    
    cdef cc.BranchAbc* branch(self):
        """Return a pointer to the wrapped C++ BranchAbc."""
        return cc.raw(self._comp)

    def downcast(self, to_type):
        try:
            return to_type.from_branch(self)
        except Exception:
            return None
   
    cdef unsigned nPhases0(self):
        """Number of phases at the first terminal."""
        return self.branch().nPhases0()
    
    cdef unsigned nPhases1(self):
        """Number of phases at the second terminal."""
        return self.branch().nPhases1()
    
    def is_in_service(self):
        """Is the branch currently in service?"""
        return self.branch().isInService()
    
    def set_is_in_service(self, is_in_service):
        """Is the branch currently in service?"""
        self.branch().setIsInService(is_in_service)

    def y(self):
        """The branch's nodal admittance matrix (zero if not in service)."""
        return topy_cmat(self.branch().Y())
    
    def in_service_y(self):
        """The branch's nodal admittance matrix, assuming it's in service."""
        return topy_cmat(self.branch().inServiceY())
    
    def bus0(self):
        """The bus connected to the first terminal."""
        return Bus.wrap(self.branch().bus0())
    
    def phases_0(self):
        """The bus phases of the connection at the first terminal."""
        return py_phases(self.branch().phases0())
    
    def bus1(self):
        """The bus connected to the second terminal."""
        return Bus.wrap(self.branch().bus1())
    
    def phases_1(self):
        """The bus phases of the connection at the second terminal."""
        return py_phases(self.branch().phases1())

    def v_term(self):
        """The voltage at each terminal phase, array of one array for each terminal."""
        cdef cc.array_ccol_2 a = self.branch().VTerm()
        return (topy_ccol(a[0]), topy_ccol(a[1]))
    
    def i_term(self):
        """The current flowing out of the bus into the branch, array of one array for each terminal."""
        cdef cc.array_ccol_2 a = self.branch().ITerm()
        return (topy_ccol(a[0]), topy_ccol(a[1]))
    
    def s_term(self):
        """The power flowing out of the bus into the branch, array of one array for each terminal."""
        cdef cc.array_ccol_2 a = self.branch().STerm()
        return (topy_ccol(a[0]), topy_ccol(a[1]))

    def s_mag_max(self):
        """Upper limit on the total power flowing through any individual terminal phase."""
        return self.branch().SMagMax()
    
    def set_s_mag_max(self, s_mag_max):
        """Upper limit on the total power flowing through any individual terminal phase."""
        self.branch().setSMagMax(s_mag_max)
    
    def s_mag_max_violated(self):
        """Returns true if the apparent power through any of the terminal phases violates the limit."""
        return self.branch().SMagMaxViolated()

    def i_mag_max(self):
        """Upper limit on the total current magnitude flowing through any individual terminal phase."""
        return self.branch().IMagMax()
    
    def set_i_mag_max(self, i_mag_max):
        """Upper limit on the total current magnitude flowing through any individual terminal phase."""
        self.branch().setIMagMax(i_mag_max)
    
    def i_mag_max_violated(self):
        """Returns true if the current magnitude through any of the terminal phases violates the limit."""
        return self.branch().IMagMaxViolated()

# Wrap Gen
cdef class Gen(Component):
    """Generator."""

    @staticmethod
    cdef Gen wrap(cc.ComponentPtr[cc.Gen, cc.Gen]& sgt_gen):
        """Wrap an existing C++ Gen in python."""
        cdef Gen result = Gen.__new__(Gen)
        result._comp = sgt_gen
        return result
    
    @staticmethod
    def from_gen(gen: Gen):
        return gen

    def __init__(self, Network netw, id_, busId, phases):
        self._comp = netw._netw.addGen(
            make_shared[cc.Gen](<string>id_, <int>len(phases)), <string>busId, cc_phases(phases))

    cdef cc.Component* comp(self):
        """Return a pointer to the wrapped C++ Component."""
        return cc.raw(self._comp)
    
    cdef cc.Gen* gen(self):
        """Return a pointer to the wrapped C++ Gen."""
        return cc.raw(self._comp)
    
    def downcast(self, to_type):
        try:
            return to_type.from_gen(self)
        except Exception:
            return None
    
    def n_phases(self):
        """Number of phases."""
        return self.gen().nPhases()
    
    def is_in_service(self):
        """Is the gen currently in service?"""
        return self.gen().isInService()
    
    def set_is_in_service(self, is_in_service):
        """Is the gen currently in service?"""
        self.gen().setIsInService(is_in_service)
    
    def s(self):
        """Generation power on each terminal phase, zero if not in service."""
        return topy_ccol(self.gen().S())
    
    def in_service_s(self):
        """Generation power on each terminal phase, assuming in service."""
        return topy_ccol(self.gen().inServiceS())
    
    def set_in_service_s(self, in_service_s):
        """Generation power on each terminal phase, assuming in service."""
        self.gen().setInServiceS(tocc_ccol(in_service_s))

    def v_term(self):
        """Voltage on each terminal phase."""
        return topy_ccol(self.gen().VTerm())
    
    def i_term(self):
        """Current *into the generator*, i.e. *load current*, at each terminal phase."""
        return topy_ccol(self.gen().ITerm())
    
    def s_term(self):
        """Power *into the generator*, i.e. *load power*, at each terminal phase."""
        return topy_ccol(self.gen().STerm())
    
    def p_min(self):
        """Minimum real power generation."""
        return self.gen().PMin()
    
    def set_p_min(self, p_min):
        """Minimum real power generation."""
        self.gen().setPMin(p_min)
    
    def p_max(self):
        """Maximum real power generation."""
        return self.gen().PMax()
    
    def set_p_max(self, p_max):
        """Maximum real power generation."""
        self.gen().setPMax(p_max)
    
    def q_min(self):
        """Minimum reactive power generation."""
        return self.gen().QMin()
    
    def set_q_min(self, q_min):
        """Minimum reactive power generation."""
        self.gen().setQMin(q_min)
    
    def q_max(self):
        """Maximum reactive power generation."""
        return self.gen().QMax()
    
    def set_q_max(self, q_max):
        """Maximum reactive power generation."""
        self.gen().setQMax(q_max)
    
    def s_min(self):
        """Minimum apparent power generation."""
        return self.gen().SMin()
    
    def set_s_min(self, s_min):
        """Minimum apparent power generation."""
        self.gen().setSMin(s_min)
    
    def s_max(self):
        """Maximum apparent power generation."""
        return self.gen().SMax()
    
    def set_s_max(self, s_max):
        """Maximum apparent power generation."""
        self.gen().setSMax(s_max)
    
    def c_startup(self):
        """Startup cost."""
        return self.gen().cStartup()
    
    def set_c_startup(self, c_startup):
        """Startup cost."""
        self.gen().setCStartup(c_startup)
    
    def c_shutdown(self):
        """Shutdown cost."""
        return self.gen().cShutdown()
    
    def set_c_shutdown(self, c_shutdown):
        """Shutdown cost."""
        self.gen().setCShutdown(c_shutdown)
    
    def c_0(self):
        """Constant cost per unit time."""
        return self.gen().c0()
    
    def set_c_0(self, c_0):
        """Constant cost per h."""
        self.gen().setC0(c_0)
    
    def c_1(self):
        """Linear cost per kW h."""
        return self.gen().c1()
    
    def set_c_1(self, c_1):
        """Linear cost per kW h."""
        self.gen().setC1(c_1)
    
    def c_2(self):
        """Quadratic cost per kW^2 h."""
        return self.gen().c2()
    
    def set_c_2(self, c_2):
        """Quadratic cost per kW^2 h."""
        self.gen().setC2(c_2)
    
    def bus(self):
        """The bus to which gen is connected."""
        return Bus.wrap(self.gen().bus())

    def phases(self):
        """The bus phases to which gen is connected."""
        return py_phases(self.gen().phases())
   
# Wrap Zip
cdef class Zip(Component):

    @staticmethod
    cdef Zip wrap(cc.ComponentPtr[cc.Zip, cc.Zip]& sgt_zip):
        """Wrap an existing C++ Zip in python."""
        cdef Zip result = Zip.__new__(Zip)
        result._comp = sgt_zip
        return result
    
    @staticmethod
    def from_zip(zip: Zip):
        return zip

    def __init__(self, Network netw, id_, busId, phases, connection):
        self._comp = netw._netw.addZip(
            make_shared[cc.Zip](
                <string>id_, <int>len(phases), tocc_umat(connection)), <string>busId, cc_phases(phases))

    cdef cc.Component* comp(self):
        """Return a pointer to the wrapped C++ Component."""
        return cc.raw(self._comp)
    
    cdef cc.Zip* zip_(self):
        """Return a pointer to the wrapped C++ Zip."""
        return cc.raw(self._comp)

    def downcast(self, to_type):
        try:
            return to_type.from_zip(self)
        except Exception:
            return None
    
    @staticmethod
    def wye_connection(n_phase):
        """Connection specification for a wye connection."""
        return topy_umat(cc.Zip.wyeConnection(n_phase))
    
    @staticmethod
    def delta_connection(n_phase):
        """Connection specification for a delta connection."""
        return topy_umat(cc.Zip.deltaConnection(n_phase))
    
    def n_phases(self):
        """Number of terminal phases."""
        return self.zip_().nPhases()
    
    def connection(self):
        """
        Connection specification.

        This is a n x 2 matrix, with each row being (i, j) where i and j specify
        the phases between which the load component is connected (i < j), or
        a phase to ground connection (i == j).
        """
        return topy_umat(self.zip_().connection())
    
    def n_comps(self):
        """Number of ZIP (load) components."""
        return self.zip_().nComps()

    def comps_to_mat(self, comps):
        """
        Convert specified load components to an upper triangular matrix.

        The row and column of a non-zero element give the terminal phases
        between which the load is connected. Diagonal elements are a
        phase-ground connection.
        """
        return topy_cmat(self.zip_().compsToMat(tocc_ccol(comps)))
    
    def is_in_service(self):
        """Is the zip currently in service?"""
        return self.zip_().isInService()
    
    def set_is_in_service(self, is_in_service):
        """Is the zip currently in service?"""
        self.zip_().setIsInService(is_in_service)
    
    def y_const(self):
        """Constant Y (or Z) component of the ZIP load."""
        return topy_ccol(self.zip_().YConst())
    
    def set_y_const(self, y_const):
        """Constant Y (or Z) component of the ZIP load."""
        return self.zip_().setYConst(tocc_ccol(y_const))
    
    def i_const(self):
        """Constant I component of the ZIP load."""
        return topy_ccol(self.zip_().IConst())
    
    def set_i_const(self, i_const):
        """Constant I component of the ZIP load."""
        return self.zip_().setIConst(tocc_ccol(i_const))
    
    def s_const(self):
        """Constant S component of the ZIP load."""
        return topy_ccol(self.zip_().SConst())
    
    def set_s_const(self, s_const):
        """Constant S component of the ZIP load."""
        return self.zip_().setSConst(tocc_ccol(s_const))
    
    def in_service_s(self):
        """Load power, assuming in service.""" 
        return topy_ccol(self.zip_().inServiceS())
    
    def s(self):
        """Actual load power, zero if not in service.""" 
        return topy_ccol(self.zip_().S())
    
    def in_service_s_tot(self):
        """Total load power (sum over components), assuming in service.""" 
        return topy_cplx(self.zip_().inServiceSTot())
    
    def s_tot(self):
        """Total load power (sum over components), zero if not in service.""" 
        return topy_cplx(self.zip_().STot())

    def v_term(self):
        """Terminal voltages."""
        return topy_ccol(self.zip_().VTerm())
    
    def i_term(self):
        """Terminal currents (into ZIP, i.e. load convention)."""
        return topy_ccol(self.zip_().ITerm())
    
    def s_term(self):
        """Terminal powers (into ZIP, i.e. load convention)."""
        return topy_ccol(self.zip_().STerm())
    
    def bus(self):
        """The bus to which zip is connected."""
        return Bus.wrap(self.zip_().bus())

    def phases(self):
        """The bus phases to which zip is connected."""
        return py_phases(self.zip_().phases())
    
# Wrap Network
cdef class Network:

    def __cinit__(self):
        self._netw = new cc.Network()

    def __dealloc__(self):
        """Destructor."""
        del self._netw

    def __repr__(self):
        """A JSON string representation."""
        return json.dumps(self.to_json(), indent=4)

    def parse_string(self, s):
        """Parse network from a YAML string."""
        cc.parseFromString(self._netw[0], s)

    def parse_yaml(self, y):
        """Parse network from python list generated from YAML."""
        cc.parseFromString(self._netw[0], yaml.dump(y))

    def p_base(self):
        """Base power for network."""
        return self._netw[0].PBase()
    
    def set_p_base(self, p_base):
        """Base power for network."""
        return self._netw[0].setPBase(p_base)

    def buses(self):
        """List of all buses in network."""
        return [Bus.wrap(x) for x in self._netw.buses()]
    
    def branches(self):
        """List of all branches in network."""
        return [Branch.wrap(x) for x in self._netw.branches()]
    
    def gens(self):
        """List of all gens in network."""
        return [Gen.wrap(x) for x in self._netw.gens()]
    
    def zips(self):
        """List of all zips in network."""
        return [Zip.wrap(x) for x in self._netw.zips()]

    def apply_flat_start(self):
        """
        Apply the flat start conditions.

        Voltages go to their nominal values, and generator powers go to zero.
        Normally this gets called automatically when power flow is solved,
        whenever use_flat_start is true.
        """
        self._netw[0].applyFlatStart()
    
    def set_solver_options(self, options_dict):
        """Set solver options using a JSON compatible dict"""
        cc.setSolverOptions(self._netw[0], tocc_jsn(options_dict))
    
    def solve_power_flow(self, use_flat_start=True):
        """Solve power flow for the network."""
        return self._netw[0].solvePowerFlow(use_flat_start)

    def is_valid_solution(self):
        """Was the last call to solve_power_flow successful?"""
        return self._netw[0].isValidSolution()
    
    def gen_cost_per_unit_time(self):
        """Total generation cost per unit time."""
        return self._netw[0].genCostPerUnitTime()

    def to_json(self):
        """Convert network to JSON (python dict)."""
        return topy_jsn(self._netw.toJson())

def run_tap_changers(Network netw, max_iter=None):
    if max_iter is None:
        return cc.runTapChangers(netw._netw[0])
    else:
        return cc.runTapChangers(netw._netw[0], max_iter)

# Wrap GenericBranch
cdef class GenericBranch(Branch):
    """A generic branch, with a user-specified branch admittance matrix."""
    
    @staticmethod
    cdef GenericBranch wrap(cc.ComponentPtr[cc.BranchAbc, cc.GenericBranch]& sgt_branch):
        """Wrap an existing C++ GenericBranch in python."""
        cdef GenericBranch result = GenericBranch.__new__(GenericBranch)
        result._comp = sgt_branch.as[cc.BranchAbc]()
        return result
    
    @staticmethod
    def from_branch(branch: Branch):
        """Downcast a Branch to a GenericBranch, returning None if unsuccessful"""
        cdef cc.ComponentPtr[cc.BranchAbc, cc.GenericBranch] comp = branch._comp.as[cc.GenericBranch]()
        return GenericBranch.wrap(comp) if cc.raw(comp) != NULL else None

    def __init__(self, Network netw, id_, bus_0_id, phases_0, bus_1_id, phases_1):
        self._comp = netw._netw.addBranch(
            static_pointer_cast[cc.BranchAbc, cc.GenericBranch](
                make_shared[cc.GenericBranch](<string>id_, <int>len(phases_0), <int>len(phases_1))),
            <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))

    cdef cc.GenericBranch* generic_branch(self):
        """Return the wrapped C++ GenericBranch.""" 
        return <cc.GenericBranch*>(cc.raw(self._comp))

    def set_in_service_y(self, in_service_y):
        """Set the bus admittance matrix for the branch.""" 
        self.generic_branch().setInServiceY(tocc_cmat(in_service_y))

# Wrap CommonBranch
cdef class CommonBranch(Branch):
    """A matpower style branch, for balanced or single phase treatments."""

    @staticmethod
    cdef CommonBranch wrap(cc.ComponentPtr[cc.BranchAbc, cc.CommonBranch]& sgt_branch):
        """Wrap an existing C++ CommonBranch in python."""
        cdef CommonBranch result = CommonBranch.__new__(CommonBranch)
        result._comp = sgt_branch.as[cc.BranchAbc]()
        return result
    
    @staticmethod
    def from_branch(branch: Branch):
        """Downcast a Branch to a CommonBranch, returning None if unsuccessful"""
        cdef cc.ComponentPtr[cc.BranchAbc, cc.CommonBranch] comp = branch._comp.as[cc.CommonBranch]()
        return CommonBranch.wrap(comp) if cc.raw(comp) != NULL else None
    
    def __init__(self, Network netw, id_, bus_0_id, phases_0, bus_1_id, phases_1):
        self._comp = netw._netw.addBranch(
            static_pointer_cast[cc.BranchAbc, cc.CommonBranch](
                make_shared[cc.CommonBranch](<string>id_)),
            <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))
    
    cdef cc.CommonBranch* common_branch(self):
        """Return the wrapped C++ CommonBranch.""" 
        return <cc.CommonBranch*>(cc.raw(self._comp))

    def tap_ratio(self):
        """Tap ratio of ideal transformer at the first terminal.""" 
        return topy_cplx(self.common_branch().tapRatio())
    
    def set_tap_ratio(self, tap_ratio):
        """Tap ratio of ideal transformer at the first terminal.""" 
        return self.common_branch().setTapRatio(tocc_cplx(tap_ratio))

    def y_series(self):
        """Series admittance.""" 
        return topy_cplx(self.common_branch().YSeries())
    
    def set_y_series(self, y_series):
        """Series admittance.""" 
        return self.common_branch().setYSeries(tocc_cplx(y_series))

    def y_shunt(self):
        """Shunt admittance.""" 
        return topy_cplx(self.common_branch().YShunt())
    
    def set_y_shunt(self, y_shunt):
        """Shunt admittance.""" 
        return self.common_branch().setYShunt(tocc_cplx(y_shunt))

    def rate_a(self):
        """Rating A.""" 
        return self.common_branch().rateA()
    
    def set_rate_a(self, rate_a):
        """Rating A.""" 
        return self.common_branch().setRateA(rate_a)
    
    def rate_b(self):
        """Rating B.""" 
        return self.common_branch().rateB()
    
    def set_rate_b(self, rate_b):
        """Rating B.""" 
        return self.common_branch().setRateB(rate_b)
    
    def rate_c(self):
        """Rating C.""" 
        return self.common_branch().rateC()
    
    def set_rate_c(self, rate_c):
        """Rating C.""" 
        return self.common_branch().setRateC(rate_c)
    
    def ang_min(self):
        """Minimum allowable voltage angle difference.""" 
        return self.common_branch().angMin()
    
    def set_ang_min(self, ang_min):
        """Minimum allowable voltage angle difference.""" 
        return self.common_branch().setAngMin(ang_min)
    
    def ang_max(self):
        """Maximum allowable voltage angle difference.""" 
        return self.common_branch().angMax()
    
    def set_ang_max(self, ang_max):
        """Maximum allowable voltage angle difference.""" 
        return self.common_branch().setAngMax(ang_max)

# Wrap OverheadLine
cdef class OverheadLine(Branch):
    """Overhead line model.""" 

    @staticmethod
    cdef OverheadLine wrap(cc.ComponentPtr[cc.BranchAbc, cc.OverheadLine]& sgt_branch):
        """Wrap an existing C++ OverheadLine in python."""
        cdef OverheadLine result = OverheadLine.__new__(OverheadLine)
        result._comp = sgt_branch.as[cc.BranchAbc]();
        return result
    
    @staticmethod
    def from_branch(branch: Branch):
        """Downcast a Branch to an OverheadLine, returning None if unsuccessful"""
        cdef cc.ComponentPtr[cc.BranchAbc, cc.OverheadLine] comp = branch._comp.as[cc.OverheadLine]()
        return OverheadLine.wrap(comp) if cc.raw(comp) != NULL else None

    def __init__(
        self, Network netw, id_, bus_0_id, phases_0, bus_1_id, phases_1, l, cond_dist, subcond_gmr, subcond_r_per_l,
        freq=None, rho_earth=None, n_in_bundle=None, adj_subcond_dist=None
    ):
        if freq is None:
            self._comp = netw._netw.addBranch(
                static_pointer_cast[cc.BranchAbc, cc.OverheadLine](make_shared[cc.OverheadLine](
                    <string>id_, <int>len(phases_0), <double>l, tocc_rmat(cond_dist), tocc_rcol(subcond_gmr),
                    tocc_rcol(subcond_r_per_l)
                )),
                <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))
        else:
            self._comp = netw._netw.addBranch(
                static_pointer_cast[cc.BranchAbc, cc.OverheadLine](make_shared[cc.OverheadLine](
                    <string>id_, <int>len(phases_0), <double>l, tocc_rmat(cond_dist), tocc_rcol(subcond_gmr),
                    tocc_rcol(subcond_r_per_l), <double>freq, <double>rho_earth, tocc_ucol(n_in_bundle),
                    tocc_rcol(adj_subcond_dist)
                )),
                <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))
    
    cdef cc.OverheadLine* overhead_line(self):
        """Return the wrapped C++ OverheadLine."""
        return <cc.OverheadLine*>(cc.raw(self._comp))

    def z_prim(self):
        """Primitive admittance matrix (see Kersting)."""
        return topy_cmat(self.overhead_line().ZPrim())
    
    def z_phase(self):
        """Phase admittance matrix (see Kersting)."""
        return topy_cmat(self.overhead_line().ZPhase())

cdef class UndergroundLine(Branch):
    """Underground line model.""" 

    @staticmethod
    cdef UndergroundLine wrap(cc.ComponentPtr[cc.BranchAbc, cc.UndergroundLine]& sgt_branch):
        """Wrap an existing C++ UndergroundLine in python."""
        cdef UndergroundLine result = UndergroundLine.__new__(UndergroundLine)
        result._comp = sgt_branch.as[cc.BranchAbc]()
        return result
    
    @staticmethod
    def from_branch(branch: Branch):
        """Downcast a Branch to an UndergroundLine, returning None if unsuccessful"""
        cdef cc.ComponentPtr[cc.BranchAbc, cc.UndergroundLine] comp = branch._comp.as[cc.UndergroundLine]()
        return UndergroundLine.wrap(comp) if cc.raw(comp) != NULL else None

    cdef cc.UndergroundLine* underground_line(self):
        """Return the wrapped C++ UndergroundLine."""
        return <cc.UndergroundLine*>(cc.raw(self._comp))

    def z_prim(self):
        """Primitive admittance matrix (see Kersting)."""
        return topy_cmat(self.underground_line().ZPrim())

    def z_phase(self):
        """Phase admittance matrix (see Kersting)."""
        return topy_cmat(self.underground_line().ZPhase())

# Wrap UndergroundLineStrandedShield
cdef class UndergroundLineStrandedShield(UndergroundLine):
    """Underground line model, stranded shielding specialization.""" 

    def __init__(
        self, Network netw, id_, bus_0_id, phases_0, bus_1_id, phases_1,
        l, has_neutral, phase_dist, gmr_phase, res_per_l_phase, gmr_neut, res_per_l_neut, freq, rho_earth,
        gmr_conc_strand, res_per_l_conc_strand, n_conc_strands, r_conc
    ):
        self._comp = netw._netw.addBranch(
            static_pointer_cast[cc.BranchAbc, cc.UndergroundLineStrandedShield](
                make_shared[cc.UndergroundLineStrandedShield](
                    <string>id, <int>len(phases_0), <double>l, <bint>has_neutral, tocc_rcol(phase_dist),
                    <double>gmr_phase, <double>res_per_l_phase, <double>gmr_neut, <double>res_per_l_neut,
                    <double> freq, <double>rho_earth, <double>gmr_conc_strand, <double>res_per_l_conc_strand,
                    <int>n_conc_strands, <double>r_conc
                )
            ),
            <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))


# Wrap UndergroundLineTapeShield
cdef class UndergroundLineTapeShield(UndergroundLine):
    """Underground line model, tape shielding specialization.""" 

    def __init__(
        self, Network netw, id_, bus_0_id, phases_0, bus_1_id, phases_1,
        l, has_neutral, phase_dist, gmr_phase, res_per_l_phase, gmr_neut, res_per_l_neut, freq, rho_earth,
        outside_r_shield, thick_shield, resistivityShield=None
    ):
        if resistivityShield is not None:
            self._comp = netw._netw.addBranch(
                static_pointer_cast[cc.BranchAbc, cc.UndergroundLineTapeShield](
                    make_shared[cc.UndergroundLineTapeShield](
                        <string>id, <int>len(phases_0), <double>l, <bint>has_neutral, tocc_rcol(phase_dist),
                        <double>gmr_phase, <double>res_per_l_phase, <double>gmr_neut, <double>res_per_l_neut,
                        <double> freq, <double>rho_earth, <double> outside_r_shield, <double>thick_shield,
                        <double>resistivityShield
                    )
                ),
                <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))
        else:
            self._comp = netw._netw.addBranch(
                static_pointer_cast[cc.BranchAbc, cc.UndergroundLineTapeShield](
                    make_shared[cc.UndergroundLineTapeShield](
                        <string>id, <int>len(phases_0), <double>l, <bint>has_neutral, tocc_rcol(phase_dist),
                        <double>gmr_phase, <double>res_per_l_phase, <double>gmr_neut, <double>res_per_l_neut,
                        <double> freq, <double>rho_earth, <double> outside_r_shield, <double>thick_shield
                    )
                ),
                <string>bus_0_id, cc_phases(phases_0), <string>bus_1_id, cc_phases(phases_1))


# Wrap Transformer
cdef class Transformer(Branch):
    """Transformer."""
    
    @staticmethod
    cdef Transformer wrap(cc.ComponentPtr[cc.BranchAbc, cc.Transformer]& sgt_branch):
        """Wrap an existing C++ Transformer in python."""
        cdef Transformer result = Transformer.__new__(Transformer)
        result._comp = sgt_branch.as[cc.BranchAbc]()
        return result
    
    @staticmethod
    def from_branch(branch: Branch):
        """Downcast a Branch to a Transformer, returning None if unsuccessful"""
        cdef cc.ComponentPtr[cc.BranchAbc, cc.Transformer] comp = branch._comp.as[cc.Transformer]()
        return Transformer.wrap(comp) if cc.raw(comp) != NULL else None
    
    def __init__(
        self, Network netw, id_, bus_0_id, phases_0, bus_1_id, phases_1,  vec_group, n_winding_pairs, is_grounded_0,
        is_grounded_1, nom_turns_ratio, z_series, y_shunt, impedance_side, min_tap, max_tap, tap_factor, tap_side
    ):
        self._comp = netw._netw.addBranch(
            static_pointer_cast[cc.BranchAbc, cc.Transformer](
                make_shared[cc.Transformer](
                    <string>id_, <string>vec_group, <unsigned int>n_winding_pairs, <bool>is_grounded_0,
                    <bool>is_grounded_1, tocc_cplx(nom_turns_ratio), tocc_cplx(z_series), tocc_cplx(y_shunt),
                    <int>min_tap, <int>max_tap, <double>tap_factor, <unsigned int>tap_side)),
            bus_0_id, cc_phases(phases_0), bus_1_id, cc_phases(phases_1))

    cdef cc.Transformer* transformer(self):
        """Return the wrapped C++ Transformer."""
        return <cc.Transformer*>(cc.raw(self._comp))

    def n_wind_pairs(self):
        """
        The number of winding pairs.
        """
        return self.transformer().wiring().nWindPairs()
    
    def n_term_0(self):
        """
        The number of primary terminals.
        """
        return self.transformer().wiring().nTerm0()
    
    def n_term_1(self):
        """
        The number of secondary terminals.
        """
        return self.transformer().wiring().nTerm1()
    
    def n_term(self):
        """
        The number of primary plus secondary terminals.
        """
        return self.transformer().wiring().nTerm()
    
    def w(self):
        """
        The n_wind x n_term W matrix giving the winding / terminal connectivity.

        The first n_wind rows are primary, and the next n_wind rows are secondary.
        The first n_term_0 cols are primary, and the next n_term_1 cols are secondary.
        1 means a given winding starts at a given terminal and -1 means it ends at a given terminal.
        1 or -1 is absent (zero) if the terminal in question is grounded.
        """
        return topy_rmat(self.transformer().wiring().W())
    
    def vector_group(self):
        """
        The vector group string.
        """
        return self.transformer().wiring().vectorGroup()

    def nom_turns_ratio(self):
        """
        The nominal turns ratio, primary / secondary.

        Note that this is not necessary the voltage ratio, e.g. when moving from delta to wye voltages.
        """
        return topy_cplx(self.transformer().nomTurnsRatio())

    def off_nom_turns_ratio(self):
        """The off-nominal turns ratio, primary / secondary, per winding."""
        return topy_ccol(self.transformer().offNomTurnsRatio())
    
    def turns_ratio(self):
        """The actual (nominal * off-nominal) turns ratio, primary / secondary, per winding."""
        return topy_ccol(self.transformer().turnsRatio())
    
    def z_series(self):
        """The series (leakage) impedance."""
        return topy_cplx(self.transformer().ZSeries())
    
    def y_shunt(self):
        """The shunt (magnetising) admittance."""
        return topy_cplx(self.transformer().YShunt())
    
    def impedance_side(self):
        """The side to which the impedance is referred, 0 = primary, 1 = secondary."""
        return self.transformer().impedanceSide()

    def min_tap(self):
        """The minimum tap setting (integer)."""
        return self.transformer().minTap()
    
    def max_tap(self):
        """The maximum tap setting (integer)."""
        return self.transformer().maxTap()

    def tap_factor(self):
        """Fractional change to turns ratio per tap."""
        return self.transformer().tapFactor()
    
    def tap_side(self):
        """Which side are the taps on."""
        return self.transformer().tapSide()
    
    def taps(self):
        """List of tap settings for each winding."""
        return topy_icol(self.transformer().taps())
    
    def set_taps(self, taps):
        """List of tap settings for each winding."""
        cdef cc.Col[int] cc_taps = tocc_icol(taps)
        self.transformer().setTaps(cc_taps)
    
    def set_equal_taps(self, tap):
        """Set all taps to tap."""
        self.transformer().setEqualTaps(tap)
    
    def set_tap(self, tap, idx):
        """Set a tap for a particular winding."""
        self.transformer().setTap(tap, idx)

    def run_tap_changers_once(self):
        """ Run tap changers for one change, returning True if a tap change was necessary."""
        return self.transformer().runTapChangersOnce()

    def v_windings(self):
        """Primary / secondary voltage for each winding."""
        cdef vector[cc.Col[cc.Complex]] v = cc.txVWindings(self.transformer()[0])
        return [topy_ccol(v[0]), topy_ccol(v[1])]
    
    def i_windings(self):
        """Primary / secondary current for each winding."""
        cdef vector[cc.Col[cc.Complex]] i = cc.txIWindings(self.transformer()[0])
        return [topy_ccol(i[0]), topy_ccol(i[1])]


class Sensitivity:
    def __init__(self, bus_id, dvdx, dtdx):
        self.bus_id = bus_id
        self.dvdx = dvdx
        self.dtdx = dtdx
    
    def __str__(self):
        return json.dumps(self.__dict__)


def sensitivity(Network netw, ds_dx):
    cdef map[string, cc.Col[cc.Complex]] ds_dx_cc
    for k, v in ds_dx.items():
        ds_dx_cc[k] = tocc_ccol(v)

    cdef cclist[cc.Sensitivity] cc_retval = cc.sensitivity(netw._netw[0], ds_dx_cc)

    retval = []
    for s in cc_retval:
        retval.append(Sensitivity(s.busId, topy_rcol(s.dvdx), topy_rcol(s.dtdx)))

    return retval
