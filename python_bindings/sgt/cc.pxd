# cython: language_level=3, boundscheck=False, c_string_type=unicode, c_string_encoding=utf8, infer_types=True

from libc.stdint cimport uint8_t
from libcpp cimport bool
from libcpp.list cimport list
from libcpp.map cimport map
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr

ctypedef unsigned long long uword 

cdef extern from '<armadillo>' namespace 'arma':
    # TODO: iterators cause all kinds of hassles with const correctness.
    # Don't use for now.
    cdef cppclass Col[T]:
        Col()
        Col(uword n)
        uword size()
        T& operator()(uword i)
        T& operator[](uword i)
        T& at(uword i)

    cdef cppclass Mat[T]:
        Mat()
        Mat(uword nrow, uword ncol)
        uword n_rows
        uword n_cols
        uword size()
        T& operator()(int i, int j)
        T& at(int i, int j)

cdef extern from '<SgtCore/Common.h>' namespace 'Sgt':
    cdef cppclass json: 
        string dump(uword indent)

        @staticmethod
        json parse(string& s) except +
    
    cdef cppclass Complex:
        Complex()
        Complex(double re, double im)
        double real()
        double imag()

cdef extern from "<SgtCore/PowerFlow.h>" namespace "Sgt":
    cdef cppclass Phase:
        json toJson()
    
    string to_string(const Phase&)
    string to_json(const Phase&)

    cdef Mat[Complex] ZLine2YNode(const Mat[Complex] ZPh) except +
    cdef Mat[Complex] approxPhaseImpedanceMatrix(Complex ZPlus, Complex Z0, unsigned int nPhase) except +

cdef extern from "<SgtCore/PowerFlow.h>" namespace "Sgt::Phase":
    cdef Phase PH_A "Sgt::Phase::A"
    cdef Phase PH_B "Sgt::Phase::B"
    cdef Phase PH_C "Sgt::Phase::C"
    cdef Phase PH_G "Sgt::Phase::G"
    cdef Phase PH_N "Sgt::Phase::N"
    cdef Phase PH_BAL "Sgt::Phase::BAL"
    cdef Phase PH_BAD "Sgt::Phase::BAD"

cdef extern from "<SgtCore/PowerFlow.h>" namespace "Sgt":
    cdef cppclass Phases: 
        Phases()
        Phases(const vector[Phase]&)
        vector[Phase].iterator begin()
        vector[Phase].iterator end()

cdef extern from "<SgtCore/Common.h>" namespace "Sgt":
    cdef cppclass LogLevel:
        pass

cdef extern from "<SgtCore/Common.h>" namespace "Sgt::LogLevel":
    cdef LogLevel LOG_LEVEL_NONE "Sgt::LogLevel::NONE"
    cdef LogLevel LOG_LEVEL_NORMAL "Sgt::LogLevel::NORMAL"
    cdef LogLevel LOG_LEVEL_VERBOSE "Sgt::LogLevel::VERBOSE"

cdef extern from "<SgtCore/PowerFlow.h>" namespace "Sgt":
    cdef cppclass BusType:
        pass
    
    string to_string(const BusType&)
    string to_json(const BusType&)

cdef extern from "<SgtCore/PowerFlow.h>" namespace "Sgt::BusType":
    cdef BusType BT_SL "Sgt::BusType::SL"
    cdef BusType BT_PQ "Sgt::BusType::PQ"
    cdef BusType BT_PV "Sgt::BusType::PV"
    cdef BusType BT_NA "Sgt::BusType::NA"
    cdef BusType BT_BAD "Sgt::BusType::BAD"

cdef extern from '<SgtCore/ComponentCollection.h>' namespace 'Sgt':
    cdef cppclass ComponentPtr[B, D]:
        D& operator*()
        ComponentPtr[B, toD] as[toD]() 

    cdef cppclass ComponentCollection[T]:
        vector[ComponentPtr[T, T]].iterator begin()
        vector[ComponentPtr[T, T]].iterator end()

cdef extern from '<SgtCore/Component.h>' namespace 'Sgt':
    cdef cppclass Component:
        Component()
        const string& id()
        const string& componentType()
        json toJson()
        const json& userData()
        void setUserData(const json&)

cdef extern from '<SgtCore/Bus.h>' namespace 'Sgt':
    cdef cppclass Bus(Component):
        Bus(const string& id, const Phases& phases,
            const Col[Complex]& VNom, double VBase)

        const Phases& phases()

        const Col[Complex]& VNom()

        double VBase()

        const BusType& type()
        void setType(BusType)

        Col[double] VMagSetpoint()
        void setVMagSetpoint(const Col[double]&)

        Col[double] VAngSetpoint()
        void setVAngSetpoint(const Col[double]&)

        Col[Complex] VSetpoint()
        void setVSetpoint(const Col[Complex]&)

        void applyVSetpoints()

        double VMagMin()
        void setVMagMin(double)
        bool VMagMinViolated()

        double VMagMax()
        void setVMagMax(double)
        bool VMagMaxViolated()

        bint isInService()
        void setIsInService(bint)

        bint isSupplied()
        void setIsSupplied(bint)
        
        const Col[Complex]& V()
        void setV(const Col[Complex]&)

        const Col[Complex]& SGenUnserved()
        void setSGenUnserved(Col[Complex])
        
        const Mat[Complex]& SZipUnserved()
        void setSZipUnserved(Mat[Complex])

        int islandIdx()
        void setIslandIdx(int)
        
        const Col[double]& coords()
        void setCoords(Col[double])

        ComponentCollection[BranchAbc]& branches0()
        ComponentCollection[BranchAbc]& branches1()
        
        ComponentCollection[Gen]& gens()
        int nInServiceGens()
        Col[Complex] SGenRequested()
        Col[Complex] SGen()
        Complex SGenTot()
        
        ComponentCollection[Zip]& zips()
        int nInServiceZips()
        Mat[Complex] YConst()
        Mat[Complex] SYConst()
        Mat[Complex] IConst()
        Mat[Complex] SIConst()
        Mat[Complex] SConst()
        Mat[Complex] SZipRequested()
        Mat[Complex] SZip()
        Complex SZipTot()

cdef extern from '<array>' namespace 'std':
    cdef cppclass array_phases_2 "std::array<Sgt::Phases, 2>":
        Phases& operator[](size_t)

    cdef cppclass array_buses_2 "std::array<Sgt::ComponentPtr<Bus, Bus>, 2>":
        ComponentPtr[Bus, Bus]& operator[](size_t)
    
    cdef cppclass array_ccol_2 "std::array<arma::Col<Sgt::Complex>, 2>":
        Col[Complex]& operator[](size_t)

cdef extern from '<SgtCore/Branch.h>' namespace 'Sgt':
    cdef cppclass BranchAbc(Component):
        unsigned int nPhases0()

        unsigned int nPhases1()

        bint isInService()
        void setIsInService(bint)

        Mat[Complex] Y()
        Mat[Complex] inServiceY()

        ComponentPtr[Bus, Bus] bus0()
        Phases& phases0()

        ComponentPtr[Bus, Bus] bus1()
        Phases& phases1()
        
        array_buses_2 buses()
        array_phases_2 phases()
        
        array_ccol_2 VTerm()
        array_ccol_2 ITerm()
        array_ccol_2 STerm()

        double SMagMax()
        void setSMagMax(double)
        bool SMagMaxViolated()
        
        double IMagMax()
        void setIMagMax(double)
        bool IMagMaxViolated()
    
    cdef cppclass GenericBranch(BranchAbc):
        GenericBranch(string id, unsigned int nPhases0, unsigned int nPhases1)

        void setInServiceY(const Mat[Complex]& Y)
    
cdef extern from '<SgtCore/CommonBranch.h>' namespace 'Sgt':
    cdef cppclass CommonBranch(BranchAbc):
        CommonBranch(string id)

        Complex tapRatio()
        void setTapRatio(Complex)
        
        Complex YSeries()
        void setYSeries(Complex)
        
        Complex YShunt()
        void setYShunt(Complex)

        double rateA()
        void setRateA(double)

        double rateB()
        void setRateB(double)

        double rateC()
        void setRateC(double)
        
        double angMin()
        void setAngMin(double)
        
        double angMax()
        void setAngMax(double)
    
cdef extern from '<SgtCore/OverheadLine.h>' namespace 'Sgt':
    cdef cppclass OverheadLine(BranchAbc):
        OverheadLine(
            string id, unsigned int nPhases, double W, const Mat[double]& condDist, const Col[double]& subcondGmr,
            const Col[double]& subcondRPerL, double freq, double rhoEarth, const Col[unsigned int]& nInBundle,
            const Col[double]& adjSubcondDist)
        OverheadLine(
            string id, unsigned int nPhases, double W, const Mat[double]& condDist, const Col[double]& subcondGmr,
            const Col[double]& subcondRPerL)

        const Mat[Complex]& ZPrim()

        const Mat[Complex]& ZPhase()
    
cdef extern from '<SgtCore/UndergroundLine.h>' namespace 'Sgt':
    cdef cppclass UndergroundLine(BranchAbc):
        const Mat[Complex]& ZPrim()

        const Mat[Complex]& ZPhase()

    cdef cppclass UndergroundLineStrandedShield(UndergroundLine):
        UndergroundLineStrandedShield(
            string id, unsigned int nPhases, double W, bint hasNeutral, const Mat[double]& phaseDist,
            double gmrPhase, double resPerLPhase, double gmrNeut, double resPerLNeut, double freq, double rhoEarth,
            double gmrConcStrand, double resPerLConcStrand, int nConcStrands, double rConc)

    cdef cppclass UndergroundLineTapeShield(UndergroundLine):
        UndergroundLineTapeShield(
            string id, unsigned int nPhases, double W, bint hasNeutral, const Mat[double]& phaseDist, double gmrPhase,
            double resPerLPhase, double gmrNeut, double resPerLNeut, double freq, double rhoEarth,
            double outsideRShield, double thickShield, double resistivityShield)

        UndergroundLineTapeShield(
            string id, unsigned int nPhases, double W, bint hasNeutral, const Mat[double]& phaseDist, double gmrPhase,
            double resPerLPhase, double gmrNeut, double resPerLNeut, double freq, double rhoEarth,
            double outsideRShield, double thickShield)

cdef extern from '<SgtCore/Transformer.h>' namespace 'Sgt':
    cdef cppclass TransformerWiring:
        const string& vectorGroup()

        const uword nWindPairs()

        const uword nTerm0()

        const uword nTerm1()

        const uword nTerm()

        const Mat[double]& W()
        
    cdef cppclass Transformer(BranchAbc):
        Transformer(
            const string& id, uword nTerm0, uword nTerm1, const string& vecGpStr, nWindPairs, isGrounded0, isGrounded1,
            const Complex& nomTurnsRatio, const Complex& ZSeries, const Complex& YShunt, uword impedanceSide,
            int minTap = 0, int maxTap = 0, double tapFactor = 0.0, unsigned int tapSide = 0)

        const TransformerWiring& wiring()

        const Complex& nomTurnsRatio()
        
        const Col[Complex] offNomTurnsRatio()
        
        const Col[Complex] turnsRatio()
        
        const Complex& ZSeries()
        
        const Complex& YShunt()

        uword impedanceSide()

        int minTap()

        int maxTap()
        
        double tapFactor()

        unsigned int tapSide()

        const Col[int]& taps()

        bool runTapChangersOnce()

        void setTaps(const Col[int])

        void setEqualTaps(int)

        void setTap(int, int)


cdef extern from '<SgtCore/Gen.h>' namespace 'Sgt':
    cdef cppclass Gen(Component):
        Gen(string id, unsigned int nPhases)

        unsigned int nPhases()
        
        bint isInService()
        void setIsInService(bint)

        Col[Complex] S()
        Col[Complex] inServiceS()
        void setInServiceS(const Col[Complex]&)

        Col[Complex]& VTerm()
        Col[Complex]& ITerm()
        Col[Complex]& STerm()

        double PMin()
        void setPMin(double)

        double PMax()
        void setPMax(double)

        double QMin()
        void setQMin(double)

        double QMax()
        void setQMax(double)

        double SMin()
        void setSMin(double)

        double SMax()
        void setSMax(double)
        
        double cStartup()
        void setCStartup(double)
        
        double cShutdown()
        void setCShutdown(double)
        
        double c0()
        void setC0(double)
        
        double c1()
        void setC1(double)
        
        double c2()
        void setC2(double)

        ComponentPtr[Bus, Bus] bus()
        const Phases& phases()

cdef extern from '<SgtCore/Zip.h>' namespace 'Sgt':
    cdef cppclass Zip(Component):
        @staticmethod
        Mat[uword] wyeConnection(uword)
        
        @staticmethod
        Mat[uword] deltaConnection(uword)

        Zip(const string&, unsigned int, const Mat[uword]&)

        uword nPhases()
        
        Mat[uword]& connection()

        uword nComps()
        
        Mat[Complex] compsToMat(const Col[Complex])
        
        bint isInService()
        void setIsInService(bint)
        
        Col[Complex] YConst()
        void setYConst(const Col[Complex]&)
        
        Col[Complex] IConst()
        void setIConst(const Col[Complex]&)
        
        Col[Complex] SConst()
        void setSConst(const Col[Complex]&)
        
        Col[Complex] inServiceS()
        Col[Complex] S()
        
        Complex inServiceSTot()
        Complex STot()
        
        Col[Complex]& VTerm()
        Col[Complex]& ITerm()
        Col[Complex]& STerm()
        
        ComponentPtr[Bus, Bus] bus()
        const Phases& phases()

cdef extern from '<SgtCore/Network.h>' namespace 'Sgt':
    cdef cppclass Network:
        Network()
        double PBase()
        void setPBase(double)
        ComponentCollection[Bus]& buses()
        ComponentPtr[Bus, Bus] addBus(shared_ptr[Bus])
        ComponentCollection[BranchAbc]& branches()
        ComponentPtr[BranchAbc, BranchAbc] addBranch(
            shared_ptr[BranchAbc], string, Phases, string, Phases)
        ComponentCollection[Gen]& gens()
        ComponentPtr[Gen, Gen] addGen(shared_ptr[Gen], string, Phases)
        ComponentCollection[Zip]& zips()
        ComponentPtr[Zip, Zip] addZip(shared_ptr[Zip], string, Phases)
        void applyFlatStart()
        bool solvePowerFlow(bool)
        bool isValidSolution()
        double genCostPerUnitTime()
        json toJson()

    cdef bool runTapChangers(Network&)
    cdef bool runTapChangers(Network&, unsigned int)

cdef extern from "helper.h":
    cdef T* raw[P, T](P& p)
    cdef D* raw[B, D](ComponentPtr[B, D]& p)
    cdef void assign[T](T&, const T&)
    cdef void parseFromFile(Network&, const string&) except +
    cdef void parseFromString(Network&, const string&) except +
    cdef vector[Phase] phases(const Phases&)
    cdef vector[Col[Complex]] txVWindings(const Transformer&)
    cdef vector[Col[Complex]] txIWindings(const Transformer&)
    cdef void setMessageLogLevel(const LogLevel&)
    cdef void setWarningLogLevel(const LogLevel&)
    cdef void setErrorLogLevel(const LogLevel&)
    cdef void setDebugLogLevel(const LogLevel&)
    cdef void setSolverOptions(Network&, json&)

cdef extern from '<SgtCore/Sensitivity.h>' namespace 'Sgt':
    cdef struct Sensitivity:
        string busId
        Col[double] dvdx
        Col[double] dtdx

    cdef list[Sensitivity] sensitivity(Network&, map[string, Col[Complex]]&) except +
