#ifndef SGTPY_HELPER_DOT_H
#define SGTPY_HELPER_DOT_H

#include<iostream>
#include<sstream>
#include<armadillo>
#include<SgtCore/json.h>
#include<SgtCore/CommonBranch.h>
#include<SgtCore/Network.h>
#include<SgtCore/Parser.h>
#include<SgtCore/PowerFlow.h>
#include<SgtCore/Transformer.h>

template<typename P, typename T> auto* raw(const P& p) {return &(*p);}

template<typename B, typename D> auto* raw(const Sgt::ComponentPtr<B, D>& p) {return &(*p);}

template<typename T> void assign(T& to, const T& from) {to = from;}

inline void parseFromFilename(Sgt::Network& netw, const std::string& fname)
{
    using namespace Sgt;
    Parser<Network> parser;
    parser.parse(fname, netw);
}

inline void parseFromString(Sgt::Network& netw, const std::string& yamlStr)
{
    using namespace Sgt;
    Parser<Network> parser;
    auto yaml = YAML::Load(yamlStr);
    parser.parse(yaml, netw); 
}

inline std::vector<Sgt::Phase> phases(const Sgt::Phases& ps)
{
    return std::vector<Sgt::Phase>(ps.begin(), ps.end());
}

inline std::vector<arma::Col<Sgt::Complex>> txVWindings(const Sgt::Transformer& t)
{
    auto vWind = t.VWindings();
    return {vWind[0], vWind[1]};
}

inline std::vector<arma::Col<Sgt::Complex>> txIWindings(const Sgt::Transformer& t)
{
    auto iWind = t.IWindings();
    return {iWind[0], iWind[1]};
}

inline void setMessageLogLevel(Sgt::LogLevel lev)
{
    Sgt::messageLogLevel() = lev;
}

inline void setWarningLogLevel(Sgt::LogLevel lev)
{
    Sgt::warningLogLevel() = lev;
}

inline void setErrorLogLevel(Sgt::LogLevel lev)
{
    Sgt::errorLogLevel() = lev;
}

inline void setDebugLogLevel(Sgt::LogLevel lev)
{
    Sgt::debugLogLevel() = lev;
}

inline void setSolverOptions(Sgt::Network& netw, const Sgt::json& options)
{
    netw.solver().setOptions(options);
}

#endif // SGTPY_HELPER_DOT_H
