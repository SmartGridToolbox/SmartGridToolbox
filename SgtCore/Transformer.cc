// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "Transformer.h"

#include "Bus.h"

using namespace arma;
using namespace std;

namespace
{
    const double s3 = sqrt(3);
}

namespace Sgt
{
    namespace WT = WindingTypes;

    namespace
    {
        auto parseWindingType(const string& s, size_t& curIdx) -> WindingType {
            const map<char, WindingType> m{
                {'n', WT::N}, {'y', WT::Y}, {'d', WT::D}, {'z', WT::Z}, {'a', WT::A}, {'v', WT::V}
            };
            WindingType result{0};
            auto c = tolower(s[curIdx++]);
            try
            {
                result += m.at(c);
            }
            catch (const out_of_range& e)
            {
                sgtError("Bad transformer vector group " + s + ".");
            }
            if (tolower(s[curIdx]) == 'n')
            {
                result += WT::N;
                ++curIdx;
            }
            return result;
        };
    }

    VectorGroup::VectorGroup(const std::string& s)
    {
        size_t curIdx = 0;

        // Special cases:
        // TODO: Make nicer!
        if (s.size() < 3)
        {
            sgtError("Transformer vector group " << s 
                    << " must be at least 3 characters long, e.g. \"dd0\" or \"dyn11\""); 
        }
        if (s.substr(0, 2) == "vv")
        {
            wt0 = wt1 = WT::V;
            curIdx = 2;
        }
        else if (s.substr(0, 2) == "da")
        {
            wt0 = WT::D;
            wt1 = WT::D + WT::A;
            curIdx = 2;
        }
        else if (s.substr(0, 2) == "ya")
        {
            wt0 = WT::Y;
            wt1 = WT::Y + WT::A;
            curIdx = 2;
        }
        else
        {
            wt0 = parseWindingType(s, curIdx);
            wt1 = parseWindingType(s, curIdx);
        }
        try
        {
            shift = stoi(s.substr(curIdx, s.size() - curIdx));
        }
        catch(std::invalid_argument& e)
        {
            sgtError("Bad phase shift clock number in transformer vector group " << s 
                    << ". Vector group should resemble e.g. \"dd0\" or \"dyn11\""); 
        }
    }

    namespace
    {
        class SingleSideWs
        {
            public:

            static const SingleSideWs& instance()
            {
                static SingleSideWs instance;
                return instance;
            }

            const Mat<double> yn3{{{1,0,0,-1},{0,1,0,-1},{0,0,1,-1}}};
            const Mat<double> y3{{{1,0,0},{0,1,0},{0,0,1}}};
            const Mat<double> yn2{{{1,0,-1},{0,1,-1}}};
            const Mat<double> y2{{{1,0},{0,1}}};
            const Mat<double> yn1{{{1,-1}}};
            const Mat<double> y1{1, 1, fill::ones};
            const Mat<double> d3{{{1,-1,0},{0,1,-1},{-1,0,1}}};
            const Mat<double> d1{{{1,-1}}};

            private:

            SingleSideWs() = default;
        };

        Mat<double> getSingleSideW(const WindingType& wt, uword nWindPairs, bool isGrounded)
        {
            // Y/D  nWindPairs isGrounded  nTerm   Description
            // Y    3          F           4       3 winding Y with explicit ungrounded N
            // Y    3          T           3       3 winding Y with implicit grounded N
            // Y    2          F           3       2 winding Y with explicit ungrounded N
            // Y    2          T           2       2 winding Y with implicit grounded N
            // Y    1          F           2       1 winding Y with explicit ungrounded N
            // Y    1          T           1       1 winding Y with implicit grounded N
            // D    3          F           3       3 winding D, ungrounded
            // D    1          F           2       1 winding D, ungrounded
            const auto& ws = SingleSideWs::instance();

            if (wt & WT::Y)
            {
                if      (nWindPairs == 3 && !isGrounded) return ws.yn3;
                else if (nWindPairs == 3 && isGrounded) return ws.y3;
                else if (nWindPairs == 2 && !isGrounded) return ws.yn2;
                else if (nWindPairs == 2 && isGrounded) return ws.y2;
                else if (nWindPairs == 1 && !isGrounded) return ws.yn1;
                else if (nWindPairs == 1 && isGrounded) return ws.y1;
            }
            else if (wt & WT::D)
            {
                if      (nWindPairs == 3 && !isGrounded) return ws.d3;
                else if (nWindPairs == 1 && !isGrounded) return ws.d1;
            }
            sgtError("Bad transformer vector group specification for winding.");
        }

        Mat<double> getW(const Mat<double>& W0, const Mat<double>& W1)
        {
            auto nWindPairs = W0.n_rows;
            auto nTerm0 = W0.n_cols;
            auto nTerm1 = W1.n_cols;
            auto nTerm = nTerm0 + nTerm1;
            sgtAssert(W1.n_rows == nWindPairs, "Unexpected W matrix dimensions.");
            Mat<double> W = Mat<double>(2 * nWindPairs, nTerm, fill::zeros);
            for (auto i = 0u; i < nWindPairs; ++i)
            {
                W(2 * i, 0, arma::size(1, nTerm0)) = W0.row(i);
                W(2 * i + 1, nTerm0, arma::size(1, nTerm1)) = W1.row(i);
            }
            return W;
        }
    }

    TransformerWiring::TransformerWiring(const std::string& vecGpStr, arma::uword nWindPairs,
            bool isGrounded0, bool isGrounded1) :
        vecGpStr_{vecGpStr}
    {
        VectorGroup vg(vecGpStr);

        sgtAssert(((vg.wt0 & WT::Y) || !isGrounded0) && ((vg.wt1 & WT::Y) || !isGrounded1),
                "Grounded windings are only supported for wye transformers.");

        if (vg.wt1 & WT::A)
        {
            // Special case: autotransformers.
            if ((vg.wt0 & WT::Y) && vg.wt1 & WT::Y)
            {
                // YY autotransformer. 
                sgtAssert(nWindPairs == 3, "Wye autotransformers are currently only implemented for 3 winding pairs.");
                sgtAssert(isGrounded0 && isGrounded1,
                        "Wye autotransformers are currently only implemented for grounded neutral.");
                sgtAssert(vg.shift == 0, "Wye autotransformers are currently only implemented for shift 0.");
                // TODO: relax the previous assert.
                nTerm0_ = 3;
                nTerm1_ = 3;
                Mat<double> W0 = getSingleSideW(WT::Y, nWindPairs, isGrounded0);
                Mat<double> W1 = getSingleSideW(WT::Y, nWindPairs, isGrounded1);
                W_ = getW(W0, W1);
            }
            else if ((vg.wt0 & WT::D) && vg.wt1 & WT::D)
            {
                // D autotransformer. 
                sgtAssert(nWindPairs == 3,
                        "Delta autotransformers are currently only implemented for 3 winding pairs.");
                sgtAssert(vg.shift == 0,
                        "Delta autotransformers are currently only implemented for shift 0.");
                nTerm0_ = 3;
                nTerm1_ = 3;
                W_ = {
                    {1,-1,0,0,0,0},
                    {1,0,0,0,-1,0},
                    {0,1,-1,0,0,0},
                    {0,1,0,0,0,-1},
                    {-1,0,1,0,0,0},
                    {0,0,1,-1,0,0}
                };
            }
            else
            {
                sgtError("Unimplemented autotransformer " << vg.str);
            }
        }
        else if ((vg.wt0 & WT::V) && (vg.wt1 & WT::V))
        {
            // Special case: VV (open delta) transformer.
            sgtAssert(nWindPairs == 2, "VV (open delta) must have 2 winding pairs.");
            sgtAssert(vg.shift == 0, "VV (open delta) transformers are currently only implemented for shift 0.");

            nTerm0_ = 3;
            nTerm1_ = 3;

            W_ = {
                {1,-1,0,0,0,0},
                {0,0,0,1,-1,0},
                {0,-1,1,0,0,0},
                {0,0,0,0,-1,1}
            };
            ties_ = {{1,4}};
        }
        else
        {
            sgtAssert(!(vg.wt0 & (WT::A | WT::V)) && !(vg.wt1 & (WT::A | WT::V)),
                    "Unexpected special character \"a\" or \"v\" in vector group" << vg.str << ".");
            
            Mat<double> W0 = getSingleSideW(vg.wt0, nWindPairs, isGrounded0);
            Mat<double> W1 = getSingleSideW(vg.wt1, nWindPairs, isGrounded1);

            uword normalClock;
            if      ((vg.wt0 & WT::Y) && (vg.wt1 & WT::Y)) normalClock = 0; 
            else if ((vg.wt0 & WT::D) && (vg.wt1 & WT::D)) normalClock = 0; 
            else if ((vg.wt0 & WT::D) && (vg.wt1 & WT::Y)) normalClock = 11; 
            else if ((vg.wt0 & WT::Y) && (vg.wt1 & WT::D)) normalClock = 1; 
            else sgtError("Unknown combination of winding types.");
            int diffClock = (12 + vg.shift - normalClock) % 12;
            if (diffClock == 2)
            {
                // 60 degrees ahead, e.g. dyn1 or yy2.
                // ABC -> -BCA.
                W1.swap_cols(0, 1); // BAC 
                W1.swap_cols(1, 2); // BCA
                W1 *= -1;
            }
            else if (diffClock == 4)
            {
                // 120 degrees ahead, e.g. dyn3 or yy4.
                // ABC -> CAB.
                W1.swap_cols(0, 2); // CBA
                W1.swap_cols(1, 2); // CAB
            }
            else if (diffClock == 6)
            {
                // 180 degrees ahead, e.g. dyn5 or yy6.
                W1 *= -1;
            }
            else if (diffClock == 8)
            {
                // 120 degrees behind, e.g. dyn7 or yy8.
                // ABC -> BCA.
                W1.swap_cols(0, 1); // BAC 
                W1.swap_cols(1, 2); // BCA
            }
            else if (diffClock == 10)
            {
                // 60 degrees behind, e.g. dyn9 or yy10.
                // ABC -> -CAB.
                W1.swap_cols(0, 2); // CBA
                W1.swap_cols(1, 2); // CAB
                W1 *= -1;
            }
            else if (diffClock != 0)
            {
                sgtError("Unexpected vector group clock shift for " << vg.str << ".");
            }

            nTerm0_ = W0.n_cols;
            nTerm1_ = W1.n_cols;
            W_ = getW(W0, W1);
        }
    }

    TransformerWiring::TransformerWiring(uword nTerm0, uword nTerm1, const Mat<double>& W,
            const std::vector<std::array<arma::uword, 2>>& ties) :
        nTerm0_{nTerm0}, nTerm1_{nTerm1}, W_{W}, ties_{ties}
    {
        sgtAssert(W_.n_cols == nTerm0_ + nTerm1_, "Number of cols in W must be nTerm");
        sgtAssert(W_.n_cols % 2 == 0, "Number of cols in W must be even");
    }

    Mat<Complex> windingY(
            const Complex& turnsRatio, const Complex& ZSeries, const Complex& YShunt, const arma::uword impedanceSide)
    {
        Complex ai = 1.0 / turnsRatio;
        Complex aci = conj(ai);
        double a2i = 1.0 / norm(turnsRatio); // Note norm(x) -> |x|^2.

        // Refer possible primary impedance to secondary.
        Complex ZSerSec = (impedanceSide == 1) ? ZSeries : ZSeries * a2i;
        Complex YShSec = (impedanceSide == 1) ? YShunt : YShunt / a2i;

        Complex Y1 = 1.0 / ZSerSec;
        Complex Y2 = Y1 + YShSec;

        return {{Y2*a2i, -Y1*aci}, {-Y1*ai, Y2}};
    }

    Transformer::Transformer(const string& id, const TransformerWiring& wiring,
            const Complex& nomTurnsRatio, const Complex& ZSeries, const Complex& YShunt, arma::uword impedanceSide,
            int minTap, int maxTap, double tapFactor, unsigned int tapSide) :
        Component{id}, wiring_{wiring},
        nomTurnsRatio_{nomTurnsRatio}, ZSeries_{ZSeries}, YShunt_{YShunt}, impedanceSide_{impedanceSide},
        minTap_{minTap}, maxTap_{maxTap}, tapFactor_{tapFactor}, tapSide_{tapSide}
    {
        taps_ = Col<int>(wiring_.nWindPairs(), fill::zeros);
    }

    Mat<Complex> Transformer::inServiceY() const
    {
        ensureValid();
        return Y_;
    }
        
    Col<Complex> Transformer::offNomTurnsRatio() const
    {
        Col<Complex> result(wiring_.nWindPairs());
        for (uword i = 0; i < wiring_.nWindPairs(); ++i)
        {
            result[i] = taps_[i] * tapFactor_ + 1.0;
            if (tapSide_ == 1) result[i] = 1.0 / result[i];
        }
        return result;
    }
        
    void Transformer::setTaps(const Col<int>& taps)
    {
        taps_ = taps;
        checkTaps();
        invalidate();
    }
    
    void Transformer::setEqualTaps(int tap)
    {
        for (auto& tapI : taps_) tapI = tap;
        checkTaps();
        invalidate();
    }

    void Transformer::setTap(int tap, int idx)
    {
        taps_[idx] = tap;
        checkTaps();
        invalidate();
    }
        
    void Transformer::addTapChanger(
            const TcTap& tcTap,
            const TcInputWinding& tcInputWinding,
            double setpoint,
            double tolerance,
            bool hasLdc,
            Complex ZLdc,
            Complex ldcTopFactorI)
    {
        tapChangers_.emplace(tcTap,
                TapChanger{*this, tcTap, tcInputWinding, setpoint, tolerance, hasLdc, ZLdc, ldcTopFactorI});
    }
        
    std::vector<std::reference_wrapper<const TapChanger>> Transformer::tapChangers() const
    {
        std::vector<std::reference_wrapper<const TapChanger>> retval;
        retval.reserve(tapChangers_.size());
        for (const auto& tc: tapChangers_) retval.push_back(tc.second);
        return retval;
    }
        
    std::vector<std::reference_wrapper<TapChanger>> Transformer::tapChangers()
    {
        std::vector<std::reference_wrapper<TapChanger>> retval;
        retval.reserve(tapChangers_.size());
        for (auto& tc: tapChangers_) retval.push_back(tc.second);
        return retval;
    }
        
    bool Transformer::runTapChangersOnce()
    {
        bool changed = false;
        for (auto & tc : tapChangers_) changed = changed || tc.second.runOnce();
        return changed;
    }

    array<Col<double>, 2> Transformer::VWindingBase() const
    {
        auto txBuses = buses();
        Col<Complex> termNomV;
        for (auto iSide = 0u; iSide < 2; ++iSide)
        {
            termNomV = join_vert(
                    termNomV, mapPhases(txBuses[iSide]->VNom(), txBuses[iSide]->phases(), phases()[iSide]));
        }
        Col<double> windNomV = abs(wiring_.W() * termNomV);
        return 
        {
            windNomV.subvec(0, wiring_.nWindPairs() - 1),
            windNomV.subvec(wiring_.nWindPairs(), 2 * wiring_.nWindPairs() - 1)
        };
    }
        
    std::array<arma::Col<Complex>, 2> Transformer::VWindings() const
    {
        ensureValid();
        std::array<arma::Col<Complex>, 2> retval{
            Col<Complex>{wiring_.nWindPairs(), fill::none}, Col<Complex>{wiring_.nWindPairs(), fill::none}};
        auto vt = VTerm();
        auto vtj = join_vert(vt[0], vt[1]);
        Col<Complex> vw = wiring_.W() * vtj;
        for (auto i = 0u; i < wiring_.nWindPairs(); ++i)
        {
            retval[0](i) = vw(2 * i);
            retval[1](i) = vw(2 * i + 1);
        }
        return retval;
    }

    std::array<arma::Col<Complex>, 2> Transformer::IWindings() const
    {
        ensureValid();
        std::array<arma::Col<Complex>, 2> retval{
            Col<Complex>{wiring_.nWindPairs(), fill::none}, Col<Complex>{wiring_.nWindPairs(), fill::none}};
        const auto vWind = VWindings();
        for (auto i = 0u; i < wiring_.nWindPairs(); ++i)
        {
            Col<Complex> vps{vWind[0](i), vWind[1](i)};
            Col<Complex> ips = windingYs_[i] * vps;
            retval[0](i) = ips[0];
            retval[1](i) = ips[1];
        }
        return retval;
    }
    
    void Transformer::invalidate() const
    {
        isValid_ = false;
        admittanceChanged().trigger();
        // TODO: Is this the correct point to do this? Or in ensureValid? Having it here replicates
        // most previous behaviour.
    }

    void Transformer::ensureValid() const
    {
        if (!isValid_)
        {
            auto a = turnsRatio();
            windingYs_.resize(wiring_.nWindPairs());
            for (auto iWind = 0u; iWind < wiring_.nWindPairs(); ++iWind)
            {
                windingYs_[iWind] = windingY(a[iWind], ZSeries_, YShunt_, impedanceSide_);
            }
            calcAndSetY();
            isValid_ = true;
        }
    }

    void Transformer::calcAndSetY() const
    {
        auto nt0 = wiring_.nTerm0();
        auto nt1 = wiring_.nTerm1();
        auto nt = nt0 + nt1;
        auto nw = wiring_.nWindPairs();

        Y_ = Mat<Complex>(nt, nt, fill::none);

        Mat<Complex> Yw(2 * nw, 2 * nw, fill::zeros);
        for (auto iw = 0u; iw < nw; ++ iw)
        {
            Yw(2 * iw, 2 * iw, arma::size(2, 2)) = windingYs_[iw];
        }
        Y_ = wiring_.W().t() * Yw * wiring_.W();

        for (const auto& tie : wiring_.ties())
        {
            auto i0 = tie[0];
            auto i1 = tie[1];
            Y_(i0, i0) += Y_(i0, i1);
            Y_(i0, i1) = 0;
            Y_(i1, i1) += Y_(i1, i0);
            Y_(i1, i0) = 0;
        }
    }

    void Transformer::checkTaps() const
    {
        sgtAssert(taps_.size() == wiring_.nWindPairs(),
                "The number of transformer taps must be the same as the number of winding pairs.");

        for (const auto& tap: taps_)
        {
            sgtAssert(tap >= minTap_ && tap <= maxTap_, "Transformer taps must lie between min_tap and max_tap.");
        }
    }

    TapChanger::TapChanger(
            Transformer& transformer,
            const TcTap& tcTap,
            const TcInputWinding& tcInputWinding,
            double setpoint,
            double tolerance,
            bool hasLdc,
            Complex ZLdc,
            Complex ldcTopFactorI) :
        trans_(transformer),
        tcTap_(tcTap),
        tcInputWinding_(tcInputWinding),
        setpoint_(setpoint),
        tolerance_(tolerance),
        hasLdc_(hasLdc),
        ZLdc_(ZLdc),
        ldcTopFactorI_(ldcTopFactorI)
    {
        // Empty.
    }
        
    int TapChanger::tap() const
    {
        return trans_.taps()[tcTap_.tapIdx];
    }

    void TapChanger::setTap(int tap)
    {
        if (tcTap_.type == TcTap::Type::SINGLE)
        {
            trans_.setTap(tap, tcTap_.tapIdx);
        }
        else
        {
            trans_.setEqualTaps(tap);
        }
    }

    double TapChanger::ctrlV() const
    {
        // TODO: consider the way the averaging works with phasing here. What is the most realistic way to do things?
        // The use of complex numbers adds some complications.
        auto VWindings = trans_.VWindings();
        auto IWindings = trans_.IWindings();
        switch (tcInputWinding_.type)
        {
            case TcInputWinding::Type::SINGLE:
                {
                    const auto wi = tcInputWinding_.idx;
                    auto V = (tcInputWinding_.side == 0 ? VWindings[0][wi] : VWindings[1][wi]);
                    if (hasLdc_)
                    {
                        const auto I = (tcInputWinding_.side == 0
                                ? IWindings[0][wi] : IWindings[1][wi]) * ldcTopFactorI_;
                        V = V - I * ZLdc_;
                    }
                    return abs(V);
                }
                break;
            case TcInputWinding::Type::AVG:
                {
                    auto V = (tcInputWinding_.side == 0 ? VWindings[0] : VWindings[1]);
                    if (hasLdc_)
                    {
                        auto I = (tcInputWinding_.side == 0
                                ? IWindings[0] : IWindings[1]) * ldcTopFactorI_;
                        V = V - I * ZLdc_;
                    }
                    return mean(abs(V));
                }
                break;
        }
        return 0.0; // Silence compiler warning. 
    }

    TcState TapChanger::state() const
    {
        auto cv = ctrlV();
        if (cv < setpoint_ - tolerance_) return TcState::BELOW;
        else if (cv > setpoint_ + tolerance_) return TcState::ABOVE;
        else return TcState::WITHIN;
    }

    int TapChanger::sign() const
    {
        return trans_.tapSide() == tcInputWinding_.side ? 1 : -1; // Lower (-1) or raise (1) tap to raise V?
    }
        
    // TODO: what if the deadband is too small and we're alternating taps?
    bool TapChanger::runOnce()
    {
        bool didChange = false;
        auto st = state();
        sgtLogDebug() << "TapChanger: " << trans_.id() << " : " << ctrlV() << " : " << static_cast<int>(st) << endl;
        if (st != TcState::WITHIN)
        {
            sgtLogDebug() << trans_.id() << " : " << (st == TcState::ABOVE ? "Overvoltage" : "Undervoltage") << endl;
            int sgn = sign();
            auto curTap = tap();
            bool increaseTap = ((sgn == 1 && st == TcState::BELOW) || (sgn == -1 && st == TcState::ABOVE));
            sgtLogDebug() << trans_.id() 
                << " : curTap = " << curTap << " sgn = " << sgn << " increaseTap = " << increaseTap << std::endl;
            if (increaseTap && curTap < trans_.maxTap())
            {
                setTap(curTap + 1);
                didChange = true;
            }
            else if (!increaseTap && curTap > trans_.minTap())
            {
                    sgtLogDebug() << trans_.id() << " : Decrease tap" << std::endl;
                    setTap(curTap - 1);
                    didChange = true;
            }
            else
            {
                sgtLogDebug() << trans_.id() << " : No more taps" << std::endl;
            }
        }
        else
        {
            sgtLogDebug() << trans_.id() << " : Within tolerance" << std::endl;
        }
        return didChange;
    }
}
