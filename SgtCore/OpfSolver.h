// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef OPF_SOLVER_DOT_H
#define OPF_SOLVER_DOT_H

#include<SgtCore/Common.h>

#include <madopt/common.hpp>
#include <madopt/ipopt_model.hpp>

namespace Sgt
{
    namespace OpfUtil
    {
        inline void printVar(const MadOpt::Var& v)
        {
            using namespace std;
            
            MadOpt::Var& vnc = const_cast<MadOpt::Var&>(v); // KLUDGE to get around MadOpt const error.
            sgtLogMessage()
                << std::setw(30) << std::left << v.name() << " "
                << std::setw(15) << std::left << vnc.lb() << " "
                << std::setw(15) << std::left << vnc.ub() << " "
                << std::setw(15) << std::left << vnc.init() << " "
                << std::setw(15) << std::left << vnc.v() << " "
                << std::endl;
        }
    }
            
    using NlpModel = MadOpt::IpoptModel;

    struct ConstraintSpec
    {
        std::string name{"UNDEFINED"};
        double lb{-infinity};
        double ub{infinity};
        MadOpt::Expr expr{0.0};
        MadOpt::Constraint constr;

        ConstraintSpec(const std::string& nameArg, const double lbArg, const double ubArg,
                const MadOpt::Expr& exprArg = MadOpt::Expr(0.0))
        {
            name = nameArg;
            lb = lbArg; 
            ub = ubArg; 
            expr = exprArg; 
        };
        
        ConstraintSpec(const ConstraintSpec& from) = delete;
        ConstraintSpec(ConstraintSpec&& from) = default;

        void addToNlp(MadOpt::Model& madOptMod)
        {
            // sgtLogMessage() << std::setw(12) << "Add constr:"; print();
            constr = madOptMod.addConstr(lb, expr, ub);
        }

        void print() const
        {
            sgtLogMessage() 
                << std::setw(30) << std::left << name << " "
                << std::setw(15) << std::left << lb << " "
                << std::setw(15) << std::left << ub << "    "
                << std::left << expr 
                << std::endl;
        }
    };

    /// @brief Base class for OPF solver.
    ///
    /// This is an abstract base class, with no particular assumed OPF.
    /// Derived classes should override the virtual abstract functions buildNlp and propagateSolutionToModel.
    /// @ingroup PowerFlowCore
    template<typename Nlp>
    class OpfSolver
    {
        public:
        
        bool debug{false}; // Show debug information.
        bool showSolver{false}; // Show solver output.
        bool pfMode{false};

        OpfSolver() {}

        virtual ~OpfSolver() = default;

        bool solveOpf(const Network& netw)
        {
            using namespace std;
            using namespace arma;
            using namespace OpfUtil;

            nlp_.reset(new Nlp()); // Assumes default constructable.
            nlp_->nlpMod_.show_solver = showSolver;

            buildNlp(netw);

            addConstraintsAndObjective();

            if (debug)
            {
                sgtLogMessage() << "Before solve:" << endl;
                print();
            }

            nlp_->nlpMod_.solve();

            bool ok = 
                (nlp_->nlpMod_.status() == MadOpt::Solution::SolverStatus::SUCCESS) ||
                (nlp_->nlpMod_.status() == MadOpt::Solution::SolverStatus::STOP_AT_ACCEPTABLE_POINT);

            if (debug)
            {
                sgtLogMessage() << "After solve:" << endl;
                sgtLogMessage() << "OK = " << (ok ? "true" : "false") << endl;
                if (ok) print();
            }

            if (ok)
            {
                applySolution();
            }

            return ok;
        }

        virtual void setOpts(const json& options)
        {
            const auto& dbgOpt = options.find("debug");
            if (dbgOpt != options.end())
            {
                sgtAssert(dbgOpt.value().is_boolean(), "opf_s_pol_solver's debug option must be a bool");
                debug = dbgOpt.value().get<bool>();
            }
            const auto& showSolverOpt = options.find("show_solver");
            if (showSolverOpt != options.end())
            {
                sgtAssert(dbgOpt.value().is_boolean(), "opf_s_pol_solver's show_solver option must be a bool");
                showSolver = showSolverOpt.value().get<bool>();
            }
            const auto& pfModeOpt = options.find("pf_mode");
            if (pfModeOpt != options.end())
            {
                sgtAssert(pfModeOpt.value().is_boolean(), "opf_s_pol_solver's pf_mode option must be a bool");
                pfMode = pfModeOpt.value().get<bool>();
            }
        }

        double objectiveValue() const
        {
            return nlp_->nlpMod_.objValue();
        }

        protected:

        virtual void buildNlp(const Network& netw) = 0;
        virtual void addConstraintsAndObjective() = 0;
        virtual void applySolution() = 0;
        virtual void print() const = 0;
        
        std::unique_ptr<Nlp> nlp_;
    };
}

#endif // OPF_SOLVER_DOT_H
