// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "Zip.h"

#include "Bus.h"

#include <ostream>

using namespace arma;

namespace Sgt
{
    Mat<uword> Zip::wyeConnection(uword nPhase)
    {
        Mat<uword> result(nPhase, 2, fill::none);
        for (uword i = 0; i < nPhase; ++i) result(i, 0) = result(i, 1) = i;
        return result;
    }

    Mat<uword> Zip::deltaConnection(uword nPhase)
    {
        switch (nPhase)
        {
            case 2:
                return {{0, 1}};
            case 3:
                return {{0, 1}, {1, 2}, {0, 2}};
            default:
                sgtError("Zip::deltaConnection requires either 2 or three phases.");
        }
    }

    Zip::Zip(const std::string& id, unsigned int nPhases, const Mat<uword>& connection) :
        Component(id),
        nPhases_(nPhases),
        connection_(connection),
        YConst_(nComps(), fill::zeros),
        IConst_(nComps(), fill::zeros),
        SConst_(nComps(), fill::zeros),
        phases_(nPhases) // Make sure size is correct just in case.
    {
        sgtAssert(connection_.n_cols == 2, "Zip constructor: connection must be an n x 2 matrix.");
        for (uword i = 0; i < nComps(); ++i)
        {
            if (connection_(i, 0) > connection_(i, 1)) std::swap(connection_(i, 0), connection_(i, 1));
        }
    }

    json Zip::toJson() const
    {
        json j = Component::toJson();
		j[sComponentType()] = {
			{"phases", phases()},
			{"connection", connection()},
			{"YConst", YConst()},
			{"IConst", IConst()},
			{"SConst", SConst()}};
		return j;
	}

    Mat<Complex> Zip::compsToMat(const arma::Col<Complex>& comps) const
    {
        Mat<Complex> result(nPhases_, nPhases_, fill::zeros);
        for (uword i = 0; i < nComps(); ++i) result(connection_(i, 0), connection_(i, 1)) = comps(i);
        return result;
    }

    void Zip::setYConst(const arma::Col<Complex>& YConst)
    {
        sgtAssert(YConst.size() == nComps(), "For Zip " << id() << " setYConst(YConst), YConst must have "
                << nComps() << " elements, but " << YConst.size() << " were supplied.");
        YConst_ = YConst;
        injectionChanged_.trigger();
    }

    void Zip::setIConst(const arma::Col<Complex>& IConst)
    {
        sgtAssert(IConst.size() == nComps(), "For Zip " << id() << " setIConst(IConst), IConst must have "
                << nComps() << " elements, but " << IConst.size() << " were supplied.");
        IConst_ = IConst;
        injectionChanged_.trigger();
    }

    void Zip::setSConst(const arma::Col<Complex>& SConst)
    {
        sgtAssert(SConst.size() == nComps(), "For Zip " << id() << " setSConst(SConst), SConst must have "
                << nComps() << " elements, but " << SConst.size() << " were supplied.");
        SConst_ = SConst;
        injectionChanged_.trigger();
    }

    Col<Complex> Zip::inServiceS() const
    {
        Col<Complex> V = mapPhases(bus_->V(), bus_->phases(), phases_);

        Col<Complex> SYConst = conj(YConst());
        Col<Complex> SIConst = conj(IConst());
        for (uword iComp = 0; iComp < nComps(); ++iComp)
        {
            uword i = connection_(iComp, 0);
            uword k = connection_(iComp, 1);
            Complex Vik = i == k ? V(i) : V(i) - V(k);
            SYConst(iComp) *= norm(Vik); // std::norm gives |Vik|^2
            SIConst(iComp) *= abs(Vik); // std::abs gives |Vik|
        }
        return SYConst + SIConst + SConst();
    }
        
    Col<Complex> Zip::ITerm() const
    {
        Col<Complex> result(nPhases_, fill::zeros);
        Col<Complex> VConj = conj(VTerm());
        Col<Complex> SConj = conj(S());
        for (uword iComp = 0; iComp < nComps(); ++iComp)
        {
            auto i = connection_(iComp, 0);
            auto k = connection_(iComp, 1);
            Complex VikConj = i == k ? VConj(i) : VConj(i) - VConj(k);
            Complex SikConj = SConj(iComp);
            Complex Iik = SikConj / VikConj;
            result(i) += Iik;
            if (k != i) result(k) -= Iik;
        }
        return result;
    }
}
