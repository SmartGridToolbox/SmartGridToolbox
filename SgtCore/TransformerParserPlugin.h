// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TRANSFORMER_PARSER_PLUGIN
#define TRANSFORMER_PARSER_PLUGIN

#include <SgtCore/NetworkParser.h>

namespace Sgt
{
    class Transformer;
    class Network;

    /// @addtogroup NetworkYamlSpec
    /// @{
    /// <b>YAML schema for `transformer` keyword.</b>
    ///
    /// The `transformer` keyword adds a new Transformer to the Network.
    ///
    /// ~~~{.yaml}
    /// - transformer:
    ///     id:                     <string>    # Unique id of component.
    ///     bus_0_id:               <string>    # ID of bus 0 of the branch.
    ///     bus_1_id:               <string>    # ID of bus 0 of the branch.
    ///     phases_0:               <phases>    # Phases of connection to bus_0, must be size 3, e.g. [A, B, C].
    ///     phases_1:               <phases>    # Phases of connection to bus_1, must be size 3, e.g. [A, B, C].
    ///     nom_turns_ratio:        <complex>   # Nominal turns ratio.
    ///     series_impedance:       <complex>   # Series (leakage) impedance parameter.
    ///     shunt_admittance:    <complex>      # Optional shunt (magnetising) admittance parameter.
    ///     min_tap:                <int>       # Optional minimum tap, default = 0
    ///     max_tap:                <int>       # Optional maximum tap, default = 0
    ///     tap_factor:             <float>     # Optional tap factor, default = 0.0
    /// ~~~
    /// @}

    /// @brief Parses the `transformer` keyword, adding a Transformer to the network.
    /// @ingroup NetworkParserPlugins
    class TransformerParserPlugin : public NetworkParserPlugin
    {
        public:
        virtual const char* key() const override
        {
            return "transformer";
        }

        virtual void parse(const YAML::Node& nd, Network& netw, const ParserBase& parser) const override;
    };
}

#endif // TRANSFORMER_PARSER_PLUGIN
