// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef POWER_FLOW_SOLVER_DOT_H
#define POWER_FLOW_SOLVER_DOT_H

#include <SgtCore/Common.h>

namespace Sgt
{
    class Network;

    /// @brief  for power flow solvers operating on Network objects.
    /// @ingroup PowerFlowCore
    class PowerFlowSolver
    {
        public:
        virtual ~PowerFlowSolver() = default;
        
        /// @brief Solve for the given network.
        virtual bool solve(Network& netw) = 0;

        /// @brief Set JSON solver options.
        ///
        /// return true if an option was actually set.
        virtual void setOptions(const json& options)
        {
            // Do nothing by default.
        }
    };
};

#endif // POWER_FLOW_SOLVER_DOT_H
