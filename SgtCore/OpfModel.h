// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef OPF_MODEL_DOT_H
#define OPF_MODEL_DOT_H

#include <SgtCore/Common.h>
#include <SgtCore/PowerFlow.h>

#include <string>
#include <vector>

namespace Sgt
{
    struct OpfBus
    {
        std::string id;
        std::size_t idx;

        arma::Col<Complex> V;
        Array<double, 2> vBnds;
        bool isAngleRef;
        bool hasAvr;
        
        std::size_t nPhase() const
        {
            return V.size();
        }
    };

    struct OpfZip
    {
        std::string id;
        std::size_t idx;

        std::string busId;
        std::vector<std::size_t> busPhIdxs;
        OpfBus* bus;

        arma::Mat<arma::uword> connection;
        arma::Col<Complex> YConst;
        arma::Col<Complex> IConst;
        arma::Col<Complex> SConst;

        std::size_t nComps() const
        {
            return connection.n_rows;
        }
        
        std::size_t nPhase() const
        {
            return busPhIdxs.size();
        }
    };

    struct OpfGen
    {
        std::string id;
        std::size_t idx;

        std::string busId;
        std::vector<std::size_t> busPhIdxs;

        OpfBus* bus;

        arma::Col<Complex> SGen; 
        Array<double, 2> pGenBnds; 
        Array<double, 2> qGenBnds; 
        Array<double, 2> sGenBnds;
        double pCostLin;
        double pCostQuad;

        std::size_t nPhase() const
        {
            return busPhIdxs.size();
        }
    };

    struct OpfBranch
    {
        std::string id;
        std::size_t idx;

        Array<std::string, 2> busIds;
        Array<std::vector<std::size_t>, 2> busPhIdxs;
        Array<OpfBus*, 2> buses;

        arma::Mat<Complex> YNd;

        // SMagMax and IMagMax are expected to be compatible and (typically) to both be defined.
        // SMagMax is used for power-based OPF formulations, and IMagMax for current-based formulations.
        // Typically, IMagMax = SMagMax / VBase, where VBase is taken from the bus at the given terminal.
        double SMagMax;
        Array<double, 2> IMagMax;

        std::size_t nPhase(int sideIdx) const
        {
            return busPhIdxs[sideIdx].size();
        }
    };

    /// @brief A mathematical model of a network, suitable for solving the AC OPF problem.
    /// @ingroup OpfCore
    class OpfModel
    {
        public:

        /// @name Lifecycle
        /// @{

        OpfModel();

        void addBus(const std::string& id, const arma::Col<Complex>& V,
                const double vMin, const double vMax, bool isAngleRef, bool hasAvr);
        
        void addZip(const std::string& id, const std::string& busId,
                const std::vector<std::size_t>& busPhIdxs, const arma::Mat<arma::uword>& connection,
                const arma::Col<Complex>& YConst, const arma::Col<Complex>& IConst, const arma::Col<Complex>& SConst);

        void addGen(const std::string& id, const std::string& busId,
                const std::vector<std::size_t>& busPhIdxs, 
                const arma::Col<Complex>& SGen, const double pGenMin, const double pGenMax,
                const double qGenMin, const double qGenMax, const double sGenMin, const double sGenMax,
                const double pCostLin, const double pCostQuad);

        void addBranch(const std::string& id,
                const std::string& busId0, const std::vector<std::size_t>& busPhIdxs0,
                const std::string& busId1, const std::vector<std::size_t>& busPhIdxs1,
                const arma::Mat<Complex>& YNd, const double SMagMax, const double IMagMax0, const double IMagMax1);

        void validate();

        /// @}

        /// @name Model accessors.
        /// @{
        
        const std::vector<OpfBus>& buses() const {return buses_;}
        std::vector<OpfBus>& buses() {return buses_;}
        const OpfBus& bus(const std::string& id) const {return *busMap_.at(id);}
        OpfBus& bus(const std::string& id) {return *busMap_.at(id);}
        
        const std::vector<OpfZip>& zips() const {return zips_;}
        std::vector<OpfZip>& zips() {return zips_;}
        const OpfZip& zip(const std::string& id) const {return *zipMap_.at(id);}
        OpfZip& zip(const std::string& id) {return *zipMap_.at(id);}
        
        const std::vector<OpfGen>& gens() const {return gens_;}
        std::vector<OpfGen>& gens() {return gens_;}
        const OpfGen& gen(const std::string& id) const {return *genMap_.at(id);}
        OpfGen& gen(const std::string& id) {return *genMap_.at(id);}

        const std::vector<OpfBranch>& branches() const {return branches_;}
        std::vector<OpfBranch>& branches() {return branches_;}
        const OpfBranch& branch(const std::string& id) const {return *branchMap_.at(id);}
        OpfBranch& branch(const std::string& id) {return *branchMap_.at(id);}
        
        /// @}

        private:
        
        std::vector<OpfBus> buses_;
        std::map<std::string, OpfBus*> busMap_;

        std::vector<OpfZip> zips_;
        std::map<std::string, OpfZip*> zipMap_;

        std::vector<OpfGen> gens_;
        std::map<std::string, OpfGen*> genMap_;

        std::vector<OpfBranch> branches_;
        std::map<std::string, OpfBranch*> branchMap_;
    };
}

#endif // OPF_MODEL_DOT_H
