// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "Network.h"

#include "PowerFlowNrRectSolver.h"
#include "Transformer.h"
#include "Zip.h"

#include <numeric>

using namespace arma;

namespace Sgt
{
    namespace
    {
        void islandDfs(ComponentPtr<Bus> bus, Island& island)
        {
            if (bus->islandIdx() != -1 || !bus->isInService()) return;

            bus->setIslandIdx(island.idx);
            island.buses.push_back(bus);
            for (auto branch : bus->branches0())
            {
                if (branch->isInService()) islandDfs(branch->bus1(), island);
            }
            for (auto branch : bus->branches1())
            {
                if (branch->isInService()) islandDfs(branch->bus0(), island);
            }
        }
    }

    Network::Network(double PBase, const Units& units) : PBase_(PBase), units_(units),
                     solver_(new PowerFlowNrRectSolver)
    {
        // Empty.
    }

    ComponentPtr<BranchAbc> Network::addBranch(std::shared_ptr<BranchAbc> branch,
            const std::string& bus0Id, const Phases& phases0,
            const std::string& bus1Id, const Phases& phases1)
    {
        auto bus0 = getConnection(bus0Id, *branch, branch->nPhases0(), phases0); 
        auto bus1 = getConnection(bus1Id, *branch, branch->nPhases1(), phases1); 
        
        branch->bus0_ = bus0;
        branch->phases0_ = phases0;

        branch->bus1_ = bus1;
        branch->phases1_ = phases1;

        bus0->branches0_.insert(branch->id(), branch);
        bus1->branches1_.insert(branch->id(), branch);

        return branches_.insert(branch->id(), branch);
    }

    ComponentPtr<Gen> Network::addGen(std::shared_ptr<Gen> gen, const std::string& busId, const Phases& phases)
    {
        auto bus = getConnection(busId, *gen, gen->nPhases(), phases); 

        gen->bus_ = bus;
        gen->phases_ = phases;

        bus->gens_.insert(gen->id(), gen);

        return gens_.insert(gen->id(), gen);
    }

    ComponentPtr<Zip> Network::addZip(std::shared_ptr<Zip> zip, const std::string& busId, const Phases& phases)
    {
        auto bus = getConnection(busId, *zip, zip->nPhases(), phases); 

        zip->bus_ = bus;
        zip->phases_ = phases;

        bus->zips_.insert(zip->id(), zip);
        
        return zips_.insert(zip->id(), zip);
    }

    Mat<Complex> Network::YBus2Pu(const Mat<Complex>& YBus,
            double VBase0, double VBase1, uword n0) const
    {
        Mat<Complex> result{YBus};
        if (n0 == 0) n0 = YBus.n_rows / 2;
        uword n1 = YBus.n_rows - n0;
        result.head_rows(n0) *= VBase0;
        result.head_cols(n0) *= VBase0;
        result.tail_rows(n1) *= VBase1;
        result.tail_cols(n1) *= VBase1;
        result /= PBase_;
        return result;
    }

    Mat<Complex> Network::pu2YBus(const Mat<Complex>& YBus,
            double VBase0, double VBase1, uword n0) const
    {
        Mat<Complex> result{YBus};
        if (n0 == 0) n0 = YBus.n_rows / 2;
        uword n1 = YBus.n_rows - n0;
        result.head_rows(n0) /= VBase0;
        result.head_cols(n0) /= VBase0;
        result.tail_rows(n1) /= VBase1;
        result.tail_cols(n1) /= VBase1;
        result *= PBase_;
        return result;
    }

    void Network::applyFlatStart()
    {
        for (auto bus : buses())
        {
            bus->V_ = bus->VNom();
            for (auto& vi : bus->V_) if (abs(vi) < 1e-6) vi = 1e-6; // TODO: zero caused numerical problems in solver.

            for (auto gen : bus->gens())
            {
                switch (bus->type())
                {
                    case BusType::SL:
                        {
                            // Set SGen to 0.
                            gen->setInServiceS(Col<Complex>(gen->nPhases(), fill::zeros));
                            break;
                        }
                    case BusType::PV:
                        {
                            // Set QGen to 0.
                            auto S = gen->inServiceS();
                            S.set_imag(zeros(gen->nPhases()));
                            gen->setInServiceS(S);
                            break;
                        }
                    default: 
                        {
                            break;
                        }
                }
            }
        }
    }

    void Network::solvePreprocess(bool useFlatStart)
    {
        // Flat start.
        if (useFlatStart)
        {
            applyFlatStart();
        }

        // Islands.
        findIslands();
        handleIslands();
    }

    bool Network::solvePowerFlow(bool useFlatStart)
    {
        sgtLogDebug() << "Network : solving power flow." << std::endl;
        sgtLogDebug(LogLevel::VERBOSE) << *this << std::endl;

        // Preprocess.
        solvePreprocess(useFlatStart);

        // Solve and update network.
        isValidSolution_ = solver_->solve(*this);
        if (!isValidSolution_)
        {
            sgtLogWarning() << "Couldn't solve power flow model" << std::endl;
        }
        
        // Postprocess.
        // TODO.

        return isValidSolution_;
    }
            
    double Network::genCostPerUnitTime()
    {
        return std::accumulate(gens_.begin(), gens_.end(), 0.0, 
                [](double d, Gen* g)->double{return d + g->cost();});
    }
            
    void Network::findIslands()
    {
        std::map<std::string, ComponentPtr<Bus>> remaining; 
        for (auto bus : buses_) remaining[bus->id()] = bus;

        // Step 1: Initialize.
        islands_.clear();
        for (auto bus : buses()) bus->islandIdx_ = -1;
        int curIdx = 0;

        // Step 2: DFS from all in-service buses with a working generator.
        for (auto gen : gens())
        {
            if (!gen->isInService()) continue;
            auto bus = gen->bus();
            if (!bus->isInService()) continue;

            Island island{curIdx, true, {}};
            islandDfs(bus, island);
            if (island.buses.size() > 0)
            {
                for (auto b : island.buses) remaining.erase(b->id());
                islands_.push_back(std::move(island));
                ++curIdx;
            }
        }
        
        // Step 3: Do all unsupplied and out-of-service buses.
        while (!remaining.empty())
        {
            auto bus = remaining.begin()->second;
            if (!bus->isInService())
            { 
                // Out of service buses get their own island.
                islands_.push_back({curIdx++, false, {bus}});
                remaining.erase(bus->id());
            }
            else
            {
                // Should be unsupplied. Note that the island will contain at least one bus.
                islands_.push_back({curIdx++, false, {}});
                islandDfs(bus, islands_.back());
                for (auto b : islands_.back().buses) remaining.erase(b->id());
            }
        }
    }
            
    void Network::handleIslands()
    {
        for (auto island : islands_)
        {
            for (auto bus : island.buses)
            {
                bus->setIsSupplied(island.isSupplied);
                if (!island.isSupplied)
                {
                    bus->setV(Col<Complex>(bus->V().size(), fill::zeros));
                    bus->setSGenUnserved(bus->SGenRequested());
                    bus->setSZipUnserved(bus->SZipRequested());
                }
            }
        }
    }
    
    json Network::toJson() const
    {
        json j;
        j["network"] = {
            {"p_base", PBase()},
            {"buses", buses()},
            {"branches", branches()},
            {"gens", gens()},
            {"zips", zips()}
        };
        return j;
    }

    ComponentPtr<Bus> Network::getConnection(const std::string& busId, const Component& comp,
            std::size_t nPhases, const Phases& phases)
    {
        auto bus = buses_[busId];

        sgtAssert(bus != nullptr, 
                "Connecting " << comp.componentType() << " " << comp.id() << " to bus " << busId
                << ". Bus was not found in the network.");
        sgtAssert(phases.size() == nPhases,
                "Connecting " << comp.componentType() << " " << comp.id() << ", with " << nPhases
                << " phases, to phases " << phases << " of bus " << bus->id()
                << ", which has phases " << bus->phases() << ". "
                << "Wrong number of phases in connection."); 
        for (const auto& p : phases)
        {
            sgtAssert(bus->phases().hasPhase(p), 
                    "Connecting " << comp.componentType() << " " << comp.id() << ", with " << nPhases
                    << " phases, to phases " << phases << " of bus " << bus->id()
                    << ", which has phases " << bus->phases() << ". "
                    << "Phase " << p << " was not found in the bus."); 
        }

        return bus;
    }
    
    bool runTapChangers(Network& netw, unsigned int maxIter)
    {
        sgtLogDebug() << "runTapChangers: started" << std::endl;
        std::list<ComponentPtr<BranchAbc, Transformer>> txs;
        for (auto b: netw.branches())
        {
            auto tx = b.as<Transformer>();
            if (tx != nullptr) txs.push_back(tx);
        }
        bool changed = true;
        bool anyChanged = false;
        for (auto i = 0u; changed && i < maxIter; ++i)
        {
            sgtLogDebug() << "    runTapChangers iteration" << std::endl;
            changed = false;
            netw.solvePowerFlow();
            for (auto tx: txs)
            {
                if (tx->runTapChangersOnce()) changed = true;
            }
            if (changed) anyChanged = true;
        }
        sgtLogDebug() << "runTapChangers: finished" << std::endl;
        return anyChanged;
    }
}
