// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SgtCore/config.h>

#include "Network.h"
#include "PfModelNetworkInterface.h"
#include "PowerFlowNrRectSolver.h"
#include "SparseHelper.h"
#ifdef WITH_KLU
#include "KluSolver.h"
#endif
#include "Stopwatch.h"

#include <algorithm>
#include <ostream>
#include <sstream>

using namespace Sgt;
using namespace arma;

namespace
{
    template<typename T, typename U> void initJcBlock(const T& Y, U& JVr, U& JVi)
    {
        JVr = -Y;
        JVi =  Complex(0, -1) * Y;
    }
}

namespace Sgt
{
    Jacobian::Jacobian(uword nPq, uword nPv)
    {
        uword nPqPv = nPq + nPv;
        dDdVr = SpMat<Complex>(nPqPv, nPqPv);
        dDdVi = SpMat<Complex>(nPqPv, nPqPv);
        dDPvdQPv = SpMat<Complex>(nPv, nPv);
    }

    bool PowerFlowNrRectSolver::solve(Network& netw)
    {
        sgtLogDebug() << "PowerFlowNrRectSolver : solve." << std::endl;
        sgtLogIndent();

        netw_ = &netw;
        bool ok = true;
        for (auto& nwIsland : netw.islands())
        {
            if (nwIsland.isSupplied)
            {
                mod_ = buildPfModel(*netw_, analyseTopology,
                        [nwIsland](const Bus& b){return b.islandIdx() == nwIsland.idx;});
                for (auto& pfIsland : mod_->islands())
                {
                    pfIsland_ = &pfIsland;
                    ok = solve() && ok;
                }
            }
        }
        return ok;
    }
        
    void PowerFlowNrRectSolver::setOptions(const json& options)
    {
        auto tolIter = options.find("tol");
        if (tolIter != options.end())
        {
            tol = tolIter->get<double>();
        }
        
        auto maxIterIter = options.find("max_iter");
        if (maxIterIter != options.end())
        {
            maxIter = maxIterIter->get<unsigned int>();
        }
        
        auto analyseTopologyIter = options.find("analyse_topology");
        if (analyseTopologyIter != options.end())
        {
            analyseTopology = analyseTopologyIter->get<bool>();
        }
    }

    void PowerFlowNrRectSolver::init()
    {
        selDrFrom_f_.set_size(pfIsland_->nPqPv());
        selDiFrom_f_.set_size(pfIsland_->nPqPv());

        selVrOrQFrom_x_.set_size(pfIsland_->nPqPv());
        selViFrom_x_.set_size(pfIsland_->nPqPv());

        for (uword i = 0; i < pfIsland_->nPqPv(); ++i)
        {
            selDrFrom_f_[i] = 2 * i;
            selDiFrom_f_[i] = 2 * i + 1;
        }

        for (uword i = 0; i < pfIsland_->nPqPv(); ++i)
        {
            selVrOrQFrom_x_[i] = 2 * i;
            selViFrom_x_[i] = 2 * i + 1;
        }
    }
    
    bool PowerFlowNrRectSolver::solve()
    {
        double duration = 0;

        Stopwatch stopwatch;
        stopwatch.reset();
        stopwatch.start();

        // Construct the model from the network.
        init();

        Col<Complex> V = pfIsland_->V();
        Col<double> Vr = real(V);
        Col<double> Vi = imag(V);
        Col<double> M2 = square(Vr) + square(Vi);
        Col<double> M = sqrt(M2);
        const Col<double> M2PvSetpt = pfIsland_->nPv() > 0 ? square(subvec(M, pfIsland_->selPv())) : Col<double>();

        Col<Complex> SGen = pfIsland_->SGen();
        const Col<double> PGenPv = pfIsland_->nPv() > 0 ? real(subvec(SGen, pfIsland_->selPv())) : Col<double>();
        Col<double> QGenPv = pfIsland_->nPv() > 0 ? imag(subvec(SGen, pfIsland_->selPv())) : Col<double>();

        const Col<Complex> SConstDiag(pfIsland_->SConst().diag());
        Col<Complex> Scg = SGen - SConstDiag;

        bool wasSuccessful = false;
        double err = 0;
        unsigned int niter;

        for (niter = 0; niter < maxIter; ++niter)
        {
            sgtLogDebug(LogLevel::VERBOSE) << "Iteration = " << niter << std::endl;
            debugPrintVars(Vr, Vi, Scg);

            auto D = calcD(V, Scg, M2PvSetpt);

            err = norm(D, "inf");
            sgtLogDebug(LogLevel::VERBOSE) << "Error = " << err << std::endl;
            if (std::isnan(err))
            {
                sgtLogWarning() << "Couldn't solve power flow: encountered NaN." << std::endl;
                break;
            }
            else if (err <= tol)
            {
                sgtLogDebug() << "Success at iteration " << niter << "." << std::endl;
                wasSuccessful = true;
                break;
            }

            auto J = calcJ(V, Scg, M2PvSetpt);

            sgtLogDebug(LogLevel::VERBOSE) << "Before modifyForPv:" << std::endl;
            debugPrintProb(D, J);

            if (pfIsland_->nPv() > 0)
            {
                modifyForPv(J, D, Vr, Vi, M2, M2PvSetpt);
            }
            
            sgtLogDebug(LogLevel::VERBOSE) << "After modifyForPv:" << std::endl;
            debugPrintProb(D, J);

            // Construct the f vector consisting of real and imgaginary parts of D.
            auto f = construct_f(D); 

            // Construct the full Jacobian from J, which contains the block structure.
            auto JMat = constructJMatrix(J);

            sgtLogDebug(LogLevel::VERBOSE) << "Before solve:" << std::endl;
            debugPrintFandJMat(f, JMat);
            
            // Solution vector.
            Col<double> x;
            
            bool ok;
#ifdef WITH_KLU
            ok = kluSolve(JMat, -f, x);
#else
            ok = spsolve(x, JMat, -f, "superlu");
#endif

            sgtLogDebug(LogLevel::VERBOSE) << "After solve: ok = " << ok << std::endl;
            sgtLogDebug(LogLevel::VERBOSE) 
                << "After solve: x  = " << std::setprecision(5) << std::setw(9) << x << std::endl;
            if (!ok)
            {
                sgtLogWarning() << "Solve failed." << std::endl;
                break;
            }

            // Update the current values of V from the solution:
            if (pfIsland_->nPq() > 0)
            {
                subvec(Vr, pfIsland_->selPq()) += x(subvec(selVrOrQFrom_x_, pfIsland_->selPq()));
                subvec(Vi, pfIsland_->selPq()) += x(subvec(selViFrom_x_, pfIsland_->selPq()));
            }

            // Explicitly deal with the voltage magnitude constraint by updating VrPv by hand.
            if (pfIsland_->nPv() > 0)
            {
                auto VrPv = subvec(Vr, pfIsland_->selPv());
                auto ViPv = subvec(Vi, pfIsland_->selPv());
                const Col<double> DeltaViPv = x(subvec(selViFrom_x_, pfIsland_->selPv()));
                VrPv += (M2PvSetpt - square(VrPv) - square(ViPv) - 2 * ViPv % DeltaViPv) / (2 * VrPv);
                ViPv += DeltaViPv;

                // Update QGen for PV buses based on the solution.
                QGenPv += x(subvec(selVrOrQFrom_x_, pfIsland_->selPv()));
            }

            // Set V, M2 and M from Vr and Vi.
            V = cx_vec(Vr, Vi); 
            M2 = square(Vr) + square(Vi);
            M = sqrt(M2);
       
            // Set SGenPv from PGenPv and QGenPv.
            if (pfIsland_->nPv() > 0)
            {
                subvec(SGen, pfIsland_->selPv()) = cx_vec(PGenPv, QGenPv); 
                subvec(Scg, pfIsland_->selPv()) = subvec(SGen, pfIsland_->selPv()) - subvec(SConstDiag, pfIsland_->selPv());
            }
        }

        if (!wasSuccessful)
        {
            sgtLogWarning() << "PowerFlowNrRectSolver: failed to converge." << std::endl;
        }
        
        // Set the slack generation.
        if (pfIsland_->nSl() > 0)
        {
            subvec(SGen, pfIsland_->selSl()) = calcSGenSl(V);
        }

        // Propagate V to the model.
        pfIsland_->setV(V);

        // Propagate SGen to the model.
        pfIsland_->setSGen(SGen);

        stopwatch.stop();
        duration = stopwatch.cpuSeconds();

        sgtLogDebug() << "PowerFlowNrRectSolver: " << std::endl; 
        {
            sgtLogIndent();
            sgtLogDebug() << "successful = " << wasSuccessful << std::endl;
            sgtLogDebug() << "error = " << err << std::endl; 
            sgtLogDebug() << "iterations = " << niter << std::endl;
            sgtLogDebug() << "total time = " << duration << std::endl;
        }

        if (wasSuccessful)
        {
            applyPfModel(*mod_, *netw_);
        }

        return wasSuccessful;
    }
        
    // At this stage, we are treating f as if all buses were PQ. PV buses will be taken into account later.
    Col<Complex> PowerFlowNrRectSolver::calcD(
            const Col<Complex>& V,
            const Col<Complex>& Scg,
            const Col<double>& M2PvSetpt) const
    {
        const auto YNdPqPv = rows(pfIsland_->YNd(), pfIsland_->selPqPv());
        const auto IConstPqPv = rows(pfIsland_->IConst(), pfIsland_->selPqPv());
        const auto SConstPqPv = rows(pfIsland_->SConst(), pfIsland_->selPqPv());

        Col<Complex> D = -YNdPqPv * V;
       
        if (pfIsland_->nPq() > 0)
        {
            subvec(D, pfIsland_->selPq()) += conj(subvec(Scg, pfIsland_->selPq()) / subvec(V, pfIsland_->selPq()));
        }

        if (pfIsland_->nPv() > 0)
        {
            subvec(D, pfIsland_->selPv()) +=
                conj(subvec(Scg, pfIsland_->selPv())) % subvec(V, pfIsland_->selPv()) / M2PvSetpt;
        }

        for (auto it = SConstPqPv.begin(); it != SConstPqPv.end(); ++it) 
        {
            uword i = it.row();
            uword k = it.col();
            if (i == k)
            {
                continue;
            }
            D(i) -= conj(*it / (V(i) - V(k)));
        }

        for (auto it = IConstPqPv.begin(); it != IConstPqPv.end(); ++it) 
        {
            uword i = it.row();
            uword k = it.col();
            Complex Vik = i == k ? V(i) : V(i) - V(k); // Ground elems stored in diagonal elems of SConst.
            D(i) -= *it * Vik / abs(Vik);
        }

        return D;
    }

    // At this stage, we are treating f as if all buses were PQ. PV buses will be taken into account later.
    Jacobian PowerFlowNrRectSolver::calcJ(
            const Col<Complex>& V,
            const Col<Complex>& Scg,
            const Col<double>& M2PvSetpt) const
    {
        auto YNdPqPv = submat(pfIsland_->YNd(), pfIsland_->selPqPv(), pfIsland_->selPqPv());
        auto IConstPqPv = submat(pfIsland_->IConst(), pfIsland_->selPqPv(), pfIsland_->selPqPv());
        auto SConstPqPv = submat(pfIsland_->SConst(), pfIsland_->selPqPv(), pfIsland_->selPqPv());

        Jacobian J(pfIsland_->nPq(), pfIsland_->nPv());

        SparseHelper<Complex> hVr(pfIsland_->nPqPv(), pfIsland_->nPqPv(), true, true, true);
        SparseHelper<Complex> hVi(pfIsland_->nPqPv(), pfIsland_->nPqPv(), true, true, true);
        SparseHelper<Complex> hQg(pfIsland_->nPv(), pfIsland_->nPv(), true, true, true);

        for (auto it = SConstPqPv.begin(); it != SConstPqPv.end(); ++it)
        {
            uword i = it.row();
            uword k = it.col();
            if (i == k)
            {
                continue;
            }

            Complex Vik = V(i) - V(k);

            Complex x = conj(static_cast<Complex>(*it) / (Vik * Vik));
            Complex imX = im * x;

            hVr.insert(i, i, x);
            hVi.insert(i, i, -imX);
            hVr.insert(i, k, -x);
            hVr.insert(i, k, imX);
        }

        for (uword i = 0; i < pfIsland_->nPq(); ++i)
        {
            uword iPq = pfIsland_->iPq(i);
            Complex x = conj(static_cast<Complex>(Scg(iPq)) / (V(iPq) * V(iPq)));
            hVr.insert(iPq, iPq, -x);
            hVi.insert(iPq, iPq, im * x);
        }

        for (uword i = 0; i < pfIsland_->nPv(); ++i)
        {
            uword iPv = pfIsland_->iPv(i);
            Complex x = conj(Scg(iPv)) / M2PvSetpt(i);
            hVr.insert(iPv, iPv, x);
            hVi.insert(iPv, iPv, im * x);
            hQg.insert(i, i, -im * V(iPv) / M2PvSetpt(i));
        }

        for (auto it = YNdPqPv.begin(); it != YNdPqPv.end(); ++it)
        {
            Complex x = -static_cast<Complex>(*it);
            hVr.insert(it.row(), it.col(), x);
            hVi.insert(it.row(), it.col(), im * x);
        }

        for (auto it = IConstPqPv.begin(); it != IConstPqPv.end(); ++it)
        {
            uword i = it.row();
            uword k = it.col();
            Complex Vik = i == k ? V(i) : V(i) - V(k);
            Complex Mik = abs(Vik);
            Complex M2ik = Mik * Mik;
            Complex M3ik = Mik * M2ik;

            Complex x = static_cast<Complex>(*it) * (-M2ik + real(Vik) * Vik) / M3ik;
            Complex imX = im * x;
            hVr.insert(i, i, x);
            hVi.insert(i, i, imX);
            if (i != k)
            {
                hVr.insert(i, k, -x);
                hVi.insert(i, k, -imX);
            }
        }
        J.dDdVr = hVr.get();
        J.dDdVi = hVi.get();
        J.dDPvdQPv = hQg.get();

        return J;
    }

    // Modify J and f to take into account PV buses.
    void PowerFlowNrRectSolver::modifyForPv(
            Jacobian& J,
            Col<Complex>& D,
            const Col<double>& Vr,
            const Col<double>& Vi,
            const Col<double>& M2,
            const Col<double>& M2PvSetpt)
    {
        for (uword k = 0; k < pfIsland_->nPv(); ++k) // Loop over PV buses.
        {
            uword kPv = pfIsland_->iPv(k); // Get bus index in full list of buses.
            double DMult = 0.5 * (M2PvSetpt(k) - M2(kPv)) / Vr(kPv);
            double JMult = -Vi(kPv) / Vr(kPv);

            auto colK = J.dDdVr.col(kPv); // Select column in J corresponding to d/dVr for PV bus k.
            for (auto it = colK.begin(); it != colK.end(); ++it)
            {
                // Loop over all buses in this column.
                uword i = it.row();
                Complex x = *it;
                D(i) += x * DMult; // Adding DMult * this column to D.
                J.dDdVi(i, kPv) += x * JMult; // Adding JMult * this column to corresponding d/dVi col.
            }
        }
    }
            
    Col<double> PowerFlowNrRectSolver::construct_f(const Col<Complex>&D) const
    {
        // Construct the real f vector from D.
        Col<double> f(nVar(), fill::none);
        for (uword i = 0; i < pfIsland_->nPqPv(); ++i)
        {
            f(selDrFrom_f_(i)) = real(D(i));
            f(selDiFrom_f_(i)) = imag(D(i));
        }
        return f;
    }

    SpMat<double> PowerFlowNrRectSolver::constructJMatrix(const Jacobian& J) const
    {
        SparseHelper<double> h(nVar(), nVar(), true, true, true);

        auto insert = [&](const auto& JSel, const auto& indDim1R, const auto& indDim1I, const auto& indDim2)
        {
            for (auto it = JSel.begin(); it != JSel.end(); ++it)
            {
                uword i = it.row(); 
                uword k = it.col(); 
                Complex x = *it;
                h.insert(indDim1R(i), indDim2(k), real(x));
                h.insert(indDim1I(i), indDim2(k), imag(x));
            }
        };
       
        insert(J.dDdVi, selDrFrom_f_, selDiFrom_f_, selViFrom_x_);
        if (pfIsland_->nPq() > 0)
        {
            insert(cols(J.dDdVr, pfIsland_->selPq()), selDrFrom_f_, selDiFrom_f_,
                   subvec(selVrOrQFrom_x_, pfIsland_->selPq())); // Vr PQ.
        }
        if (pfIsland_->nPv() > 0)
        {
            insert(J.dDPvdQPv, subvec(selDrFrom_f_, pfIsland_->selPv()), subvec(selDiFrom_f_, pfIsland_->selPv()), 
                   subvec(selVrOrQFrom_x_, pfIsland_->selPv())); // Q PV.
        }

        return h.get();
    }
            
    Col<Complex> PowerFlowNrRectSolver::calcSGenSl(const Col<Complex>& V)
    {
        auto VSl = subvec(V, pfIsland_->selSl());
        auto YNdSl = rows(pfIsland_->YNd(), pfIsland_->selSl());
        auto IConstSl = rows(pfIsland_->IConst(), pfIsland_->selSl());
        auto SConstSl = rows(pfIsland_->SConst(), pfIsland_->selSl());

        Col<Complex> SGenSl = conj(YNdSl * V);

        for (auto it = SConstSl.begin(); it != SConstSl.end(); ++it)
        {
            uword i = it.row(); // Index in submatrix.
            uword k = it.col(); // Index in both submatrix and full matrix, since we're using rows().
            uword iSl = pfIsland_->iSl(i); // Index in full matrix.
            Complex Vik = iSl == k ? VSl(i) : VSl(i) - V(k); // Ground elems stored in diagonal elems of SConst.
            SGenSl(i) += static_cast<Complex>(*it) / Vik;
        }

        for (auto it = IConstSl.begin(); it != IConstSl.end(); ++it)
        {
            uword i = it.row(); // Index in submatrix.
            uword k = it.col(); // Index in both submatrix and full matrix, since we're using rows().
            uword iSl = pfIsland_->iSl(i); // Index in full matrix.
            Complex Vik = iSl == k ? VSl(i) : VSl(i) - V(k); // Ground elems stored in diagonal elems of SConst.
            SGenSl(i) += conj(static_cast<Complex>(*it) * Vik) / abs(Vik);
        }

        SGenSl %= VSl;

        return SGenSl;
    }

    void PowerFlowNrRectSolver::debugPrintVars(const Col<double>& Vr, const Col<double>& Vi,
            const Col<Complex>& Scg) const
    {
            if (debugLogLevel() >= LogLevel::VERBOSE)
            {
                sgtLogDebug() << "Vr = " << std::setprecision(5) << std::setw(9) << Vr << std::endl;
                sgtLogDebug() << "Vi = " << std::setprecision(5) << std::setw(9) << Vi << std::endl;
                if (pfIsland_->nPv() > 0)
                {
                    auto ScgPv = subvec(Scg, pfIsland_->selPv());
                    Col<double> PcgPv = real(ScgPv);
                    Col<double> QcgPv = imag(ScgPv);
                    sgtLogDebug() << "PcgPv = " << std::setprecision(5) << std::setw(9) << PcgPv << std::endl;
                    sgtLogDebug() << "QcgPv = " << std::setprecision(5) << std::setw(9) << QcgPv << std::endl;
                }
            }
    }

    void PowerFlowNrRectSolver::debugPrintProb(const Col<Complex>& D, const Jacobian& J) const
    {
            if (debugLogLevel() >= LogLevel::VERBOSE)
            {
                if (pfIsland_->nPq() > 0)
                {
                    sgtLogDebug(LogLevel::VERBOSE) << "DPq" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << static_cast<Mat<double>>(real(subvec(D, pfIsland_->selPq()))) << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << static_cast<Mat<double>>(imag(subvec(D, pfIsland_->selPq()))) << std::endl;
                }
                
                if (pfIsland_->nPv() > 0)
                {
                    sgtLogDebug(LogLevel::VERBOSE) << "DPv" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << static_cast<Mat<double>>(real(subvec(D, pfIsland_->selPv()))) << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << static_cast<Mat<double>>(imag(subvec(D, pfIsland_->selPv()))) << std::endl;
                }

                if (pfIsland_->nPq() > 0)
                {
                    sgtLogDebug(LogLevel::VERBOSE) << "DPq VrPq" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVr, pfIsland_->selPq(), pfIsland_->selPq())));
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVr, pfIsland_->selPq(), pfIsland_->selPq())));

                    sgtLogDebug(LogLevel::VERBOSE) << "DPq ViPq" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVi, pfIsland_->selPq(), pfIsland_->selPq())));
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVi, pfIsland_->selPq(), pfIsland_->selPq())));

                    if (pfIsland_->nPv() > 0)
                    {
                        sgtLogDebug(LogLevel::VERBOSE) << "DPq VrPv" << std::endl;
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVr, pfIsland_->selPq(), pfIsland_->selPv())));
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVr, pfIsland_->selPq(), pfIsland_->selPv())));

                        sgtLogDebug(LogLevel::VERBOSE) << "DPq ViPv" << std::endl;
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVi, pfIsland_->selPq(), pfIsland_->selPv())));
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVi, pfIsland_->selPq(), pfIsland_->selPv())));
                    }
                }

                if (pfIsland_->nPv() > 0)
                {
                    if (pfIsland_->nPq() > 0)
                    {
                        sgtLogDebug(LogLevel::VERBOSE) << "DPv VrPq" << std::endl;
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVr, pfIsland_->selPv(), pfIsland_->selPq())));
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVr, pfIsland_->selPv(), pfIsland_->selPq())));

                        sgtLogDebug(LogLevel::VERBOSE) << "DPv ViPq" << std::endl;
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVi, pfIsland_->selPv(), pfIsland_->selPq())));
                        sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVi, pfIsland_->selPv(), pfIsland_->selPq())));
                    }

                    sgtLogDebug(LogLevel::VERBOSE) << "DPv VrPv" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVr, pfIsland_->selPv(), pfIsland_->selPv())));
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVr, pfIsland_->selPv(), pfIsland_->selPv())));

                    sgtLogDebug(LogLevel::VERBOSE) << "DPv ViPv" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(submat(J.dDdVi, pfIsland_->selPv(), pfIsland_->selPv())));
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(submat(J.dDdVi, pfIsland_->selPv(), pfIsland_->selPv())));

                    sgtLogDebug(LogLevel::VERBOSE) << "DPv QPv" << std::endl;
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(real(J.dDPvdQPv));
                    sgtLogDebug(LogLevel::VERBOSE) << Mat<double>(imag(J.dDPvdQPv));
                }
            }
        }

        void PowerFlowNrRectSolver::debugPrintFandJMat(const Col<double>& f, const SpMat<double>& JMat) const
        {
            if (debugLogLevel() >= LogLevel::VERBOSE)
            {
                sgtLogDebug(LogLevel::VERBOSE) 
                    << "Before solve: f      = " << std::setprecision(5) << std::setw(9) << f << std::endl;
                sgtLogDebug(LogLevel::VERBOSE) 
                    << "Before solve: JMat   = " << std::setprecision(5) << std::setw(9) << JMat << std::endl;
            }
        }
}
