// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef OPF_S_POL_SOLVER_DOT_H
#define OPF_S_POL_SOLVER_DOT_H

#include <optional>

#include <SgtCore/Network.h>
#include <SgtCore/OpfModel.h>
#include <SgtCore/OpfModelNetworkInterface.h>
#include <SgtCore/OpfSolver.h>
#include <SgtCore/PowerFlowSolver.h>

#include <madopt/common.hpp>
#include <madopt/ipopt_model.hpp>

namespace Sgt
{
    namespace OpfUtil
    {
        inline double clamped(const double x, const double lb, const double ub)
        {
            return std::min(std::max(x, lb), ub);
        }

        template<typename T> std::string uid(const T& t, const size_t iPh)
        {
            return t.id + "_" + to_string(iPh);
        }

        template<typename T> std::string uid(const T& t, const size_t iSide, const size_t iPh)
        {
            return t.id + "_" + to_string(iSide) + "_" + to_string(iPh);
        }
    }

    using OptCSpec = std::optional<ConstraintSpec>;

    class OpfSPolBusData
    {
        public:

        OpfSPolBusData() = default;
        OpfSPolBusData(OpfSPolBusData&&) = default;

        OpfBus* opfBus;

        std::vector<MadOpt::Var> vVars; // Voltage magnitude.
        std::vector<MadOpt::Var> tVars; // Voltage angle.
        
        std::vector<MadOpt::Var> pSlackInjVars; // Slack real power.
        std::vector<MadOpt::Var> pSlackConVars; // Slack real power.
        std::vector<MadOpt::Var> qSlackInjVars; // Slack reactive power.
        std::vector<MadOpt::Var> qSlackConVars; // Slack reactive power.

        std::vector<OptCSpec> kclPConstrs; // Expression for KCL, real. +ve = injection into the bus.
        std::vector<OptCSpec> kclQConstrs; // Expression for KCL, imag.

        std::vector<OptCSpec> vInterPhConstrs;
        std::vector<OptCSpec> tInterPhConstrs;

        virtual ~OpfSPolBusData() = default;

        void init(NlpModel& mod, OpfBus& opfBusArg, const bool hasGens, const bool hasZips, const bool pfMode)
        {
            using namespace OpfUtil;

            opfBus = &opfBusArg;

            auto nPhase = opfBus->nPhase();

            vVars.resize(nPhase);
            tVars.resize(nPhase);
            for (auto iBusPh = 0u; iBusPh < nPhase; ++iBusPh)
            {
                double vInit;
                double vMin;
                double vMax;

                double tInit = arg(opfBus->V[iBusPh]);
                double tMin = opfBus->isAngleRef ? tInit : -MadOpt::INF;
                double tMax = opfBus->isAngleRef ? tInit : MadOpt::INF;

                if (pfMode)
                {
                    vInit = abs(opfBus->V[iBusPh]);
                    if (opfBus->hasAvr)
                    {
                        // Slack or PV bus
                        vMin = vMax = vInit;
                    }
                    else
                    {
                        vMin = -MadOpt::INF;
                        vMax = MadOpt::INF;
                    }
                }
                else
                {
                    vInit = clamped(abs(opfBus->V[iBusPh]), opfBus->vBnds[0], opfBus->vBnds[1]);
                    vMin = opfBus->vBnds[0];
                    vMax = opfBus->vBnds[1];
                }
                


                auto id = uid(*opfBus, iBusPh);

                vVars[iBusPh] = mod.addVar(vMin, vMax, vInit, "v_" + id);
                tVars[iBusPh] = mod.addVar(tMin, tMax, tInit, "t_" + id);
            }
            
            if (hasZips)
            {
                pSlackInjVars.resize(nPhase);
                pSlackConVars.resize(nPhase);
                qSlackInjVars.resize(nPhase);
                qSlackConVars.resize(nPhase);
                for (auto iBusPh = 0u; iBusPh < nPhase; ++iBusPh)
                {
                    auto id = uid(*opfBus, iBusPh);
                    pSlackInjVars[iBusPh] = mod.addVar(0.0, infinity, 0.0, "p_slack_inj_" + id);
                    pSlackConVars[iBusPh] = mod.addVar(0.0, infinity, 0.0, "p_slack_con_" + id);
                    qSlackInjVars[iBusPh] = mod.addVar(0.0, infinity, 0.0, "q_slack_inj_" + id);
                    qSlackConVars[iBusPh] = mod.addVar(0.0, infinity, 0.0, "q_slack_con_" + id);
                }
            }

            kclPConstrs.resize(nPhase);
            kclQConstrs.resize(nPhase);
            for (auto iBusPh = 0u; iBusPh < nPhase; ++iBusPh)
            {
                kclPConstrs[iBusPh].emplace("kcl_p_" + uid(opfBusArg, iBusPh), 0.0, 0.0);
                kclQConstrs[iBusPh].emplace("kcl_q_" + uid(opfBusArg, iBusPh), 0.0, 0.0);
            }

            if (!pfMode && hasGens && (nPhase > 1))
            {
                vInterPhConstrs.resize(nPhase - 1);
                for (size_t iBusPh = 1; iBusPh < nPhase; ++iBusPh)
                {
                    vInterPhConstrs[iBusPh - 1].emplace(
                            "v_inter_ph_" + uid(opfBusArg, iBusPh), 0.0, 0.0, vVars[iBusPh] - vVars[0]);
                }
                if (!opfBus->isAngleRef)
                {
                    tInterPhConstrs.resize(nPhase - 1);
                    for (size_t iBusPh = 1; iBusPh < nPhase; ++iBusPh)
                    {
                        double dtI0 = arg(opfBus->V[iBusPh]) - arg(opfBus->V[0]);
                        // Reference bus will always have fixed variable anyway.
                        tInterPhConstrs[iBusPh - 1].emplace(
                                "t_inter_ph_" + uid(opfBusArg, iBusPh), 0.0, 0.0, tVars[iBusPh] - tVars[0] - dtI0);
                    }
                }
            }
        }

        virtual void addConstraints(NlpModel& mod)
        {
            for (auto& constr: kclPConstrs)
            {
                constr->addToNlp(mod);
            }
            for (auto& constr: kclQConstrs)
            {
                constr->addToNlp(mod);
            }
            for (auto& constr: vInterPhConstrs)
            {
                constr->addToNlp(mod);
            }
            for (auto& constr: tInterPhConstrs)
            {
                constr->addToNlp(mod);
            }
        }
        
        void print() const
        {
            using namespace OpfUtil;

            sgtLogMessage() << opfBus->id << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Bus Vars" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            for (const auto& v: vVars) printVar(v);
            for (const auto& v: tVars) printVar(v);
            for (const auto& v: pSlackInjVars) printVar(v);
            for (const auto& v: pSlackConVars) printVar(v);
            for (const auto& v: qSlackInjVars) printVar(v);
            for (const auto& v: qSlackConVars) printVar(v);
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Bus Constrs" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            for (const auto& c: kclPConstrs) if (c.has_value()) c->print();
            for (const auto& c: kclQConstrs) if (c.has_value()) c->print();
            for (const auto& c: vInterPhConstrs) if (c.has_value()) c->print();
            for (const auto& c: tInterPhConstrs) if (c.has_value()) c->print();
        }
    };

    class OpfSPolGenData
    {
        public:
        
        OpfSPolGenData() = default;
        OpfSPolGenData(OpfSPolGenData&&) = default;

        OpfGen* opfGen;

        // TODO: positive and negative generation, reactive cost.
        std::vector<MadOpt::Var> pVars; // Real power injection.
        std::vector<MadOpt::Var> qVars; // Real power injection.

        OptCSpec sMaxConstr;
        
        virtual ~OpfSPolGenData() = default;
        
        void init(NlpModel& mod, OpfGen& opfGenArg, const bool pfMode)
        {
            using namespace OpfUtil;

            opfGen = &opfGenArg;
            auto opfBus = opfGen->bus;

            auto nPhase = opfGen->nPhase();
            pVars.resize(nPhase);
            qVars.resize(nPhase);

            for (auto iGenPh = 0u; iGenPh < nPhase; ++iGenPh)
            {
                double pInit;
                double pMin;
                double pMax;
                double qInit;
                double qMin;
                double qMax;

                if (!pfMode)
                {
                    pInit = clamped(opfGen->SGen[iGenPh].real(), opfGen->pGenBnds[0], opfGen->pGenBnds[1]);
                    pMin = opfGen->pGenBnds[0];
                    pMax = opfGen->pGenBnds[1];

                    qInit = clamped(opfGen->SGen[iGenPh].imag(), opfGen->qGenBnds[0], opfGen->qGenBnds[1]);
                    qMin = opfGen->qGenBnds[0];
                    qMax = opfGen->qGenBnds[1];
                }
                else
                {
                    pInit = opfGen->SGen[iGenPh].real();
                    qInit = opfGen->SGen[iGenPh].imag();
                    if (opfBus->hasAvr)
                    {
                        if (opfBus->isAngleRef)
                        {
                            // Slack bus
                            pMin = -MadOpt::INF;
                            pMax = MadOpt::INF;
                        }
                        else
                        {
                            // PV bus
                            pMin = pMax = pInit;
                        }
                        qMin = -MadOpt::INF; 
                        qMax = MadOpt::INF; 
                    }
                    else
                    {
                        // PQ bus
                        pMin = pMax = pInit;
                        qMin = qMax = qInit;
                    }
                }

                auto id = uid(*opfGen, iGenPh);
                pVars[iGenPh] = mod.addVar(pMin, pMax, pInit, "pg_" + id);
                qVars[iGenPh] = mod.addVar(qMin, qMax, qInit, "qg_" + id);
            }

            if (opfGen->sGenBnds[1] != infinity && !pfMode)
            {
                sMaxConstr.emplace("s_max_" + opfGen->id, -infinity, pow(opfGen->sGenBnds[1], 2));
                for (size_t iGenPh = 0; iGenPh < opfGen->nPhase(); ++iGenPh)
                {
                    sMaxConstr->expr += pow(pVars[iGenPh], 2) + pow(qVars[iGenPh], 2);
                }
            }
        }
        
        virtual void addConstraints(NlpModel& mod)
        {
            if (sMaxConstr.has_value())
            {
                sMaxConstr->addToNlp(mod);
            }
        }
        
        void print() const
        {
            using namespace OpfUtil;

            sgtLogMessage() << opfGen->id << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Gen Vars" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            for (const auto& v: pVars) printVar(v);
            for (const auto& v: qVars) printVar(v);
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Gen Constrs" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            if (sMaxConstr.has_value()) sMaxConstr->print();
        }
    };
    
    class OpfSPolZipData
    {
        public:
        
        OpfSPolZipData() = default;
        OpfSPolZipData(OpfSPolZipData&&) = default;
        
        OpfZip* opfZip;

        virtual ~OpfSPolZipData() = default;

        void init(OpfZip& opfZipArg)
        {
            using namespace OpfUtil;
            opfZip = &opfZipArg;
        }

        virtual void addConstraints([[maybe_unused]] NlpModel& mod) {}
        
        void print() const
        {
            sgtLogMessage() << opfZip->id << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Zip Vars" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Zip Constrs" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
        }
    };

    class OpfSPolBranchData
    {
        public:
        
        OpfSPolBranchData() = default;
        OpfSPolBranchData(OpfSPolBranchData&&) = default;

        OpfBranch* opfBranch;

        std::vector<MadOpt::Expr> pOutExprs; // Real part of power out of node into branch.
        std::vector<MadOpt::Expr> qOutExprs; // Imag part of power out of node into branch.

        std::vector<OptCSpec> sMaxConstrs; // Expression for upper limit for SMag^2 constraint.

        std::vector<Complex> sOutInits; // Initial value to satisfy branch flow.
        
        virtual ~OpfSPolBranchData() = default;

        size_t idx(const size_t iSide, const size_t iBrPh)
        {
            return iSide * opfBranch->nPhase(0) + iBrPh;
        }
        
        void init([[maybe_unused]] NlpModel& mod, OpfBranch& opfBranchArg, const bool pfMode)
        {
            using namespace OpfUtil;

            opfBranch = &opfBranchArg;
            
            auto nPhase0 = opfBranch->nPhase(0);
            auto nPhase1 = opfBranch->nPhase(1);
            auto nPhaseTot = nPhase0 + nPhase1;

            pOutExprs.resize(nPhaseTot);
            qOutExprs.resize(nPhaseTot);

            if (!pfMode)
            {
                sMaxConstrs.resize(nPhaseTot);
                for (auto iSide = 0u; iSide < 2; ++iSide)
                {
                    for (auto iBrPh = 0u; iBrPh < opfBranch->nPhase(iSide); ++iBrPh)
                    {
                        auto iSidePh = idx(iSide, iBrPh);
                        // Need to set up expressions later.
                        auto id = uid(opfBranchArg, iSide, iBrPh);
                        sMaxConstrs[iSidePh].emplace("s_max_" + id, -infinity, pow(opfBranch->SMagMax, 2), 0.0);
                        // Note powers of infinity etc. obey correct arithmetic.
                    }
                }
            }

            sOutInits.resize(nPhaseTot);
        }

        virtual void addConstraints(NlpModel& mod)
        {
            for (auto& constr : sMaxConstrs)
            {
                constr->addToNlp(mod);
            }
        }
        
        void print() const
        {
            using namespace OpfUtil;

            sgtLogMessage() << opfBranch->id << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Branch Vars" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Branch Constrs" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            for (const auto& c: sMaxConstrs) if (c.has_value()) c->print();
        }
    };
    
    class OpfSPolNetwData
    {
        public:
        
        OpfSPolNetwData() = default;
        OpfSPolNetwData(OpfSPolNetwData&&) = default;

        virtual void addConstraints([[maybe_unused]] NlpModel& mod) {}
        
        virtual ~OpfSPolNetwData() = default;
            
        void print() const
        {
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Netw vars" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
            sgtLogMessage() << "Netw constrs" << std::endl;
            sgtLogMessage() << "-------------------------" << std::endl;
        }
    };

    /// @brief Optimal power flow (OPF) solver.
    ///
    /// Uses the external PowerTools library.
    /// @ingroup PowerFlowCore
    template<
        typename BusDataT = OpfSPolBusData,
        typename GenDataT = OpfSPolGenData,
        typename ZipDataT = OpfSPolZipData,
        typename BranchDataT = OpfSPolBranchData,
        typename NetwDataT = OpfSPolNetwData
    >
    struct OpfSPolNlp
    {
        using BusData = BusDataT;
        using GenData = OpfSPolGenData;
        using ZipData = OpfSPolZipData;
        using BranchData = OpfSPolBranchData;
        using NetwData = OpfSPolNetwData;

        NlpModel nlpMod_;
        std::vector<BusData> busData_;
        std::vector<GenData> genData_;
        std::vector<ZipData> zipData_;
        std::vector<BranchData> branchData_;
        NetwData netwData_;
        MadOpt::Expr obj_;
    };
    
    template<typename Nlp> class OpfSPolPfSolver;

    template<typename Nlp = OpfSPolNlp<>>
    class OpfSPolSolver : public OpfSolver<Nlp>
    {
        friend class OpfSPolPfSolver<Nlp>;

        public:
        
        using BusData = typename Nlp::BusData;
        using GenData = typename Nlp::GenData;
        using ZipData = typename Nlp::ZipData;
        using BranchData = typename Nlp::BranchData;
        using NetwData = typename Nlp::NetwData;
        
        double slackPen{1.0e3};

        OpfSPolSolver() {}

        virtual ~OpfSPolSolver() = default;

        protected:

        virtual void buildNlp(const Network& netw) override
        {
            using namespace std;
            using namespace arma;
            using namespace OpfUtil;

            this->opfMod_ = buildOpfModel(netw, this->pfMode);

            this->nlp_->busData_.resize(opfMod_->buses().size());
            this->nlp_->genData_.resize(opfMod_->gens().size());
            this->nlp_->zipData_.resize(opfMod_->zips().size());
            this->nlp_->branchData_.resize(opfMod_->branches().size());

            std::set<std::string> busesWithGens;
            for (auto& opfGen: this->opfMod_->gens()) busesWithGens.insert(opfGen.bus->id);
           
            std::set<std::string> busesWithZips;
            for (auto& opfZip: this->opfMod_->zips()) busesWithZips.insert(opfZip.bus->id);
            
            for (auto& opfBus: this->opfMod_->buses())
            {
                auto& busDat = this->nlp_->busData_[opfBus.idx];
                bool hasGens = busesWithGens.count(opfBus.id) > 0;
                bool hasZips = busesWithZips.count(opfBus.id) > 0;
                busDat.BusData::init(this->nlp_->nlpMod_, opfBus, hasGens, hasZips, this->pfMode);
            }

            for (auto& opfGen: this->opfMod_->gens())
            {
                auto& genDat = this->nlp_->genData_[opfGen.idx];
                genDat.GenData::init(this->nlp_->nlpMod_, opfGen, this->pfMode);
            }

            for (auto& opfZip: this->opfMod_->zips())
            {
                auto& zipDat = this->nlp_->zipData_[opfZip.idx];
                zipDat.ZipData::init(opfZip);
            }

            for (auto& opfBranch: this->opfMod_->branches())
            {
                auto& branchDat = this->nlp_->branchData_[opfBranch.idx];
                branchDat.BranchData::init(this->nlp_->nlpMod_, opfBranch, this->pfMode);
            }

            // ---------------------------------------------------------------------
            // Branch flow constraints

            for (auto& branchDat : this->nlp_->branchData_)
            {
                auto& opfBranch = *branchDat.opfBranch;
                SpMat<Complex> YNdT(strans(opfBranch.YNd)); // Transpose for better iteration over rows.
                for (auto it = YNdT.begin(); it != YNdT.end(); ++it)
                {
                    size_t iRow = it.col(); // Remember, using transpose.
                    size_t kCol = it.row(); // Remember, using transpose.
                    double gik = (*it).real();
                    double bik = (*it).imag();

                    size_t iSide = iRow < opfBranch.nPhase(0) ? 0 : 1;
                    size_t iBrPh = iSide == 0 ? iRow : iRow - opfBranch.nPhase(0);
                    auto iSidePh = branchDat.idx(iSide, iBrPh);
                    size_t iBus = opfBranch.buses[iSide]->idx;
                    size_t iBusPh = opfBranch.busPhIdxs[iSide][iBrPh];
                    auto& busDatI = this->nlp_->busData_[iBus];

                    size_t kSide = kCol < opfBranch.nPhase(0) ? 0 : 1;
                    size_t kBrPh = kSide == 0 ? kCol : kCol - opfBranch.nPhase(0);
                    size_t kBus = opfBranch.buses[kSide]->idx;
                    size_t kBusPh = opfBranch.busPhIdxs[kSide][kBrPh];
                    auto& busDatK = this->nlp_->busData_[kBus];
                    
                    auto& viVar = busDatI.vVars[iBusPh];
                    auto& tiVar = busDatI.tVars[iBusPh];
                    auto& vkVar = busDatK.vVars[kBusPh];
                    auto& tkVar = busDatK.tVars[kBusPh];


                    if (iSide == kSide && iBrPh == kBrPh)
                    {
                        auto vi2 = pow(viVar, 2);
                        branchDat.pOutExprs[iSidePh] += vi2 * gik;
                        branchDat.qOutExprs[iSidePh] += -vi2 * bik;

                        auto vi20 = pow(viVar.init(), 2);
                        branchDat.sOutInits[iSidePh] += Complex(vi20 * gik, -vi20 * bik);
                    }
                    else
                    {
                        auto vivk = viVar * vkVar;
                        auto cik = cos(tiVar - tkVar);
                        auto sik = sin(tiVar - tkVar);
                        branchDat.pOutExprs[iSidePh] += vivk * (gik * cik + bik * sik);
                        branchDat.qOutExprs[iSidePh] += vivk * (gik * sik - bik * cik);

                        double vivk0 = viVar.init() * vkVar.init();
                        auto cik0 = cos(tiVar.init() - tkVar.init());
                        auto sik0 = sin(tiVar.init() - tkVar.init());
                        branchDat.sOutInits[iBrPh] += Complex(
                                vivk0 * (gik * cik0 + bik * sik0),
                                vivk0 * (gik * sik0 - bik * cik0));
                    }
                }
            }

            // ---------------------------------------------------------------------
            // KCL constraints

            for (auto& branchDat : this->nlp_->branchData_)
            {
                auto& opfBranch = *branchDat.opfBranch;
                for (size_t iSide = 0; iSide < 2; ++ iSide)
                {
                    size_t iBus = opfBranch.buses[iSide]->idx;
                    auto& busDatI = this->nlp_->busData_[iBus];
                    for (size_t iBrPh = 0; iBrPh < opfBranch.nPhase(iSide); ++iBrPh)
                    {
                        auto iSidePh = branchDat.idx(iSide, iBrPh);
                        size_t iBusPh = opfBranch.busPhIdxs[iSide][iBrPh];
                        busDatI.kclPConstrs[iBusPh]->expr += -branchDat.pOutExprs[iSidePh]; // -ve -> injection.
                        busDatI.kclQConstrs[iBusPh]->expr += -branchDat.qOutExprs[iSidePh]; // -ve -> injection.
                    }
                }
            }

            // KCL: from gens
            for (const auto& genDat : this->nlp_->genData_)
            {
                auto& opfGen = *genDat.opfGen;
                size_t iBus = opfGen.bus->idx;
                auto& busDat = this->nlp_->busData_[iBus];
                for (size_t iGenPh = 0; iGenPh < opfGen.nPhase(); ++iGenPh)
                {
                    size_t iBusPh = opfGen.busPhIdxs[iGenPh];
                    busDat.kclPConstrs[iBusPh]->expr += genDat.pVars[iGenPh]; // Injection.
                    busDat.kclQConstrs[iBusPh]->expr += genDat.qVars[iGenPh]; // Injection.
                }
            }

            // KCL: from zips
            for (const auto& zipDat : this->nlp_->zipData_)
            {
                auto& opfZip = *zipDat.opfZip;
                size_t iBus = opfZip.bus->idx;
                auto& busDat = this->nlp_->busData_[iBus];
                
                for (auto iComp = 0u; iComp < opfZip.connection.n_rows; ++iComp)
                {
                    auto iZipPh = opfZip.connection(iComp, 0);
                    auto kZipPh = opfZip.connection(iComp, 1);
                    size_t iBusPh = opfZip.busPhIdxs[iZipPh];
                    size_t kBusPh = opfZip.busPhIdxs[kZipPh];

                    // Constant Y
                    {
                        Complex yik = opfZip.YConst(iComp);
                        double gik = yik.real();
                        double bik = yik.imag();
                        if (iZipPh == kZipPh)
                        {
                            auto v2 = pow(busDat.vVars[iBusPh], 2); 
                            busDat.kclPConstrs[iBusPh]->expr += -gik * v2;
                            busDat.kclQConstrs[iBusPh]->expr += bik * v2;
                        }
                        else
                        {
                            auto cik = cos(busDat.tVars[iBusPh] - busDat.tVars[kBusPh]);
                            auto sik = sin(busDat.tVars[iBusPh] - busDat.tVars[kBusPh]);
                            auto a = busDat.vVars[iBusPh] * (busDat.vVars[iBusPh] - cik * busDat.vVars[kBusPh]);
                            auto b = sik * busDat.vVars[iBusPh] * busDat.vVars[kBusPh];
                            auto pi = -gik * a + bik * b;
                            auto qi = gik * b + bik * a;

                            busDat.kclPConstrs[iBusPh]->expr += pi;
                            busDat.kclQConstrs[iBusPh]->expr += qi;
                            busDat.kclPConstrs[kBusPh]->expr += -pi;
                            busDat.kclQConstrs[kBusPh]->expr += -qi;
                        }
                    }

                    // Constant I.
                    {
                        Complex iik = opfZip.IConst(iComp);
                        double icRe = iik.real();
                        double icIm = iik.imag();

                        if (iZipPh == kZipPh)
                        {
                            busDat.kclPConstrs[iBusPh]->expr += -icRe * busDat.vVars[iBusPh];
                            busDat.kclQConstrs[iBusPh]->expr += icIm * busDat.vVars[iBusPh];
                        }
                        else
                        {
                            assert(iZipPh < kZipPh);
                            size_t kBusPh = opfZip.busPhIdxs[kZipPh];
                            auto vi2 = pow(busDat.vVars[iBusPh], 2);
                            auto vk2 = pow(busDat.vVars[kBusPh], 2);
                            auto vivk = busDat.vVars[iBusPh] * busDat.vVars[kBusPh];
                            auto cik = cos(busDat.tVars[iBusPh] - busDat.tVars[kBusPh]);
                            auto sik = sin(busDat.tVars[iBusPh] - busDat.tVars[kBusPh]);
                            auto absVik = sqrt(vi2 + vk2 - 2 * vivk * cik);

                            auto pik = ( vi2 * icRe - vivk * ( icRe * cik + icIm * sik)) / absVik;
                            auto pki = ( vk2 * icRe - vivk * ( icRe * cik - icIm * sik)) / absVik;
                            auto qik = (-vi2 * icIm + vivk * ( icIm * cik - icRe * sik)) / absVik;
                            auto qki = (-vk2 * icIm + vivk * ( icIm * cik + icRe * sik)) / absVik;

                            busDat.kclPConstrs[iBusPh]->expr += -pik;
                            busDat.kclPConstrs[kBusPh]->expr += -pki;
                            busDat.kclQConstrs[iBusPh]->expr+= -qik;
                            busDat.kclQConstrs[kBusPh]->expr += -qki;
                        }
                    }

                    // Constant S.
                    {
                        Complex sik = opfZip.SConst[iComp];
                        double p = sik.real();
                        double q = sik.imag();

                        if (iZipPh == kZipPh)
                        {
                            busDat.kclPConstrs[iBusPh]->expr += -p;
                            busDat.kclQConstrs[iBusPh]->expr += -q;
                        }
                        else
                        {
                            assert(iZipPh < kZipPh);
                            size_t kBusPh = opfZip.busPhIdxs[kZipPh];
                            auto vi2 = pow(busDat.vVars[iBusPh], 2);
                            auto vk2 = pow(busDat.vVars[kBusPh], 2);
                            auto vivk = busDat.vVars[iBusPh] * busDat.vVars[kBusPh];
                            auto cik = cos(busDat.tVars[iBusPh] - busDat.tVars[kBusPh]);
                            auto sik = sin(busDat.tVars[iBusPh] - busDat.tVars[kBusPh]);
                            auto absVik2 = vi2 + vk2 - 2 * vivk * cik;

                            auto pik =  (vi2 * p - vivk * ( p * cik - q * sik)) / absVik2;
                            auto pki =  (vk2 * p - vivk * ( p * cik + q * sik)) / absVik2;
                            auto qik =  (vi2 * q - vivk * ( p * sik + q * cik)) / absVik2;
                            auto qki =  (vk2 * q - vivk * (-p * sik + q * cik)) / absVik2;

                            busDat.kclPConstrs[iBusPh]->expr += -pik;
                            busDat.kclPConstrs[kBusPh]->expr += -pki;
                            busDat.kclQConstrs[iBusPh]->expr += -qik;
                            busDat.kclQConstrs[kBusPh]->expr += -qki;
                        }
                    }
                }
            }

            // KCL: from slack
            for (auto& busDat : this->nlp_->busData_)
            {
                for (size_t iBusPh = 0; iBusPh < busDat.pSlackInjVars.size(); ++iBusPh)
                {
                    busDat.kclPConstrs[iBusPh]->expr += busDat.pSlackInjVars[iBusPh] - busDat.pSlackConVars[iBusPh];
                    busDat.kclQConstrs[iBusPh]->expr += busDat.qSlackInjVars[iBusPh] - busDat.qSlackConVars[iBusPh];
                }
            }

            for (auto& branchDat: this->nlp_->branchData_)
            {
                for (auto iSidePh = 0u; iSidePh < branchDat.sMaxConstrs.size(); ++iSidePh)
                {
                    branchDat.sMaxConstrs[iSidePh]->expr += pow(branchDat.pOutExprs[iSidePh], 2)
                        + pow(branchDat.qOutExprs[iSidePh], 2);
                }
            }

            // ---------------------------------------------------------------------
            // Objective

            if (!this->pfMode)
            {
                // Don't add objective for pfMode.
                for (auto& genDat : this->nlp_->genData_)
                {
                    auto& opfGen = *genDat.opfGen;
                    for (size_t iGenPh = 0; iGenPh < opfGen.nPhase(); ++iGenPh)
                    {
                        this->nlp_->obj_ += genDat.pVars[iGenPh] * opfGen.pCostLin +
                            pow(genDat.pVars[iGenPh], 2) * opfGen.pCostQuad;
                    }
                }
            }

            for (auto& busDat : this->nlp_->busData_)
            {
                for (size_t iBusPh = 0; iBusPh < busDat.pSlackInjVars.size(); ++iBusPh)
                {
                    this->nlp_->obj_ += this->slackPen * (
                            busDat.pSlackInjVars[iBusPh] + busDat.pSlackConVars[iBusPh] +
                            busDat.qSlackInjVars[iBusPh] + busDat.qSlackConVars[iBusPh]
                    );
                }
            }
        }
            
        virtual void addConstraintsAndObjective() override
        {
            // Add all constraints and objective to the internal NLP.
            for (auto& busDat: this->nlp_->busData_) busDat.addConstraints(this->nlp_->nlpMod_);
            for (auto& genDat: this->nlp_->genData_) genDat.addConstraints(this->nlp_->nlpMod_);
            for (auto& zipDat: this->nlp_->zipData_) zipDat.addConstraints(this->nlp_->nlpMod_);
            for (auto& branchDat: this->nlp_->branchData_) branchDat.addConstraints(this->nlp_->nlpMod_);
            this->nlp_->netwData_.addConstraints(this->nlp_->nlpMod_);
            this->nlp_->nlpMod_.setObj(this->nlp_->obj_);
        }

        virtual void applySolution() override
        {
            for (auto& busDat : this->nlp_->busData_)
            {
                auto& opfBus = *busDat.opfBus;
                for (size_t iBusPh = 0; iBusPh < opfBus.nPhase(); ++iBusPh)
                {
                    double v = busDat.vVars[iBusPh].x();
                    double t = busDat.tVars[iBusPh].x();
                    opfBus.V[iBusPh] = {v * cos(t), v * sin(t)};
                }
            }

            for (auto& genDat : this->nlp_->genData_)
            {
                auto& opfGen = *genDat.opfGen;
                for (size_t iGenPh = 0; iGenPh < opfGen.nPhase(); ++iGenPh)
                {
                    opfGen.SGen[iGenPh] = {genDat.pVars[iGenPh].x(), genDat.qVars[iGenPh].x()};
                }
            }
        }

        virtual void print() const override
        {
            using namespace std;

            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << "Bus Data" << std::endl;
            sgtLogMessage() << "=========================" << std::endl;
            for (const auto& busData : this->nlp_->busData_) busData.print();
            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << "Gen Data" << std::endl;
            sgtLogMessage() << "=========================" << std::endl;
            for (const auto& genData : this->nlp_->genData_) genData.print();
            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << "Zip Data" << std::endl;
            sgtLogMessage() << "=========================" << std::endl;
            for (const auto& zipData : this->nlp_->zipData_) zipData.print();
            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << "Branch Data" << std::endl;
            sgtLogMessage() << "=========================" << std::endl;
            for (const auto& branchData : this->nlp_->branchData_) branchData.print();
            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << "Netw Data" << std::endl;
            sgtLogMessage() << "=========================" << std::endl;
            this->nlp_->netwData_.print();
            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << "Objective" << std::endl;
            sgtLogMessage() << "=========================" << std::endl;
            sgtLogMessage() << this->nlp_->obj_ << endl;
            sgtLogMessage() << "=========================" << std::endl;
        }

        protected:

        std::unique_ptr<OpfModel> opfMod_;
    };
   
    /// @brief Adapt OpfSolver to a PowerFlowSolver
    template<typename Nlp = OpfSPolNlp<>>
    class OpfSPolPfSolver : public PowerFlowSolver
    {
        public:

        virtual bool solve(Network& netw) override
        {
            bool ok = opfSPolSolver_.solveOpf(netw);
            if (ok) applyOpfModel(*opfSPolSolver_.opfMod_, netw);
            return ok;
        }

        virtual void setOptions(const json& options) override
        {
            opfSPolSolver_.setOpts(options);
        }

        private:

        OpfSPolSolver<Nlp> opfSPolSolver_;
    };
}

#endif // OPF_S_POL_SOLVER_DOT_H
