// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "PfModelNetworkInterface.h"

using namespace arma;
using namespace std;

namespace Sgt
{
    namespace
    {
        vector<size_t> indices(const Phases& of, const Phases& in)
        {
            vector<size_t> result;
            result.reserve(in.size());
            for (const auto& p : of)
            {
                result.push_back(in.index(p));
            }
            return result;
        }
    }

    unique_ptr<PfModel> buildPfModel(const Network& netw, bool analyseTopology,
            const function<bool (const Bus&)> selBus)
    {
        unique_ptr<PfModel> mod(new PfModel);
        for (auto bus : netw.buses())
        {
            if (selBus(*bus) && bus->isInService())
            {
                bool isEnabledSv = bus->setpointChanged().isEnabled();
                bus->setpointChanged().setIsEnabled(false);
                BusType busTypeSv = bus->type();
                if (bus->type() != BusType::PQ)
                {
                    if (bus->nInServiceGens() == 0)
                    {
                        sgtLogWarning(LogLevel::VERBOSE) << "Bus " << bus->id() << " has type " << bus->type()
                            << ", but does not have any in service generators. Temporarily setting type to PQ."
                            << endl;
                        bus->setType(BusType::PQ);
                    }
                }

                bus->applyVSetpoints();
                mod->addBus(bus->id(), bus->phases().size(), bus->type(),
                        netw.Y2Pu(bus->YConst(), bus->VBase()), netw.I2Pu(bus->IConst(), bus->VBase()),
                        netw.S2Pu(bus->SConst()), netw.V2Pu(bus->V(), bus->VBase()), netw.S2Pu(bus->SGenRequested()));

                bus->setType(busTypeSv);
                bus->setpointChanged().setIsEnabled(isEnabledSv);
            }
        }
        for (auto branch : netw.branches())
        {
            if (branch->isInService() && 
                    selBus(*branch->bus0()) && branch->bus0()->isInService() &&
                    selBus(*branch->bus1()) && branch->bus1()->isInService())
            {
                mod->addBranch(
                        branch->bus0()->id(), indices(branch->phases0(), branch->bus0()->phases()),
                        branch->bus1()->id(), indices(branch->phases1(), branch->bus1()->phases()),
                        netw.YBus2Pu(
                            branch->Y(), branch->bus0()->VBase(), branch->bus1()->VBase(), branch->nPhases0())
                        );
            }
        }
        mod->validate(analyseTopology);
        return mod;
    }

    void applyPfModel(const PfModel& mod, Network& netw)
    {
        for (const auto& modBus: mod.buses())
        {
            auto bus = netw.buses()[modBus->id];

            int nInService = bus->nInServiceGens();

            Col<Complex> SGen = netw.pu2S(
                    nInService > 0 ? (modBus->SGen) / nInService : Col<Complex>(bus->phases().size(), fill::zeros)
            );

            bus->setV(netw.pu2V(modBus->V, bus->VBase()));
            bus->setSGenUnserved(Col<Complex>(bus->phases().size(), fill::zeros));
            bus->setSZipUnserved(Mat<Complex>(bus->phases().size(), bus->phases().size(), fill::zeros)); 
            // TODO: allow unserved.
            switch (bus->type())
            {
                case BusType::SL:
                    for (auto gen : bus->gens())
                    {
                        if (gen->isInService())
                        {
                            gen->setInServiceS(SGen);
                        }
                    }
                    break;
                case BusType::PQ:
                    break;
                case BusType::PV:
                    for (auto gen : bus->gens())
                    {
                        if (gen->isInService())
                        {
                            // Keep P for gens, distribute Q amongst all gens.
                            Col<Complex> SNew(gen->S().size());
                            for (uword i = 0; i < SNew.size(); ++i)
                            {
                                SNew(i) = Complex(gen->S()(i).real(), SGen(i).imag());
                            }
                            gen->setInServiceS(SNew);
                        }
                    }
                    break;
                default:
                    sgtError("Bad bus type.");
            }
        }
    }
}
