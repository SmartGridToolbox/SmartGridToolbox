// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TRANSFORMER_DOT_H
#define TRANSFORMER_DOT_H

#include <SgtCore/Branch.h>

namespace Sgt
{
    using WindingType = unsigned short int;

    namespace WindingTypes
    {
        // Winding types
        constexpr WindingType N = 1; // Neutral modifier
        constexpr WindingType Y = 2;
        constexpr WindingType YN = Y + N;
        constexpr WindingType D = 4;
        constexpr WindingType Z = 8;
        constexpr WindingType A = 16; // Autotranformer winding - only used for secondary to denote an autotx
        constexpr WindingType V = 32; // Open-delta winding
    }
    
    struct VectorGroup
    {
        std::string str;

        WindingType wt0;
        WindingType wt1;
        unsigned int shift;

        VectorGroup(const std::string& s);
    };

    /// @brief Representation of the wiring topology of a transformer.
    ///
    /// A transformer wiring is represented as a list of winding pairs. Note that in this context, e.g.
    /// "3 winding pairs" is different from the common terminology "3 winding transformer". The latter is used to refer
    /// to a transformer with windings that come in triplets rather than pairs. A winding pair is made up of two paired
    /// windings, or coils. The word "side" refers to primary (0) or secondary (1) sides of a winding. The word "end"
    /// refers to the top (0) or bottom (1) ends of a winding.
    ///
    /// Each winding pair has four winding terminals: primary top, primary bottom, secondary top, secondary bottom.
    ///
    /// Each side also has a list of connection terminals, representing the non-grounded connection terminals of the
    /// transformer. Each winding terminal either has a corresponding connection terminal, or else is deemed to be
    /// grounded.
    ///
    /// For each side i, a W (Winding) matrix is defined.
    ///
    /// The rows are indexed as wisj, w = winding, s = side:
    /// w0s0, w0s1, w1s0, w1s1, w2s0, w2s1 ...
    /// There are therefore 2 nWindPairs rows.
    ///
    /// The columns are indexed as tisj, t = terminal, s = side:
    /// t0s0, t1s0, t2s0 ... t0s1, t1s1, t2s1 ...
    /// There are therefore nTerm columns.
    /// 
    /// Elements can be either 1 or -1, depending on whether the given winding begins (1) or ends (-1) at the given
    /// terminal. If a winding begins or ends at ground, the corresponding 1 or -1 is absent.
    /// 
    /// The terminal voltages can be mapped to winding voltages using the W matrix:
    /// V_wind = W V_term.
    /// The winding currents can be mapped to the terminal current injections using the transpose of the W matrix:
    /// I_term = W^T I_wind.
    ///
    /// For convenience, in addition to low level constructors, we also provide a constructor that takes a vector group,
    /// a number of winding pairs, and the grounding.
    class TransformerWiring
    {
        public:

        TransformerWiring(const std::string& vecGpStr, arma::uword nWindPairs, bool isGrounded0, bool isGrounded1);

        TransformerWiring(arma::uword nTerm0, arma::uword nTerm1, const arma::Mat<double>& W,
                const std::vector<std::array<arma::uword, 2>>& ties = {});

        /// @brief The number of winding pairs.
        auto nWindPairs() const {return W_.n_rows / 2;}

        /// @brief The number of terminals on side 0 (primary).
        auto nTerm0() const {return nTerm0_;}

        /// @brief The number of terminals on side 1 (secondary).
        auto nTerm1() const {return nTerm1_;}

        /// @brief The total number of terminals on side 0 and 1.
        auto nTerm() const {return nTerm0_ + nTerm1_;}

        /// @brief The W matrix.
        const auto& W() const {return W_;}

        /// @brief Vector group.
        ///
        /// Annotation only; may not be known.
        const auto& vectorGroup() const {return vecGpStr_;}

        /// @brief Ties are a kludge that allows a primary terminal to be solidly tied to a secondary terminal.
        ///
        /// Currently used only for open delta transformers.
        const auto& ties() const {return ties_;}

        private:

        arma::uword nTerm0_;
        arma::uword nTerm1_;
        arma::Mat<double> W_; ///< 2 nWindPairs x nTerm, 1 if winding begins at terminal, -1 if it ends at terminal.
        
        std::string vecGpStr_{"unknown"};

        std::vector<std::array<arma::uword, 2>> ties_;
    };
    
    /// @brief Single phase transformer admittance matrix.
    ///
    /// This utility function returns Y for a single phase transformer, such that
    /// [I_{winding,p}, I_{winding_s}]^T = Y[V_{winding,p}, V_{winding_s}].
    /// Thus, it is the bus admittance matrix for a single phase, grounded wye transformer.
    /// @param turnsRatio The complex turns ratio
    /// @param ZSeries The series impedance, referred to either primary or secondary as specified by impedanceSide. 
    /// @param YShunt The shunt impedance, referred to either primary or secondary as specified by impedanceSide. 
    arma::Mat<Complex> windingY(
            const Complex& turnsRatio, const Complex& ZSeries, const Complex& YShunt, const arma::uword impedanceSide);

    struct TcTap
    {
        enum class Type {ALL, SINGLE};

        Type type;
        arma::uword tapIdx;
    };
    inline bool operator<(const TcTap& lhs, const TcTap& rhs)
    {
        return (lhs.type < rhs.type) || (lhs.type == rhs.type && lhs.tapIdx < rhs.tapIdx);
    }

    inline TcTap tcSingleTap(arma::uword tapIdx)
    {
        return {TcTap::Type::SINGLE, tapIdx};
    }

    inline TcTap tcAllTaps()
    {
        return {TcTap::Type::ALL, 0};
    }

    struct TcInputWinding
    {
        enum class Type {SINGLE, AVG};

        Type type;
        arma::uword side;
        arma::uword idx;
    };

    inline TcInputWinding tcInputWindingSingle(arma::uword side, arma::uword idx)
    {
        return {TcInputWinding::Type::SINGLE, side, idx};
    }

    inline TcInputWinding tcInputWindingAvg(arma::uword side)
    {
        return {TcInputWinding::Type::AVG, side, 0};
    }

    class Transformer;

    enum class TcState {BELOW, WITHIN, ABOVE};

    class TapChanger
    {
        public:

        /// @brief Tap changer constructor.
        TapChanger(
                Transformer& transformer,
                const TcTap& tcTap,
                const TcInputWinding& tcInputWinding,
                double setpoint,
                double tolerance,
                bool hasLdc = false,
                Complex ZLdc = {0.0, 0.0},
                Complex ldcTopFactorI = 1.0);

        /// @brief Transformer.
        const Transformer& transformer() const {return trans_;}

        /// @brief Transformer.
        Transformer& transformer() {return trans_;}

        /// @brief Controlled tap(s).
        const TcTap& tcTap() const {return tcTap_;}

        /// @brief Winding(s) from which control voltage is calculated.
        const TcInputWinding& tcInputWinding() const {return tcInputWinding_;}
        
        /// @brief Voltage setpoint.
        double setpoint() const {return setpoint_;}

        /// @brief Tolerance above and below setpoint (determines deadband).
        double tolerance() const {return tolerance_;}

        /// @brief Line drop compensation impedance.
        Complex ZLdc() const {return ZLdc_;}
        
        /// @brief Topological multiplication factor for I, for the purpose of line drop compensation.
        Complex ldcTopFactorI() const {return ldcTopFactorI_;}

        /// @brief The tap(s) value.
        int tap() const;

        /// @brief Set the controlled tap(s) to the given value.
        void setTap(int tap);
        
        /// @brief The control voltage.
        double ctrlV() const;

        /// @brief Are we below, within or above tolerance?
        TcState state() const;

        /// @brief Lower (-1) or raise (1) tap to raise V?
        int sign() const;
        
        /// @brief Change taps one step up or down if necessary and possible.
        bool runOnce();
        
        /// @}

        private:

        Transformer& trans_; // Target transformer.

        TcTap tcTap_; // Which winding does this tap changer use?
        TcInputWinding tcInputWinding_; // Do we use a voltage across a single winding, or the average across all windings?

        double setpoint_; // Voltage setpoint.
        double tolerance_{0.02}; // Tolerance before changing taps.
        bool hasLdc_; // Is there line drop compensation?
        Complex ZLdc_; // Impedance for LDC.
        Complex ldcTopFactorI_; // LDC topological factor that projects I in winding to a line current.
    };
        
    /// @brief Transformer
    ///
    /// This class represents a large range of two winding transformers such as three phase delta and wye transformers.
    /// 
    /// The transformer topology is represented by the TransformerWiring class. A TransformerWiring may be passed to the
    /// Transformer constructor, or as a more convenient alternative there is a constructor that takes a vector group,
    /// the number of winding pairs, and the primary and secondary grounding. Please see the documentation for the
    /// constructor for a detailed description of these parameters.
    ///
    /// The transformer ratio is represented as two numbers: the nominal turns ratio, and an off-nominal ratio.
    ///
    /// The nominal ratio is n_turns_primary / n_turns_secondary. This will also be equal to the ratio of the primary
    /// voltage and the secondary voltage *across the windings*. For example, a DYN11 transformer has a delta winding
    /// on the primary and a wye winding on the secondary. Suppose we're working with an 11 kV (line-to-line) network
    /// on the primary side and a 415 V (line-to-line) network on the secondary side. The primary, delta windings are
    /// line to line, so the nominal voltage across the windings will be 11 kV. The secondary, wye windings are line to
    /// ground, so the nominal voltage will be the line-to-ground voltage equivalent to 415 V line to line, i.e.
    /// 415 / sqrt(3) = 240 V. The nominal turns ratio will be 415000/240 = 45.83.
    ///
    /// The off-nominal ratio is a fraction that modifies the nominal ratio, usually due to transformer taps. In the
    /// example above, if the off-nominal ratio is 1.05, then the actual turns ratio will be 45.83 * 1.05 = 48.125.
    /// Note that a *higher* ratio will lead to a *lower* secondary voltage on the transformer.
    class Transformer : public BranchAbc
    {
        public:

        SGT_PROPS_INIT(Transformer);
        SGT_PROPS_INHERIT(BranchAbc);

        static const std::string& sComponentType()
        {
            static std::string result("transformer");
            return result;
        }
       
        /// @brief Transformer constructor taking a vector group string, number of windings and grounding.
        ///
        /// Regular transformer vector groups can be split into two sides, e.g. for a dyn11 transformer with grounded
        /// neutral and three winding pairs, side 0 is delta, 3 terminals, ungrounded and side 1 is wye, 3 terminals,
        /// grounded. The options are set out below:
        ///
        /// Y/D nWindPairs isGrounded  nTerm   Description
        /// Y    3          F           4       3 winding Y with explicit ungrounded N
        /// Y    3          T           3       3 winding Y with implicit grounded N
        /// Y    2          F           3       2 winding Y with explicit ungrounded N
        /// Y    2          T           2       2 winding Y with implicit grounded N
        /// Y    1          F           2       1 winding Y with explicit ungrounded N
        /// Y    1          T           1       1 winding Y with implicit grounded N
        /// D    3          F           3       3 winding D, ungrounded
        /// D    1          F           2       1 winding D, ungrounded
        ///
        /// In addition, the vector group can be yya0/yyan0 with 3 winding pairs (wye autotransformer), dda0 with 3
        /// winding pairs (delta autotransformer), and vv0 with 2 winding pairs (open delta or VV transformer).
        ///
        /// The presence of neutral in the vector group refers to whether the neutral terminal is physically
        /// brought out of the transformer for connection. In SGT, we ignore this: an ungrounded neutral
        /// is considered to always be available as an explicit terminal, and a grounded neutral is considered to
        /// never be available as an explicit terminal. The user is always free to make sure an explicit neutral
        /// remains unconnected, whenever it is not brought out.
        /// 
        /// Non-solid ground connections must currently modelled by having an explicit neutral phase, and connecting
        /// a Zip component to that neutral phase, with a constant Y load component representing the impedance to
        /// ground.
        Transformer(const std::string& id,
                const std::string& vecGpStr, arma::uword nWindPairs, bool isGrounded0, bool isGrounded1,
                const Complex& nomTurnsRatio, const Complex& ZSeries, const Complex& YShunt, arma::uword impedanceSide,
                int minTap = 0, int maxTap = 0, double tapFactor = 0.0, unsigned int tapSide = 0) :
            Transformer(id, TransformerWiring(vecGpStr, nWindPairs, isGrounded0, isGrounded1),
                    nomTurnsRatio, ZSeries, YShunt, impedanceSide, minTap, maxTap, tapFactor, tapSide) 
        {}

        /// @brief Low level transformer constructor using TransformerWiring.
        Transformer(const std::string& id, const TransformerWiring& wiring,
                const Complex& nomTurnsRatio, const Complex& ZSeries, const Complex& YShunt, arma::uword impedanceSide,
                int minTap = 0, int maxTap = 0, double tapFactor = 0.0, unsigned int tapSide = 0);

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        /// @name Overridden from BranchAbc:
        /// @{

        virtual unsigned int nPhases0() const override {return wiring_.nTerm0();}
        
        virtual unsigned int nPhases1() const override {return wiring_.nTerm1();}

        virtual arma::Mat<Complex> inServiceY() const override;

        /// @}
        
        /// @brief Wiring
        virtual const TransformerWiring& wiring() const {return wiring_;}

        /// @brief Nominal turns ratio.
        const Complex& nomTurnsRatio() const {return nomTurnsRatio_;}

        SGT_PROP_GET(nomTurnsRatio, const Complex&, nomTurnsRatio);
        
        /// @brief Off nominal turns ratio.
        arma::Col<Complex> offNomTurnsRatio() const;

        SGT_PROP_GET(offNomTurnsRatio, arma::Col<Complex>, offNomTurnsRatio);

        // @brief Turns ratio.
        arma::Col<Complex> turnsRatio() const {return nomTurnsRatio_ * offNomTurnsRatio();}

        SGT_PROP_GET(turnsRatio, arma::Col<Complex>, turnsRatio);

        /// @brief Leakage impedance.
        const Complex& ZSeries() const {return ZSeries_;}

        SGT_PROP_GET(ZSeries, const Complex&, ZSeries);

        /// @brief Magnetising admittance.
        const Complex& YShunt() const {return YShunt_;}

        SGT_PROP_GET(YShunt, const Complex&, YShunt);

        /// @brief Impedance side.
        arma::uword impedanceSide() const {return impedanceSide_;}
        
        SGT_PROP_GET(impedanceSide, arma::uword, impedanceSide);
        
        /// @brief Min tap.
        int minTap() const {return minTap_;}

        SGT_PROP_GET(minTap, int, minTap);

        /// @brief Max tap.
        int maxTap() const {return maxTap_;}
        
        SGT_PROP_GET(maxTap, int, maxTap);

        /// @brief Tap factor.
        double tapFactor() const {return tapFactor_;}
        
        SGT_PROP_GET(tapFactor, double, tapFactor);

        /// @brief Tap side.
        unsigned int tapSide() const {return tapSide_;}
        
        SGT_PROP_GET(tapSide, unsigned int, tapSide);

        /// @brief Taps.
        const arma::Col<int>& taps() const {return taps_;}

        /// @brief Set taps to a vector of values.
        void setTaps(const arma::Col<int>& taps);

        SGT_PROP_GET_SET(taps, const arma::Col<int>&, taps, setTaps);
        
        /// @brief Set all taps to the same value.
        void setEqualTaps(int tap);
        
        SGT_PROP_SET(equalTaps, int, setEqualTaps);

        /// @brief Set a single tap.
        void setTap(int tap, int idx);

        /// @brief Add a tap changer.
        void addTapChanger(
                const TcTap& tcTap,
                const TcInputWinding& tcInputWinding,
                double setpoint,
                double tolerance,
                bool hasLdc = false,
                Complex ZLdc = {0.0, 0.0},
                Complex ldcTopFactorI = 1.0);

        /// @brief Tap changers.
        std::vector<std::reference_wrapper<const TapChanger>> tapChangers() const;

        /// @brief Tap changers.
        std::vector<std::reference_wrapper<TapChanger>> tapChangers();

        /// @brief Tap changer for a particular winding (or all windings).
        const TapChanger& tapChanger(const TcTap& tcTap) const
        {
            return tapChangers_.at(tcTap);
        }
        
        /// @brief Tap changer for a particular winding (or all windings).
        TapChanger& tapChanger(TcTap& tcTap)
        {
            return tapChangers_.at(tcTap);
        }

        bool runTapChangersOnce();

        /// @brief Bus admittance matrix for each winding.
        const std::vector<arma::Mat<Complex>>& windingYs() const {return windingYs_;}

        /// @brief Base voltages across primary, secondary windings.
        std::array<arma::Col<double>, 2> VWindingBase() const;
        
        SGT_PROP_GET(VWindingBase, SGT_PROPS_ARG(std::array<arma::Col<double>, 2>), VWindingBase);

        /// @brief Voltages across primary, secondary windings.
        std::array<arma::Col<Complex>, 2> VWindings() const;
        
        SGT_PROP_GET(VWindings, SGT_PROPS_ARG(std::array<arma::Col<Complex>, 2>), VWindings);

        /// @brief Load currents across primary, secondary windings.
        ///
        /// Excludes any circular currents that may exist.
        std::array<arma::Col<Complex>, 2> IWindings() const;
        
        SGT_PROP_GET(IWindings, SGT_PROPS_ARG(std::array<arma::Col<Complex>, 2>), IWindings);

        protected:

        void invalidate() const;

        void ensureValid() const;

        private:

        void calcAndSetY() const;

        void checkTaps() const;

        private:

        TransformerWiring wiring_;

        Complex nomTurnsRatio_{1.0, 0.0};
        
        Complex ZSeries_{0.0, 0.0};
        Complex YShunt_{0.0, 0.0};
        arma::uword impedanceSide_{1};
        
        int minTap_{0};
        int maxTap_{0};
        double tapFactor_{0.0};
        unsigned int tapSide_{0};
        
        arma::Col<int> taps_{0};
        
        mutable bool isValid_{false};
        mutable std::vector<arma::Mat<Complex>> windingYs_;
        mutable arma::Mat<Complex> Y_;

        std::map<TcTap, TapChanger> tapChangers_;
    };
}

#endif // TRANSFORMER_DOT_H
