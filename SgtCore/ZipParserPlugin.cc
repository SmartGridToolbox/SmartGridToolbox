// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ZipParserPlugin.h"

#include "Bus.h"
#include "Network.h"
#include "Zip.h"
#include "YamlSupport.h"

namespace Sgt
{
    void ZipParserPlugin::parse(const YAML::Node& nd, Network& netw, const ParserBase& parser) const
    {
        auto zip = parseZip(nd, parser);

        assertFieldPresent(nd, "bus_id");

        std::string busId = parser.expand<std::string>(nd["bus_id"]);
        Phases phases = parser.expand<Phases>(nd["phases"]);

        netw.addZip(std::move(zip), busId, phases);
    }

    std::unique_ptr<Zip> ZipParserPlugin::parseZip(const YAML::Node& nd,
            const ParserBase& parser) const
    {
        assertFieldPresent(nd, "id");
        assertFieldPresent(nd, "phases");
        assertFieldPresent(nd, "connection");

        std::string id = parser.expand<std::string>(nd["id"]);
        Phases phases = parser.expand<Phases>(nd["phases"]);
        arma::Mat<arma::uword> con;
        auto conNd = parser.expand(nd["connection"]);
        if (conNd.IsScalar())
        {
            auto conStr = conNd.as<std::string>();
            if (conStr == "wye")
            {
                con = Zip::wyeConnection(phases.size());
            }
            else if (conStr == "delta")
            {
                con = Zip::deltaConnection(phases.size());
            }
        }
        else
        {
            con = conNd.as<arma::Mat<arma::uword>>();
        }

        std::unique_ptr<Zip> zip(new Zip(id, phases.size(), con));

        auto ndYConst = nd["Y_const"];
        auto ndIConst = nd["I_const"];
        auto ndSConst = nd["S_const"];

        if (ndYConst)
        {
            zip->setYConst(parser.expand<arma::Col<Complex>>(nd["Y_const"]));
        }
        if (ndIConst)
        {
            zip->setIConst(parser.expand<arma::Col<Complex>>(nd["I_const"]));
        }
        if (ndSConst)
        {
            zip->setSConst(parser.expand<arma::Col<Complex>>(nd["S_const"]));
        }

        return zip;
    }

}
