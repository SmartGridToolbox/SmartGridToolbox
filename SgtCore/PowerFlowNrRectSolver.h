// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef POWER_FLOW_NR_RECT_SOLVER_DOT_H
#define POWER_FLOW_NR_RECT_SOLVER_DOT_H

#include "Common.h"
#include "PfModel.h"
#include "PowerFlowSolver.h"

// Terminology:
// "Bus" and "Branch" refer to n-phase objects i.e. they can contain several phases.
// "Node" and "Link" refer to individual bus conductors and single phase lines.
// A three phase network involving buses and branches can always be decomposed into a single phase network
// involving nodes and links. Thus use of buses and branches is simply a convenience that lumps together nodes and
// links.

namespace Sgt
{
    struct Jacobian
    {
        arma::SpMat<Complex> dDdVr;
        arma::SpMat<Complex> dDdVi;
        arma::SpMat<Complex> dDPvdQPv;

        Jacobian(arma::uword nPq, arma::uword nPv);
    };

    /// @brief Newton-Raphson AC power flow solver.
    /// @ingroup PowerFlowCore
    class PowerFlowNrRectSolver : public PowerFlowSolver
    {
        public:

        virtual bool solve(Network& netw) override;

        private:

        void init();

        bool solve();
        
        virtual void setOptions(const json& options) override;
    
        arma::uword nVar() const
        {
            return 2 * pfIsland_->nPqPv();
        }
    
        arma::Col<Complex> calcD(
                const arma::Col<Complex>& V, 
                const arma::Col<Complex>& Scg,
                const arma::Col<double>& M2PvSetpt) const;

        Jacobian calcJ(
                const arma::Col<Complex>& V,
                const arma::Col<Complex>& Scg,
                const arma::Col<double>& M2PvSetpt) const;

        void modifyForPv(
                Jacobian& J,
                arma::Col<Complex>& D,
                const arma::Col<double>& Vr,
                const arma::Col<double>& Vi,
                const arma::Col<double>& M2,
                const arma::Col<double>& M2PvSetpt);

        arma::Col<double> construct_f(const arma::Col<Complex>&D) const;

        arma::SpMat<double> constructJMatrix(const Jacobian& J) const;

        arma::Col<Complex> calcSGenSl(const arma::Col<Complex>& V);

        void debugPrintVars(const arma::Col<double>& Vr, const arma::Col<double>& Vi,
                const arma::Col<Complex>& Scg) const;
    
        void debugPrintProb(const arma::Col<Complex>& D, const Jacobian& J) const;
        
        void debugPrintFandJMat(const arma::Col<double>& f, const arma::SpMat<double>& JMat) const;

        public:

        // Options:
        double tol{1e-7};
        unsigned int maxIter{20};
        bool analyseTopology{true};

        private:

        Network* netw_;
        std::unique_ptr<PfModel> mod_;
        PfIsland* pfIsland_;

        arma::Col<arma::uword> selDrFrom_f_;
        arma::Col<arma::uword> selDiFrom_f_;

        arma::Col<arma::uword> selVrOrQFrom_x_;
        arma::Col<arma::uword> selViFrom_x_;
    };
}

#endif // POWER_FLOW_NR_RECT_SOLVER_DOT_H
