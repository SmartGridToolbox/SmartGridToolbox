// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef BUS_DOT_H
#define BUS_DOT_H

#include <SgtCore/Common.h>
#include <SgtCore/Component.h>
#include <SgtCore/ComponentCollection.h>
#include <SgtCore/Event.h>
#include <SgtCore/PowerFlow.h>

#include<iostream>
#include<map>
#include<vector>

namespace Sgt
{
    class BranchAbc;
    class Gen;
    class Zip;

    /// @brief A Bus is a grouped set of conductors / terminals, one per phase.
    /// @ingroup PowerFlowCore
    class Bus : virtual public Component
    {
        friend class Network;

        public:

        SGT_PROPS_INIT(Bus);
        SGT_PROPS_INHERIT(Component);

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("bus");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        Bus(const std::string& id, const Phases& phases, const arma::Col<Complex>& VNom, double VBase);

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        virtual json toJson() const override;

        /// @}

        /// @name Identity and type:
        /// @{

        virtual const Phases& phases() const
        {
            return phases_;
        }

        SGT_PROP_GET(phases, const Phases&, phases);

        virtual arma::Col<Complex> VNom() const
        {
            return VNom_;
        }

        SGT_PROP_GET(VNom, arma::Col<Complex>, VNom);

        virtual double VBase() const
        {
            return VBase_;
        }

        SGT_PROP_GET(VBase, double, VBase);

        /// @}

        /// @name Control and limits:
        /// @{

        virtual BusType type() const
        {
            return type_;
        }

        virtual void setType(BusType type);

        SGT_PROP_GET_SET(type, BusType, type, setType);

        virtual arma::Col<double> VMagSetpoint() const
        {
            return VMagSetpoint_;
        }

        virtual void setVMagSetpoint(const arma::Col<double>& VMagSetpoint);

        SGT_PROP_GET_SET(VMagSetpoint, arma::Col<double>, VMagSetpoint, setVMagSetpoint);

        virtual arma::Col<double> VAngSetpoint() const
        {
            return VAngSetpoint_;
        }

        virtual void setVAngSetpoint(const arma::Col<double>& VAngSetpoint);

        SGT_PROP_GET_SET(VAngSetpoint, arma::Col<double>, VAngSetpoint, setVAngSetpoint);

        virtual arma::Col<Complex> VSetpoint() const
        {
            return polar(VMagSetpoint_, VAngSetpoint_);
        }

        virtual void setVSetpoint(const arma::Col<Complex>& VSetpoint)
        {
            setVMagSetpoint(abs(VSetpoint));
            setVAngSetpoint(arg(VSetpoint));
        }

        SGT_PROP_GET_SET(VSetpoint, arma::Col<Complex>, VSetpoint, setVSetpoint);

        virtual void applyVSetpoints();

        virtual double VMagMin() const
        {
            return VMagMin_;
        }

        virtual void setVMagMin(double VMagMin);

        SGT_PROP_GET_SET(VMagMin, double, VMagMin, setVMagMin);

        bool VMagMinViolated() const;
        
        SGT_PROP_GET(VMagMinViolated, double, VMagMinViolated);

        virtual double VMagMax() const
        {
            return VMagMax_;
        }

        virtual void setVMagMax(double VMagMax);

        SGT_PROP_GET_SET(VMagMax, double, VMagMax, setVMagMax);

        bool VMagMaxViolated() const;
        
        SGT_PROP_GET(VMagMaxViolated, double, VMagMaxViolated);

        /// @}

        /// @name State:
        /// @{

        virtual bool isInService() const
        {
            return isInService_;
        }

        virtual void setIsInService(bool isInService);

        SGT_PROP_GET_SET(isInService, bool, isInService, setIsInService);

        virtual bool isSupplied() const
        {
            return isSupplied_;
        }

        virtual void setIsSupplied(bool isSupplied);

        SGT_PROP_GET_SET(isSupplied, bool, isSupplied, setIsSupplied);

        virtual const arma::Col<Complex>& V() const
        {
            return V_;
        }

        SGT_PROP_GET(V, const arma::Col<Complex>&, V);

        virtual void setV(const arma::Col<Complex>& V);

        virtual const arma::Col<Complex>& SGenUnserved() const
        {
            return SGenUnserved_;
        }

        SGT_PROP_GET(SGenUnserved, const arma::Col<Complex>&, SGenUnserved);

        virtual void setSGenUnserved(const arma::Col<Complex>& SGenUnserved)
        {
            SGenUnserved_ = SGenUnserved;
        }

        virtual const arma::Mat<Complex>& SZipUnserved() const
        {
            return SZipUnserved_;
        }

        virtual void setSZipUnserved(const arma::Mat<Complex>& SZipUnserved)
        {
            SZipUnserved_ = SZipUnserved;
        }

        SGT_PROP_GET(SZipUnserved, const arma::Mat<Complex>&, SZipUnserved);

        /// @}

        /// @name Island id:
        /// @{

        int islandIdx() const
        {
            return islandIdx_;
        }

        void setIslandIdx(int islandIdx)
        {
            islandIdx_ = islandIdx;
        }

        SGT_PROP_GET_SET(islandIdx, int, islandIdx, setIslandIdx);

        /// @}

        /// @name Coordinates:
        /// @{

        arma::Col<double> coords() const
        {
            return coords_;
        }

        void setCoords(const arma::Col<double>& coords)
        {
            coords_ = coords;
        }

        SGT_PROP_GET_SET(coords, arma::Col<double>, coords, setCoords);

        /// @}

        /// @name Events:
        /// @{

        /// @brief Event triggered when I go in or out of service.
        virtual const Event& isInServiceChanged() const
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when I go in or out of service.
        virtual Event& isInServiceChanged()
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when my supplied state changes.
        virtual const Event& isSuppliedChanged() const
        {
            return isSuppliedChanged_;
        }

        /// @brief Event triggered when my supplied state changes.
        virtual Event& isSuppliedChanged()
        {
            return isSuppliedChanged_;
        }

        /// @brief Event triggered when bus setpoint has changed.
        virtual const Event& setpointChanged() const
        {
            return setpointChanged_;
        }

        /// @brief Event triggered when bus setpoint has changed.
        virtual Event& setpointChanged()
        {
            return setpointChanged_;
        }

        /// @brief Event triggered when bus state (e.g. voltage) has been updated.
        virtual const Event& voltageUpdated() const
        {
            return voltageUpdated_;
        }

        /// @brief Event triggered when bus state (e.g. voltage) has been updated.
        virtual Event& voltageUpdated()
        {
            return voltageUpdated_;
        }

        /// @}

        /// @name Branches:
        /// @{

        const ComponentCollection<BranchAbc>& branches0() const
        {
            return branches0_;
        }
        ComponentCollection<BranchAbc>& branches0()
        {
            return branches0_;
        }

        const ComponentCollection<BranchAbc>& branches1() const
        {
            return branches1_;
        }
        ComponentCollection<BranchAbc>& branches1()
        {
            return branches1_;
        }

        /// @}

        /// @name Generators:
        /// @{

        const ComponentCollection<Gen>& gens() const
        {
            return gens_;
        }
        ComponentCollection<Gen>& gens()
        {
            return gens_;
        }

        int nInServiceGens() const;

        SGT_PROP_GET(nInServiceGens, int, nInServiceGens);

        /// @brief Requested power of gens.
        arma::Col<Complex> SGenRequested() const;

        SGT_PROP_GET(SGenRequested, arma::Col<Complex>, SGenRequested);

        /// @brief Actual of gens, including possible unserved generation.
        arma::Col<Complex> SGen() const
        {
            return SGenRequested() - SGenUnserved_;
        }

        SGT_PROP_GET(SGen, arma::Col<Complex>, SGen);

        /// @brief Sum of actual generation at bus.
        Complex SGenTot() const
        {
            return sum(SGen());
        }

        SGT_PROP_GET(SGenTot, Complex, SGenTot);

        /// @}

        /// @name Zips:
        /// @{

        const ComponentCollection<Zip>& zips() const
        {
            return zips_;
        }
        ComponentCollection<Zip>& zips()
        {
            return zips_;
        }

        int nInServiceZips() const;

        SGT_PROP_GET(nInServiceZips, int, nInServiceZips);

        /// @brief Constant impedance component of zip, as described in Zip documentation.
        arma::Mat<Complex> YConst() const;

        SGT_PROP_GET(YConst, arma::Mat<Complex>, YConst);

        /// @brief Complex power from constant impedance component.
        arma::Mat<Complex> SYConst() const;

        SGT_PROP_GET(SYConst, arma::Mat<Complex>, SYConst);

        /// @brief Constant current component of zip, as described in Zip documentation.
        ///
        /// Relative to phase of V. Actual current will be IConst V / |V|, so that S doesn't depend on phase of V.
        arma::Mat<Complex> IConst() const;

        SGT_PROP_GET(IConst, arma::Mat<Complex>, IConst);

        /// @brief Complex power from constant current component of zip.
        /// 
        /// Independent of phase of V.
        arma::Mat<Complex> SIConst() const;

        SGT_PROP_GET(SIConst, arma::Mat<Complex>, SIConst);

        /// @brief Complex power component of zip, as described in Zip documentation.
        arma::Mat<Complex> SConst() const;

        SGT_PROP_GET(SConst, arma::Mat<Complex>, SConst);

        /// @brief Requested power of zips.
        arma::Mat<Complex> SZipRequested() const
        {
            return SYConst() + SIConst() + SConst();
        }

        SGT_PROP_GET(SZipRequested, arma::Mat<Complex>, SZipRequested);

        /// @brief Actual power of zips, taking into account possible unserved load.
        arma::Mat<Complex> SZip() const
        {
            return SZipRequested() - SZipUnserved_;
        }

        SGT_PROP_GET(SZip, arma::Mat<Complex>, SZip);

        /// @brief Scalar actual power consumed at bus.
        Complex SZipTot() const
        {
            return sum(sum(trimatu(SZip())));
        }

        SGT_PROP_GET(SZipTot, Complex, SZipTot);

        /// @}

        private:

        Phases phases_{Phase::BAL};
        arma::Col<Complex> VNom_;
        double VBase_{1.0};

        BusType type_{BusType::PQ};
        arma::Col<double> VMagSetpoint_;
        arma::Col<double> VAngSetpoint_;
        double VMagMin_{0.0};
        double VMagMax_{infinity};

        bool isInService_{true};
        bool isSupplied_{true};
        arma::Col<Complex> V_;
        arma::Col<Complex> SGenUnserved_; // SGen + SGenUnserved = SGenRequested
        arma::Mat<Complex> SZipUnserved_; // SZip + SZipUnserved = SZipRequested

        int islandIdx_{-1};

        arma::Col<double>::fixed<2> coords_{{0.0, 0.0}};

        Event isInServiceChanged_{std::string(componentType()) + ": Is in service changed"};
        Event isSuppliedChanged_{std::string(componentType()) + ": Is supplied changed"};
        Event setpointChanged_{std::string(componentType()) + ": Setpoint changed"};
        Event voltageUpdated_{std::string(componentType()) + ": Voltage updated"};

        MutableComponentCollection<BranchAbc> branches0_;
        MutableComponentCollection<BranchAbc> branches1_;
        MutableComponentCollection<Gen> gens_;
        MutableComponentCollection<Zip> zips_;
    };
}

#endif // BUS_DOT_H
