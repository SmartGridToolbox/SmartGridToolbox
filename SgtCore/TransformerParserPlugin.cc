// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TransformerParserPlugin.h"

#include "Transformer.h"
#include "Network.h"

namespace Sgt
{
    void TransformerParserPlugin::parse(const YAML::Node& nd, Network& netw, const ParserBase& parser) const
    {
        assertFieldPresent(nd, "id");
        assertFieldPresent(nd, "bus_0_id");
        assertFieldPresent(nd, "bus_1_id");
        assertFieldPresent(nd, "nom_turns_ratio");
        assertFieldPresent(nd, "series_impedance");
        assertFieldPresent(nd, "vector_group");
        assertFieldPresent(nd, "n_winding_pairs");
        assertFieldPresent(nd, "is_grounded_p");
        assertFieldPresent(nd, "is_grounded_s");

        std::string id = parser.expand<std::string>(nd["id"]);
        std::string bus0Id = parser.expand<std::string>(nd["bus_0_id"]);
        std::string bus1Id = parser.expand<std::string>(nd["bus_1_id"]);

        auto getPhases = [&parser, &id](const YAML::Node& ndPhases, const YAML::Node& ndPhase)
        {
            if (ndPhases && !ndPhase)
            {
                return parser.expand<Phases>(ndPhases);
            }
            else if (!ndPhases && ndPhase)
            {
                return Phases(parser.expand<Phase>(ndPhase));
            }
            else
            {
                sgtError(
                        "Parsing transformer " + id + 
                        ". Must have either \"phase_[01]\" or \"phases_[01]\" keywords but not both.");
            }
        };

        Phases phases0 = getPhases(nd["phases_0"], nd["phase_0"]);
        Phases phases1 = getPhases(nd["phases_1"], nd["phase_1"]);
        Complex nomTurnsRatio = parser.expand<Complex>(nd["nom_turns_ratio"]);
        Complex seriesImpedance = parser.expand<Complex>(nd["series_impedance"]);
        Complex shuntAdmittance = parser.expand<Complex>(nd["shunt_admittance"], 0.0);
        unsigned int impSide = parser.expand<unsigned int>(nd["impedance_side"], 0);
        sgtAssert(impSide == 0 || impSide == 1, "Parsing transformer " + id + ". impedance_side must be 0 or 1.");

        int minTap = parser.expand<int>(nd["min_tap"], 0);
        int maxTap = parser.expand<int>(nd["max_tap"], 0);
        double tapFactor = parser.expand<double>(nd["tap_factor"], 0);
        unsigned int tapSide = parser.expand<unsigned int>(nd["tap_side"], 0);
        sgtAssert(tapSide == 0 || tapSide == 1, "Parsing transformer " + id + ". tap_side must be 0 or 1.");

        auto vecGroup = parser.expand<std::string>(nd["vector_group"]);
        auto nWindPairs = parser.expand<unsigned int>(nd["n_winding_pairs"]);
        auto isGroundedP = parser.expand<bool>(nd["is_grounded_p"]);
        auto isGroundedS = parser.expand<bool>(nd["is_grounded_s"]);

        auto tx = netw.newBranch<Transformer>(bus0Id, phases0, bus1Id, phases1,
                id, vecGroup, nWindPairs, isGroundedP, isGroundedS,
                nomTurnsRatio, seriesImpedance, shuntAdmittance, impSide, minTap, maxTap, tapFactor, tapSide);

        auto ndTaps = nd["taps"];
        auto ndTap = nd["tap"];

        if (ndTaps)
        {
            tx->setTaps(parser.expand<arma::Col<int>>(nd["taps"]));
        }
        else if (ndTap)
        {
            tx->setEqualTaps(parser.expand<int>(nd["tap"]));
        }
        
        auto ndTapChangers = nd["tap_changers"];
        if (ndTapChangers)
        {
            for (const auto& ndTc : ndTapChangers)
            {
                assertFieldPresent(ndTc, "tap");
                assertFieldPresent(ndTc, "input_winding_side");
                assertFieldPresent(ndTc, "input_winding_idx");

                auto tapNd = ndTc["tap"];
                std::string tapStr = parser.expand<std::string>(tapNd);
                TcTap tcTap = (tapStr == "all") ? tcAllTaps() : tcSingleTap(parser.expand<int>(tapNd));

                arma::uword inputWindingSide = parser.expand<arma::uword>(ndTc["input_winding_side"]);
                auto inputWindingIdxNd = ndTc["input_winding_idx"];
                std::string inputWindingStr = parser.expand<std::string>(inputWindingIdxNd);
                TcInputWinding tcInputWinding = (inputWindingStr == "avg")
                    ? tcInputWindingAvg(inputWindingSide)
                    : tcInputWindingSingle(inputWindingSide, parser.expand<arma::uword>(inputWindingIdxNd));

                double setpoint = parser.expand<double>(ndTc["setpoint"]);

                double tolerance = parser.expand<double>(ndTc["tolerance"]);

                bool hasLdc = false;
                Complex ZLdc = 0;
                Complex topFactorLdc = 1.0;
                auto ndZLdc = ndTc["ldc_impedance"];
                if (ndZLdc)
                {
                    hasLdc = true;
                    ZLdc = parser.expand<Complex>(ndZLdc);
                    auto ndTopFactorLdc = ndTc["ldc_top_factor"];
                    if (ndTopFactorLdc)
                    {
                        topFactorLdc = parser.expand<Complex>(ndTopFactorLdc);
                    }
                }

                tx->addTapChanger(tcTap, tcInputWinding, setpoint, tolerance, hasLdc, ZLdc, topFactorLdc);
            }
        }
    }
}
