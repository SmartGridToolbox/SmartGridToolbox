// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SgtCore/config.h>

#include "PowerFlowSolverParserPlugin.h"

#include "json.h"
#include "Network.h"
#include "PowerFlowNrRectSolver.h"
#ifdef ENABLE_OPF
#include "OpfSPolSolver.h"
#endif
#include "YamlSupport.h"

namespace Sgt
{
    void PowerFlowSolverParserPlugin::parse(const YAML::Node& nd, Network& netw,
            [[maybe_unused]] const ParserBase& parser) const
    {
        assertFieldPresent(nd, "solver");

        auto solverStr = nd["solver"].as<std::string>();
        if (solverStr == "nr_rect")
        {
            sgtLogMessage() << "Using Newton-Raphson (rectangular) solver." << std::endl;
            netw.setSolver(std::make_unique<PowerFlowNrRectSolver>());
        }
        else if (solverStr == "opf_s_pol" or solverStr == "opf_s_pol_debug")
        {
#ifdef ENABLE_OPF
            sgtLogMessage() << "Using OPF (Madopt) SPol solver." << std::endl;
            auto solver = std::make_unique<OpfSPolPfSolver<>>();
            if (solverStr == "opf_s_pol_debug") solver->setOptions({{"debug", true}});
            netw.setSolver(std::move(solver));
#else // ENABLE_OPF
            sgtError("OPF solver is not available, since SmartGridToolbox was not compiled with --enable-opf.");
#endif // ENABLE_OPF
        }

        auto ndOptions = nd["options"];
        if (ndOptions)
        {
            netw.solver().setOptions(json::parse(ndOptions.as<std::string>()));
        }

    }
}
