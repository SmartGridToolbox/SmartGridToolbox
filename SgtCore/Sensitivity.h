// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SENSITIVITY_DOT_H
#define SENSITIVITY_DOT_H

#include <SgtCore/Common.h>
#include <SgtCore/Network.h>

namespace Sgt
{
    /// @brief Voltage sensitivy per bus.
    struct Sensitivity
    {
        std::string busId;
        arma::Col<double> dvdx; ///< Sensitivity of p.u. voltage magnitute v to nominal load increase x.
        arma::Col<double> dtdx; ///< Sensitivity of voltage angle t(heta) to nominal load increase x.
    };

    /// @brief Calculate the rate of increase in voltage dv/dx, corresponding to a "nominal load" parameter x.
    ///
    /// @param dSdx Scaling factor that sets the load power for each load.
    ///
    /// dSdx is given as a map, with the key being the load id, and the value being the load increase (as a vector
    /// of load phases) for a unit increase in the "nominal load" x.
    std::list<Sensitivity> sensitivity(const Network& nw, const std::map<std::string, arma::Col<Complex>>& dSdx);
}

#endif // SENSITIVITY_DOT_H
