// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ZIP_DOT_H
#define ZIP_DOT_H

#include <SgtCore/Bus.h>
#include <SgtCore/Component.h>
#include <SgtCore/ComponentCollection.h>
#include <SgtCore/Event.h>
#include <SgtCore/PowerFlow.h>

#include<string>

namespace Sgt
{
    /// @brief A Zip is a power consumption at a bus with constant impedance / current / complex power components.
    ///
    /// These components are described by an N x N matrix, where N is the number of phases.
    /// Diagonal components represent a power consumption between the phase and ground.
    /// Upper off-diagonal components, (i, j) where j > i, represent a power consumption between two phases. 
    /// Lower off-diagonal components are ignored.
    /// Positive real power = consumption, negative = generation.
    ///
    /// @ingroup PowerFlowCore
    class Zip : virtual public Component
    {
        friend class Network;

        public:

        SGT_PROPS_INIT(Zip);
        SGT_PROPS_INHERIT(Component);

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("zip");
            return result;
        }

        /// @brief Return a connection matrix for a wye-connected load.
        static arma::Mat<arma::uword> wyeConnection(arma::uword nPhase);

        /// @brief Return a connection matrix for a delta-connected load.
        static arma::Mat<arma::uword> deltaConnection(arma::uword nPhase);

        /// @}

        /// @name Lifecycle:
        /// @{

        Zip(const std::string& id, unsigned int nPhases, const arma::Mat<arma::uword>& connection);

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        virtual json toJson() const override;

        /// @}

        /// @name Connection:
        /// @{
       
        /// @brief Number of terminal phases.
        unsigned int nPhases() const
        {
            return nPhases_;
        }
        
        SGT_PROP_GET(nPhases, unsigned int, nPhases);
        
        /// @brief The connection defines the nonzero elements of the load components.
        ///
        /// The connection is an nComps x 2 matrix, where each row (i, k) specifies the terminal phase(s) of a nonzero
        /// load component, where by convention k >= i. Diagonal elements (i, i) represent a load component between
        /// terminal phase i and ground. Off-diagonal elements (i, k) represent a load component between terminal
        /// phases i and k.
        const arma::Mat<arma::uword>& connection() const
        {
            return connection_;
        }
        
        SGT_PROP_GET(connection, const arma::Mat<arma::uword>&, connection);

        /// @brief The number of load components.
        ///
        /// This is the number of rows in the connection matrix.
        arma::uword nComps() const
        {
            return connection_.n_rows;
        }

        /// @brief Utility function to map load components to a matrix, using the connection. 
        ///
        /// The resulting matrix is an nPhase x nPhase matrix, whose nonzero elements (i, k) are the load component
        /// (i, k).
        arma::Mat<Complex> compsToMat(const arma::Col<Complex>& comps) const;

        /// @}

        /// @name In service:
        /// @{

        virtual bool isInService() const
        {
            return isInService_;
        }

        virtual void setIsInService(bool isInService)
        {
            isInService_ = isInService;
        }

        SGT_PROP_GET_SET(isInService, bool, isInService, setIsInService);

        /// @}

        /// @name ZIP parameters:
        /// @{

        /// @brief Constant admittance component.
        ///
        /// Each value corresponds to a load component, whose terminal phases are given by the connection.
        virtual arma::Col<Complex> YConst() const
        {
            return YConst_;
        }

        virtual void setYConst(const arma::Col<Complex>& YConst);
        
        SGT_PROP_GET_SET(YConst, arma::Col<Complex>, YConst, setYConst);

        /// @brief Constant current component.
        ///
        /// Each value corresponds to a load component, whose terminal phases are given by the connection.
        virtual arma::Col<Complex> IConst() const
        {
            return IConst_;
        }

        virtual void setIConst(const arma::Col<Complex>& IConst);

        SGT_PROP_GET_SET(IConst, arma::Col<Complex>, IConst, setIConst);

        /// @brief Constant power component.
        ///
        /// Each value corresponds to a load component, whose terminal phases are given by the connection.
        virtual arma::Col<Complex> SConst() const
        {
            return SConst_;
        }

        virtual void setSConst(const arma::Col<Complex>& SConst);

        SGT_PROP_GET_SET(SConst, arma::Col<Complex>, SConst, setSConst);

        /// @}

        /// @name Total Power:
        /// @{

        /// @brief Consumed power, by phase components, when in service.
        ///
        /// Does not take into account any unserved power - this is instead taken into account at the bus level.
        arma::Col<Complex> inServiceS() const;

        /// @brief Consumed power, by phase components, zero if not in service.
        ///
        /// Does not take into account any unserved power - this is instead taken into account at the bus level.
        arma::Col<Complex> S() const
        {
            return isInService_ 
                ? inServiceS()
                : arma::Col<Complex>(nComps(), arma::fill::zeros);
        }

        /// @brief Total consumed power, when in service.
        ///
        /// Does not take into account any unserved power - this is instead taken into account at the bus level.
        Complex inServiceSTot() const
        {
            return sum(inServiceS());
        }

        /// @brief Total consumed power, zero if not in service.
        ///
        /// Does not take into account any unserved power - this is instead taken into account at the bus level.
        Complex STot() const
        {
            return isInService_ ? inServiceSTot() : Complex(0.0, 0.0);
        }

        /// @}

        /// @name Terminal functions:
        /// @{

        /// @brief Voltage at the terminal phases.
        arma::Col<Complex> VTerm() const
        {
            return mapPhases(bus()->V(), bus()->phases(), phases());
        }

        /// @brief Current out of terminal phases into zip.
        arma::Col<Complex> ITerm() const;

        /// @brief Power out of terminal phases into zip.
        arma::Col<Complex> STerm() const
        {
            return conj(ITerm()) % VTerm();
        }

        /// @}

        /// @name Events:
        /// @{

        /// @brief Event triggered when I go in or out of service.
        virtual const Event& isInServiceChanged() const
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when I go in or out of service.
        virtual Event& isInServiceChanged()
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when my terminal injection is changed.
        virtual const Event& injectionChanged() const
        {
            return injectionChanged_;
        }

        /// @brief Event triggered when my terminal injection is changed.
        virtual Event& injectionChanged()
        {
            return injectionChanged_;
        }

        /// @brief Event triggered when a setpoint is changed.
        virtual const Event& setpointChanged() const
        {
            return setpointChanged_;
        }

        /// @brief Event triggered when a setpoint is changed.
        virtual Event& setpointChanged()
        {
            return setpointChanged_;
        }

        /// @}

        /// @name Connection:
        /// @{

        ConstComponentPtr<Bus> bus() const
        {
            return bus_;
        }

        ComponentPtr<Bus> bus()
        {
            return bus_;
        }

        virtual const Phases& phases() const
        {
            return phases_;
        }

        SGT_PROP_GET(phases, const Phases&, phases);

        /// @}

        private:

        unsigned int nPhases_;
        bool isInService_{true};

        arma::Mat<arma::uword> connection_; // n x 2 matrix where each row is the row, col of a load component.
        arma::Col<Complex> YConst_;
        arma::Col<Complex> IConst_;
        arma::Col<Complex> SConst_;

        Event isInServiceChanged_{std::string(componentType()) + ": Is in service changed"};
        Event injectionChanged_{std::string(componentType()) + ": Injection changed"};
        Event setpointChanged_{std::string(componentType()) + ": Setpoint changed"};

        ComponentPtr<Bus> bus_;
        Phases phases_;
    };
}

#endif // ZIP_DOT_H
