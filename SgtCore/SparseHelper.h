// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SPARSE_HELPER_DOT_H
#define SPARSE_HELPER_DOT_H

#include <armadillo>

#include <algorithm>

namespace Sgt
{
    /// @brief Object to efficiently construct large sparse matrices in an incremental manner.
    ///
    /// Use insert(...) to add elements, and when ready, get() to construct and return the sparse matrix.
    /// @ingroup Utilities
    template<typename T> class SparseHelper
    {
        public:

        SparseHelper(arma::uword nRow, arma::uword nCol, bool sort = true, bool add = true,
                bool checkZeros = false, arma::uword nBatch = 0) :
            sort_(sort),
            add_(add),
            checkZeros_(checkZeros),
            m_(nRow, nCol)
        {
            if (nBatch == 0) nBatch = static_cast<arma::uword>(std::min(
                        static_cast<unsigned long long>(nRow) * static_cast<unsigned long long>(nCol),
                        1ull << 16));
            locs_.set_size(2, nBatch); // Doesn't initialize.
            vals_.set_size(nBatch); // Doesn't initialize.
        }

        void insert(arma::uword iRow, arma::uword iCol, const T& x)
        {
            if (sz_ == vals_.size())
            {
                flush();
            }
            locs_(0, sz_) = iRow;
            locs_(1, sz_) = iCol;
            vals_(sz_++) = x;
        }

        arma::SpMat<T> get()
        {
            flush();
            return std::move(m_); // Should apply move constructor.
        }

        private:

        void flush()
        {
            if (sz_ > 0)
            {
                arma::span sp1(0, 1);
                arma::span sp2(0, sz_ - 1);
                m_ += arma::SpMat<T>(add_, locs_(sp1, sp2), vals_(sp2), m_.n_rows, m_.n_cols, sort_, checkZeros_);
                sz_ = 0;
            }
        }

        private:

        arma::uword nRow_{0};
        arma::uword nCol_{0};
        bool sort_;
        bool add_;
        bool checkZeros_;

        arma::uword sz_{0};
        arma::Mat<arma::uword> locs_;
        arma::Col<T> vals_;
        arma::SpMat<T> m_;
    };
}

#endif // SPARSE_HELPER_DOT_H
