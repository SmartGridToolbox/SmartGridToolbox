// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "OpfModelNetworkInterface.h"

using namespace arma;
using namespace std;

namespace Sgt
{
    namespace
    {
        vector<size_t> indices(const Phases& of, const Phases& in)
        {
            vector<size_t> result;
            result.reserve(in.size());
            for (const auto& p : of)
            {
                result.push_back(in.index(p));
            }
            return result;
        }
    }

    unique_ptr<OpfModel> buildOpfModel(const Network& netw, bool pfMode,
            const function<bool (const Bus&)> selBus)
    {
        unique_ptr<OpfModel> mod(new OpfModel());

        for (auto bus : netw.buses())
        {
            if (selBus(*bus) && bus->isInService())
            {
                if (pfMode) bus->applyVSetpoints();
                bool isAngleRef = bus->type() == BusType::SL;
                bool hasAvr = (
                        pfMode &&
                        (bus->type() == BusType::PV || bus->type() == BusType::SL) &&
                        (bus->nInServiceGens() > 0)
                        );
                mod->addBus(bus->id(), netw.V2Pu(bus->V(), bus->VBase()), netw.V2Pu(bus->VMagMin(), bus->VBase()),
                        netw.V2Pu(bus->VMagMax(), bus->VBase()), isAngleRef, hasAvr);  
            }
        }
        
        for (auto zip : netw.zips())
        {
            if (zip->isInService() && selBus(*zip->bus()) && zip->bus()->isInService())
            {
                mod->addZip(
                        zip->id(), 
                        zip->bus()->id(), indices(zip->phases(), zip->bus()->phases()),
                        zip->connection(), netw.Y2Pu(zip->YConst(), zip->bus()->VBase()), 
                        netw.I2Pu(zip->IConst(), zip->bus()->VBase()), netw.S2Pu(zip->SConst()));
            }
        }

        double SInv2Pu = 1.0 / netw.S2Pu(1.0);
        double SInv2Pu2 = pow(SInv2Pu, 2);
        for (auto gen : netw.gens())
        {
            const auto& bus = *gen->bus();
            if (gen->isInService() && selBus(bus) && bus.isInService())
            {
                double pMin; double pMax;
                double qMin; double qMax;
                double sMin; double sMax;
                auto sPu = netw.S2Pu(gen->S());
                if (!pfMode)
                {
                    pMin = netw.S2Pu(gen->PMin());
                    pMax = netw.S2Pu(gen->PMax());
                    qMin = netw.S2Pu(gen->QMin());
                    qMax = netw.S2Pu(gen->QMax());
                    sMin = netw.S2Pu(gen->SMin());
                    sMax = netw.S2Pu(gen->SMax());
                }
                else
                {
                    if (bus.type() == BusType::SL)
                    {
                        pMin = -infinity;
                        pMax = infinity;
                        qMin = -infinity;
                        qMax = infinity;
                        sMin = -infinity;
                        sMax = infinity;
                    }
                    else if (bus.type() == BusType::PV)
                    {
                        pMin = pMax = arma::sum(sPu).real();
                        qMin = -infinity;
                        qMax = infinity;
                        sMin = -infinity;
                        sMax = infinity;
                    }
                    else {
                        Complex sTotPu = arma::sum(sPu);
                        pMin = pMax = sTotPu.real();
                        qMin = qMax = sTotPu.imag();
                        sMin = -infinity;
                        sMax = infinity;
                    }
                }
                mod->addGen(
                        gen->id(), bus.id(), indices(gen->phases(), bus.phases()),
                        sPu, pMin, pMax, qMin, qMax, sMin, sMax,
                        gen->c1() * SInv2Pu, gen->c2() * SInv2Pu2
                ); 
            }
        }

        for (auto branch : netw.branches())
        {
            if (branch->isInService() && 
                    selBus(*branch->bus0()) && branch->bus0()->isInService() &&
                    selBus(*branch->bus1()) && branch->bus1()->isInService())
            {
                double SMagMax = infinity;
                double IMagMax0 = infinity;
                double IMagMax1 = infinity;
                if (branch->SMagMax() != infinity)
                {
                    SMagMax = branch->SMagMax();
                    IMagMax0 = branch->SMagMax() / branch->bus0()->VBase();
                    IMagMax1 = branch->SMagMax() / branch->bus1()->VBase();
                }
                if (branch->IMagMax() != infinity)
                {
                    SMagMax = std::min({
                            SMagMax,
                            branch->IMagMax() * branch->bus0()->VBase(),
                            branch->IMagMax() * branch->bus1()->VBase()
                            });
                    IMagMax0 = std::min({IMagMax0, branch->IMagMax()});
                    IMagMax1 = std::min({IMagMax1, branch->IMagMax()});
                }

                mod->addBranch(branch->id(),
                        branch->bus0()->id(), indices(branch->phases0(), branch->bus0()->phases()),
                        branch->bus1()->id(), indices(branch->phases1(), branch->bus1()->phases()),
                        netw.YBus2Pu(branch->Y(), branch->bus0()->VBase(), branch->bus1()->VBase(), branch->nPhases0()),
                        netw.S2Pu(SMagMax), netw.I2Pu(IMagMax0, branch->bus0()->VBase()),
                        netw.I2Pu(IMagMax1, branch->bus1()->VBase()));
            }
        }
        mod->validate();
        return mod;
    }

    void applyOpfModel(const OpfModel& mod, Network& netw)
    {
        for (const auto& modBus: mod.buses())
        {
            auto bus = netw.buses()[modBus.id];
            bus->setV(netw.pu2V(modBus.V, bus->VBase()));
        }
        for (const auto& modGen: mod.gens())
        {
            auto gen = netw.gens()[modGen.id];
            gen->setInServiceS(netw.pu2S(modGen.SGen));
        }
    }
}
