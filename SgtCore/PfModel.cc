// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "PfModel.h"

#include "SparseHelper.h"

#include <iostream>
#include <sstream>

using namespace arma;
using namespace std;

namespace Sgt
{
    Col<Complex> PfIsland::V() const
    {
        auto it = nodeVec_.begin();
        return Col<Complex>(nodeVec_.size()).imbue([&](){return (**(it++)).V();});
    }

    void PfIsland::setV(const Col<Complex>& V) const
    {
        for (uword i = 0; i < V.size(); ++i)
        {
            nodeVec_[i]->V() = V[i];
        }
    }

    Col<Complex> PfIsland::SGen() const
    {
        auto it = nodeVec_.begin();
        return Col<Complex>(nodeVec_.size()).imbue([&](){return (**(it++)).SGen();});
    }

    void PfIsland::setSGen(const Col<Complex>& SGen) const
    {
        for (uword i = 0; i < SGen.size(); ++i)
        {
            nodeVec_[i]->SGen() = SGen[i];
        }
    }

    void PfModel::addBus(const string& id, size_t nPhase, BusType type,
            const arma::Mat<Complex>& YConst, const arma::Mat<Complex>& IConst, const arma::Mat<Complex>& SConst,
            const arma::Col<Complex>& V, const arma::Col<Complex> SGen)
    {
        sgtLogDebug(LogLevel::VERBOSE) << "PfModel : add bus " << id << endl;
        buses_.push_back(unique_ptr<PfBus>(
                    new PfBus{id, nPhase, type, YZipToYNode(YConst), IConst, SConst, V, SGen, vector<PfNode>()}));
        auto& bus = *buses_.back();
        bus.nodes.reserve(nPhase);
        for (size_t i = 0; i < nPhase; ++i) bus.nodes.push_back({&bus, i});
        busMap_[id] = &bus;
    }

    void PfModel::addBranch(const std::string& idBus0, std::vector<std::size_t> phaseIdxs0,
            const std::string& idBus1, std::vector<std::size_t> phaseIdxs1, const arma::Mat<Complex>& YNd)
    {
        sgtLogDebug(LogLevel::VERBOSE) << "PfModel : addBranch " << idBus0 << " " << idBus1 << endl;
        PfBus* bus0 = busMap_[idBus0];
        PfBus* bus1 = busMap_[idBus1];
        branches_.push_back(unique_ptr<PfBranch>(
                    new PfBranch{{bus0, bus1}, {phaseIdxs0, phaseIdxs1}, YNd}));
    }

    void PfModel::validate(bool analyseTopology)
    {
        sgtLogDebug() << "PfModel : validate." << std::endl;
        sgtLogIndent();

        // Sort buses in order: PQ, PV, SL
        buses_.sort([](const auto& x, const auto& y){
                return (x->type == BusType::PQ && y->type != BusType::PQ)
                || (x->type == BusType::PV && y->type == BusType::SL);
                });

        // Create vector of nodes and temporarily index according to their position in the list.
        size_t nNodes{0}; for (const auto& b : buses_) nNodes += b->nPhase;
        std::vector<PfNode*> nodeVec; nodeVec.reserve(nNodes);
        for (auto& b : buses_) for (auto& n : b->nodes)
        {
            n.idxInModel = nodeVec.size();
            nodeVec.push_back(&n);
        }

        SpMat<Complex> YNd; SpMat<Complex> IConst; SpMat<Complex> SConst;
        fillMatrices(nodeVec, YNd, IConst, SConst);

        buildIslands(nodeVec, YNd, IConst, SConst, analyseTopology);

        sgtLogDebug() << "PfModel : validate complete." << std::endl;
    }
    
    void PfModel::fillMatrices(const vector<PfNode*> nodeVec, 
            arma::SpMat<Complex>& YNd, arma::SpMat<Complex>& IConst, arma::SpMat<Complex>& SConst)
    {
        static const Complex extraCap{0.0, 1e-12};

        std::size_t nNode = nodeVec.size();
        SparseHelper<Complex> YNdHelper(nNode, nNode, true, true, true);
        SparseHelper<Complex> IConstHelper(nNode, nNode, true, true, true);
        SparseHelper<Complex> SConstHelper(nNode, nNode, true, true, true);

        for (auto& bus : buses_)
        {
            for (std::size_t iPh = 0; iPh < bus->nPhase; ++iPh)
            {
                const auto& ndI = bus->nodes[iPh];
                auto iNd = ndI.idxInModel;

                YNdHelper.insert(iNd, iNd, bus->YNd(iPh, iPh)); // NOTE: filters out zeros.
                IConstHelper.insert(iNd, iNd, bus->IConst(iPh, iPh)); // NOTE: filters out zeros.
                SConstHelper.insert(iNd, iNd, bus->SConst(iPh, iPh)); // NOTE: filters out zeros.

                for (std::size_t jPh = iPh + 1; jPh < bus->nPhase; ++jPh)
                {
                    const auto& ndJ = bus->nodes[jPh];
                    auto jNd = ndJ.idxInModel;
                    YNdHelper.insert(iNd, jNd, bus->YNd(iPh, jPh)); // NOTE: filters out zeros.
                    IConstHelper.insert(iNd, jNd, bus->IConst(iPh, jPh)); // NOTE: filters out zeros.
                    SConstHelper.insert(iNd, jNd, bus->SConst(iPh, jPh)); // NOTE: filters out zeros.
                    YNdHelper.insert(jNd, iNd, bus->YNd(iPh, jPh)); // NOTE: filters out zeros.
                    IConstHelper.insert(jNd, iNd, bus->IConst(iPh, jPh)); // NOTE: filters out zeros.
                    SConstHelper.insert(jNd, iNd, bus->SConst(iPh, jPh)); // NOTE: filters out zeros.
                }
            }
        }

        // Branch admittances:
        for (const auto& branch : branches_)
        {
            std::size_t nTerm = branch->phaseIdxs[0].size() + branch->phaseIdxs[1].size();

            // There is one link per distinct pair of bus/phase pairs.
            for (uword i = 0; i < nTerm; ++i)
            {
                bool firstBusI = (i < branch->phaseIdxs[0].size());
                std::size_t busIdxI = firstBusI ? 0 : 1;
                std::size_t branchPhaseIdxI = firstBusI ? i : i - branch->phaseIdxs[0].size();

                const PfBus* busI = branch->buses[busIdxI];
                std::size_t busPhaseIdxI = branch->phaseIdxs[busIdxI][branchPhaseIdxI];
                const PfNode& nodeI = busI->nodes[busPhaseIdxI];
                std::size_t idxNodeI = nodeI.idxInModel;

                // Only count each diagonal element in branch->YNd_ once!
                YNdHelper.insert(idxNodeI, idxNodeI, branch->YNd(i, i));

                if (addExtraCapacitance_)
                {
                    // For some pathological cases where nodes are electrically isolated from ground, this can help.
                    YNdHelper.insert(idxNodeI, idxNodeI, extraCap);
                }

                for (uword k = i + 1; k < nTerm; ++k)
                {
                    bool firstBusK = (k < branch->phaseIdxs[0].size());
                    std::size_t busIdxK = firstBusK ? 0 : 1;
                    std::size_t branchPhaseIdxK = firstBusK ? k : k - branch->phaseIdxs[0].size();

                    const PfBus* busK = branch->buses[busIdxK];
                    std::size_t busPhaseIdxK = branch->phaseIdxs[busIdxK][branchPhaseIdxK];
                    const PfNode& nodeK = busK->nodes[busPhaseIdxK];
                    std::size_t idxNodeK = nodeK.idxInModel;

                    YNdHelper.insert(idxNodeI, idxNodeK, branch->YNd(i, k));
                    YNdHelper.insert(idxNodeK, idxNodeI, branch->YNd(k, i));
                }
            }
        } // Loop over branches.

        YNd = YNdHelper.get();
        IConst = IConstHelper.get();
        SConst = SConstHelper.get();
    }

    void PfModel::buildIslands(const std::vector<PfNode*>& nodeVec,
            const arma::SpMat<Complex>& YNd,
            const arma::SpMat<Complex>& IConst,
            const arma::SpMat<Complex>& SConst,
            bool analyseTopology)
    {
        vector<set<uword>> con(nodeVec.size());
        for (auto& mat : {&YNd, &IConst, &SConst})
        {
            for (auto it = mat->begin(); it != mat->end(); ++it)
            {
                con[it.row()].insert(it.col());
                con[it.col()].insert(it.row());
            }
        }

        size_t nIslands = 0; 
        if (analyseTopology)
        {
            for (size_t i = 0; i < nodeVec.size(); ++i)
            {
                if (nodeVec[i]->islandIdx == -1)
                {
                    islandDfs(nodeVec, i, nIslands, con);
                    ++nIslands;
                }
            }
        }
        else
        {
            for (auto& nd : nodeVec) nd->islandIdx = 0;
            nIslands = 1; 
        }

        islands_.reserve(nIslands);
        for (size_t i = 0; i < nIslands; ++i) islands_.emplace_back(i);

        for (const auto& node : nodeVec)
        {
            auto& island = islands_[node->islandIdx];
            switch (node->bus->type) 
            {
                case BusType::PQ:
                    island.nPq_ += 1;
                    break;
                case BusType::PV:
                    island.nPv_ += 1;
                    break;
                case BusType::SL:
                    island.nSl_ += 1;
                    break;
                default:
                    break;
            }
        }

        for (auto& island : islands_)
        {
            island.nodeVec_.reserve(island.nPq_ + island.nPv_ + island.nSl_);
        }

        for (const auto& node : nodeVec)
        {
            auto& island = islands_[node->islandIdx];
            node->idxInIsland = island.nodeVec_.size();
            island.nodeVec_.push_back(node);
        }

        {
            vector<SparseHelper<Complex>> YNdHelpers; YNdHelpers.reserve(nIslands);
            vector<SparseHelper<Complex>> IConstHelpers; IConstHelpers.reserve(nIslands);
            vector<SparseHelper<Complex>> SConstHelpers; SConstHelpers.reserve(nIslands);
            for (size_t i = 0; i < nIslands; ++i)
            {
                auto n = islands_[i].nodeVec_.size();
                YNdHelpers.emplace_back(n, n, true, true, true);
                IConstHelpers.emplace_back(n, n, true, true, true);
                SConstHelpers.emplace_back(n, n, true, true, true);
            }

            auto fillHelpers = [&nodeVec](vector<SparseHelper<Complex>>& helpers, const SpMat<Complex>& from)
            {
                for (auto it = from.begin(); it != from.end(); ++it)
                {
                    uword iGl = it.row(); uword kGl = it.col();
                    auto iIs = nodeVec[iGl]->idxInIsland; auto kIs = nodeVec[kGl]->idxInIsland;
                    size_t iIsland = nodeVec[iGl]->islandIdx;
                    auto& helper = helpers[iIsland];
                    helper.insert(iIs, kIs, from(iGl, kGl));
                }
            };
            fillHelpers(YNdHelpers, YNd);
            fillHelpers(IConstHelpers, IConst);
            fillHelpers(SConstHelpers, SConst);

            for (size_t i = 0; i < nIslands; ++i)
            {
                islands_[i].YNd_ = YNdHelpers[i].get();
                islands_[i].IConst_ = IConstHelpers[i].get();
                islands_[i].SConst_ = SConstHelpers[i].get();
            }
        }
    }

    void PfModel::islandDfs(const vector<PfNode*>& nodeVec, size_t nodeIdx, size_t islandIdx,
            const vector<set<uword>>& con)
    {
        auto node = nodeVec[nodeIdx];
        if (node->islandIdx != -1) return; // Already done.
        node->islandIdx = islandIdx;

        for (auto other : con[nodeIdx])
        {
            islandDfs(nodeVec, other, islandIdx, con);
        }
    }
}
