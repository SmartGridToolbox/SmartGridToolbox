// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROPERTIES_DOT_H
#define PROPERTIES_DOT_H

#include <SgtCore/json.h>

#include<map>
#include<memory>
#include<stdexcept>
#include<string>
#include<sstream>
#include<type_traits>
#include<vector>

namespace Sgt
{
    using Default = nlohmann::json;
    // NOTE: we can easily treat the "default" type of a property as another template parameter, but this has a performance
    // penalty, for example in increasing the already large file size overhead due to debug symbols.

    // -----------------------------------------------------------------------------------------------------------------
    // Properties

    template<typename T> using D = std::remove_pointer_t<std::decay_t<T>>;
    template<typename T> using CR = const D<T>&;
    template<typename T> using NR = std::remove_reference_t<T>;
    template<typename T> using NP = std::remove_pointer_t<T>;

    template<typename T> constexpr bool isC = std::is_const<NP<NR<T>>>::value;

    /// @brief Exception that is thrown when a property is not gettable.
    /// @ingroup Properties
    class NotGettableException : public std::logic_error
    {
        public:
        NotGettableException() : std::logic_error("Property is not gettable")
        {}
    };

    /// @brief Exception that is thrown when a property is not settable.
    /// @ingroup Properties
    class NotSettableException : public std::logic_error
    {
        public:
        NotSettableException() : std::logic_error("Property is not settable")
        {}
    };

    /// @brief Base class for property implementation.
    /// @ingroup Properties
    class PropertyImplInterface
    {
        public:
        virtual ~PropertyImplInterface() {};
        virtual bool isGettable() const {return false;}
        virtual Default get() const {throw NotGettableException();}
        virtual bool isSettable() const {return false;}
        virtual void set([[maybe_unused]] CR<Default>& val) const {throw NotSettableException();}
    };

    /// @brief Interface for property with underlying type.
    /// @ingroup Properties
    template<typename Underlying>
    class PropertyImplUInterface
    {
        public:
        virtual ~PropertyImplUInterface() {};
        virtual Underlying getUnderlying() const {throw NotGettableException();}
        virtual void setUnderlying([[maybe_unused]] const D<Underlying>& val) const {throw NotSettableException();}
    };

    /// @brief A const property (PIMPL wrapper).
    /// @ingroup Properties
    class ConstProperty
    {
        public:

        ConstProperty(PropertyImplInterface* impl) : impl_{impl}
        {}
        
        ConstProperty(const ConstProperty& from) = delete;

        ConstProperty(ConstProperty&& from) : impl_(from.impl_)
        {
            from.impl_ = nullptr;
        }
        
        ConstProperty& operator=(ConstProperty&& from) 
        {
            impl_ = from.impl_;
            from.impl_ = nullptr;
            return *this;
        }

        ~ConstProperty() {delete impl_;}

        bool isGettable() const
        {
            return impl_->isGettable();
        }

        Default get() const
        {
            if (!isGettable()) throw NotGettableException();
            return impl_->get();
        }

        template<typename Underlying> Underlying getUnderlying() const
        {
            if (!isGettable()) throw NotGettableException();
            return dynamic_cast<PropertyImplUInterface<Underlying>&>(*impl_).getUnderlying();
        }

        protected:

        PropertyImplInterface* impl_;
    };

    /// @brief A non-const property (PIMPL wrapper).
    /// @ingroup Properties
    class NonConstProperty : public ConstProperty
    {
        public:

        NonConstProperty(PropertyImplInterface* impl) : ConstProperty{impl}
        {}

        bool isSettable() const
        {
            return impl_->isSettable();
        }

        void set(CR<Default>& val) const
        {
            if (!isSettable()) throw NotSettableException();
            return impl_->set(val);
        }

        template<typename Underlying> void setUnderlying(const D<Underlying>& val) const
        {
            if (!isSettable()) throw NotSettableException();
            dynamic_cast<PropertyImplUInterface<Underlying>&>(*impl_).setUnderlying(val);
        }
    };
        
    template<bool isConst>
    using Property = typename std::conditional<isConst, ConstProperty, NonConstProperty>::type;

    template<bool isConst>
    class PropertiesIter
    {
        public:

        using iterator_category = std::input_iterator_tag;
        using value_type = std::pair<std::string, Property<isConst>>;
        using pointer = std::pair<std::string, Property<isConst>>*;
        using reference = std::pair<std::string, Property<isConst>>&;

        class ImplAbc
        {
            public:
            virtual ~ImplAbc() {};
            virtual ImplAbc* clone() const = 0;
            virtual void incr() = 0;
            virtual bool eq(const ImplAbc& other) const = 0;
            virtual const std::pair<std::string, Property<isConst>>& get() const = 0;
        };

        PropertiesIter(ImplAbc* impl) : impl_(impl)
        {}

        ~PropertiesIter() {delete impl_;}

        PropertiesIter& operator++()
        {
            impl_->incr();
            return *this;
        }

        PropertiesIter operator++(int)
        {
            PropertiesIter ret{impl_->clone()};
            impl_->incr();
            return ret;
        }

        bool operator==(const PropertiesIter& rhs) const
        {
            return impl_->eq(*rhs.impl_);
        }
        bool operator!=(const PropertiesIter& rhs) const
        {
            return !(*this == rhs);
        }
        const std::pair<std::string, Property<isConst>>& operator*() const
        {
            return impl_->get();
        }
        const std::pair<std::string, Property<isConst>>* operator->() const
        {
            return &impl_->get();
        }

        private:

        ImplAbc* impl_;
    };

    /// @brief A collection of properties, all with the same default type.
    /// @ingroup Properties
    template<bool isConst>
    class Properties
    {
        using Iter = PropertiesIter<isConst>;

        public:

        class ImplAbc
        {
            public:

            virtual ~ImplAbc() {};

            virtual Iter begin() const = 0;
            virtual Iter end() const = 0;

            virtual Iter find(const std::string& key) const = 0;

            virtual Property<isConst> at(const std::string& key) const = 0;
        };

        Properties(ImplAbc* impl) : impl_{impl} {}

        ~Properties() {delete impl_;}

        Iter begin() const
        {
            return impl_->begin();
        }
        Iter end() const
        {
            return impl_->end();
        }

        Iter find(const std::string& key) const
        {
            return impl_->find(key);
        }

        Property<isConst> at(const std::string& key) const
        {
            return impl_->at(key);
        }

        private:

        ImplAbc* impl_;
    };

    // -----------------------------------------------------------------------------------------------------------------
    // ClassProperties

    /// @brief A "class property"
    ///
    /// A ClassPropertyT is to a Property as a class is to an object.
    ///
    /// @ingroup Properties
    template<typename Targ>
    class ClassPropT
    {
        static_assert(std::is_same<Targ, D<Targ>>::value, "Targ should be a bare type");

        public:

        virtual ~ClassPropT() {};

        virtual bool isGettable() const {return false;}
        virtual Default get([[maybe_unused]] const D<Targ>& targ) const {throw NotGettableException();}

        virtual bool isSettable() const {return false;}
        virtual void set([[maybe_unused]] D<Targ>& targ, [[maybe_unused]] CR<Default>& val) const
        {
            throw NotSettableException();
        }
        virtual PropertyImplInterface* bind(const D<Targ>& targ) const = 0;
        virtual PropertyImplInterface* bind(D<Targ>& targ) const
        {return bind(const_cast<const D<Targ>&>(targ));}
    };
    
    template<typename Targ, typename Underlying>
    class ClassPropTU: public ClassPropT<Targ>
    {
        public:
        virtual Underlying getUnderlying([[maybe_unused]] const D<Targ>& targ) const
        {
            throw NotGettableException();
        }
        virtual void setUnderlying([[maybe_unused]] D<Targ>& targ, [[maybe_unused]] const D<Underlying>& val) const
        {
            throw NotSettableException();
        }
    };

    /// @brief A gettable class property.
    /// @ingroup Properties
    template<typename Targ, typename Underlying>
    class ClassPropGet : virtual public ClassPropTU<Targ, Underlying>
    {
        private:

        public:
        
        using Getter = std::function<Underlying(const D<Targ>&)>;

        ClassPropGet(Getter* g) : getter_{g}
        {}

        ~ClassPropGet() {delete getter_;}

        template<typename G> static ClassPropGet* makeNewGet(G&& g)
        {
            return new ClassPropGet(new Getter(std::move(g)));
        }
        
        virtual bool isGettable() const override {return getter_ != nullptr;}

        virtual Default get(const D<Targ>& targ) const override
        {
            return (*getter_)(targ);
        }

        virtual Underlying getUnderlying(const D<Targ>& targ) const override
        {
            return (*getter_)(targ);
        }
        
        virtual PropertyImplInterface* bind(const D<Targ>& targ) const override;
        virtual PropertyImplInterface* bind(D<Targ>& targ) const override;

        private:
        
        Getter* getter_;
    };

    /// @brief A settable class property.
    /// @ingroup Properties
    template<typename Targ, typename Underlying>
    class ClassPropSet : virtual public ClassPropTU<Targ, Underlying>
    {
        private:

        public:
        
        using Setter = std::function<void(D<Targ>&, const D<Underlying>&)>;

        ClassPropSet(Setter* s) : setter_{s}
        {}

        ~ClassPropSet() {delete setter_;}

        template<typename S> static ClassPropSet* makeNewSet(S&& s)
        {
            return new ClassPropSet(new Setter(std::move(s)));
        }
        
        virtual bool isSettable() const override {return setter_ != nullptr;}

        virtual void set(D<Targ>& targ, CR<Default>& val) const override
        {
            (*setter_)(targ, val);
        }
        
        virtual void setUnderlying(D<Targ>& targ, const D<Underlying>& val) const override
        {
            return (*setter_)(targ, val);
        }
        
        virtual PropertyImplInterface* bind(const D<Targ>& targ) const override;
        virtual PropertyImplInterface* bind(D<Targ>& targ) const override;

        private:
        
        Setter* setter_;
    };
    
    /// @brief A gettable and settable class property.
    /// @ingroup Properties
    template<typename Targ, typename Underlying>
    class ClassPropGetSet : virtual public ClassPropGet<Targ, Underlying>, virtual public ClassPropSet<Targ, Underlying>
    {
        public:

        using Getter = typename ClassPropGet<Targ, Underlying>::Getter;
        using Setter = typename ClassPropSet<Targ, Underlying>::Setter;

        ClassPropGetSet(Getter* g, Setter* s) : ClassPropGet<Targ, Underlying>{g}, ClassPropSet<Targ, Underlying>(s) {}
        
        template<typename G, typename S> static ClassPropGetSet* makeNewGetSet(G&& g, S&& s)
        {
            return new ClassPropGetSet(new Getter(std::move(g)), new Setter(std::move(s)));
        }

        virtual PropertyImplInterface* bind(const D<Targ>& targ) const override;
        virtual PropertyImplInterface* bind(D<Targ>& targ) const override;
    };

    template<typename Targ, typename Underlying, template<typename, typename> class ClassProp>
    class BoundClassProp : public PropertyImplInterface, public PropertyImplUInterface<Underlying>
    {
        static_assert(!std::is_reference<Targ>::value && !std::is_pointer<Targ>::value,
                "Targ should be a value or const value");

        public:

        BoundClassProp(const ClassProp<D<Targ>, Underlying>& classProp, Targ& targ) :
            classProp_(&classProp), targ_(&targ)
        {}

        virtual ~BoundClassProp() {}

        virtual bool isGettable() const override
        {
            return classProp_->isGettable();
        }

        virtual Default get() const override
        {
            return classProp_->get(*targ_);
        }

        virtual Underlying getUnderlying() const override
        {
            return classProp_->getUnderlying(*targ_);
        }
        
        virtual bool isSettable() const override
        {
            return classProp_->isSettable() && !std::is_const<Targ>::value;
        }

        virtual void set(CR<Default>& val) const override
        {
            if (std::is_const<Targ>::value) throw NotSettableException();
            classProp_->set(*const_cast<D<Targ>*>(targ_), val);
        }

        virtual void setUnderlying(const D<Underlying>& val) const override
        {
            if (std::is_const<Targ>::value) throw NotSettableException();
            classProp_->set(*const_cast<D<Targ>*>(targ_), val);
        }

        private:

        const ClassProp<D<Targ>, Underlying>* classProp_{nullptr};
        Targ* targ_{nullptr};
    };

    template<typename Targ, typename Underlying>
    PropertyImplInterface* ClassPropGet<Targ, Underlying>::bind(const D<Targ>& targ) const
    {
        return new BoundClassProp<const D<Targ>, Underlying, ClassPropGet>(*this, targ);
    }

    template<typename Targ, typename Underlying>
    PropertyImplInterface* ClassPropGet<Targ, Underlying>::bind(D<Targ>& targ) const
    {
        return new BoundClassProp<D<Targ>, Underlying, ClassPropGet>(*this, targ);
    }

    template<typename Targ, typename Underlying>
    PropertyImplInterface* ClassPropSet<Targ, Underlying>::bind(const D<Targ>& targ) const
    {
        return new BoundClassProp<const D<Targ>, Underlying, ClassPropSet>(*this, targ);
    }

    template<typename Targ, typename Underlying>
    PropertyImplInterface* ClassPropSet<Targ, Underlying>::bind(D<Targ>& targ) const
    {
        return new BoundClassProp<D<Targ>, Underlying, ClassPropSet>(*this, targ);
    }

    template<typename Targ, typename Underlying>
    PropertyImplInterface* ClassPropGetSet<Targ, Underlying>::bind(const D<Targ>& targ) const
    {
        return new BoundClassProp<const D<Targ>, Underlying, ClassPropGetSet>(*this, targ);
    }

    template<typename Targ, typename Underlying>
    PropertyImplInterface* ClassPropGetSet<Targ, Underlying>::bind(D<Targ>& targ) const
    {
        return new BoundClassProp<D<Targ>, Underlying, ClassPropGetSet>(*this, targ);
    }
    
    /// @brief An adaptor that enables a ClassPropT to be downcast cast from a base class to a derived class.
    /// @ingroup Properties
    template<typename Base, typename Der>
    class ClassPropAdapt : public ClassPropT<Der>
    {
        static_assert(std::is_same<Base, D<Base>>::value, "Base should be a bare type");
        static_assert(std::is_same<Der, D<Der>>::value, "Der should be a bare type");

        public:

        ClassPropAdapt(const ClassPropT<Base>& base) : cpBase_(&base) {}

        static ClassPropAdapt* makeNew(const ClassPropT<Base>& base)
        {
            return new ClassPropAdapt(base);
        }

        virtual bool isGettable() const override
        {
            return cpBase_->isGettable();
        }
        
        virtual bool isSettable() const override
        {
            return cpBase_->isSettable();
        }
        
        virtual PropertyImplInterface* bind(const D<Der>& targ) const override
        {
            return cpBase_->bind(targ);
        }

        virtual PropertyImplInterface* bind(D<Der>& targ) const override
        {
            return cpBase_->bind(targ);
        }

        private:
        const ClassPropT<Base>* cpBase_;
    };

    /// @brief A collection of class properties with the same default type and target class.
    /// @ingroup Properties
    template<typename Targ>
    class ClassProps
    {
        static_assert(std::is_same<typename std::decay_t<Targ>, Targ>::value,
                "In ClassProps<Targ>, Targ should be unqualified.");

        template<typename>
        friend class BoundClassProps;

        public:
        
        using Map = std::map<std::string, ClassPropT<Targ>*>;

        ~ClassProps()
        {
            for (const auto& p : map_) delete p.second;
        }

        template<typename Underlying, typename G>
        void addGet(const std::string& key, G&& g)
        {
            map_.emplace(key, ClassPropGet<Targ, Underlying>::makeNewGet(std::move(g)));
        }
        
        template<typename Underlying, typename S>
        void addSet(const std::string& key, S&& s)
        {
            map_.emplace(key, ClassPropSet<Targ, Underlying>::makeNewSet(std::move(s)));
        }

        template<typename Underlying, typename G, typename S>
        void addGetSet(const std::string& key, G&& g, S&& s)
        {
            map_.emplace(key, ClassPropGetSet<Targ, Underlying>::makeNewGetSet(std::move(g), std::move(s)));
        }

        auto begin() const
        {
            return map_.begin();
        }
        auto end() const
        {
            return map_.end();
        }

        void add(const std::string& key, ClassPropT<Targ>* classProp)
        {
            map_[key] = classProp;
        }

        private:

        Map map_;
    };

    template<typename Targ, typename I>
    class BoundClassPropsIterImpl : public PropertiesIter<isC<Targ>>::ImplAbc
    {
        public:

        BoundClassPropsIterImpl(const I& it, Targ* targ) : it_(it), targ_(targ), cur_{"", nullptr}
        {}

        virtual typename PropertiesIter<isC<Targ>>::ImplAbc* clone() const override
        {
            return new BoundClassPropsIterImpl(it_, targ_);
        }
        virtual void incr() override
        {
            ++it_;
        }
        virtual bool eq(const typename PropertiesIter<isC<Targ>>::ImplAbc& other) const override
        {
            auto x = dynamic_cast<const BoundClassPropsIterImpl*>(&other);
            return x != nullptr ? it_ == x->it_ : false;
        }
        virtual const std::pair<std::string, Property<isC<Targ>>>& get() const override
        {
            cur_ = {it_->first, it_->second->bind(*targ_)};
            return cur_;
        }

        private:

        I it_; // Iterator into map
        NR<Targ>* targ_;
        mutable std::pair<std::string, Property<isC<Targ>>> cur_;
    };

    /// @brief A collection of class properties bound to a target - a dynamic version of an object.
    /// @ingroup Properties
    template<typename Targ>
    class BoundClassProps : public Properties<isC<Targ>>::ImplAbc
    {
        static_assert(!std::is_reference<Targ>::value, "Targ must not be a reference.");

        private:

        public:

        BoundClassProps(ClassProps<D<Targ>>& classProps, NR<Targ>& targ) :
            classProps_{&classProps}, targ_{&targ}
        {}

        virtual PropertiesIter<isC<Targ>> begin() const override
        {
            return {new BoundClassPropsIterImpl<Targ, decltype(classProps_->begin())>(
                    classProps_->begin(), targ_)};
        }
        virtual PropertiesIter<isC<Targ>> end() const override
        {
            return {new BoundClassPropsIterImpl<Targ, decltype(classProps_->end())>(
                    classProps_->end(), targ_)};
        }

        virtual PropertiesIter<isC<Targ>> find(const std::string& key) const override
        {
            return {new BoundClassPropsIterImpl<Targ, decltype(classProps_->map_.find(key))>(
                    classProps_->map_.find(key), targ_)};
        }

        virtual Property<isC<Targ>> at(const std::string& key) const override
        {
            return classProps_->map_.at(key)->bind(*targ_);
        }

        private:

        ClassProps<D<Targ>>* classProps_;
        NR<Targ>* targ_;
    };

    // -----------------------------------------------------------------------------------------------------------------
    // HasProperties

    /// @brief An interface for a class that has properties.
    /// @ingroup Properties
    class HasProperties
    {
        public:
        virtual ~HasProperties() {};
        virtual Properties<true> properties() const = 0;
        virtual Properties<false> properties() = 0;
        ConstProperty property(const std::string& key) const
        {
            return properties().at(key);
        }
        NonConstProperty property(const std::string& key)
        {
            return properties().at(key);
        }
    };
}

// ---------------------------------------------------------------------------------------------------------------------
// Macros

// Helper to construct a single argument when using a macro with a template.
#define SGT_PROPS_ARG(...) __VA_ARGS__

/// Use this inside a class that has properties.
/// @ingroup Properties
#define SGT_PROPS_INIT(Targ) \
using PropsTarg = Targ; \
static Sgt::ClassProps<Targ>& classProperties() \
{ \
    static Sgt::ClassProps<Targ> sClassProps; \
    return sClassProps; \
} \
virtual Sgt::Properties<true> properties() const override \
{ \
    return Properties<true>(new BoundClassProps<const Targ>(classProperties(), *this)); \
} \
virtual Sgt::Properties<false> properties() override \
{ \
    return Properties<false>(new BoundClassProps<Targ>(classProperties(), *this)); \
}

/// Copy all properties from the base class into this class.
///
/// This allows an override mechanism, just like method overrides.
/// @ingroup Properties
#define SGT_PROPS_INHERIT(Base) \
struct Inherit ## Base \
{ \
    Inherit ## Base () \
    { \
        for (const auto& x : Base::classProperties()) \
        { \
            classProperties().add(x.first, ClassPropAdapt<Base, PropsTarg>::makeNew(*x.second)); \
        } \
    } \
}; \
struct DoInherit ## Base \
{ \
    DoInherit ## Base() {static Inherit ## Base inherit ## Base;} \
} doinherit ## Base \

/// Use a member function as a property getter.
/// @ingroup Properties
#define SGT_PROP_GET(name, underlying, getter) \
struct InitProp_ ## name \
{ \
    InitProp_ ## name() \
    { \
        classProperties().addGet<underlying>(#name, [](const PropsTarg& targ)->underlying {return targ.getter();}); \
    } \
}; \
struct Prop_ ## name \
{ \
    Prop_ ## name() \
    { \
        static InitProp_ ## name _; \
    } \
} prop_ ## name;

/// Use a lambda as a property getter.
/// @ingroup Properties
#define SGT_PROP_GET_NEW(name, underlying, lambdaGet) \
struct InitProp_ ## name \
{ \
    InitProp_ ## name() \
    { \
        classProperties().addGet<underlying>(#name, lambdaGet); \
    } \
}; \
struct Prop_ ## name \
{ \
    Prop_ ## name() \
    { \
        static InitProp_ ## name _; \
    } \
} prop_ ## name;

/// Use a member function as a property setter.
/// @ingroup Properties
#define SGT_PROP_SET(name, underlying, setter) \
struct InitProp_ ## name \
{ \
    InitProp_ ## name() \
    { \
        classProperties().addSet<underlying>(#name, \
                [](PropsTarg& targ, const D<underlying>& val)->void {targ.setter(val);}); \
    } \
}; \
struct Prop_ ## name { \
    Prop_ ## name() \
    { \
        static InitProp_ ## name _; \
    } \
} prop_ ## name

/// Use a lambda as a property setter.
/// @ingroup Properties
#define SGT_PROP_SET_NEW(name, underlying, lambdaSet) \
struct InitProp_ ## name \
{ \
    InitProp_ ## name() \
    { \
        classProperties().addSet<underlying>(#name, lambdaSet); \
    } \
}; \
struct Prop_ ## name { \
    Prop_ ## name() \
    { \
        static InitProp_ ## name _; \
    } \
} prop_ ## name

/// Use member functions as property getters and setters.
/// @ingroup Properties
#define SGT_PROP_GET_SET(name, underlying, getter, setter) \
struct InitProp_ ## name \
{ \
    InitProp_ ## name() \
    { \
        classProperties().addGetSet<underlying>(#name, \
                [](const PropsTarg& targ)->underlying {return targ.getter();}, \
                [](PropsTarg& targ, const D<underlying>& val)->void {targ.setter(val);}); \
    } \
}; \
struct Prop_ ## name \
{ \
    Prop_ ## name() \
    { \
        static InitProp_ ## name _; \
    } \
} prop_ ## name

/// Use lambdas as property getters and setters.
/// @ingroup Properties
#define SGT_PROP_GET_SET_NEW(name, underlying, lambdaGet, lambdaSet) \
struct InitProp_ ## name \
{ \
    InitProp_ ## name() \
    { \
        classProperties().addGetSet<underlying>(#name, lambdaGet, lambdaSet); \
    } \
}; \
struct Prop_ ## name \
{ \
    Prop_ ## name() \
    { \
        static InitProp_ ## name _; \
    } \
} prop_ ## name

#endif // PROPERTIES_DOT_H
