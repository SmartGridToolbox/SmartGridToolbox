// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef POWERFLOW_MODEL_DOT_H
#define POWERFLOW_MODEL_DOT_H

#include <SgtCore/Common.h>
#include <SgtCore/PowerFlow.h>

#include <list>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

namespace Sgt
{
    // The following structures etc. are designed to present a general framework for solving power flow problems,
    // e.g. using Newton-Raphson or the swing equations.

    struct PfBus;

    struct PfNode
    {
        PfBus* bus;
        std::size_t phIdx;

        std::size_t idxInModel{0}; // Index in model.

        int islandIdx{-1}; // After validation, will be index of island.
        std::size_t idxInIsland{0}; // After validation, will be node index in island. 
        
        Complex& V();
        Complex& SGen();
    };

    struct PfBus
    {
        std::string id;
        std::size_t nPhase;
        BusType type;
        arma::Mat<Complex> YNd; // Bus admittance matrix, = YZipToYConst(YConst)
        arma::Mat<Complex> IConst; 
        arma::Mat<Complex> SConst; 
        arma::Col<Complex> V; 
        arma::Col<Complex> SGen; 
        std::vector<PfNode> nodes;
    };

    inline Complex& PfNode::V() {return bus->V(phIdx);}
    inline Complex& PfNode::SGen() {return bus->SGen(phIdx);}

    struct PfBranch
    {
        Array<PfBus*, 2> buses;
        Array<std::vector<std::size_t>, 2> phaseIdxs;
        arma::Mat<Complex> YNd; ///< Bus admittance matrix.
    };

    class PfIsland
    {
        private:

        friend class PfModel;

        public:
        
        PfIsland(std::size_t idx) : idx_(idx) {}
        
        /// @name Model accessors:
        /// @{

        auto nodes() const
        {
            return makeSpan(nodeVec_.cbegin(), nodeVec_.cend());
        }

        auto nodes()
        {
            return makeSpan(nodeVec_.begin(), nodeVec_.end());
        }

        std::size_t idx()
        {
            return idx_;
        }

        const arma::SpMat<Complex>& YNd() const
        {
            return YNd_;
        }

        const arma::SpMat<Complex>& IConst() const
        {
            return IConst_;
        }

        const arma::SpMat<Complex>& SConst() const
        {
            return SConst_;
        }

        arma::Col<Complex> V() const;
        void setV(const arma::Col<Complex>& V) const;
        
        arma::Col<Complex> SGen() const;
        void setSGen(const arma::Col<Complex>& SGen) const;

        /// @}

        /// @name Count nodes of different types:
        /// Nodes are ordered as: PQ, PV, SL.
        /// @{

        std::size_t nNode() const
        {
            return nodeVec_.size();
        }
        std::size_t nPq() const
        {
            return nPq_;
        }
        std::size_t nPv() const
        {
            return nPv_;
        }
        std::size_t nPqPv() const
        {
            return nPq_ + nPv_;
        }
        std::size_t nSl() const
        {
            return nSl_;
        }

        /// @}

        /// @name Ordering of variables etc:
        /// @{

        // Note: a more elegant general solution to ordering would be to use matrix_slice. But assigning into
        // a matrix slice of a compressed_matrix appears to destroy the sparsity. MatrixRange works, but does not
        // present a general solution to ordering. Thus, when assigning into a compressed_matrix, we need to work
        // element by element, using an indexing scheme.

        arma::uword iPq(arma::uword i) const
        {
            return i;
        }
        arma::uword iPv(arma::uword i) const
        {
            return nPq_ + i;
        }
        arma::uword iSl(arma::uword i) const
        {
            return nPq_ + nPv_ + i;
        }
        ISpan selPq() const
        {
            return {iPq(0), nPq_};
        }
        ISpan selPv() const
        {
            return {iPv(0), nPv_};
        }
        ISpan selPqPv() const
        {
            return {iPq(0), nPq_ + nPv_};
        }
        ISpan selSl() const
        {
            return {iSl(0), nSl_};
        }
        ISpan selAll() const
        {
            return {iPq(0), nPq_ + nPv_ + nSl_};
        }

        private:

        std::size_t idx_;

        std::vector<PfNode*> nodeVec_;
        std::size_t nPq_{0}; ///< Number of PQ nodes.
        std::size_t nPv_{0}; ///< Number of PV nodes.
        std::size_t nSl_{0}; ///< Number of SL nodes.

        arma::SpMat<Complex> YNd_; ///< bus admittance matrix for constant admittance.
        arma::SpMat<Complex> IConst_; ///< constant current elements.
        arma::SpMat<Complex> SConst_; ///< constant power elements.
    };

    /// @brief A mathematical model of a network, suitable for solving the AC power flow problem.
    /// @ingroup PowerFlowCore
    class PfModel
    {
        public:

        /// @name Lifecycle:
        /// @{

        void addBus(const std::string& id, std::size_t nPhase, BusType type,
                const arma::Mat<Complex>& YConst, const arma::Mat<Complex>& IConst, const arma::Mat<Complex>& SConst,
                const arma::Col<Complex>& V, const arma::Col<Complex> SGen);

        void addBranch(const std::string& idBus0, std::vector<std::size_t> phaseIdxs0,
                const std::string& idBus1, std::vector<std::size_t> phaseIdxs1, const arma::Mat<Complex>& YNd);

        /// @brief Tiny extra capacitance that can make some pathological cases more robust.
        bool addExtraCapacitance() const {return addExtraCapacitance_;}
        /// @brief Tiny extra capacitance that can make some pathological cases more robust.
        void setAddExtraCapacitance(bool val) {addExtraCapacitance_ = val;}
        
        /// @brief Validate the model.
        /// @param analyseTopology If analyseTopology == true, separate network into electrically isolated islands.
        void validate(bool analyseTopology = true);

        /// @}

        /// @name Model accessors:
        /// @{
        
        auto buses() const {return makeSpan(buses_.cbegin(), buses_.cend());}
        auto buses() {return makeSpan(buses_.begin(), buses_.end());}

        auto bus(const std::string& id) const {return busMap_.at(id);}
        auto bus(const std::string& id) {return busMap_.at(id);}
        
        auto branches() const {return makeSpan(branches_.cbegin(), branches_.cend());}
        auto branches() {return makeSpan(branches_.begin(), branches_.end());}
        
        auto islands() const {return makeSpan(islands_.cbegin(), islands_.cend());}
        auto islands() {return makeSpan(islands_.begin(), islands_.end());}

        /// @}

        private:
        
        void fillMatrices(const std::vector<PfNode*> nodeVec, 
                arma::SpMat<Complex>& YNd, arma::SpMat<Complex>& IConst, arma::SpMat<Complex>& SConst);
        void buildIslands(const std::vector<PfNode*>& nodeVec,
                const arma::SpMat<Complex>& YNd,
                const arma::SpMat<Complex>& IConst,
                const arma::SpMat<Complex>& SConst,
                bool analyseTopology);
        void islandDfs(const std::vector<PfNode*>& nodeVec, size_t nodeIdx, size_t islandIdx,
                const std::vector<std::set<arma::uword>>& con);

        private:

        bool addExtraCapacitance_{true};

        std::list<std::unique_ptr<PfBus>> buses_;
        std::list<std::unique_ptr<PfBranch>> branches_;
        
        std::unordered_map<std::string, PfBus*> busMap_;

        std::vector<PfIsland> islands_;
    };
}

#endif // POWERFLOW_MODEL_DOT_H
