// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef BRANCH_DOT_H
#define BRANCH_DOT_H

#include <SgtCore/Bus.h>
#include <SgtCore/Component.h>
#include <SgtCore/ComponentCollection.h>
#include <SgtCore/Event.h>
#include <SgtCore/PowerFlow.h>

#include <iostream>

namespace Sgt
{
    /// @brief Common abstract base class for a branch.
    ///
    /// Implement some common functionality for convenience.
    /// @ingroup PowerFlowCore
    class BranchAbc : virtual public Component
    {
        friend class Network;

        public:

        SGT_PROPS_INIT(BranchAbc);
        SGT_PROPS_INHERIT(Component);

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("branch");
            return result;
        }

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        virtual json toJson() const override;

        /// @}

        /// @name Number of phases:
        /// @{
        
        virtual unsigned int nPhases0() const = 0;
        
        SGT_PROP_GET(nPhases0, unsigned int, nPhases0);
        
        virtual unsigned int nPhases1() const = 0;
        
        SGT_PROP_GET(nPhases1, unsigned int, nPhases1);

        /// @}

        /// @name In service:
        /// @{

        virtual bool isInService() const
        {
            return isInService_;
        }

        virtual void setIsInService(bool isInService);

        SGT_PROP_GET_SET(isInService, bool, isInService, setIsInService);

        /// @}

        /// @name Nodal admittance matrix (Y):
        /// @{

        virtual arma::Mat<Complex> Y() const final
        {
            size_t n = nPhases0() + nPhases1();
            return isInService_ ? inServiceY() : arma::Mat<Complex>(n, n, arma::fill::zeros);
        }

        SGT_PROP_GET(Y, arma::Mat<Complex>, Y);

        /// @brief The admittance whenever isInService.
        virtual arma::Mat<Complex> inServiceY() const = 0;

        SGT_PROP_GET(inServiceY, arma::Mat<Complex>, inServiceY);

        /// @}

        /// @name Events:
        /// @{

        /// @brief Event triggered when I go in or out of service.
        virtual const Event& isInServiceChanged() const
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when I go in or out of service.
        virtual Event& isInServiceChanged()
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when my admittance changes.
        virtual const Event& admittanceChanged() const
        {
            return admittanceChanged_;
        }

        /// @brief Event triggered when my admittance changes.
        virtual Event& admittanceChanged()
        {
            return admittanceChanged_;
        }

        /// @}

        /// @name Connections:
        /// @{

        ConstComponentPtr<Bus> bus0() const
        {
            return bus0_;
        }

        ComponentPtr<Bus> bus0()
        {
            return bus0_;
        }
        
        virtual const Phases& phases0() const
        {
            return phases0_;
        }

        SGT_PROP_GET(phases0, const Phases&, phases0);

        ConstComponentPtr<Bus> bus1() const
        {
            return bus1_;
        }

        ComponentPtr<Bus> bus1()
        {
            return bus1_;
        }

        virtual const Phases& phases1() const
        {
            return phases1_;
        }

        SGT_PROP_GET(phases1, const Phases&, phases1);

        std::array<ConstComponentPtr<Bus>, 2> buses() const
        {
            return {{bus0_, bus1_}};
        }

        std::array<ComponentPtr<Bus>, 2> buses()
        {
            return {{bus0_, bus1_}};
        }
        
        std::array<Phases, 2> phases() const
        {
            return {{phases0_, phases1_}};
        }
        
        SGT_PROP_GET(phases, SGT_PROPS_ARG(std::array<Phases, 2>), phases);

        /// @}

        /// @name Terminal functions:
        /// @{

        /// @brief Voltage at each bus, mapped to the two branch terminals.
        std::array<arma::Col<Complex>, 2> VTerm() const
        {
            return {{mapPhases(bus0()->V(), bus0()->phases(), phases0()), 
                mapPhases(bus1()->V(), bus1()->phases(), phases1())}};
        }

        SGT_PROP_GET(VTerm, SGT_PROPS_ARG(std::array<arma::Col<Complex>, 2>), VTerm);

        /// @brief Current out of each bus into branch, mapped to the two branch terminals.
        std::array<arma::Col<Complex>, 2> ITerm() const;
        
        SGT_PROP_GET(ITerm, SGT_PROPS_ARG(std::array<arma::Col<Complex>, 2>), ITerm);

        /// @brief Power out of each bus into branch, mapped to the two branch terminals.
        std::array<arma::Col<Complex>, 2> STerm() const;
        
        SGT_PROP_GET(STerm, SGT_PROPS_ARG(std::array<arma::Col<Complex>, 2>), STerm);

        /// @}
        
        /// @name Max apparent power through any individual terminal phase:
        /// @{

        double SMagMax() const
        {
            return SMagMax_;
        }

        void setSMagMax(const double SMagMax)
        {
            SMagMax_ = SMagMax;
        }
        
        SGT_PROP_GET_SET(SMagMax, double, SMagMax, setSMagMax);
        
        bool SMagMaxViolated() const;

        SGT_PROP_GET(SMagMaxViolated, double, SMagMaxViolated);

        /// @}
        
        /// @name Max current magnitude through any individual terminal phase:
        /// @{

        double IMagMax() const
        {
            return IMagMax_;
        }

        void setIMagMax(const double IMagMax)
        {
            IMagMax_ = IMagMax;
        }
        
        SGT_PROP_GET_SET(IMagMax, double, IMagMax, setIMagMax);
        
        bool IMagMaxViolated() const;

        SGT_PROP_GET(IMagMaxViolated, double, IMagMaxViolated);

        /// @}

        private:

        bool isInService_{true}; ///< Am I in service?

        Event isInServiceChanged_{componentType() + ": Is in service changed"};
        Event admittanceChanged_{componentType() + ": Admittance changed"};

        ComponentPtr<Bus> bus0_; ///< Bus 0.
        Phases phases0_; ///< Phases on bus 0.

        ComponentPtr<Bus> bus1_; ///< Bus 1.
        Phases phases1_; ///< Phases on bus 1.

        double SMagMax_{infinity};
        double IMagMax_{infinity};
    };

    /// @brief A concrete, generic branch.
    /// @ingroup PowerFlowCore
    class GenericBranch : public BranchAbc
    {
        public:

        SGT_PROPS_INIT(GenericBranch);
        SGT_PROPS_INHERIT(BranchAbc);

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("generic_branch");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        GenericBranch(const std::string& id, int nPhases0, int nPhases1) :
            Component(id),
            nPhases0_(nPhases0), nPhases1_(nPhases1),
            Y_(nPhases0 + nPhases1, nPhases0 + nPhases1, arma::fill::zeros)
        {
            // Empty.
        }

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        // virtual json toJson() const override; // TODO
        /// @}

        /// @name BranchAbc virtual overridden member functions:
        /// @{

        virtual unsigned int nPhases0() const override
        {
            return nPhases0_;
        }
        
        virtual unsigned int nPhases1() const override
        {
            return nPhases1_;
        }

        virtual arma::Mat<Complex> inServiceY() const override
        {
            return Y_;
        }

        /// @}

        /// @name Setter for Y:
        /// @{

        void setInServiceY(const arma::Mat<Complex>& Y)
        {
            sgtAssert(Y.n_cols == Y.n_rows && Y.n_rows == nPhases0_ + nPhases1_,
                    "Wrong number of rows and columns for Y in GenericBranch");
            Y_ = Y;
        }

        /// @}

        protected:

        private:

        unsigned int nPhases0_;
        unsigned int nPhases1_;
        arma::Mat<Complex> Y_;
    };
}

#endif // BRANCH_DOT_H
