// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "OpfModel.h"

#include "SparseHelper.h"

#include <iostream>
#include <sstream>

using namespace arma;
using namespace std;

namespace Sgt
{
    OpfModel::OpfModel()
    {
        buses_.reserve(500);
        gens_.reserve(20);
        branches_.reserve(500);
    }

    void OpfModel::addBus(const string& id, const arma::Col<Complex>& V,
            const double vMin, const double vMax, bool isAngleRef, bool hasAvr)
    {
        buses_.push_back({id, 0, V, {vMin, vMax}, isAngleRef, hasAvr});
    }
        
    void OpfModel::addZip(const std::string& id, const std::string& busId,
            const std::vector<std::size_t>& busPhIdxs, const arma::Mat<arma::uword>& connection,
            const arma::Col<Complex>& YConst, const arma::Col<Complex>& IConst, const arma::Col<Complex>& SConst)
    {
        zips_.push_back({id, 0, busId, busPhIdxs, nullptr, connection, YConst, IConst, SConst});
    }

    void OpfModel::addGen(const std::string& id, const std::string& busId,
            const std::vector<std::size_t>& busPhIdxs, 
            const arma::Col<Complex>& SGen, const double pGenMin, const double pGenMax,
            const double qGenMin, const double qGenMax, const double sGenMin, const double sGenMax,
            const double pCostLin, const double pCostQuad)
    {
        gens_.push_back({
                id, 0, busId, busPhIdxs, nullptr, SGen, {pGenMin, pGenMax}, {qGenMin, qGenMax}, {sGenMin, sGenMax},
                pCostLin, pCostQuad});
    }

    void OpfModel::addBranch(const std::string& id,
            const std::string& busId0, const std::vector<std::size_t>& busPhIdxs0,
            const std::string& busId1, const std::vector<std::size_t>& busPhIdxs1,
            const arma::Mat<Complex>& YNd, const double SMagMax, const double IMagMax0, const double IMagMax1)
    {
        branches_.push_back(
                {id, 0, {busId0, busId1}, {busPhIdxs0, busPhIdxs1}, {nullptr, nullptr}, YNd,
                SMagMax, {IMagMax0, IMagMax1}});
    }

    void OpfModel::validate()
    {
        {
            int i = 0;
            for (auto& x: buses_)
            {
                busMap_[x.id] = &x;
                x.idx = i++;
            }
        }
        {
            int i = 0;
            for (auto& x: zips_)
            {
                zipMap_[x.id] = &x;
                x.bus = busMap_[x.busId];
                x.idx = i++;
            }
        }
        {
            int i = 0;
            for (auto& x: gens_)
            {
                genMap_[x.id] = &x;
                x.bus = busMap_[x.busId];
                x.idx = i++;
            }
        }
        {
            int i = 0;
            for (auto& x: branches_)
            {
                branchMap_[x.id] = &x;
                x.buses[0] = busMap_[x.busIds[0]];
                x.buses[1] = busMap_[x.busIds[1]];
                x.idx = i++;
            }
        }
    }
}
