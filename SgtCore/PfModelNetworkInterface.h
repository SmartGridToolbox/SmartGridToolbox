// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PF_MODEL_NETWORK_INTERFACE_DOT_H
#define PF_MODEL_NETWORK_INTERFACE_DOT_H

#include<SgtCore/Network.h>
#include<SgtCore/PfModel.h>

namespace Sgt
{
    std::unique_ptr<PfModel> buildPfModel(const Network& netw, bool analyseTopology = true,
            const std::function<bool (const Bus&)> selBus = [](const Bus&){return true;});

    void applyPfModel(const PfModel& mod, Network& netw);
}

#endif // PF_MODEL_NETWORK_INTERFACE_DOT_H
