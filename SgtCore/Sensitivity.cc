// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "KluSolver.h"
#include "Sensitivity.h"

#include "PfModelNetworkInterface.h"

using namespace arma;
using namespace std;

namespace Sgt
{
    list<Sensitivity> sensitivity(const Network& nw, const std::map<std::string, Col<Complex>>& dSdx)
    {
        // Build a PfModel, which decomposes the unbalanced multi-phase network into a representation with single
        // phase "nodes" and matrices and vectors (Y, SConst, IConst, etc.) that represent the branches, loads and
        // generation.
        auto mod = buildPfModel(nw, false);
        sgtLogMessage() << "Number of islands: " << mod->islands().size() << endl;

        // Map to hold the result of this function. For each bus, store the voltage sensitivity dv/dx to a
        // "nominal" load increase x.
        map<string, Sensitivity> dvtdx; // Maintain order.
        // For now, set dvtdx to zero for bus.
        for (const auto& bus : nw.buses())
        {
            Col<double> z(bus->phases().size(), fill::zeros);
            dvtdx[bus->id()] = {bus->id(), z, z};
        }

        for (auto island : mod->islands())
        {
            sgtLogMessage() << "Doing island " << island.idx() << endl;
            const auto& nds = island.nodes();
            auto nNd = island.nNode();
            auto nPq = island.nPq();
            auto nPv = island.nPv();
            auto nPqPv = nPq + nPv;

            // Debug printing --------------------------------------------------
            sgtLogDebug() << endl;
            sgtLogDebug() << "Nodes" << endl;
            for (const auto& n : nds)
            {
                sgtLogDebug() << n->bus->id << " " << n->idxInIsland << " " << n->V() << endl;
            }
            sgtLogDebug() << endl;

            sgtLogDebug() << "YNd" << endl;
            sgtLogDebug() << island.YNd() << endl;
            sgtLogDebug() << endl;

            sgtLogDebug() << "SConst" << endl;
            sgtLogDebug() << island.SConst() << endl;
            sgtLogDebug() << endl;

            sgtLogDebug() << "SGen" << endl;
            sgtLogDebug() << island.SGen() << endl;
            sgtLogDebug() << endl;
            // Finished debug printing -----------------------------------------

            const auto& Y = island.YNd();
            const auto& S = island.SConst();
            // TODO: We're currently ignoring constant current loads.

            // Since we're interested in the fractional increase in voltage over the base voltage, we need the base
            // voltage for each node.
            Col<double> vBase(nNd, fill::none);
            for (const auto& nd : nds)
            {
                auto busId = nd->bus->id;
                auto nwBus = nw.buses()[busId];
                vBase[nd->idxInIsland] = nwBus->VBase();
            }

            // dSdxMat is the increase in complex power S corresponding to a unit increase in some "nominal" load.
            // The row and column correspond to node indices; diagonal elements are wye load components and
            // off-diagonal elements are delta load components.
            SpMat<Complex> dSdxMat(nNd, nNd); 
            // TODO: If a bottleneck, use SparseHelper which is quicker.
            for (const auto& kv : dSdx)
            {
                // Find the ZIP (load) for this dict item.
                string zipId = kv.first;
                const Col<Complex>& curDsDx = kv.second;
                const auto& nwZip = nw.zips()[zipId];
                sgtAssert(nwZip != nullptr, "Zip " << zipId << " was not found." << endl);

                // Find the bus to which the load is attached. 
                const auto& nwBus = nwZip->bus();
                const auto& modBus = mod->bus(nwBus->id());

                // Loop over load components/phases.
                for (uword i = 0; i < nwZip->nComps(); ++i)
                {
                    // Each component involves two terminals, which may be the same (terminal to ground/wye)
                    // or different (terminal to terminal/delta).
                    auto zipTermI = nwZip->connection()(i, 0);
                    auto zipTermK = nwZip->connection()(i, 1);

                    // Find the bus phase indices corresponding to these two terminals. 
                    auto busPhI = nwBus->phases().index(nwZip->phases()[zipTermI]);
                    auto busPhK = nwBus->phases().index(nwZip->phases()[zipTermK]);

                    // Retrieve the node for each of these bus phases.
                    auto ndI = modBus->nodes[busPhI];
                    auto ndK = modBus->nodes[busPhK];
                    assert(ndI.islandIdx == ndK.islandIdx);
                    
                    // They should be in the same island, by virtue of them being connected by the load.
                    // Only proceed if this island is the one we're currently interested in!
                    if (static_cast<unsigned int>(ndI.islandIdx) != island.idx()) continue;

                    Complex curDsDx1 = curDsDx[i]; // Distribute dSdxMat among load comps.
                    dSdxMat(ndI.idxInIsland, ndK.idxInIsland) += curDsDx1;
                    if (ndI.idxInIsland != ndK.idxInIsland) dSdxMat(ndK.idxInIsland, ndI.idxInIsland) += curDsDx1;
                }
            }

            // A, B matrices:
            SparseHelper<Complex> AH(nNd, nNd);
            SparseHelper<Complex> BH(nNd, nNd);

            // rhs vector
            Col<Complex> rhsCplx(nNd, fill::zeros);
            
            // Y dependent bits:
            for (auto it = Y.begin(); it != Y.end(); ++it)
            {
                uword i = it.row();
                uword k = it.col();

                auto ndI = nds[i];
                auto ndK = nds[k];

                auto tpI = ndI->bus->type;
                auto tpK = ndK->bus->type;
                
                Complex Ycik = conj(*it);
                auto vi = abs(ndI->V()); 
                auto vk = abs(ndK->V()); 
                auto ti = arg(ndI->V()); 
                auto tk = arg(ndK->V());
                auto timk = ti - tk;

                // Do A
                if (tpI == BusType::PQ || tpI == BusType::PV)
                {
                    if (tpK == BusType::PQ)
                    {
                        AH.insert(i, k, Ycik * vi * polar(1.0, timk));
                    }
                    if (tpI == BusType::PQ)
                    {
                        AH.insert(i, i, Ycik * vk * polar(1.0, timk));
                    }
                }
                
                // Do B
                if (tpI == BusType::PQ || tpI == BusType::PV)
                {
                    auto x = im * Ycik * vi * vk * polar(1.0, timk);
                    if (tpK == BusType::PQ || tpK == BusType::PV)
                    {
                        BH.insert(i, k, -x);
                    }
                    BH.insert(i, i, x);
                }
            }

            // Do the S dependent bits:
            for (auto it = S.begin(); it != S.end(); ++it)
            {
                uword i = it.row();
                uword k = it.col();

                auto ndI = nds[i];
                auto ndK = nds[k];

                auto tpI = ndI->bus->type;
                auto tpK = ndK->bus->type;
                
                Complex Sik = *it;
                auto vi = abs(ndI->V()); 
                auto vk = abs(ndK->V()); 
                auto ti = arg(ndI->V()); 
                auto tk = arg(ndK->V());
                auto tipk = ti + tk;
                
                // Do A
                if ((tpI == BusType::PQ || tpI == BusType::PV) && i != k)
                {
                    Complex x = Sik * vi * polar(1.0, tipk) / pow(ndI->V() - ndK->V(), 2);
                    if (tpK == BusType::PQ)
                    {
                        AH.insert(i, k, x);
                    }
                    if (tpI == BusType::PQ)
                    {
                        AH.insert(i, i, -x);
                    }
                }
                
                // Do B
                if ((tpI == BusType::PQ || tpI == BusType::PV) && i != k)
                {
                    auto x = im * Sik * vi * vk * polar(1.0, tipk) / pow(ndI->V() - ndK->V(), 2);
                    if (tpK == BusType::PQ || tpK == BusType::PV)
                    {
                        BH.insert(i, k, x);
                    }
                    BH.insert(i, i, -x);
                }
            }

            // Do rhsCplx
            for (auto it = dSdxMat.begin(); it != dSdxMat.end(); ++it)
            {
                uword i = it.row();
                uword k = it.col();
                Complex dSdxMat = *it;
                
                auto ndI = nds[i];
                auto ndK = nds[k];
                
                // Do rhsCplx
                if (i != k)
                {
                    rhsCplx(i) -= dSdxMat * ndI->V() / (ndI->V() - ndK->V());
                }
                else
                {
                    rhsCplx(i) -= dSdxMat;
                }
            }

            SpMat<Complex> A = AH.get();
            SpMat<Complex> B = BH.get();

            auto dimM = 2 * nPq + nPv;
            SpMat<double> M(dimM, dimM);
            
            submat(M, {0, nPqPv}, {0, nPq}) = real(submat(A, {0, nPqPv}, {0, nPq}));
            submat(M, {0, nPqPv}, {nPq, nPqPv}) = real(submat(B, {0, nPqPv}, {0, nPqPv}));
            submat(M, {nPqPv, nPq}, {0, nPq}) = imag(submat(A, {0, nPq}, {0, nPq}));
            submat(M, {nPqPv, nPq}, {nPq, nPqPv}) = imag(submat(B, {0, nPq}, {0, nPqPv}));
            
            Col<double> rhs = join_vert(real(subvec(rhsCplx, {0, nPqPv})), imag(subvec(rhsCplx, {0, nPq})));
            Col<double> sol;

            sgtLogDebug() << "M:" << endl;
            sgtLogDebug() << Mat<double>(M) << endl;
            sgtLogDebug() << "rhs:" << endl;
            sgtLogDebug() << rhs << endl;

            kluSolve(M, rhs, sol);

            for (size_t i = 0; i < nPq; ++i)
            {
                auto ndi = nds[i];
                dvtdx[ndi->bus->id].dvdx[ndi->phIdx] = sol[i];
            }
            for (size_t i = 0; i < island.nPqPv(); ++i)
            {
                auto ndi = nds[i];
                dvtdx[ndi->bus->id].dtdx[ndi->phIdx] = sol[nPq + i];
            }
        }

        list<Sensitivity> result;
        for (const auto& x : dvtdx) result.push_back(x.second);
        result.sort([](const auto& x, const auto& y)->bool{return (max(abs(x.dvdx)) > max(abs(y.dvdx)));});
        return result;
    }
}
