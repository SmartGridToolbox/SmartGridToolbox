// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GEN_DOT_H
#define GEN_DOT_H

#include <SgtCore/Bus.h>
#include <SgtCore/Common.h>
#include <SgtCore/Component.h>
#include <SgtCore/ComponentCollection.h>
#include <SgtCore/Event.h>
#include <SgtCore/PowerFlow.h>

namespace Sgt
{
    /// @brief Generation at a bus.
    /// @ingroup PowerFlowCore
    class Gen : virtual public Component
    {
        friend class Network;

        public:

        SGT_PROPS_INIT(Gen);
        SGT_PROPS_INHERIT(Component);

        /// @name Static member functions:
        /// @{

        static const std::string& sComponentType()
        {
            static std::string result("gen");
            return result;
        }

        /// @}

        /// @name Lifecycle:
        /// @{

        Gen(const std::string& id, unsigned int nPhases) :
            Component(id),
            nPhases_(nPhases),
            S_(nPhases, arma::fill::zeros),
            phases_(nPhases) // Make sure size is correct just in case.
        {
            // Empty.
        }

        /// @}

        /// @name Component virtual overridden member functions:
        /// @{

        virtual const std::string& componentType() const override
        {
            return sComponentType();
        }

        virtual json toJson() const override;

        /// @}

        /// @name Number of phases:
        /// @{
        
        unsigned int nPhases() const
        {
            return nPhases_;
        }
        
        SGT_PROP_GET(nPhases, unsigned int, nPhases);
        
        /// @}

        /// @name In service:
        /// @{

        virtual bool isInService() const
        {
            return isInService_;
        }

        virtual void setIsInService(bool isInService);

        SGT_PROP_GET_SET(isInService, bool, isInService, setIsInService);

        /// @}

        /// @name Power injection:
        /// @{

        /// @brief The actual generation power, as an injection into the bus terminals.
        virtual arma::Col<Complex> S() const final
        {
            return isInService_ ? inServiceS() : arma::Col<Complex>(nPhases_, arma::fill::zeros);
        }

        SGT_PROP_GET(S, arma::Col<Complex>, S);

        /// @brief The generation power assuming the component is in service.
        virtual arma::Col<Complex> inServiceS() const
        {
            return S_;
        }

        /// @brief The generation power assuming the component is in service.
        virtual void setInServiceS(const arma::Col<Complex>& S)
        {
            S_ = S;
            generationChanged().trigger();
        }

        SGT_PROP_GET_SET(inServiceS, arma::Col<Complex>, inServiceS, setInServiceS);

        /// @}

        /// @name Terminal functions:
        /// @{

        /// @brief Voltage at bus, mapped to the terminal.
        arma::Col<Complex> VTerm() const
        {
            return mapPhases(bus()->V(), bus()->phases(), phases());
        }

        /// @brief Current out of bus into gen, mapped to the terminal.
        ///
        /// (We use draw rather than injection here to be consistent with Branches and Zips).
        arma::Col<Complex> ITerm() const
        {
            return conj(STerm()) / conj(VTerm());
        }

        /// @brief Power out of bus into gen, mapped to the branch terminals.
        ///
        /// (We use draw rather than injection here to be consistent with Branches and Zips).
        arma::Col<Complex> STerm() const
        {
            return -S();
        }

        /// @}

        /// @name Generation bounds:
        /// @{

        virtual double PMin() const
        {
            return PMin_;
        }

        virtual void setPMin(double PMin)
        {
            PMin_ = PMin;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(PMin, double, PMin, setPMin);

        virtual double PMax() const
        {
            return PMax_;
        }

        virtual void setPMax(double PMax)
        {
            PMax_ = PMax;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(PMax, double, PMax, setPMax);

        virtual double QMin() const
        {
            return QMin_;
        }

        virtual void setQMin(double QMin)
        {
            QMin_ = QMin;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(QMin, double, QMin, setQMin);

        virtual double QMax() const
        {
            return QMax_;
        }

        virtual void setQMax(double QMax)
        {
            QMax_ = QMax;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(QMax, double, QMax, setQMax);

        virtual double SMin() const
        {
            return SMin_;
        }

        virtual void setSMin(double SMin)
        {
            SMin_ = SMin;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(SMin, double, SMin, setSMin);

        virtual double SMax() const
        {
            return SMax_;
        }

        virtual void setSMax(double SMax)
        {
            SMax_ = SMax;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(SMax, double, SMax, setSMax);

        /// @}

        /// @name Generation costs:
        /// @{

        virtual double cStartup() const
        {
            return cStartup_;
        }

        virtual void setCStartup(double cStartup)
        {
            cStartup_ = cStartup;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(cStartup, double, cStartup, setCStartup);

        virtual double cShutdown() const
        {
            return cShutdown_;
        }

        virtual void setCShutdown(double cShutdown)
        {
            cShutdown_ = cShutdown;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(cShutdown, double, cShutdown, setCShutdown);

        virtual double c0() const
        {
            return c0_;
        }

        virtual void setC0(double c0)
        {
            c0_ = c0;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(c0, double, c0, setC0);

        virtual double c1() const
        {
            return c1_;
        }

        virtual void setC1(double c1)
        {
            c1_ = c1;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(c1, double, c1, setC1);

        virtual double c2() const
        {
            return c2_;
        }

        virtual void setC2(double c2)
        {
            c2_ = c2;
            setpointChanged().trigger();
        }

        SGT_PROP_GET_SET(c2, double, c2, setC2);

        double cost() const;
        SGT_PROP_GET(cost, double, cost);

        /// @}

        /// @name Events:
        /// @{

        /// @brief Event triggered when I go in or out of service.
        virtual const Event& isInServiceChanged() const
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when I go in or out of service.
        virtual Event& isInServiceChanged()
        {
            return isInServiceChanged_;
        }

        /// @brief Event triggered when the amount of generated power changes.
        virtual const Event& generationChanged() const
        {
            return generationChanged_;
        }

        /// @brief Event triggered when the amount of generated power changes.
        virtual Event& generationChanged()
        {
            return generationChanged_;
        }

        /// @brief Event triggered when a setpoint is changed.
        virtual const Event& setpointChanged() const
        {
            return setpointChanged_;
        }

        /// @brief Event triggered when a setpoint is changed.
        virtual Event& setpointChanged()
        {
            return setpointChanged_;
        }

        /// @}

        /// @name Connection:
        /// @{

        ConstComponentPtr<Bus> bus() const
        {
            return bus_;
        }

        ComponentPtr<Bus> bus()
        {
            return bus_;
        }

        virtual const Phases& phases() const
        {
            return phases_;
        }

        SGT_PROP_GET(phases, const Phases&, phases);

        /// @}

        private:

        unsigned int nPhases_;

        bool isInService_{true};

        arma::Col<Complex> S_;

        double PMin_{-infinity};
        double PMax_{infinity};
        double QMin_{-infinity};
        double QMax_{infinity};
        double SMin_{0.0};
        double SMax_{infinity};

        double cStartup_{0.0};
        double cShutdown_{0.0};
        double c0_{0.0};
        double c1_{0.0};
        double c2_{0.0};

        Event isInServiceChanged_{std::string(componentType()) + ": Is in service changed"};
        Event generationChanged_{std::string(componentType()) + ": Generation changed"};
        Event setpointChanged_{std::string(componentType()) + ": Setpoint changed"};

        ComponentPtr<Bus> bus_;
        Phases phases_;
    };
}

#endif // GEN_DOT_H
