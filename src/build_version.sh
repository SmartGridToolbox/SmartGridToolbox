#!/bin/bash

echo '#!/bin/bash' > $2
echo "echo \"SmartGridToolbox version $1 (git SHA $(git rev-parse --short HEAD))\"" > $2
chmod a+x $2
