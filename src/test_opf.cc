// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SgtCore/config.h>

#include <SgtCore.h>

#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>

using namespace arma;
using namespace std;

int main(int argc, char** argv)
{
    using namespace Sgt;

    string type = argv[1];
    string inFName = argv[2];

    Network nw(100.0);

    auto printNetw = [&]()->void
    {
        cout << "Buses:" << endl;
        for (const auto& b : nw.buses())
        {
            cout << b->id() << endl;
            Col<double> absV = abs(b->V());
            Col<double> argV = arg(b->V());
            Col<double> reV = real(b->V());
            Col<double> imV = imag(b->V());
            cout << "    " << absV << " " << argV << " " << reV << " " << imV << endl;
        }
        cout << "Zips:" << endl;
        for (const auto& z : nw.zips())
        {
            cout << z->id() << endl;
            cout << "    " << z->STerm() << " " << z->ITerm() << endl;
        }
        cout << "Gens:" << endl;
        for (const auto& g : nw.gens())
        {
            cout << g->id() << endl;
            Col<Complex> v = mapPhases(g->bus()->V(), g->bus()->phases(), g->phases());
            Col<Complex> i = conj(v % g->S());
            Col<double> p = real(g->S());
            Col<double> q = real(g->S());
            Col<double> ire = real(i);
            Col<double> iim = imag(i);
            cout << "    " << p << " " << q << " " << ire << " " << iim << endl;
        }
        cout << "Branches:" << endl;
        for (const auto& b : nw.branches())
        {
            cout << b->id() << endl;
            for (size_t iSide = 0; iSide < 2; ++iSide)
            {
                cout << "    " << b->STerm()[iSide] << " " << b->ITerm()[iSide] << endl;
            }
        }
    };

    Parser<Network> p;
    if (inFName.substr(inFName.size() - 2, 2) == ".m")
    {
        std::string yamlStr = std::string("--- [{matpower : {input_file : ") + inFName + ", default_kV_base : 11}}]";
        YAML::Node n = YAML::Load(yamlStr);
        p.parse(n, nw);
    }
    else
    {
        p.parse(inFName, nw);
    }
    cout << "Starting initial solve" << endl;
    nw.solvePowerFlow();
    cout << "Initial solve: status = " << nw.isValidSolution() << endl;
    printNetw();
    cout << "Starting OPF" << endl;
    unique_ptr<PowerFlowSolver> solver;
    if (type == "s_pol")
    {
        auto solver1 = std::make_unique<OpfSPolPfSolver<>>();
        solver1->setOptions({{"debug", true}, {"pf_mode", true}});
        solver = std::move(solver1);
    }
    nw.setSolver(std::move(solver));
    nw.solvePowerFlow(false);
    printNetw();
}
