// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SgtCore/config.h>

#include <SgtCore.h>

#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>

using namespace arma;
using namespace std;

char* getCmdOption(char** argv, int argc, const string & option)
{
    auto begin = argv;
    auto end = argv + argc;
    char ** itr = find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** argv, int argc, const string& option)
{
    auto begin = argv;
    auto end = argv + argc;
    return find(begin, end, option) != end;
}

int main(int argc, char** argv)
{
    using namespace Sgt;

    sgtAssert(argc > 1,
            "Usage: solve_network [--solver solver_arg] [--prefix prefix] [--warm_start] [--quiet] [--debug] infile");

    string inFName = argv[argc - 1];

    string solver = "nr_rect";
    {
        const char* opt = getCmdOption(argv, argc, "--solver");
        if (opt != nullptr) solver = opt;
    }
    
    json solverOptions = json::object();
    {
        char* opt = getCmdOption(argv, argc, "--solver_options");
        if (opt != nullptr) solverOptions = json::parse(opt);
    }

    string outPrefix = "out";
    {
        const char* opt = getCmdOption(argv, argc, "--prefix");
        if (opt != nullptr) outPrefix = opt;
    }

    bool warmStart = cmdOptionExists(argv, argc, "--warm_start");
    
    bool quiet = cmdOptionExists(argv, argc, "--quiet");
    
    const char* debug = getCmdOption(argv, argc, "--debug");

    if (debug)
    {
        cout << "debug" << endl;
        Sgt::messageLogLevel() = Sgt::LogLevel::VERBOSE;
        Sgt::warningLogLevel() = Sgt::LogLevel::VERBOSE;
        Sgt::debugLogLevel() = string(debug) == string("verbose") 
            ? Sgt::LogLevel::VERBOSE : Sgt::LogLevel::NORMAL;
    }

    Network nw(1.0);
    Parser<Network> p;
    if (inFName.substr(inFName.size() - 2, 2) == ".m")
    {
        std::string yamlStr = std::string("--- [{matpower : {input_file : ") + inFName + ", default_kV_base : 11}}]";
        YAML::Node n = YAML::Load(yamlStr);
        p.parse(n, nw);
    }
    else
    {
        p.parse(inFName, nw);
    }

    if (solver == "nr_rect")
    {
        sgtLogMessage() << "Using nr_rect solver." << endl;
        nw.setSolver(std::make_unique<PowerFlowNrRectSolver>());
    }
    else if (solver == "opf_s_pol")
    {
#ifdef ENABLE_OPF
        sgtLogMessage() << "Using opf_s_pol solver." << endl;
        nw.setSolver(std::make_unique<OpfSPolPfSolver<>>());
#else // ENABLE_OPF
        sgtError("OPF solver is not available, since SmartGridToolbox was not compiled with --enable-madopt.");
#endif // ENABLE_OPF
    }
    else
    {
        sgtError("Solver " << solver << " was not found.");
    }

    nw.solver().setOptions(solverOptions);

    map<string, int> busMap;

    Stopwatch sw;
    sw.start();
    bool success = nw.solvePowerFlow(!warmStart);
    sw.stop();
    cout << "(success, time) = " << success << " " << sw.cpuSeconds() << endl;
    cout << "\nCost          = " << nw.genCostPerUnitTime() << endl;

    if (!quiet) 
    {
        cout << "\nBuses:" << endl;
        for (auto bus : nw.buses())
        {
            ostringstream VMag;
            ostringstream VAng;
            for (auto vi : bus->V())
            {
                VMag << abs(vi) << " ";
                VAng << (180 / pi) * arg(vi) << " ";
            }
            cout << bus->id() << " [ " << VMag.str() << "] @ [ " << VAng.str() << " ]" << endl;
        }
        cout << "\nGens:" << endl;
        for (auto gen : nw.gens())
        {
            cout << gen->id() << " " << gen->S() << endl;
        }
    }
}
