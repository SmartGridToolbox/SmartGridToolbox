// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SgtCore.h>
#include <SgtCore/SparseHelper.h>
#include <SgtCore/KluSolver.h>

#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>

using namespace arma;
using namespace std;
using namespace Sgt;

char* getCmdOption(char** argv, int argc, const string & option)
{
    auto begin = argv;
    auto end = argv + argc;
    char ** itr = find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** argv, int argc, const string& option)
{
    auto begin = argv;
    auto end = argv + argc;
    return find(begin, end, option) != end;
}

int main(int argc, char** argv)
{
    using namespace Sgt;

    sgtAssert(argc > 1,
            "Usage: solve_network [--delta delta] [--solver solver_arg] [--prefix prefix] "
            "[--solve_options solver_options] [--warm_start] [--print_loads] [--debug] networkfile loadsfile");

    string networkFName = argv[argc - 2];
    string loadsFName = argv[argc - 1];
    
    double delta = 1;
    {
        const char* opt = getCmdOption(argv, argc, "--delta");
        if (opt != nullptr) delta = stod(opt);
    }

    string solver = "nr_rect";
    {
        const char* opt = getCmdOption(argv, argc, "--solver");
        if (opt != nullptr) solver = opt;
    }

    const char* solverOptions = getCmdOption(argv, argc, "--solver_options");
    json solverOptionsJson;
    if (solverOptions != nullptr) solverOptionsJson = json::parse(solverOptions);
    
    string outPrefix = "out";
    {
        const char* opt = getCmdOption(argv, argc, "--prefix");
        if (opt != nullptr) outPrefix = opt;
    }

    bool warmStart = cmdOptionExists(argv, argc, "--warm_start");
    
    bool printLoads = cmdOptionExists(argv, argc, "--print_loads");
    
    const char* debug = getCmdOption(argv, argc, "--debug");

    if (debug)
    {
        Sgt::messageLogLevel() = Sgt::LogLevel::VERBOSE;
        Sgt::warningLogLevel() = Sgt::LogLevel::VERBOSE;
        Sgt::debugLogLevel() = string(debug) == string("verbose") 
            ? Sgt::LogLevel::VERBOSE : Sgt::LogLevel::NORMAL;
    }

    Network nw(100.0);
    Parser<Network> p;
    if (networkFName.substr(networkFName.size() - 2, 2) == ".m")
    {
        string yamlStr = string("--- [{matpower : {input_file : ") + networkFName + ", default_kV_base : 11}}]";
        YAML::Node n = YAML::Load(yamlStr);
        p.parse(n, nw);
    }
    else
    {
        p.parse(networkFName, nw);
    }

    auto loadJsn = json::parse(ifstream(loadsFName));

    if (printLoads)
    {
        // Print load ids and quit.
        sgtLogMessage() << "Load Deltas:" << endl;
        for (const auto& z : nw.zips()) sgtLogMessage() << setw(16) << left << z->id()
            << " " << left << sum(real(z->S())) << endl;
        return 0;
    }

    if (solver == "nr_rect")
    {
        nw.setSolver(make_unique<PowerFlowNrRectSolver>());
    }

    if (!solverOptionsJson.is_null())
    {
        nw.solver().setOptions(solverOptionsJson);
    }

    nw.solvePowerFlow(!warmStart);

    map<string, Col<Complex>> loads;
    for (auto it = loadJsn.begin(); it != loadJsn.end(); ++it)
    {
        const auto& zip = nw.zips()[it.key()];
        const auto nComp = zip->nComps();
        Complex ldPerPh{it.value()[0], it.value()[1]}; ldPerPh /= nComp;
        Col<Complex> ldVec(nComp, arma::fill::none); ldVec.fill(ldPerPh);
        loads[it.key()] = ldVec;
    }

    auto sens = sensitivity(nw, loads);

    map<string, array<Col<double>, 6>> vs;
    for (const auto& x : sens)
    {
        const string& id = x.busId;
        vs[id][0] = x.dvdx; // Calcd sensitivity vmag
        vs[id][1] = x.dtdx; // Calcd sensitivity vang
        vs[id][2] = abs(nw.buses()[id]->V()); // Initial voltage magnitude.
        vs[id][3] = arg(nw.buses()[id]->V()); // Initial voltage angle.
    }

    for (const auto& x : sens)
    {
        sgtLogMessage()
            << setw(16) << left << x.busId 
            << setw(16) << left << x.dvdx
            << setw(16) << left << x.dtdx << endl;
    }

    for (auto it = loadJsn.begin(); it != loadJsn.end(); ++it)
    {
        string zipId = it.key();
        auto nwZip = nw.zips()[zipId];
        Col<Complex> s = nwZip->SConst();
        sgtLogMessage() << zipId << " " << nwZip->SConst() << " ";
        Complex ds = delta * it.value().get<Complex>() / double(nwZip->nComps());
        for (auto& x : s) x += ds;
        nwZip->setSConst(s);
        sgtLogMessage() << " -> " << " " << nwZip->SConst() << " ";
    }

    nw.solvePowerFlow();

    for (const auto& x : sens)
    {
        const string& id = x.busId;
        vs[id][4] = abs(nw.buses()[id]->V()); // Final voltage magnitude.
        vs[id][5] = arg(nw.buses()[id]->V()); // Final voltage angle.
    }
   
    sgtLogMessage() << endl;
    for (const auto& x : vs)
    {
        const auto& id = x.first;
        const auto& dat = x.second;
        Col<double> expectedDv = delta * dat[0];
        Col<double> expectedDt = delta * dat[1];
        Col<double> observedDv = dat[4] - dat[2]; 
        Col<double> observedDt = dat[5] - dat[3]; 
        Col<double> errDv = all(expectedDv == observedDv)
            ? Col<double>(size(expectedDv), fill::zeros) : (expectedDv - observedDv) / observedDv;
        Col<double> errDt = all(expectedDt == observedDt)
            ? Col<double>(size(expectedDt), fill::zeros) : (expectedDt - observedDt) / observedDt;
        sgtLogMessage() << id << endl;
        sgtLogMessage() << "    " << expectedDv <<  endl;
        sgtLogMessage() << "    " << observedDv <<  endl;
        sgtLogMessage() << "    " << errDv <<  endl;
        sgtLogMessage() << endl;
        sgtLogMessage() << "    " << expectedDt <<  endl;
        sgtLogMessage() << "    " << observedDt <<  endl;
        sgtLogMessage() << "    " << errDt <<  endl;
    }
}
