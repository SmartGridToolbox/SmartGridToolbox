#!/bin/bash

REPO="dexterurbane/sgt"

LINUX_VERS=ubuntu:latest
SGT_BRANCH=$(git rev-parse --short HEAD)
ARCHS="amd64 arm64"
PUSH=F
ALLOW_SUDO=F
NO_CACHE=

while getopts "a:l:b:psn" OPT; do
    case "$OPT" in
        a)
            ARCHS=${OPTARG}
            ;;
        l)
            LINUX_VERS=${OPTARG}
            ;;
        b)
            SGT_BRANCH=${OPTARG}
            ;;
        p)
            PUSH=T
            ;;
        s)
            ALLOW_SUDO=T
            ;;
        n)
            NO_CACHE="--no-cache"
            ;;
    esac
done

TAG_SUFFIX=$(echo ${SGT_BRANCH} | tr -- ':-.' '_')

for ARCH in ${ARCHS}; do
    TAG_LINUX_VERS=$(echo ${LINUX_VERS} | tr -- ':-.' '_')
    TAG=${REPO}:${ARCH}-${TAG_LINUX_VERS}-${TAG_SUFFIX}
    if [ "${ALLOW_SUDO}" == T ]; then
        TAG="${TAG}-sudo"
    fi

    echo "Building tag ${TAG}"
    CMD="docker build --platform ${ARCH} --build-arg LINUX_VERS=${LINUX_VERS} --build-arg SGT_BRANCH=${SGT_BRANCH} --build-arg ALLOW_SUDO=${ALLOW_SUDO} -t ${TAG} ${NO_CACHE} ."
    echo ${CMD}
    eval ${CMD}

    echo "To tag and push another tag: docker tag ${TAG} <new_tag>; docker push <new_tag>"

    if [ "${PUSH}" == T ]; then
        docker push ${TAG}
    fi
done
