// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//<1
#include <SgtCore.h>
#include <SgtSim.h>
//>

//<2
int main(int argc, char** argv)
{
    using namespace Sgt;

    Simulation sim;
        // The top level simulation object.
    Parser<Simulation> p;
        // Parse information into sim from config file "simulation.yaml".
    p.parse("simulation.yaml", sim);
//>

//<3
    // for (const auto& comp : sim.simComponents()) sgtLogMessage() << *comp << std::endl;
//>
    
//<4
    // We just parsed in a simulation. The user is also free to modify this programmatically, e.g. below we add
    // an extra zip load to the network, and associate it with an inverter connected to a battery.
    
    // Get the SimNetwork by name. The SimNetwork, with id "network", was parsed in from simulation.yaml.
    auto simNetw = sim.simComponent<SimNetwork>("sim_network");
    
    // Add a new Zip to the network, to represent the battery inverter. The first two arguments to batteryZip are the
    // bus ID and phase, and the rest of the arguments are given to the Zip(...) constructor.
    auto batteryZip = simNetw->network().newZip<Zip>("bus_2", Phase::BAL, "battery_inv_zip", 1, Zip::wyeConnection(1));
    
    // Create an inverter that is associated with this zip. The first argument is the ID of the new component,
    // the second is the ID of the SimNetwork, and the third is the ID of the associated network ZIP.
    auto batteryInv = sim.newSimComponent<Inverter>("battery_inv", "sim_network", "battery_inv_zip");

    // Create a battery and set its parameters. Arguments are the battery ID and the inverter ID.
    auto batt = sim.newSimComponent<Battery>("battery_batt", "battery_inv");
    batt->setMaxSoc(0.01); // 10 kWh
    batt->setMaxChargePower(0.005); // Max 5 kW charging
    batt->setMaxDischargePower(0.005); // Max 5 kW discharging
    batt->setChargeEfficiency(0.9); // Max 5 kW discharging
    batt->setDischargeEfficiency(0.9); // Max 5 kW discharging
    batt->setInitSoc(0.01); // Start at full charge 
    batt->setDt(minutes(5)); // Update state every 5 minutes
    batt->setRequestedPower(0.005); // Set to discharging at maximum power
//>
    
//<5
    // After parsing or making other modifications, we need to call initialize. All components will be initialized
    // to time sim.startTime().
    sim.initialize();

    sgtLogMessage() << *sim.simComponent<SimNetwork>("sim_network") << std::endl;
        // Print the SimNetwork, at the start of the simulation.
//>

//<6
    // Obtain some additional references for output.
    
    auto bus2 = simNetw->network().buses()["bus_2"];
        // Get bus by name.
    auto pv = sim.simComponent<SolarPv>("solar_pv");
        // Get solar PV by name.
    auto pvInv = sim.simComponent<Inverter>("pv_inverter");
        // Get the PV inverter by name.
//>

//<7
    while (!sim.isFinished())
    {
        sgtLogMessage() << "Timestep " << localDateTime(sim.currentTime()) << std::endl;
        sim.doTimestep();
            // Perform a single timestep: keep updating components until (1) the time has increased and all components
            // are up to date at that time, or (2) the simulation finishes. Finer grained control is also possible,
            // e.g. sim.doNextUpdate() will update one component at a time.
        {
            sgtLogIndent();
                // Indent within scope.
            sgtLogMessage() << "solar inverter P       = " << pvInv->zip()->STot().real() << std::endl;
            sgtLogMessage() << "battery inverter P     = " << batteryInv->zip()->STot().real() << std::endl;
            sgtLogMessage() << "battery charge         = " << batt->soc() << std::endl << std::endl;
            sgtLogMessage() << "bus_2 V = " << bus2->V() << std::endl;
        }
    }
//>
}
