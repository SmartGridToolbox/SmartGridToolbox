[modeline]: # ( vim: set ft=markdown tw=2000: )
# [](#simulation-tutorial)Simulation Tutorial
In this tutorial, we will explore how to set up and run a discrete event simulation.

The tutorial can be found in the `tutorials/simulation` subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. `cd` into the directory. Setup for compiling by typing `meson setup build`. To compile or recompile, use this: `ninja --verbose -C build`. (the `--verbose` flag shows you the actual compilation and linking commands). This will compile an executable in the build directory, which can be run by typing `build/simulation`.

Below, we shall list sections of the code, along with explanations.

For discrete event simulation, we use the `SgtCore` and `SgtSim` header files.

```cxx
#include <SgtCore.h>
#include <SgtSim.h>
```

We first create a simulation, and populate it by parsing in data from an input file named [`simulation.yaml`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/simulation/simulation.yaml).

```cxx
int main(int argc, char** argv)
{
    using namespace Sgt;

    Simulation sim;
        // The top level simulation object.
    Parser<Simulation> p;
        // Parse information into sim from config file "simulation.yaml".
    p.parse("simulation.yaml", sim);
```

At this point, you should take a look at the contents of [`simulation.yaml`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/simulation/simulation.yaml):

```yaml
- parameters:
    start_time: 2013-01-02 06:00:00
    end_time: 2013-01-03 06:00:00
    input_file: simulation.m
- simulation:
    start_time: <start_time>
    end_time: <end_time>
    lat_long: [-35.3075, 149.1244]
    timezone: AEST10AEDT,M10.5.0/02,M3.5.0/03 # Timezone info for Canberra.
- time_series:
    id: cloud_cover_series
    value_type: real_vector
    dimension: 3
    type: data_time_series
    data_file: cloud.txt
    relative_to_time: <start_time>                                                        
    time_unit: m                                                                          
    interp_type: lerp                 
- weather:
    id: weather
    dt: 00:20:00
    irradiance: 
        solar_model: standard
    cloud_attenuation_factors:
        series: cloud_cover_series
- sim_network:
    id: sim_network
    P_base: 1e6
    freq_Hz: 60
- matpower: # Use a matpower case for the network design.
    sim_network_id: sim_network
    input_file: <input_file>
    default_kV_base: 11
- inverter: # Add an inverter.
    id: pv_inverter
    sim_network_id: sim_network
    zip:
        bus_id: bus_2
        phases: [BAL]
        connection: wye
    max_S_mag: 60
    efficiency_dc_to_ac: 0.86
    efficiency_ac_to_dc: 0.86
- solar_pv: # Add a solar farm to the inverter.
    id: solar_pv
    weather: weather
    inverter_id: pv_inverter
    zenith_degrees: 60 
    azimuth_degrees: 0
    n_panels: 3000
    panel_area_m2: 1.64 
    irradiance_ref_W_per_m2: 1000 
    T_cell_ref_C: 25 
    P_max_ref_W: 265
    temp_coeff_P_max_per_C: -0.004
    NOCT_C: 44
```

What do you think the simulation does? Try printing the contents of the simulation: uncomment the following line:

```cxx
    // for (const auto& comp : sim.simComponents()) sgtLogMessage() << *comp << std::endl;
```

What is the output?
    
The code below shows that, as well as setting up simulations by parsing YAML input files, we can also create simulation objects programmatically. Below, we add an extra [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) to the network, and associate it with an inverter connected to a battery. Details are given in the code comments.

```cxx
    // We just parsed in a simulation. The user is also free to modify this programmatically, e.g. below we add
    // an extra zip load to the network, and associate it with an inverter connected to a battery.
    
    // Get the SimNetwork by name. The SimNetwork, with id "network", was parsed in from simulation.yaml.
    auto simNetw = sim.simComponent<SimNetwork>("sim_network");
    
    // Add a new Zip to the network, to represent the battery inverter. The first two arguments to batteryZip are the
    // bus ID and phase, and the rest of the arguments are given to the Zip(...) constructor.
    auto batteryZip = simNetw->network().newZip<Zip>("bus_2", Phase::BAL, "battery_inv_zip", 1, Zip::wyeConnection(1));
    
    // Create an inverter that is associated with this zip. The first argument is the ID of the new component,
    // the second is the ID of the SimNetwork, and the third is the ID of the associated network ZIP.
    auto batteryInv = sim.newSimComponent<Inverter>("battery_inv", "sim_network", "battery_inv_zip");

    // Create a battery and set its parameters. Arguments are the battery ID and the inverter ID.
    auto batt = sim.newSimComponent<Battery>("battery_batt", "battery_inv");
    batt->setMaxSoc(0.01); // 10 kWh
    batt->setMaxChargePower(0.005); // Max 5 kW charging
    batt->setMaxDischargePower(0.005); // Max 5 kW discharging
    batt->setChargeEfficiency(0.9); // Max 5 kW discharging
    batt->setDischargeEfficiency(0.9); // Max 5 kW discharging
    batt->setInitSoc(0.01); // Start at full charge 
    batt->setDt(minutes(5)); // Update state every 5 minutes
    batt->setRequestedPower(0.005); // Set to discharging at maximum power
```

There is a fair bit going on here - let\'s unpack it a bit more. [`Simulation`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_simulation.html)s consist of a collection of [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html)s. These are objects that undergo updates, at discrete times, that are determined both by their own internal workings as well as by their interactions with other [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html)s.

A [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html) is a particular type of [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html) that wraps a [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html), and ensures that the [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html) triggers a contingent update whenever the [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html) changes - for example, if one of the [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html)s changes its power consumption - which causes the [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html) solver to be run. (Note that [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html)s are not [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html)s - they are part of the SgtCore library).

The [`Inverter`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_inverter.html) in the code above derives from the [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) class. In the same way that [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html)s wrap [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html)s, [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html)s wrap [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html)s. Not every [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) in a [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html) needs a corresponding [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html). A [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) is useful when we want a network [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) to be driven by a simulation object; they keep a convenient reference to the associated [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) and, more importantly, will maintain the correct dependency to their [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html) so that they are always updated prior to the [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html). Just as we have the [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) class, we also have [`SimBus`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_bus.html), [`SimBranch`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_branch.html), and [`SimGen`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_gen.html).


When the [`Battery`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_battery.html) in the code above undergoes a scheduled update (which is configured in the code to occur at 5 minute intervals), it will trigger a contingent update in the associated [`Inverter`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_inverter.html) which, as we mentioned, is a [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html). This will cause the [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html)'s associated [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) to update its power, which will trigger an update in the [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html). This in turn will cause the network solver to be used to update the network.

Regardless of any complicated sequences of dependencies and updates that may exist in the simulation, the [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html) will always be updated *after* all [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html)s. This prevents redundant cycles where, for example, one [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) update triggers a call to the network solver, and then another [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) update triggers *another* call to the network solver, and so on.

This introduces the idea of [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html) dependencies. Behind the scenes, when a [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) is added to a [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html), the `SimComponent::dependsOn(...)` member function will be called on the [`SimNetwork`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_network.html), with the [`SimZip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_zip.html) as its argument. If [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html) A depends on [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html) B, then B will update before A.

OK - let\'s move on! After parsing in and/or creating the components in the simulation, `Simulation::initialize()` must be called. During this phase, dependencies are processed, and components will be initialized to time `sim.startTime()`.

```cxx
    // After parsing or making other modifications, we need to call initialize. All components will be initialized
    // to time sim.startTime().
    sim.initialize();

    sgtLogMessage() << *sim.simComponent<SimNetwork>("sim_network") << std::endl;
        // Print the SimNetwork, at the start of the simulation.
```

We are now ready to begin timestepping. First, however, we grab references to particular [`SimComponent`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_sim_component.html)s for output purposes.

```cxx
    // Obtain some additional references for output.
    
    auto bus2 = simNetw->network().buses()["bus_2"];
        // Get bus by name.
    auto pv = sim.simComponent<SolarPv>("solar_pv");
        // Get solar PV by name.
    auto pvInv = sim.simComponent<Inverter>("pv_inverter");
        // Get the PV inverter by name.
```

The timestepping loop is then started:

```cxx
    while (!sim.isFinished())
    {
        sgtLogMessage() << "Timestep " << localDateTime(sim.currentTime()) << std::endl;
        sim.doTimestep();
            // Perform a single timestep: keep updating components until (1) the time has increased and all components
            // are up to date at that time, or (2) the simulation finishes. Finer grained control is also possible,
            // e.g. sim.doNextUpdate() will update one component at a time.
        {
            sgtLogIndent();
                // Indent within scope.
            sgtLogMessage() << "solar inverter P       = " << pvInv->zip()->STot().real() << std::endl;
            sgtLogMessage() << "battery inverter P     = " << batteryInv->zip()->STot().real() << std::endl;
            sgtLogMessage() << "battery charge         = " << batt->soc() << std::endl << std::endl;
            sgtLogMessage() << "bus_2 V = " << bus2->V() << std::endl;
        }
    }
```

At each iteration, information about several components is printed. By looking at the output, you may notice that the voltage changes by a small amount at each timestep, in response to changing PV and battery loads.

There is a lot more to learn about discrete event simulations in SmartGridToolbox. Next, you may wish to check out the [Time Varying Loads Tutorial](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/tvloads_tutorial.html).
