#!/bin/bash

# Assumes we're in the tutorials directory.
EGDOC=$(pwd)/egdoc.py
for D in basics component_collection network parsing properties simulation time_varying_loads; do
    echo $D
    cd $D
    T=$(ls *.template)
    MD=${T%.template}
    ${EGDOC} ${T} ${MD}
    cd ..
done
