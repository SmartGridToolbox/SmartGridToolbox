#!/usr/bin/env python3

# Script to produce markdown documentation for specially commented C++ code

import argparse
import os.path
import re
import sys

parser = argparse.ArgumentParser(
    description='Produce tutorial-style markdown from code')

parser.add_argument('input', help='Input markdown')
parser.add_argument('output', help='Output markdown')
args = parser.parse_args()

def parse_code(fn):
    with open(fn) as f:
        ls = [x for x in f] # Strip off explicit newlines

    result = {}
    cur = None
    for l in ls:
        l3 = l.strip().ljust(3)
        if l3[0:3] == '//<':
            cur = l3[3:]
        elif l3[0:2] == '#<' or l3[0:2] == '#>':
            cur = l3[2:]
        elif l3[0:3] == '//>' or l3[0:2] == '#>':
            cur = None
        elif cur is not None:
            result.setdefault(cur, []).append(l)

    return result

langs = {
    '.c': 'c',
    '.cc': 'cxx',
    '.cpp': 'cxx',
    '.yaml': 'yaml',
    '.yml': 'yaml',
    '.py': 'python',
    '.txt': 'text',
    '.out': 'text',
    '.log': 'text'
}

def sub(fn, k):
    _, ext = os.path.splitext(fn)
    lang = langs[ext]

    try:
        code = sub.code_dict[fn]
    except KeyError:
        code = parse_code(fn)
        sub.code_dict[fn] = code

    return ['```' + lang + '\n'] + code[k] + ['```\n']

sub.code_dict = {}

out_lines = []

with open(args.input) as f:
    in_lines = [x for x in f]

re_def = re.compile(r'^\[DEF\]:\s+#\s+\(\s*([^\s]+)\s+(.*)\)\s*$')
re_inc = re.compile(r'^\[INC\]:\s+#\s+\(\s*([^\s]+)\s+([0-9]+)\s*\)\s*$')

defs = {}
for l in in_lines:
    for k, v in defs.items():
        l = l.replace('`@' + k + '@`', v)

    match_def = re_def.fullmatch(l)
    match_inc = re_inc.fullmatch(l)

    if match_def:
        k, v = match_def.groups()
        defs[k] = v
    elif match_inc:
        fn, k = match_inc.groups()
        out_lines.extend(sub(fn, k))
    else:
        out_lines.append(l)

with open(args.output, 'w+') as f:
    f.write(''.join(out_lines))
