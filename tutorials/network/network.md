[modeline]: # ( vim: set ft=markdown tw=2000: )

# [](#network-tutorial)Network Tutorial

## Getting Started
In this tutorial, we will explore the basics of how to set up and solve models of electricity networks. We are not performing any discrete event simulations, and therefore we link only to `SgtCore`, not `SgtSim`.

The tutorial can be found in the `tutorials/network` subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. `cd` into the directory. Setup for compiling by typing `meson setup build`. To compile or recompile, use this: `ninja --verbose -C build`. (the `--verbose` flag shows you the actual compilation and linking commands). This will compile two executables in the build directory, `network` and `network_yaml`, which can be run by typing `build/network` or `build/network_yaml`.

## A Network Primer
The figure below shows how a SmartGridToolbox network is constructed. ![](images/sgt_network.png) The network is a [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html) object. It is, essentially, a collection of objects of four types: buses, branches, gens and zips. Each of these components can be seen in part (A) of the diagram above.

Buses derive from the [`Bus`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_bus.html) class. They represent network nodes, and can be thought of as a set of conducting connection points (one for each phase) with well defined voltages.

Branches derive from the [`BranchAbc`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_branch_abc.html) class. They consist of lines and transformers in the network, and connect two buses.

Gens derive from the [`Gen`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_gen.html) class. They represent controlled generators.

Zips derive from the [`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) class. They represent loads or uncontrolled generators. A *load convention* is used for their power, such that power consumption is positive and generation is negative.

Buses have of one or more *bus phases*, represented by the [`Phase`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/group___power_flow_core.html#ga9995c7b16c14b177680f5c21cdde3702) enum. These are essentially labels for the electrical phases at the bus. A collection of phases is stored in a [`Phases`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_phases.html) object; each bus has one of these.

Branches, gens and zips have *terminals*. As shown in the diagram, branches have two terminals, indexed 0 and 1, and gens and zips have a single terminal. Terminals are an abstract concept; there is no `Terminal` class. Each terminal consists of a number of *terminal phases*, that are indexed 0, 1, etc.

When an element (branch, gen or zip) is added to a network, a bus is associated with each terminal, and a bus phase is associated with each terminal phase. Details will be demonstrated in the code in the next section.

As shown in part (C) of the diagram, each bus has an asociated *bus voltage*, a vector consisting of the voltage on each bus phase, in order. Each terminal has a *terminal voltage*, equal to the voltages of the associated bus phases, and *terminal current/power*, equal to the current/power flowing *from* each associated bus phase *into* the corresponding terminal (and, thus, into the component).  

Buses, branches, gens and zips are connected together to form a meshed graph, as shown in part (B) of the diagram, above. Note that multiple branches, gens and zips may be connected to each bus.

## Tutorial Code
Run the `network` executable by typing `build/network`, taking note of the output. Now read through the code
[`network.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network.cc), paying special attention to the documentation and comparing the code to the output.

```cxx
#include <SgtCore.h> // Include SgtCore headers.
```

SmartGridToolbox is packaged as two libraries: SgtCore, and SgtSim. SgtCore contains utility code for handling dates, time, logging, parsing, vectors and matrices, complex numbers and so on, as well as basic code for solving power flow problems. SgtSim adds a layer for performing discrete event simulations, as well as a component library of simulation objects such as batteries, PV panels, inverters and so on.

```cxx
    using namespace Sgt;
```

Most SmartGridToolbox objects are in the an namespace called \"`Sgt`\".

```cxx
    Network nw;
        // Create a network.
        
    auto bus1 = std::make_shared<Bus>("bus_1", Phase::BAL, arma::Col<Complex>{11}, 11);
        // Create a bus named "bus_1", with a single balanced (BAL) phase, a nominal voltage vector of [11 kV]
        // (with only 1 element because it is single phase), and base voltage of 11 kV. 
    bus1->setType(BusType::SL);
        // Make it the slack bus for powerflow.
    nw.addBus(bus1);
        // Add it to the network.
```

Here, we create a shared pointer to a new [`Bus`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_bus.html) object, with id \"bus\_1\", using the parameters of the [`Bus`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_bus.html) constructor. A bus represents a node in the network. It can have one or more electrical phases, and has an associated set of voltages, one for each phase. Components such as [loads](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html) and [generators](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_gen.html) can be associated with a bus, and two buses may be connected together using a [branch](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_branch_abc.html). After creating the bus, its type (for the purpose of power flow calculations) is changed from the default of `BusType::PQ` to `BusType::SL`, making it into the special \"slack\" generator bus for the network. Finally, the bus is added to the network object that was created earlier. Please consult a text or tutorial on AC power flow to find out more about bus types and other aspects of the power flow problem.

```cxx
    auto gen1 = std::make_shared<Gen>("gen_1", 1);
        // Create a generator named "gen_1", with one phase terminal.
    nw.addGen(gen1, "bus_1", Phase::BAL);
        // Add it to the network, attaching it to the (only) phase BAL of bus_1.
   
    auto bus2 = std::make_shared<Bus>("bus_2", Phase::BAL, arma::Col<Complex>{11}, 11);
        // Another bus...
    bus2->setType(BusType::PQ);
        // Make it a PQ bus (load or fixed generation).
    nw.addBus(bus2);
        // Add it to the network.
    
    auto load2 = std::make_shared<Zip>("load_2", 1, Zip::wyeConnection(1));
        // Create a "ZIP" load named load_2, with one phase and a one-phase wye phase-ground connection.
    load2->setSConst({std::polar(0.1, -5.0 * pi / 180.0)}); 
        // The load consumes a constant S component of 0.1 MW = 100 kW, at a phase angle of -5 degrees.
    nw.addZip(load2, "bus_2", Phase::BAL);
        // Add it to the network and to bus_2.
```

Here, we add a generator ([`Gen`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_gen.html)) to bus\_1, create another bus named bus\_2, and add a load ([`Zip`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html)) to bus\_2, all using the same patten that was used to create and add bus\_1.

```cxx
    auto line = std::make_shared<CommonBranch>("line");
        // Make a "CommonBranch" (Matpower style) line.
    line->setYSeries({Complex(0.005, -0.05)});
        // Set the series admittance of the line.
    nw.addBranch(line, "bus_1", Phase::BAL, "bus_2", Phase::BAL);
        // Add the line to the network, between the two buses.
```

A line is now added, of class [`CommonBranch`](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_common_branch.html), which derives from [`BranchAbc`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_branch_abc.html). The line connects the two buses together.

At this point, our network consists of two buses, bus\_1 and bus\_2. bus\_1 is a slack bus, with a generator attached,
and bus\_2 is a PQ bus, with a load attached. We may now solve the power flow problem:

```cxx
    nw.solvePowerFlow();
        // Solve the power flow problem, using default Newton-Raphson solver.
```
    
After doing so, we print out some information about the state of the network:

```cxx
    // We show below how to retrieve some information:
    auto bus = nw.buses()["bus_2"];
    sgtLogMessage() << "Bus " << bus->id() << ": " << " voltage is " << bus->V() << std::endl;
        // Note logging...

    sgtLogMessage() << "Network: " << nw << std::endl;
        // Print the network.
```

## Use of YAML Configuration Files
Typically, one wouldn\'t explicitly code all the network components, as is done in this first example. Instead, objects would be loaded in from a YAML configuration file. [`network_yaml.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network_yaml.cc) loads the same problem as [`network.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network.cc) in from the configuration file [`network.yaml`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network.yaml), and again solves the network, achieving the same solution. The relevant code looks like this:

```cxx
    Network nw;
        // Create a network named "network".
    Parser<Network> p; 
        // Make a network parser to parse information into the network.
    p.parse("network.yaml", nw);
        // Read in from the config file "network.yaml".
        
    nw.solvePowerFlow();
        // Solve the power flow problem, using default Newton-Raphson solver.
```

and the YAML configuration file looks like this:

```yaml
- bus:
    id: bus_1
    phases: [BAL]
    type: SL
    V_base: 11
    V_nom: [11]
- gen:
    id: gen_1
    bus_id: bus_1
    phases: [BAL]
- bus:
    id: bus_2
    phases: [BAL]
    type: PQ
    V_base: 11
    V_nom: [11]
- zip:
    id: load_2
    bus_id: bus_2
    phases: [BAL]
    connection: wye
    S_const: [0.1D-5.0] # 0.1 @ -5 degrees.
- common_branch:
    id: line
    bus_0_id: bus_1
    bus_1_id: bus_2
    Y_series: 0.005-0.05j
```

To find out more about the YAML specification for SmartGridToolbox networks, see [here](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/group___network_yaml_spec.html) (this information may be incomplete). To see the API reference section relating to networks, see [here](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/group___power_flow_core.html).
