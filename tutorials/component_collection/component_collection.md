[modeline]: # ( vim: set ft=markdown tw=2000: )
# [](#component-collection-tutorial)ComponentCollection Tutorial

In this tutorial, we will explore the [`ComponentCollection<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html) template class, that is frequently used to store collections of objects. [`ComponentCollection<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html)s hold [`ComponentPtr<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_ptr.html)s: smart pointers to objects of type `T`. They have two special properties.

Firstly, they act as order preserving maps - like a cross between a `map` and a `vector`. Elements may be accessed
either by their key, or by an integer index, and the elements may be iterated over in order of insertion.

Secondly, a [`ComponentPtr<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_ptr.html) will remain pointing to the object in the [`ComponentCollection<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html) with the given key, even when that object is replaced in the [`ComponentCollection<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html). Thus, we can safely replace elements in the collection, and be confident that any pointers that may be held will reflect these changes. This can be useful when dealing with complicated discrete event simulations involving many mutually interacting components.

[`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html)s are often used to store members of the [`Component`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component.html) class, that is used in SmartGridToolbox to provide a handy base class for network components and components of discrete event simulations. However, [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html)s may be used to store any data type.

Under the hood, elements of a [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html) are stored as `shared_ptr`s, and therefore are automatically
deallocated when there are no more references. Normally, [`ComponentPtr`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_ptr.html)s should be treated as lightweight objects, and
should be passed by value to maintain the correct reference count.

The tutorial can be found in the `tutorials/component_collection` subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. `cd` into the directory. Setup for compiling by typing `meson setup build`. To compile or recompile, use this: `ninja --verbose -C build`. (the `--verbose` flag shows you the actual compilation and linking commands). This will compile an executable in the build directory, which can be run by typing `build/component_collection`.

Open an editor for [`component_collection.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/-/blob/master/tutorials/component_collection/component_collection.cc). The code is heavily documented. Run `build/component_collection`, and examine the output with reference to the code and the documentation. Below, we consider sections of code in more detail. We assume that the [basics tutorial](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/basics_tutorial.html) has already been completed.

```cxx
    MutableComponentCollection<Foo> cc;
    // To insert elements, we need a MutableComponentCollection.
    cc.insert("f", make_shared<Foo>("foo"));
    // Members of a ComponentCollection are a special ComponentPtr<T> type, passed in using a shared_ptr
    // as shown above.
    cc.insert("b", make_shared<Bar>("bar"));
    // Members of a ComponentCollection are a special ComponentPtr<T> type, passed in using a shared_ptr
    // as shown above.
```

The [`MutableComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_mutable_component_collection.html) class derives from [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html), and allows objects to be inserted. (This is distinct from a `const` [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html), which provides const-only access to components but doesn\'t allow insertion, or a non-const [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html), which provides non-const access and doesn\'t allow insertion. For example, in SmartGridToolbox, a discrete event simulation holds a private [`MutableComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_mutable_component_collection.html), which is publicly exposed only as its [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html) base class through a `components()` member function.

```cxx
    // We can retrieve components like this:
    auto p1 = cc["f"];
    auto p2 = cc["b"];
    
    // Both of these are ComponentPtr<Foo>:
    sgtLogMessage() << "Calling whoAmI on p1:" << endl;
    p1->whoAmI();
    sgtLogMessage() << "Calling whoAmI on p2:" << endl;
    p2->whoAmI();
    
    // We can also retrieve elements by index and iterate over them in order of insertion:
    sgtLogMessage() << "Calling whoAmI on cc[0]:" << endl;
    cc[0]->whoAmI();
    sgtLogMessage() << "Calling whoAmI on cc[1]:" << endl;
    cc[1]->whoAmI();
    sgtLogMessage() << "Iterating:" << endl;
    for (const auto x : cc)
    {
        x->whoAmI();
    }
```
The code above shows various ways of accessing members of a [`ComponentCollection<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html) as a [`ComponentPtr<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_ptr.html).

```cxx
    // We can replace elements, and ComponentPtrs will remain valid and will point to the new element.
    cc.insert("f", make_shared<Foo>("foo replaced"));
    sgtLogMessage() << "Calling whoAmI on p1 after replace:" << endl;
    p1->whoAmI(); // Note: p1 now points to the new component.
```
This code shows how [`ComponentCollection`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html)s have the special property that upon replacement of an element, existing [`ComponentPtr`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_ptr.html)s will point to the new element.

```cxx
    // We can downcast p2 to point to derived type Bar:
    auto p2a = p2.as<Bar>();
    sgtLogMessage() << "Calling whoAmI on p2 after downcast:" << endl;
    p2a->whoAmI();

    // What if we try to downcast p1?
    auto p1a = p1.as<Bar>();
    sgtLogMessage() << "p1 is null after invalid downcast:" << endl;
    sgtLogMessage() << "p1a == null? " << (p1a == nullptr ? "T" : "F") << endl;
```
Here, we see how elements may be downcast, and what happens if we try to make an invalid downcast.

```cxx
    // What if we try to retrieve a component that isn't there?
    auto z = cc["z"];
    sgtLogMessage() << "Attempting to retrieve a component that is not there gives null:" << endl;
    sgtLogMessage() << "z == null? " << (p1a == nullptr ? "T" : "F") << endl;
```
Non-existent keys also return null.

Finally, const-correctness is demonstrated in the following code. You could try to uncomment the two commented out lines, and see what happens.
```cxx
    // There is a const version of ComponentPtr:
    const ComponentCollection<Foo>& constCc = cc;
    // Get a const reference to cc so we can try some stuff.

    // The following code would be a compile error (uncomment and see!):
    // ComponentPtr<Foo> ccp = constCc["a"];

    ConstComponentPtr<Foo> ccp = constCc["f"];
    sgtLogMessage() << "Const method on ConstComponentPtr:" << endl;
    ccp->whoAmI();
    // The following code would be a compile error (uncomment and see!):
    // ccp->nonConstMethod();
```
