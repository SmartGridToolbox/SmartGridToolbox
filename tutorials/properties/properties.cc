//<1
#include <SgtCore/Common.h>
#include <SgtCore/Properties.h>

using namespace Sgt;
using namespace std;

// Base class should derive from HasProperties this so we can use virtual
// properties() and property(...) member functions.
class Foo : public Sgt::HasProperties
{
    public:
        SGT_PROPS_INIT(Foo);
        // Use this whenever we want the class to have properties.
        // The arguments are the default property getter/setter type (when we don't know the underlying type),
        // and the class name.

        int four() const {return 4;}
        SGT_PROP_GET(four, int, four);
        // A gettable property. Arguments are (property_name, getter_return_type, getter_method)
        // Here, the underlying getter return type is int, but often we won't know this,
        // so the property will be gotten as the default type, in this case json.

        vector<int> vec() const {return {1, 2, 3};}
        SGT_PROP_GET(vec, vector<int>, vec);
        // A gettable property, this time for a vector.

        const string& s() const {return s_;}
        void setS(const string& s) {s_ = s;}
        SGT_PROP_GET_SET(s, const string&, s, setS);
        // A gettable and settable property.
        // Arguments are property_name, getter_return_type, getter_method, setter_method)
        // Note that it is possible to get a property by const &, as we do here.
        // Properties are always set using a const & to the bare type specified by getter_return_type.


        SGT_PROP_SET(sSetOnly, string, setS);
        // A property that can be set but not gotten. Arguments are property_name, setter_method)
        // Arguments are property_name, setter_bare_type, setter_method.
        // The argument to setter will always be a const &.

        string s_{"hello"};
};

class Bar : public Foo
{
    public:
        SGT_PROPS_INIT(Bar);
        SGT_PROPS_INHERIT(Foo);
        // Bar inherits properties from it's base class Foo.

        const double& x() const {return x_;}
        void setX(const double& x) {x_ = x;}
        SGT_PROP_GET_SET(xProp, const double&, x, setX);
        // It can also define its own properties or even override the properties from Foo.
        // Note also: in this case, we get the property as a const double& rather than a double value.

        Complex c() const {return c_;}
        void setC(Complex c) {c_ = c;}
        SGT_PROP_GET_SET(c, Complex, c, setC);
        // A Complex get/set property.

    public:
        double x_{3.14};
        Complex c_{1.2, 3.4};
};

int main()
{
    Foo foo;

    // Iterating through properties.
    cout << "Foo properties:" << endl;
    for (auto& p : foo.properties())
    {
        cout << "Property " << p.first << endl;
        if (p.second.isGettable()) // We can check if property is gettable or settable.
        {
            cout << "    " << p.second.get() << " " << typeid(p.second.get()).name() << endl;
            // The default type returned from get() is not the underlying type of the property, but is
            // rather the default type specified in SGT_PROPS_INIT - json in the current case.
        }
        else
        {
            cout << "    " << "(Not gettable)" << endl;
        }
    }

    // Getting a property for which we know the id.
    cout << "Get a particular property: " << foo.property("vec").get() << endl;
    cout << "Set a particular property " << endl; 
    foo.property("s").set("The quick brown fox");
    cout << "    -> " << foo.s() << endl;
    
    // Getting and setting a property using a known underlying type.
    auto s = foo.property("s").getUnderlying<const string&>();
    cout << "Get a particular property using underlying type: " << s << endl;
    cout << "Set a particular property using underlying type" << endl; 
    foo.property("s").setUnderlying<const string&>("Reset");
    cout << "s -> " << s << endl; 

    // Trying to get a non-gettable property or set a non-settable property will result in an exception.
    try
    {
        cout << "Trying to get sSetOnly:";
        auto x = foo.property("sSetOnly").get();
        cout << "    sSetOnly = " << x << endl;
    }
    catch (const NotGettableException& e)
    {
        cout << "    Exception: " << e.what() << endl;
    }

    try
    {
        cout << "Trying to set four:";
        foo.property("four").set(2);
    }
    catch (const NotSettableException& e)
    {
        cout << "    Exception: " << e.what() << endl;
    }

    // The following demonstrates that Bar inherits Foo's properties, as well as defining its own.
    Bar bar;
    cout << "Bar properties:" << endl;
    for (auto& p : bar.properties())
    {
        cout << p.first << endl; 
    }

    // If we uncomment the following code, we see that it is a compile time error to try to set a property of a
    // const object.
    const Foo constFoo;
    // constFoo.property("s").set("I can't do this!");

    // The following shows how a Property<json> can be gotten and set by using a class that is convertible to json.
    cout << bar.property("c").get().get<Complex>() << endl;
    bar.property("c").set(Complex(55, 66));
    cout << bar.property("c").get().get<Complex>() << endl;
};
//>
