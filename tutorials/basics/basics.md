[modeline]: # ( vim: set ft=markdown tw=2000: )
# [](#basics-tutorial)Basics Tutorial

In this tutorial, we will explore basic classes and functions used in SmartGridToolbox: logging, complex numbers, linear algebra, time and JSON.

The tutorial can be found in the `tutorials/basics` subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. `cd` into the directory. Setup for compiling by typing `meson setup build`. To compile or recompile, use this: `ninja --verbose -C build`. (the `--verbose` flag shows you the actual compilation and linking commands). This will compile an executable in the build directory, which can be run by typing `build/basics`.

Open an editor for [`basics.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/basics/basics.cc). The code is heavily documented. Run `build/basics`, and examine the output with reference to the code and documentation. Below, we consider sections of code in more detail, starting with SmartGridToolbox headers and namespaces:

```cxx
#include <SgtCore.h> // Include SgtCore headers.

// Please note the following namespaces:
using namespace Sgt;
using namespace std;
using namespace arma;
```

SmartGridToolbox is packaged as two libraries: SgtCore, and SgtSim. SgtCore contains utility code for handling dates, time, logging, parsing, vectors and matrices, complex numbers and so on, as well as basic code for solving power flow problems. SgtSim adds a layer for performing discrete event simulations, as well as a component library of simulation objects such as batteries, PV panels, inverters and so on.

Most SmartGridToolbox classes and functions are defined in the `Sgt` namespace. The `arma` namespace is used for the [Armadillo](http://arma.sourceforge.net) library, which is used in SmartGridToolbox for vectors, matrices and linear algebra.

The following code shows how logging and errors are used.

```cxx
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Logging and Errors:" << endl;
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "This is how we log a message in SmartGridToolbox" << endl;
        sgtLogWarning() << "This is how we log a warning in SmartGridToolbox" << endl;
        {
            sgtLogIndent();
            sgtLogMessage() << "This scope will be indented." << endl;
        }
        sgtLogMessage() << "This message will not not be indented." << endl;
        sgtLogMessage(LogLevel::VERBOSE) << "This will not be logged" << endl;
        messageLogLevel() = LogLevel::VERBOSE;
        sgtLogMessage(LogLevel::VERBOSE) << "This will be logged" << endl;
        messageLogLevel() = LogLevel::NONE;
        sgtLogMessage(LogLevel::VERBOSE) << "This will not be logged" << endl;
        messageLogLevel() = LogLevel::NORMAL;
        sgtLogMessage() << "This will be logged" << endl;
        sgtAssert(true == true, "Assertion error message");
        if (false) sgtError("There was an error! I will throw an exception.");
    }
```

It produces this output:

```text
MESSAGE: ----------------
MESSAGE: Logging and Errors:
MESSAGE: ----------------
MESSAGE: This is how we log a message in SmartGridToolbox
WARNING: This is how we log a warning in SmartGridToolbox
MESSAGE:     This scope will be indented.
MESSAGE: This message will not not be indented.
MESSAGE: This will be logged
MESSAGE: This will be logged
```

Complex numbers are handled using the Sgt::Complex class, which is actually an alias for std::complex<double\>.

```cxx
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Complex Numbers:" << endl;
        sgtLogMessage() << "----------------" << endl;
        Complex c{4.0, 5.0};
        sgtLogMessage() << "Complex c = " << c << endl;
        sgtLogIndent();
        sgtLogMessage() << "real part = " << c.real() << endl;
        sgtLogMessage() << "imag part = " << c.imag() << endl;
        sgtLogMessage() << "abs = " << abs(c) << endl;
        sgtLogMessage() << "conj = " << conj(c) << endl;
    }
```

The code above code produces the following output:

```text
MESSAGE: ----------------
MESSAGE: Complex Numbers:
MESSAGE: ----------------
MESSAGE: Complex c = 4+5j
MESSAGE:     real part = 4
MESSAGE:     imag part = 5
MESSAGE:     abs = 6.40312
MESSAGE:     conj = 4-5j
```

Linear algebra (mainly only used for processing vector and matrix classes plus some sparse matrix code that is used
internally) is handled with the help of the [Armadillo](http://arma.sourceforge.net) library. The `arma::Col<double>`
and `arma::Col<Complex>` classes are particularly widely used, for example, to represent vectors of voltages or powers.
The code below:

```cxx
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Linear Algebra:" << endl;
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "SmartGridToolbox uses Armadillo for linear algebra." << endl;
        sgtLogMessage() << "Please see the Armadillo documentation." << endl;
        Col<double> v{1.0, 2.0, 3.0, 3.0};
        sgtLogMessage() << "Real vector:" << endl << v << endl;
        Mat<Complex> m{{{1.0, 0.0}, {2.0, 1.0}}, {{3.0, 10.0}, {4.0, 20.0}}};
        sgtLogMessage() << "Complex matrix:" << endl << m << endl;
    }
```

produces the following output:

```text
MESSAGE: ----------------
MESSAGE: Linear Algebra:
MESSAGE: ----------------
MESSAGE: SmartGridToolbox uses Armadillo for linear algebra.
MESSAGE: Please see the Armadillo documentation.
MESSAGE: Real vector:
         [1, 2, 3, 3]
MESSAGE: Complex matrix:

         [
             [1, 2+1j],
             [3+10j, 4+20j]
         ]
```

Time is handled using the `Sgt::TimePoint` and `Sgt::Duration classes, which are actually aliases for `boost::posix_time::ptime` and `boost::posix_time::time_duration`. The code below:

```cxx
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Time:" << endl;
        sgtLogMessage() << "----------------" << endl;
        
        sgtLogMessage() << "Setting the global timezone using a timezone/DST rule." << endl;
        Sgt::localTimezone() = Timezone("AEST10AEDT,M10.5.0/02,M3.5.0/03");

        sgtLogMessage() << "Durations" << endl;
        Duration d1 = seconds(6); 
        sgtLogMessage() << "d1 = " << d1 << endl;

        Duration d2 = minutes(6); 
        sgtLogMessage() << "d2 = " << d2 << " " << dSeconds(d2) << endl;
        
        Duration d3 = durationFromDSeconds(124.8); 
        sgtLogMessage() << "d3 = " << d3 << " " << dSeconds(d3) << endl;

        sgtLogMessage() << "TimePoints" << endl;
        TimePoint t4 = timePointFromString("2018-04-07 20:00:00");
        sgtLogMessage() << "t4 (UTC time)   = " << t4 << " " << endl;
        sgtLogMessage() << "t4 (local time) = " << localDateTime(t4) << " " << endl;
        
        TimePoint t5 = timePointFromString("2018-04-07T20:00:00Z");
        sgtLogMessage() << "t5 (UTC time)   = " << t5 << " " << endl;
        sgtLogMessage() << "t5 (local time) = " << localDateTime(t5) << " " << endl;
        
        TimePoint t6 = timePointFromString("2018-04-07T20:00:00+5:00");
        sgtLogMessage() << "t6 (UTC time)   = " << t6 << " " << endl;
        sgtLogMessage() << "t6 (local time) = " << localDateTime(t6) << " " << endl;

        TimePoint t7 = TimeSpecialValues::pos_infin;
        TimePoint t8 = TimeSpecialValues::neg_infin;
        TimePoint t9 = TimeSpecialValues::not_a_date_time;
        sgtLogMessage() << "t7 = " << t7 << endl;
        sgtLogMessage() << "t8 = " << t8 << endl;
        sgtLogMessage() << "t9 = " << t9 << endl;
    }
```

produces this output:

```text
MESSAGE: ----------------
MESSAGE: Time:
MESSAGE: ----------------
MESSAGE: Setting the global timezone using a timezone/DST rule.
MESSAGE: Durations
MESSAGE: d1 = 00:00:06
MESSAGE: d2 = 00:06:00 360
MESSAGE: d3 = 00:02:04.799999 124.8
MESSAGE: TimePoints
MESSAGE: t4 (UTC time)   = 2018-Apr-07 10:00:00 
MESSAGE: t4 (local time) = 2018-Apr-07 20:00:00 AEST 
MESSAGE: t5 (UTC time)   = 2018-Apr-07 20:00:00 
MESSAGE: t5 (local time) = 2018-Apr-08 06:00:00 AEST 
MESSAGE: t6 (UTC time)   = 2018-Apr-07 15:00:00 
MESSAGE: t6 (local time) = 2018-Apr-08 01:00:00 AEST 
MESSAGE: t7 = +infinity
MESSAGE: t8 = -infinity
MESSAGE: t9 = not-a-date-time
```

SmartGridToolbox provides a facility for handling the JSON format, using the
[nlohmann/json](https://github.com/nlohmann/json) library. SmartGridToolbox provides a `Sgt::json` class, that is simply
an alias for `nlohmann::json`. The following code shows how this may be used:

```cxx
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "JSON:" << endl;
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "JSON in SmartGridToolbox is handled using the nlohmann::json library." << endl;
        sgtLogMessage() << "Please see the documentation for this library." << endl;
        sgtLogMessage() << "We use the alias Sgt::json = nlohmann::json." << endl;
        json obj = {{"key_1", 1}, {"key_2", {2, 3, 4, nullptr}}};
        sgtLogMessage() << "obj = " << obj.dump(2) << endl;
    }
```

It gives the following output:

```text
MESSAGE: ----------------
MESSAGE: JSON:
MESSAGE: ----------------
MESSAGE: JSON in SmartGridToolbox is handled using the nlohmann::json library.
MESSAGE: Please see the documentation for this library.
MESSAGE: We use the alias Sgt::json = nlohmann::json.
MESSAGE: obj = {
           "key_1": 1,
           "key_2": [
             2,
             3,
             4,
             null
           ]
         }
```

Two important parts of SmartGridToolbox that haven\'t been explored in this tutorial are the special [YAML](https://yaml.org) data format, that is used for SmartGridToolbox configuration files, and the [`ComponentCollection`](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_component_collection.html) class, widely used for collections of objects in SmartGridToolbox. These topics are explored separately in the [parsing tutorial](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/parsing_tutorial.html) and the [component\_collection tutorial](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/component_collection_tutorial.html), respectively.
