// Copyright 2015-2016 National ICT Australia Limited (NICTA)
// Copyright 2016-2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//<1
#include <SgtCore.h> // Include SgtCore headers.

// Please note the following namespaces:
using namespace Sgt;
using namespace std;
using namespace arma;
//>
int main(int argc, char** argv)
{
//<2
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Logging and Errors:" << endl;
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "This is how we log a message in SmartGridToolbox" << endl;
        sgtLogWarning() << "This is how we log a warning in SmartGridToolbox" << endl;
        {
            sgtLogIndent();
            sgtLogMessage() << "This scope will be indented." << endl;
        }
        sgtLogMessage() << "This message will not not be indented." << endl;
        sgtLogMessage(LogLevel::VERBOSE) << "This will not be logged" << endl;
        messageLogLevel() = LogLevel::VERBOSE;
        sgtLogMessage(LogLevel::VERBOSE) << "This will be logged" << endl;
        messageLogLevel() = LogLevel::NONE;
        sgtLogMessage(LogLevel::VERBOSE) << "This will not be logged" << endl;
        messageLogLevel() = LogLevel::NORMAL;
        sgtLogMessage() << "This will be logged" << endl;
        sgtAssert(true == true, "Assertion error message");
        if (false) sgtError("There was an error! I will throw an exception.");
    }
//>

//<3
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Complex Numbers:" << endl;
        sgtLogMessage() << "----------------" << endl;
        Complex c{4.0, 5.0};
        sgtLogMessage() << "Complex c = " << c << endl;
        sgtLogIndent();
        sgtLogMessage() << "real part = " << c.real() << endl;
        sgtLogMessage() << "imag part = " << c.imag() << endl;
        sgtLogMessage() << "abs = " << abs(c) << endl;
        sgtLogMessage() << "conj = " << conj(c) << endl;
    }
//>

//<4
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Linear Algebra:" << endl;
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "SmartGridToolbox uses Armadillo for linear algebra." << endl;
        sgtLogMessage() << "Please see the Armadillo documentation." << endl;
        Col<double> v{1.0, 2.0, 3.0, 3.0};
        sgtLogMessage() << "Real vector:" << endl << v << endl;
        Mat<Complex> m{{{1.0, 0.0}, {2.0, 1.0}}, {{3.0, 10.0}, {4.0, 20.0}}};
        sgtLogMessage() << "Complex matrix:" << endl << m << endl;
    }
//>

//<5
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "Time:" << endl;
        sgtLogMessage() << "----------------" << endl;
        
        sgtLogMessage() << "Setting the global timezone using a timezone/DST rule." << endl;
        Sgt::localTimezone() = Timezone("AEST10AEDT,M10.5.0/02,M3.5.0/03");

        sgtLogMessage() << "Durations" << endl;
        Duration d1 = seconds(6); 
        sgtLogMessage() << "d1 = " << d1 << endl;

        Duration d2 = minutes(6); 
        sgtLogMessage() << "d2 = " << d2 << " " << dSeconds(d2) << endl;
        
        Duration d3 = duration(124.8); 
        sgtLogMessage() << "d3 = " << d3 << " " << dSeconds(d3) << endl;

        sgtLogMessage() << "TimePoints" << endl;
        TimePoint t4 = timePoint("2018-04-07 20:00:00");
        sgtLogMessage() << "t4 (UTC time)   = " << t4 << " " << endl;
        sgtLogMessage() << "t4 (local time) = " << localDateTime(t4) << " " << endl;
        
        TimePoint t5 = timePoint("2018-04-07T20:00:00Z");
        sgtLogMessage() << "t5 (UTC time)   = " << t5 << " " << endl;
        sgtLogMessage() << "t5 (local time) = " << localDateTime(t5) << " " << endl;
        
        TimePoint t6 = timePoint("2018-04-07T20:00:00+5:00");
        sgtLogMessage() << "t6 (UTC time)   = " << t6 << " " << endl;
        sgtLogMessage() << "t6 (local time) = " << localDateTime(t6) << " " << endl;

        TimePoint t7 = TimeSpecialValues::pos_infin;
        TimePoint t8 = TimeSpecialValues::neg_infin;
        TimePoint t9 = TimeSpecialValues::not_a_date_time;
        sgtLogMessage() << "t7 = " << t7 << endl;
        sgtLogMessage() << "t8 = " << t8 << endl;
        sgtLogMessage() << "t9 = " << t9 << endl;
    }
//>

//<6
    {
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "JSON:" << endl;
        sgtLogMessage() << "----------------" << endl;
        sgtLogMessage() << "JSON in SmartGridToolbox is handled using the nlohmann::json library." << endl;
        sgtLogMessage() << "Please see the documentation for this library." << endl;
        sgtLogMessage() << "We use the alias Sgt::json = nlohmann::json." << endl;
        json obj = {{"key_1", 1}, {"key_2", {2, 3, 4, nullptr}}};
        sgtLogMessage() << "obj = " << obj.dump(2) << endl;
    }
//>
}
