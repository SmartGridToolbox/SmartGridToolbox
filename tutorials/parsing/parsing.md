[modeline]: # ( vim: set ft=markdown tw=2000: )
# [](#parsing-tutorial)Parsing Tutorial

In this tutorial, we will explore the SmartGridToolbox mechanism for parsing YAML configuration files. SmartGridToolbox uses [YAML](http://yaml.org) as the primary language by which to store configuration data such as network data or simulation setups. A [`Parser<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) object is used to parse configuration files. [`Parser<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) functionality is extended by subclassing the [`Parser<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) base class.

The tutorial can be found in the `tutorials/parsing` subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. `cd` into the directory. Setup for compiling by typing `meson setup build`. To compile or recompile, use this: `ninja --verbose -C build`. (the `--verbose` flag shows you the actual compilation and linking commands). This will compile an executable in the build directory, which can be run by typing `build/parsing`.

## SmartGridToolbox YAML format

You will have already encountered SmartGridToolbox YAML configuration files in the network and simulation tutorials. Each configuration file is takes the form of a YAML list of dictionaries. Complex numbers, datetimes, and durations are recognised, as shown below:
```yaml
--- # Document separator
- first_key: first_value # This value is a string
- second_key:
    second_value_is_a_dict: 4.7 # This value is a float
    another_key: [1, 2, 6] # This value is a list of ints
- a_complex_number: 5+4j # This value is a complex number
- a_date_time: 2020-01-01 13:31:26 # Use shown format, with seconds being optional.
- a_duration: 23:21:00 
```

### Advanced Features: Parameters and Loops
The file [`int.yaml`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/parsing/int.yaml), in the parsing turorial directory, shows the advanced features that are particular to SmartGridToolbox YAML: parameters and loops. The YAML is fairly self-explanatory: parameters are basically macros, and loops are a way of effectively generating highly repetitive fragments of YAML. Don't worry about the significance of the `int` keys at this point; we will go into this below. At this stage, we're only concerned with the structure of the YAML.

```yaml
- parameters:
    n: 4
    m: 5

- int: 3
- int: <n> # Expands to "- int: 4"
- int: <<m> + <n>> # Expands to "- int: 9"

- loop:
    loop_variable: [j, 10, 30, 5] # for (int j = 10; j < 30; j += 5)
    loop_body:
        - int: <j>

# The loop above expands to the following four items:
# - int: 10
# - int: 15
# - int: 20
# - int: 25
```

## Parsers and ParserPlugins

SmartGridToolbox uses the [yaml-cpp](https://github.com/jbeder/yaml-cpp) library to handle basic YAML processing. In SmartGridToolbox, YAML is parsed using the [`Parser`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) template. A [`Parser<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) provides two key member functions: `parse(const std::string &fname, T &into)`, which will parse in from a file, and `parse(const YAML::Node &node, T &into)`, which will parse in from a `YAML::Node` object. These functions take as their second argument a non-const reference to a `T` object, and it is this object that will be modified by the `parse` functions as a result of the contents of the YAML configuration.

In this tutorial, we shall be going through the
[`parsing.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/parsing/parsing.cc) file. We start with the `main(...)` function of this file.

```cxx
        sgtLogMessage() << "NetworkParsers: " << endl;
        // An empty network:
        Network netw;
        sgtLogMessage() << "Before parsing network: " << endl;
        sgtLogMessage() << netw << endl;

        // Create a parser that is able to modify a Network based on a YAML config:
        Parser<Network> p; // An alias for this is "NetworkParser".

        // Parse a yaml file into netw;
        p.parse("network.yaml", netw); // File is read, and as a result, netw is modified.
        sgtLogMessage() << "After parsing network: " << endl;
        sgtLogMessage() << netw << endl;
```

If you have completed the earlier tutorials, you should have already seen code like that above. A [`Parser<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) is created, and parses a YAML configuration into an existing [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html) object. It does so by dispatching the parsing of each top level `key: value` pair to a [`ParserPlugin<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html).

For example: a [`Parser<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) has associated with it a [`BusParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_bus_parser_plugin.html), that is called on to interpret the YAML value whenever the `bus` keywork is encountered, a [`ZipParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip_parser_plugin.html), that is called on to interpret the YAML value whenever the `zip` keywork is encountered, and so on.

Standard [`ParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html)s, such as [`BusParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_bus_parser_plugin.html) and [`ZipParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip_parser_plugin.html), are automatically registered for use, in this case by the network parser class, [`Parser<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html). Non-standard [`ParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html)s may also be used, but will need to be explicitly registered for use by the corresponding [`Parser<T>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html), as will be seen shortly.

We continue with the main function of [`parsing.cc`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/parsing/parsing.cc).

```cxx
        // Now we'll show how to add custom parsing code.
        // Each Parser<T> has a corresponding registerParserPlugins<>(Parser<T>&) function that registers a standard
        // set of plugins. For example, Parser<Network> registers BusParserPlugin, BranchParserPlugin, etc.
        // You can also register individual plugins user Parser<T>::registerParserPlugin(), like below:
        p.registerParserPlugin<SillyPlugin>();

        // Lets try it now:
        Network netwB;
        p.parse("network.yaml", netwB);
        sgtLogMessage() << "After parsing network (including SillyPlugin): " << endl;
        sgtLogMessage() << netwB << endl;
        for (const auto& b : netwB.buses())
        {
            std::cout << "Bus " << b->id() << " user data : " << b->userData().dump() << endl;
        }
```

Hold on a second! What is this `SillyPlugin`, that we've apparently just registered and used to help parse in a [`Network`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_network.html)?. We've defined this class earlier in the code.

```cxx
    class SillyPlugin: public ParserPlugin<Network>
    {
        public:
        virtual const char* key() const override {return "silly";} // Parse YAML under the "silly" key.

        virtual void parse(const YAML::Node& nd, Network& netw, const ParserBase& parser) const override
        {
            // Asserts that various fields are present:
            assertFieldPresent(nd, "silly_string");
            assertFieldPresent(nd, "silly_int");

            // Parser the YAML in node nd:
            string sillyString = parser.expand<string>(nd["silly_string"]);
            int sillyInt = parser.expand<int>(nd["silly_int"]);

            // Create prefix based on YAML:
            string sillyPrefix = sillyString + "_" + to_string(sillyInt) + "_";

            auto ndOptionalSuffix = nd["optional_suffix"];
            string suffix = "";
            if (ndOptionalSuffix)
            {
                suffix = "_" + parser.expand<string>(ndOptionalSuffix);
            }

            // Modify the network by giving all buses a "silly name" in their user data JSON.
            for (auto b : netw.buses()) b->userData()["silly_name"] = sillyPrefix + b->id() + suffix;
        }
    };
```

Notice that `SillyPlugin` derives from [`ParserPlugin<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html), and therefore is meant to be used in conjunction with a [`Parser<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html). It overrides a virtual `key(...)` function by returning `"silly"`. This means that if we register it with any [`Parser<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) as shown previously, then any time the YAML `"silly"` key is encountered in one of the top level key-value pairs, then the `SillyPlugin` class will be used to parse the given value, using the `parse(...)` function shown above.

All [`ParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html)s should define the two virtual functions shown above, a `key()` function that defines which YAML keyword will cause this particular [`ParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html) to be called, and a `parse(...)` function that contains the actual parsing code.

We now consider the contents of the `parse(...)` function, above. The code is somewhat self-explanatory. `assertFieldPresent` is used to require given YAML key/value pairs. Data is normally extracted using the `parser.expand<T>(node)`. Using `expand(...)` like this ensures that the parameter and loop expansion discussed at the beginning of the tutorial will be correctly handled. The code also shows how optional keys may be handled.

The code causes some additional `JSON` user data to be added to all buses in the network whenever the `silly` keyword is encountered. See if you can work out what this data should look like, and then compile and run the code to see if you were right.

We also define another [`ParserPlugin`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html), this time called `IntAdderParserPlugin`.

```cxx
    // The following ParserPlugin<int> parses an int and adds it to an int:
    class IntAdderParserPlugin: public ParserPlugin<int>
    {
        public:

        virtual const char* key() const override {return "int";} // Parse YAML under the "int" key.
        
        virtual void parse(const YAML::Node& nd, int& i, const ParserBase& parser) const override
        {
            sgtLogMessage() << "IntAdderParserPlugin: adding node " << nd << " onto int." << endl;
            sgtLogMessage() << "Old i = " << i << endl;
            i += parser.expand<int>(nd); // Add to existing .
            sgtLogMessage() << "New i = " << i << endl;
        }
    };

    // We can implement Parser<T> for any type T. We do, however, need to make sure there is a 
    // corresponding registerParserPlugins<T> function defined.
    // Here, we implement a Parser<int>. Because IntAdderParserPlugin is registered inside registerParserPlugins,
    // it doesn't need to be separately registered after the parser is created. This can sometimes be useful,
    // but note that, for example, the registerParserPlugins<Network> function is already defined and can't be
    // overridden.
    template <> void registerParserPlugins<int>(Parser<int>& p)
    {
        p.registerParserPlugin<IntAdderParserPlugin>();
    }
}
```

This time, the plugin derives from [`ParserPlugin<int>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser_plugin.html), and therefore causes a reference to an `int` to be modified whenever YAML is parsed. We also see some auto-registration code, but note that this is only useful when working with a new kind of [`Parser`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html), and not, for example, a [`Parser<Network>`](https://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_parser.html) which already has its own autoregistration code.

`IntAdderParserPlugin` is meant to parse YAML that looks like that in [`int.yaml`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/parsing/int.yaml):

```yaml
- parameters:
    n: 4
    m: 5

- int: 3
- int: <n> # Expands to "- int: 4"
- int: <<m> + <n>> # Expands to "- int: 9"

- loop:
    loop_variable: [j, 10, 30, 5] # for (int j = 10; j < 30; j += 5)
    loop_body:
        - int: <j>

# The loop above expands to the following four items:
# - int: 10
# - int: 15
# - int: 20
# - int: 25
```

It is invoked in the usual way:

```cxx
    {
        sgtLogMessage() << "IntAdderParserPlugin: " << endl;
        sgtLogMessage() << 
            "Please take a look at int.yaml."
            "It demonstrates several things including parser parameters and parser loops." << endl;
        int i = 3;
        sgtLogMessage() << "Before parsing: " << i << endl;
        Parser<int> p;
        p.parse("int.yaml", i);
        sgtLogMessage() << "After parsing: " << i << endl;
    }
```

See if you can work out what it does, and then compile and run the code to see if you are right.
