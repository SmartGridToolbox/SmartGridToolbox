[modeline]: # ( vim: set ft=markdown tw=2000: )
# [](#tvloads-tutorial)Time Varying Loads Tutorial

In this tutorial, we will see how time series load data can be incorporated into a simulation.

The tutorial can be found in the `tutorials/time_varying_loads` subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. `cd` into the directory. Setup for compiling by typing `meson setup build`. To compile or recompile, use this: `ninja --verbose -C build`. (the `--verbose` flag shows you the actual compilation and linking commands). This will compile an executable in the build directory, which can be run by typing `build/tvloads`.


Briefly, `tvloads.cc` parses the file [input.yaml](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/time_varying_loads/input.yaml) into a `Simulation` object, and then runs the simulation. All of the interesting detail is in the [YAML input file](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/time_varying_loads/input.yaml). This file is documented; please take a look at it. Briefly, a loop is set up to create a set of 57 time series objects, which are then used as inputs to a set of 57 `TimeSeriesZip` simulation objects. Run the simulation by typing `tvloads`. The output of the program is stored in the file `output.txt`. A python script, `plot_results.py`, is included to plot the contents of this file. To make the plotting script work, you may need to install the python packages numpy and matplotlib, using, e.g. `pip3 install numpy; pip3 install matplotlib`.

This tutorial is under construction, but the code includes useful comments, and` is reproduced below.
```cxx
#include <SgtCore/Parser.h>

#include <SgtSim/Simulation.h>
#include <SgtSim/SimNetwork.h>

int main(int argc, const char ** argv)
{
    using namespace Sgt;

    std::string configName = "input.yaml";
    std::string outputName = "out.txt";

    std::ofstream outFile(outputName);

    Simulation sim;
    Parser<Simulation> p;
    p.parse(configName, sim);
    SimNetwork& simNetwork = *sim.simComponent<SimNetwork>("network");
    Network& network = simNetwork.network();
    network.setUseFlatStart(true);

    auto sumLoad = [&] () {Complex x = 0; for (auto bus : network.buses()) x -= bus->SZip()(0); return x;};
    auto sumGen = [&] () {Complex x = 0; for (auto gen : network.gens()) x += gen->S()(0); return x;};

    auto minV = [&] () {
        double minV1 = 100;
        for (auto bus : network.buses())
        {
            double V = network.V2Pu(std::abs(bus->V()(0)), bus->VBase());
            if (V < minV1)
            {
                minV1 = V;
            }
        }
        return minV1;
    };
    auto maxV = [&] () {
        double maxV1 = 0;
        for (auto bus : network.buses())
        {
            double V = network.V2Pu(std::abs(bus->V()(0)), bus->VBase());
            if (V > maxV1)
            {
                maxV1 = V;
            }
        }
        return maxV1;
    };

    auto print = [&] () {
        double h = dSeconds(sim.currentTime() - sim.startTime()) / 3600.0;
        outFile << h << "\t" << sumLoad() << "\t" << sumGen() << "\t" << minV() << "\t" << maxV() << "\t" << std::endl;
    };

    sim.initialize();
    while (!sim.isFinished())
    {
        std::cout << sim.currentTime()-sim.startTime() << std::endl;
        sim.doTimestep();
        print();
    }
    print();
}
```
